﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CashierUtils
{
    public class Globals
    {
        public enum Providers
        {
            [EnumDescription(Name = "ALLCHARGE", Description = "ALLCHARGE Provider")]
            ALLCHARGE,

            [EnumDescription(Name = "NETPAY", Description = "NETPAY Provider")]
            NETPAY,

            [EnumDescription(Name = "NETPAY3DS", Description = "NETPAY3DS Provider")]
            NETPAY3DS,

            [EnumDescription(Name = "DATACASH", Description = "DATACASH Provider")]
            DATACASH,

            [EnumDescription(Name = "EMERCHANTPAY", Description = "EMERCHANTPAY Provider")]
            EMERCHANTPAY,

            [EnumDescription(Name = "EMP_KOR", Description = "EMERCHANTPAY Provider")]
            EMP_KOR,

            [EnumDescription(Name = "EMP_KOR", Description = "EMERCHANTPAY Provider")]
            EMP_KOR1,

            [EnumDescription(Name = "EMERCHANTPAY3DS", Description = "EMERCHANTPAY3DS Provider")]
            EMERCHANTPAY3DS,

			[EnumDescription(Name = "EMERCHANTPAYGENESIS", Description = "EmerchantPayGenesis Provider")]
			EMERCHANTPAYGENESIS,

			[EnumDescription(Name = "EMERCHANTPAYGENESIS3DS", Description = "EmerchantPayGenesis3DS Provider")]
			EMERCHANTPAYGENESIS3DS,

			[EnumDescription(Name = "EMP_KOR_3D", Description = "EMERCHANTPAY3DS Provider")]
            EMP_KOR_3D,

            [EnumDescription(Name = "EMP_KOR_3D", Description = "EMERCHANTPAY3DS Provider")]
            EMP_KOR1_3D,

            [EnumDescription(Name = "EMERCHANTPAYTE", Description = "EMERCHANTPAY Provider for Transact Europe bank")]
            EMERCHANTPAYTE,

            [EnumDescription(Name = "EMP_TE", Description = "EMERCHANTPAY Provider for Transact Europe bank")]
            EMP_TE,

            [EnumDescription(Name = "EMERCHANTPAYTE3DS", Description = "EMERCHANTPAYTE3DS Provider for Transact Europe bank")]
            EMERCHANTPAYTE3DS,

            [EnumDescription(Name = "EMP_TE_3D", Description = "EMERCHANTPAYTE3DS Provider for Transact Europe bank")]
            EMP_TE_3D,

            [EnumDescription(Name = "SOLID", Description = "SOLID Provider")]
            SOLID,

            [EnumDescription(Name = "SafeCharge", Description = "SafeCharge Provider")]
            SAFECHARGE,

            [EnumDescription(Name = "Inatech", Description = "Inatech Provider")]
            INATECH,

            [EnumDescription(Name = "Inatech INTRX", Description = "Inatech INTRX Provider")]
            INTRX,

            [EnumDescription(Name = "GlobalCollect", Description = "GlobalCollect Provider")]
            GLOBALCOLLECT,

            [EnumDescription(Name = "Astech", Description = "Astech Provider")]
            ASTECH,

            [EnumDescription(Name = "Astech 3DS", Description = "Astech 3DS Provider")]
            ASTECH3DS,

            [EnumDescription(Name = "Fibonatix", Description = "Fibonatix Provider")]
            FIBONATIX,

            [EnumDescription(Name = "Fibonatix 3DS", Description = "Fibonatix 3DS Provider")]
            FIBONATIX3DS,
            
            [EnumDescription(Name = "Solid PaynetEasy Engine", Description = "Solid PaynetEasy Engine Provider")]
            SOLIDPAYNETEASY,

            [EnumDescription(Name = "Solid PaynetEasy Engine 3DS", Description = "Solid PaynetEasy Engine 3DS Provider")]
            SOLIDPAYNETEASY3DS,

            [EnumDescription(Name = "Zotapay", Description = "Zotapay Provider")]
            ZOTAPAY,

            [EnumDescription(Name = "Zotapay 3DS", Description = "Zotapay 3DS Provider")]
            ZOTAPAY3DS,

            [EnumDescription(Name = "Zotapay MYR", Description = "Zotapay MYR Payment Form Provider")]
            ZOTAPAYMYR,

            [EnumDescription(Name = "YouPay", Description = "YouPay Provider")]
            YOUPAY,

            [EnumDescription(Name = "TRUSTPAY", Description = "TRUSTPAY Provider")]
            TRUSTPAY,

            [EnumDescription(Name = "MERCHANTSWORLDWIDE", Description = "MerchantsWorldWide Provider")]
            MERCHANTSWORLDWIDE,

            [EnumDescription(Name = "PAYOBIN3DS", Description = "Payobin3DS Provider")]
            PAYOBIN3DS,

            [EnumDescription(Name = "PAYOBIN", Description = "Payobin Provider")]
            PAYOBIN,

            [EnumDescription(Name = "WIROPAY3DS", Description = "Wiropay3DS Provider")]
            WIROPAY3DS,

            [EnumDescription(Name = "WIROPAY", Description = "Wiropay Provider")]
            WIROPAY,

            [EnumDescription(Name = "PAYTECHNIQUE", Description = "PayTechnique Provider")]
            PAYTECHNIQUE,

            [EnumDescription(Name = "PAYTECHNIQUE3DS", Description = "PayTechnique3DS Provider")]
            PAYTECHNIQUE3DS,

            [EnumDescription(Name = "Inatech3DS", Description = "Inatech3DS Provider")]
            INATECH3DS,

            [EnumDescription(Name = "Solid3DS", Description = "Solid3DS Provider")]
            SOLID3DS,

            [EnumDescription(Name = "Snow", Description = "Snow Provider")]
            SNOW,

            [EnumDescription(Name = "Snow 3DS", Description = "Snow 3DS Provider")]
            SNOW3DS,

            [EnumDescription(Name = "Paydelta", Description = "Paydelta Provider")]
            PAYDELTA,

            [EnumDescription(Name = "Paydelta3DS", Description = "Paydelta3DS Provider")]
            PAYDELTA3DS,

            [EnumDescription(Name = "ProcessingCom", Description = "ProcessingCom Provider")]
            PROCESSINGCOM,

            [EnumDescription(Name = "ProcessingCom3DS", Description = "ProcessingCom 3DS Provider")]
            PROCESSINGCOM3DS,

            [EnumDescription(Name = "S4UCash3DS", Description = "S4UCash3DS Provider")]
            S4UCASH3DS,

            [EnumDescription(Name = "WireCapital", Description = "WireCapital Provider")]
            WIRECAPITAL,

            [EnumDescription(Name = "IMGLobal", Description = "IMGlobal Provider")]
            IMGLOBAL,

            [EnumDescription(Name = "Coala", Description = "Coala Provider")]
            COALA,            

            [EnumDescription(Name = "Acapture", Description = "Acapture Provider")]
            ACAPTURE,

            [EnumDescription(Name = "WonderlandPay", Description = "WonderlandPay Provider")]
            WONDERLANDPAY,

            [EnumDescription(Name = "MeikoPay", Description = "MeikoPay Provider")]
            MEIKOPAY,

            [EnumDescription(Name = "MeikoPay3DS", Description = "MeikoPay3DS Provider")]
            MEIKOPAY3DS,

            [EnumDescription(Name = "GPN", Description = "GPN Provider")]
            GPN,

            [EnumDescription(Name = "GPN3DS", Description = "GPN3DS Provider")]
            GPN3DS,

            [EnumDescription(Name = "EMP_BORGUN", Description = "EMERCHANTPAY Provider for Burgan")]
            EMP_BORGUN,

            [EnumDescription(Name = "EMP_BORGUN_3D", Description = "EMERCHANTPAY3DS Provider for Burgan")]
            EMP_BORGUN_3D,

            [EnumDescription(Name = "DreamsPay", Description = "DreamsPay Provider for DreamsPay")]
            DREAMSPAY,

            [EnumDescription(Name = "MasterPay", Description = "MasterPay Provider for MasterPay")]
            MASTERPAY,

            [EnumDescription(Name = "Certus", Description = "Certus  Provider")]
            CERTUS,

            [EnumDescription(Name = "Solid_Burgon", Description = "Solid_Burgon Provider for Solid_Burgon")]
            SOLID_BURGON,

            [EnumDescription(Name = "Swish", Description = "Swish Provider for Swish")]
            SWISH,

            [EnumDescription(Name = "Swish3DS", Description = "Swish3DS Provider for Swish")]
            SWISH3DS,

            [EnumDescription(Name = "Wirecard", Description = "WireCard Provider")]
            WIRECARD,

            [EnumDescription(Name = "Wirecard3DS", Description = "WireCard3DS Provider")]
            WIRECARD3DS,

            [EnumDescription(Name = "EMP_TE1", Description = "eMerchantpay Provider for Transact Europe bank")]
            EMP_TE1,

            [EnumDescription(Name = "EMP_TE1_3D", Description = "eMerchantpay 3DS Provider for Transact Europe bank")]
            EMP_TE1_3D,

            [EnumDescription(Name = "EMP_ECP", Description = "eMerchantpay Provider for ECP")]
            EMP_ECP,

            [EnumDescription(Name = "EMP_ECP_3D", Description = "eMerchantpay Provider for ECP 3DS")]
            EMP_ECP_3D,

			[EnumDescription(Name = "EcommPay", Description = "eMerchantpay Provider for EcommPay")]
			ECOMMPAY,

			[EnumDescription(Name = "EcommPay3DS", Description = "eMerchantpay Provider for EcommPay 3DS")]
			ECOMMPAY3DS,

            [EnumDescription(Name = "ORANGEPAY", Description = "OrangePay Provider")]
            ORANGEPAY,

            [EnumDescription(Name = "ORANGEPAY3DS", Description = "OrangePay3DS Provider")]
            ORANGEPAY3DS,

            [EnumDescription(Name = "NASPAY", Description = "NASPAY Provider")]
            NASPAY,

            [EnumDescription(Name = "NASPAY3DS", Description = "NASPAY3DS Provider")]
            NASPAY3DS,

            [EnumDescription(Name = "PayGate", Description = "PayGate Provider for PayGate")]
            PAYGATE,

            [EnumDescription(Name = "EcommPay3DS", Description = "eMerchantpay Provider for PayGate 3DS")]
            PAYGATE3DS,

            [EnumDescription(Name = "Acapture3DS", Description = "Acapture 3DS Provider")]
            ACAPTURE3DS,

			[EnumDescription(Name = "TRUEVO3DS", Description = "TRUEVO3DS 3DS Provider")]
			TRUEVO3DS,

            [EnumDescription(Name = "IPAYMENT", Description = "IPAYMENT Provider")]
            IPAYMENT,

            [EnumDescription(Name = "IPAYMENT3DS", Description = "IPAYMENT 3DS Provider")]
            IPAYMENT3DS,

            [EnumDescription(Name = "NOPROBLEMPAY", Description = "NOPROBLEMPAY Provider")]
            NOPROBLEMPAY,

            [EnumDescription(Name = "NOPROBLEMPAY3DS", Description = "NOPROBLEMPAY3DS 3DS Provider")]
            NOPROBLEMPAY3DS,

            [EnumDescription(Name = "NETPAY_PAYOBIN", Description = "NETPAY_PAYOBIN Provider")]
            NETPAY_PAYOBIN,

            [EnumDescription(Name = "NETPAY_PAYOBIN_3D", Description = "NETPAY_PAYOBIN_3D 3DS Provider")]
            NETPAY_PAYOBIN_3D
        }

        public enum ErrorCodes
        {
            NO_ERROR,
            WRONG_EMAIL,
            NOT_ENOUGH_MONEY,
            FAIL
        }
    }

    /// <summary>
    /// Custom attribute to add additional meta data to an Enum
    /// </summary>
    public class EnumDescription : Attribute
    {
        /// <summary>
        /// Enum's full name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Enum's description
        /// </summary>
        public string Description { get; set; }
    }
}
