﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashierUtils
{
    public class Error
    {
        public Globals.ErrorCodes ErrorCode = Globals.ErrorCodes.NO_ERROR;
        public string Description;
    }
}
