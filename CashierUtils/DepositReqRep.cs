﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CashierUtils
{    
    public class DepositRequest
    {
        public DepositRequest()
        {
            CountryISOCodeThreeLetters = string.Empty;
        }

        //Customers Useragent from their Browser
        public string UserAgent { get; set; }
        public string Referer { get; set; }
        //Customers Accept Headers from their Browser
        public string Accept { get; set; }
        public string Host { get; set; }
        public string CallbackData { get; set; }

        public double Amount { get; set; }
        public string Email { get; set; }
        public string CurrencyISOCode { get; set; }
        public string CountryISOCode { get; set; }
        public string CountryISOCodeThreeLetters { get; set; }
        public Globals.Providers provider;
        public string LastClearingCompany;
        public string CustomerID;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string StateISOCode { get; set; }
        public string CardNumber { get; set; }
        public string CardCVV2 { get; set; }
        public string CardExpMonth { get; set; }
        public string CardExpYear { get; set; }
        public string IpAddress { get; set; }
        public string NameOnCard { get; set; }

        public string Timeout { get; set; }
        public string Method { get; set; }
        public string CardStartDate { get; set; }
        public string CardIssueNumber { get; set; }

        public string TransactionType { get; set; }
        public string Payments { get; set; }
        public string TypeCredit { get; set; }

        public string PmType { get; set; }
        public string CardType { get; set; }
        public string Description { get; set; }
        public string ThmSessionId { get; set; }

        public string Sender { get; set; }
        public string Channel { get; set; }

		public string Biography { get; set; }
        public int BrandID { get; set; }
    }



    public class RedirectValues
    {
        public string Key;
        public string Value;
    }

    public class DepositResponse
    {
        public Error error = new Error();        
        public Globals.Providers provider;

        public bool IsPending = false;
        public bool Is3DSecure = false;

        public string RedirectUrl;
        public List<RedirectValues> RedirectParameters;

        public string ClearingUserID;

        public string TransactionID;
        public string CurrencyISOCode { get; set; }
        public string Amount { get; set; }
        public string CardType { get; set; }

        public string Receipt { get; set; }
        public string ErrorDescripotion { get; set; }
        public string RetCode { get; set; }
        public string Reply { get; set; }
        public string Date { get; set; }
        public string ReplyDescription { get; set; }
        public string Descriptor { get; set; }
        public string CardLast4Digits { get; set; }
        public string Payments { get; set; }
        public string TransactionType { get; set; }
        public string AuthCode { get; set; }
        public string Country { get; set; }
        public string Issuer { get; set; }
        public string DataCashReference { get; set; }
        public string Mode { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public string Time { get; set; }

        public string Type { get; set; }
        public string PaynetOrderID { get; set; }
        public string MerchantOrderID { get; set; }
        public string SerialNnumber { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorCode { get; set; }
        public string html3D { get; set; }
        public string bin { get; set; }
        public string BankName { get; set; }
        public string ApprovalCode { get; set; }
        public string OrderStage { get; set; }
        public string SerialNnumberStatus { get; set; }
        public string CardHashID { get; set; }

        //For 3DS - Use to save temporary data for 3DS callback use
        public string DataToSaveKey { get; set; }
        public string DataToSave { get; set; }

        public List<FailedDeposit> FailedDeposits = new List<FailedDeposit>();
    }

    public class FailedDeposit
    {
        public Globals.Providers provider;
        public string Email { get; set; }
        public string TransactionID { get; set; }
        public double Amount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }       
        public string Description { get; set; }
    }
}
