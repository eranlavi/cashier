﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier;

namespace TestCashier
{
    class Program
    {
        static void Main(string[] args)
        {

            DepositRequest depositRequestParams = new DepositRequest();
            depositRequestParams.CountryISOCode = "IL";
            depositRequestParams.CurrencyISOCode = "USD";
            depositRequestParams.CardType = "visa";
            depositRequestParams.Amount = 1000;
            depositRequestParams.CardNumber = "4111111111111111";
            ProvidersFacade providerFacade = new ProvidersFacade("C:\\Users\\Panda82\\Desktop\\CTOptionNew\\Cashier.config");
            providerFacade.Deposit(depositRequestParams); 

        }
    }
}
