﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    internal class CashierProvider
    {
        public Globals.Providers provider;
        ICashierProvider Cashier;
        public int Percentage;
        public int PercentageCoeff;
        public int Level;

        public CashierProvider(Globals.Providers provider, ProviderConfigBase config)
        {
            this.Percentage = config.Percentage;
            this.PercentageCoeff = 0;
            this.Level = config.Level;
            this.provider = provider;

            ProviderFactory _ProviderFactory = new ProviderCreator();

            Cashier = _ProviderFactory.GetProvider(provider, config);
        }

        public string Url
        {
            get { return Cashier.Url; }
            set { Cashier.Url = value; }    
        }

        public string MerchantID
        {
            get { return Cashier.MerchantID; }
            set { Cashier.MerchantID = value; }
        }

        public string MerchantPassword
        {
            get { return Cashier.MerchantPassword;}
            set { Cashier.MerchantPassword = value;}
        }

        public DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = null;

            //System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
            System.Globalization.CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");

            _DepositResponse = Cashier.Deposit(_DepositRequest);

            System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture;

            return _DepositResponse;

        }

        
    }
}
