﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Web;
using System.Net;
using System.IO;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    class ProviderNetpay : ProviderBase
    {

        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        #region Constants

        private const string POST = "POST";

        private const string COMPANY_NUM = "CompanyNum";
        private const string TRANS_TYPE = "TransType";
        private const string CARD_NUM = "CardNum";
        private const string EXP_MONTH = "ExpMonth";
        private const string EXP_YEAR = "ExpYear";
        private const string MEMBER = "Member";
        private const string TYPE_CREDIT = "TypeCredit";
        private const string PAYMENTS = "Payments";
        private const string AMOUNT = "Amount";
        private const string CURRENCY = "Currency";
        private const string CVV2 = "CVV2";
        private const string EMAIL = "Email";
        private const string PHONE_NUMBER = "PhoneNumber";
        private const string BILLING_ADDRESS1 = "BillingAddress1";
        private const string BILLING_CITY = "BillingCity";
        private const string BILLING_ZIPCODE = "BillingZipCode";
        private const string BILLING_STATE = "BillingState";
        private const string BILLING_COUNTRY = "BillingCountry";

        private const string REPLY = "Reply";
        private const string TRANS_ID = "TransID";
        private const string DATE = "Date";
        private const string CONFIRMATION_NUM = "ConfirmationNum";
        private const string REPLY_DESC = "ReplyDesc";
        private const string CCTYPE = "CCType";
        private const string DESCRIPTOR = "Descriptor";
        private const string LAST4 = "Last4";
        private const string D3REDIRECT = "D3Redirect";

        private const string SUCCESSFUL_REPLY_CODE = "000";
        private const string D3S_REPLY_CODE = "553";

        private string ReturnURL;
        private string PersonalHashKey;
        private string TrmCode;

        #endregion

        #region Private members

        //NetPay <-> ISO currency code mapping 
        private Dictionary<string, string> currencyCodeMapping = new Dictionary<string, string>()
        {
            {"0", "ILS"}, // Israel New Shekel
            {"1", "USD"}, // US Dollar
            {"2", "EUR"}, // Euro
            {"3", "GBP"}, // UK Pound Sterling
            {"4", "AUD"}, // Australian Dollar
            {"5", "CAD"}, // Canadian Dollar
            {"6", "JPY"}, // Japanese Yen
            {"7", "NOK"}, // Norwegian Krone
            {"8", "PLN"}, // Polski zloty
            {"9", "MXN"}, // Mexican Peso
            {"10", "ZAR"}, //South African Rand
            {"11", "RUB"}, //Russian Ruble
            {"12", "TRY"}, //Turkish lira
            {"13", "CHF"}, //Swiss franc
            {"14", "INR"}, //Indian rupee
            {"15", "DKK"}, //Danish krone
            {"16", "SEK"}, //Swedish krona
            {"17", "CNY"}, //Chinese yuan
            {"18", "HUF"}, //Hungarian forint
            {"19", "NZD"}, //New Zealand dollar
            {"20", "HKD"}, //Hong Kong dollar
            {"21", "KRW"}, //South Korean won
            {"22", "SGD"}  //Singapore dollar
      };

        #endregion

        #region Constructor

        public ProviderNetpay(ProviderConfigBase config) : base(config)
        {
            MerchantsWorldWideProviderConfig NetpayConfig = config as MerchantsWorldWideProviderConfig;
            if (NetpayConfig == null)
            {
                throw new ArgumentException("NetpayConfig expects to get MerchantsWorldWideProviderConfig object!");
            }

            ReturnURL = NetpayConfig.CallbackUrl;
            PersonalHashKey = NetpayConfig.PrivateKey;
            TrmCode = NetpayConfig.TrmCode;
        }
        #endregion

        #region Public methods

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            if (_DepositRequest.CurrencyISOCode == "UST")
            {
                _DepositRequest.CurrencyISOCode = "USD";
            }
            DepositResponse _DepositResponse = new DepositResponse();

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString("0.00");
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.provider = _DepositRequest.provider;

            string sCurrency = ConvertToNetPayCurrency(_DepositRequest.CurrencyISOCode, currencyCodeMapping);
            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            _DepositResponse.TransactionID = transactionID;
            
            if (_DepositRequest.CardExpYear.Length != 2 && _DepositRequest.CardExpYear.Length != 4)
            {
                _DepositResponse.ErrorDescripotion = "Invalid credit card expiry year";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = _DepositResponse.TransactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
                return _DepositResponse;
            }

            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            if (_DepositRequest != null)
            {
                try
                {
                    string Sig = GenerateSignature(MerchantID + _DepositRequest.TransactionType + _DepositRequest.TypeCredit + _DepositRequest.Amount.ToString() + sCurrency + _DepositRequest.CardNumber + transactionID + PersonalHashKey);
                    Sig = System.Web.HttpUtility.UrlEncode(Sig);

                    string req = string.Format(@"CompanyNum={0}&TransType={1}&CardNum={2}&ExpMonth={3}&ExpYear={4}&Member={5}
                    &TypeCredit={6}&Payments={7}&Amount={8}&Currency={9}&CVV2={10}&Email={11}&PhoneNumber={12}&BillingAddress1={13}
                    &BillingCity={14}&BillingZipCode={15}&BillingState={16}&BillingCountry={17}&Order={18}&RetURL={19}&Signature={20}",
                    MerchantID, _DepositRequest.TransactionType,
                     _DepositRequest.CardNumber, ExpMonth, ExpYear, System.Web.HttpUtility.UrlEncode(string.Format("{0} {1}", _DepositRequest.FirstName, _DepositRequest.LastName)),
                      _DepositRequest.TypeCredit, _DepositRequest.Payments, _DepositRequest.Amount.ToString(),
                     sCurrency, _DepositRequest.CardCVV2,
                      _DepositRequest.Email, _DepositRequest.PhoneNumber, System.Web.HttpUtility.UrlEncode(_DepositRequest.Address), _DepositRequest.City,
                      _DepositRequest.ZipCode, _DepositRequest.StateISOCode, _DepositRequest.CountryISOCode, transactionID, ReturnURL, Sig);

                    if (!string.IsNullOrWhiteSpace(TrmCode))
                        req = string.Format(req + "&TrmCode={0}", TrmCode);

                    Log($"Request: Provider={_DepositRequest.provider}; {Url} {req}{Environment.NewLine}");

                    // send POST
                    Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded", "", 20000, 20000);

                    Log($"Response: Provider={_DepositRequest.provider}; {e.Description}{Environment.NewLine}");

                    if (e.Code != 0)
                    {
                        _DepositResponse.ErrorDescripotion = $"Http request to {_DepositRequest.provider} failed.";
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.ErrorCode = "1";
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                        // fill the class FailedDeposit with details in order to send email to support later
                        FailedDeposit failedDeposit = new FailedDeposit();
                        failedDeposit.provider = _DepositRequest.provider;
                        failedDeposit.Email = _DepositRequest.Email;
                        failedDeposit.TransactionID = _DepositResponse.TransactionID;
                        failedDeposit.Amount = _DepositRequest.Amount;
                        failedDeposit.FirstName = _DepositRequest.FirstName;
                        failedDeposit.LastName = _DepositRequest.LastName;
                        failedDeposit.Description = e.Description;

                        _DepositResponse.FailedDeposits.Add(failedDeposit);
                    }
                    else
                    {
                        ParseResponse(e.Description, currencyCodeMapping, _DepositResponse, _DepositRequest);

                    }
                }
                catch (Exception ex)
                {

                    _DepositResponse.ErrorDescripotion = $"{_DepositRequest.provider} Exception error: {ex.Message}";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                }
            }
            else
            {
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.error.Description = "DepositRequest parameters can't be null";
            }

            

            if (_DepositResponse.error.ErrorCode == Globals.ErrorCodes.FAIL)
            {
                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = _DepositResponse.TransactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }

            return _DepositResponse;
        }

        #endregion

        #region Private methods

        private string GenerateSignature(string plain)
        {
            System.Security.Cryptography.SHA256 sh = System.Security.Cryptography.SHA256.Create();
            byte[] hashValue = sh.ComputeHash(System.Text.Encoding.UTF8.GetBytes(plain));
            return System.Convert.ToBase64String(hashValue);
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                /*ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );*/

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }


            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private void ParseResponse(string response, Dictionary<string, string> currMapping, DepositResponse _DepositResponse, DepositRequest _DepositRequest)
        {
            FailedDeposit failedDeposit = new FailedDeposit();
            try
            {

                // check if its 3DS ( html format )
                int startOfHTML = response.IndexOf("<html");
                if (startOfHTML > -1)
                {
                    _DepositResponse.Is3DSecure = true;

                    Log("Is3DSecure= " + _DepositResponse.Is3DSecure.ToString());
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;

                    _DepositResponse.html3D = response;

                    _DepositResponse.ErrorDescripotion = "";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                }
                else
                {
                    NameValueCollection col = HttpUtility.ParseQueryString(response);

                    _DepositResponse.Reply = col[REPLY];
                    //_DepositResponse.TransactionID = col[TRANS_ID]; // in 3DS callback , netpay returns different transid , so we save order number instead
                    _DepositResponse.Date = col[DATE];
                    _DepositResponse.Amount = col[AMOUNT];
                    _DepositResponse.CurrencyISOCode = string.IsNullOrEmpty(col[CURRENCY]) ? "" : ConvertToISOCurrencyCode(col[CURRENCY], currMapping);
                    _DepositResponse.Receipt = col[TRANS_ID];
                    _DepositResponse.ReplyDescription = col[REPLY_DESC];
                    _DepositResponse.CardType = col[CCTYPE];
                    _DepositResponse.Descriptor = col[DESCRIPTOR];

                    _DepositResponse.error.ErrorCode = _DepositResponse.Reply == SUCCESSFUL_REPLY_CODE ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
                    _DepositResponse.ErrorMessage = _DepositResponse.Descriptor;
                    _DepositResponse.ErrorDescripotion = _DepositResponse.ReplyDescription;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                    // check if this is 3DS connection
                    if (_DepositResponse.Reply == D3S_REPLY_CODE)
                    {
                        _DepositResponse.Is3DSecure = true;
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        _DepositResponse.ErrorDescripotion = "";
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                        _DepositResponse.RedirectUrl = col[D3REDIRECT];
                    }
                }
                return;
            }
            catch (Exception ex)
            {

                _DepositResponse.ErrorDescripotion = $"{_DepositRequest.provider} An error has occured during parsing response '{response}'. Exception: {ex.Message}";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            }
        }

        private string ConvertToISOCurrencyCode(string netPayCurrency, Dictionary<string, string> currencyMapping)
        {
            if (!currencyMapping.ContainsKey(netPayCurrency))
            {
                throw new Exception(string.Format("Exception: Can't convert provider currency '{0}' into ISO currency code", netPayCurrency));
            }
            return currencyMapping[netPayCurrency].ToString();
        }

        private string ConvertToNetPayCurrency(string isoCurrencyCode, Dictionary<string, string> currencyMapping)
        {
            if (!currencyMapping.ContainsValue(isoCurrencyCode))
            {
                throw new Exception(string.Format("Exception: Can't covert ISO currency code '{0}' into provider currency", isoCurrencyCode));
            }
            return currencyMapping.First(pair => pair.Value == isoCurrencyCode).Key;
        }

        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.NETPAY; }
            set { }
        }
    }
}
