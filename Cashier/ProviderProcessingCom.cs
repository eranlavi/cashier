﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using CashierUtils;

namespace Cashier
{
    class ProviderProcessingCom : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private string Mid;
        private string MidQ;
        private string MidCombined;

        #region Constructor

        public ProviderProcessingCom(ProviderConfigBase config)
            : base(config)
        {
            ProcessingComProviderConfig processingComConfig = config as ProcessingComProviderConfig;
            if (processingComConfig == null)
            {
                throw new ArgumentException("processingComConfig expects to get ProcessingComProviderConfig object!");
            }

            Mid = processingComConfig.Mid;
            MidQ = processingComConfig.MidQ;
            MidCombined = processingComConfig.MidCombined;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.PROCESSINGCOM; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            string transactionID = ((int)((DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds)).ToString();

            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = ProviderType;

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string req = BuildRequestXML(_DepositRequest, ExpMonth, ExpYear, transactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for ProcessingCom";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=ProcessingCom; {0} {1}{2}", Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider=ProcessingCom; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to ProcessingCom failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
                ParseResponse(e.Description, _DepositResponse);

            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

            return _DepositResponse;

        }

        private string BuildRequestXML(DepositRequest _DepositRequest, string CardExpMonth, string CardExpYear, string TransactionID)
        {
            string req = string.Empty;

            try
            {
                string m = "";
                string mq = "";
                if (!string.IsNullOrEmpty(MidCombined))
                {
                    //PROCESSINGCOM MidCombined: <CURRENCY=[MID ID];[MID QUAL]  SEPERATOR IS: @
                    var dict = MidCombined.Split(new[] { '@' }, StringSplitOptions.RemoveEmptyEntries)
                           .Select(part => part.Split('='))
                           .ToDictionary(split => split[0], split => split[1]);
                    string temp = dict[_DepositRequest.CurrencyISOCode.ToUpper()];
                    string[] cred = temp.Split(';');
                    m = cred[0];
                    mq = cred[1];
                }
                else
                {
                    m = Mid;
                    mq = MidQ;
                }

                req = string.Format("account_username={0}&account_password={1}&type=purchase&amount={2}&mid={3}&mid_q={4}&card_number={5}&card_expiry_month={6}&card_expiry_year={7}&card_cvv2={8}&ip_address={9}&first_name={10}&last_name={11}&email_address={12}&street_address_1={13}&zip={14}&city={15}&country={16}&phone_number={17}",
                    MerchantID, MerchantPassword, _DepositRequest.Amount.ToString(), m, mq, _DepositRequest.CardNumber, CardExpMonth, CardExpYear, _DepositRequest.CardCVV2, _DepositRequest.IpAddress,
                    _DepositRequest.FirstName, _DepositRequest.LastName, _DepositRequest.Email, _DepositRequest.Address, _DepositRequest.ZipCode, _DepositRequest.City, _DepositRequest.CountryISOCode, _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", ""));
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=ProcessingCom; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }

            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                            Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode).ToString() + " ";
                        resp.Code = -1;
                        resp.Description = Code + responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private void ParseResponse(string data, DepositResponse _DepositResponse)
        {
            NameValueCollection qscoll = HttpUtility.ParseQueryString(data);

            if (qscoll.Count == 0)
            {
                Log(string.Format("Response: Provider=ProcessingCom; Invalid http response"));
                _DepositResponse.ErrorDescripotion = "ProcessingCom returned empty response for deposit request";
                return;
            }

            //success
            if (qscoll["code"] == "00" || qscoll["code"] == "0")
            {
                _DepositResponse.Receipt = qscoll["transaction_id"];
                _DepositResponse.Amount = qscoll["amount"];
                _DepositResponse.ErrorDescripotion = "";
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
            }
            else
            {
                _DepositResponse.ErrorDescripotion = "Code: " + qscoll["code"] + " , Message: " + qscoll["message"];
                if (!string.IsNullOrEmpty(qscoll["sub_code"]) && !string.IsNullOrEmpty(qscoll["sub_message"]))
                    _DepositResponse.ErrorDescripotion += " , SubCode: " + qscoll["sub_code"] + " , SubMessage: " + qscoll["sub_message"];
            }
        }


    }
}
