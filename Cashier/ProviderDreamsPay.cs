﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Collections.Specialized;
using CashierUtils;

namespace Cashier
{
    class ProviderDreamsPay : ProviderBase
    {
        public class Error
        {
            public int Code;
            public string Description;
        }

        #region Create Purchase
        //Request

        [DataContract]
        public class CreateRequest
        {
            [DataMember]
            public Request request { get; set; }
        }

        [DataContract]
        public class Request
        {
            [DataMember]
            public string order_id { get; set; }
            [DataMember]
            public string merchant_id { get; set; }
            [DataMember]
            public string order_desc { get; set; }
            [DataMember]
            public string signature { get; set; }
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currency { get; set; }
            [DataMember]
            public string card_number { get; set; }
            [DataMember]
            public string cvv2 { get; set; }
            [DataMember]
            public string expiry_date { get; set; }
            [DataMember]
            public string client_ip { get; set; }
            [DataMember]
            public string reservation_data { get; set; }
        }

        [DataContract]
        public class reservation_data
        {
            [DataMember]
            public string customer_name { get; set; }
        }

        //Response
        [DataContract]
        public class CreateResponseObject
        {
            [DataMember]
            public Response response { get; set; }
        }
        [DataContract]
        public class Response
        {
            [DataMember]
            public string order_id { get; set; }
            [DataMember]
            public string merchant_id { get; set; }
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currency { get; set; }
            [DataMember]
            public string order_status { get; set; }
            [DataMember]
            public string response_status { get; set; }
            [DataMember]
            public string signature { get; set; }
            [DataMember]
            public string tran_type { get; set; }
            [DataMember]
            public string sender_cell_phone { get; set; }
            [DataMember]
            public string sender_account { get; set; }
            [DataMember]
            public string masked_card { get; set; }
            [DataMember]
            public string card_bin { get; set; }
            [DataMember]
            public string card_type { get; set; }
            [DataMember]
            public string rrn { get; set; }
            [DataMember]
            public string approval_code { get; set; }
            [DataMember]
            public string response_code { get; set; }
            [DataMember]
            public string response_description { get; set; }
            [DataMember]
            public string reversal_amount { get; set; }
            [DataMember]
            public string settlement_amount { get; set; }
            [DataMember]
            public string settlement_currency { get; set; }
            [DataMember]
            public string order_time { get; set; }
            [DataMember]
            public string settlement_date { get; set; }
            [DataMember]
            public string eci { get; set; }
            [DataMember]
            public string fee { get; set; }
            [DataMember]
            public string payment_system { get; set; }
            [DataMember]
            public string sender_email { get; set; }
            [DataMember]
            public string payment_id { get; set; }
            [DataMember]
            public string actual_amount { get; set; }
            [DataMember]
            public string actual_currency { get; set; }
            [DataMember]
            public string product_id { get; set; }
            [DataMember]
            public string merchant_data { get; set; }
            [DataMember]
            public string verification_status { get; set; }
            [DataMember]
            public string rectoken { get; set; }
            [DataMember]
            public string rectoken_lifetime { get; set; }
            [DataMember]
            public string acs_url { get; set; }
            [DataMember]
            public string pareq { get; set; }
            [DataMember]
            public string md { get; set; }
            [DataMember]
            public string error_code { get; set; }
            [DataMember]
            public string error_message { get; set; }
        }

        #endregion

        private string apiKey;
        private string ReturnURL;

        private const string ORDER_ID = "order_id";
        private const string MERCHANT_ID = "merchant_id";
        private const string ORDER_DESC = "order_desc";
        private const string SIGNATURE = "signature";
        private const string AMOUNT = "amount";
        private const string CURRENCY = "currency";
        private const string CARD_NUMBER = "card_number";
        private const string CVV2 = "cvv2";
        private const string EXPIRY_DATE = "expiry_date";
        private const string CLIENT_IP = "client_ip";
        private const string RESERVATION_DATA = "reservation_data";

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.DREAMSPAY; }
            set { }
        }

        #region Constructor

        public ProviderDreamsPay(ProviderConfigBase config)
            : base(config)
        {
            MerchantsWorldWideProviderConfig DreamsPayConfig = config as MerchantsWorldWideProviderConfig;
            if (DreamsPayConfig == null)
            {
                throw new ArgumentException("DreamsPayConfig expects to get DreamsPayProviderConfig object!");
            }

            apiKey = DreamsPayConfig.PrivateKey;
            ReturnURL = DreamsPayConfig.CallbackUrl;
        }
        #endregion

        public override DepositResponse Deposit(DepositRequest depositRequest)
        {
            DepositResponse depositResponse = new DepositResponse();
            //depositResponse.error = new Cashier.Error();

            depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            depositResponse.ErrorCode = "1";
            string transactionID = depositResponse.TransactionID = Guid.NewGuid().ToString().Replace("-", "");
            depositResponse.Amount = depositRequest.Amount.ToString();
            depositResponse.CurrencyISOCode = depositRequest.CurrencyISOCode;
            depositResponse.provider = depositRequest.provider;
            depositResponse.ClearingUserID = MerchantID;
            depositResponse.TransactionID = transactionID;

            #region Trim            
            depositRequest.Address = depositRequest.Address.Trim();
            depositRequest.CardCVV2 = depositRequest.CardCVV2.Trim();
            depositRequest.CardExpMonth = depositRequest.CardExpMonth.Trim();
            depositRequest.CardExpYear = depositRequest.CardExpYear.Trim();
            depositRequest.CardNumber = depositRequest.CardNumber.Trim();
            depositRequest.City = depositRequest.City.Trim();
            depositRequest.CountryISOCode = depositRequest.CountryISOCode.Trim();
            if (!string.IsNullOrEmpty(depositRequest.CountryISOCodeThreeLetters))
                depositRequest.CountryISOCodeThreeLetters = depositRequest.CountryISOCodeThreeLetters.Trim();
            depositRequest.CurrencyISOCode = depositRequest.CurrencyISOCode.Trim();
            if (!string.IsNullOrEmpty(depositRequest.Description))
                depositRequest.Description = depositRequest.Description.Trim();
            depositRequest.Email = depositRequest.Email.Trim();
            depositRequest.FirstName = depositRequest.FirstName.Trim();
            depositRequest.LastName = depositRequest.LastName.Trim();
            if (!string.IsNullOrEmpty(depositRequest.NameOnCard))
                depositRequest.NameOnCard = depositRequest.NameOnCard.Trim();
            depositRequest.PhoneNumber = depositRequest.PhoneNumber.Trim();
            if (!string.IsNullOrEmpty(depositRequest.StateISOCode))
                depositRequest.StateISOCode = depositRequest.StateISOCode.Trim();
            depositRequest.ZipCode = depositRequest.ZipCode.Trim();

            #endregion

            CreateRequest createRequest = BuildPurchaseRequestObject(depositRequest, transactionID);

            var json = JSONSerializer<CreateRequest>.Serialize(createRequest).Replace("\\/", "/");

            Log(string.Format("Request: Provider=DreamsPay; {0} {1}{2}", Url, json, Environment.NewLine));

            Error e = HttpReq("POST", Url, json, null, "application/json", "", 20000, 20000);
            Log(string.Format("Response : Provider=DreamsPay; {0}{1}", e.Description, Environment.NewLine));
            if (e.Code != 0)
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "HTTP request for create PURCHASE failed: " + e.Description;
                Log(string.Format("Response: Provider=DreamsPay; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = depositRequest.provider;
                failedDeposit.Email = depositRequest.Email;
                failedDeposit.TransactionID = depositResponse.TransactionID;
                failedDeposit.Amount = depositRequest.Amount;
                failedDeposit.FirstName = depositRequest.FirstName;
                failedDeposit.LastName = depositRequest.LastName;
                failedDeposit.Description = e.Description;

                depositResponse.FailedDeposits.Add(failedDeposit);

                return depositResponse;
            }

            CreateResponseObject createResponseObject = null;
            try
            {
                createResponseObject = JSONSerializer<CreateResponseObject>.DeSerialize(e.Description);
            }
            catch
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "Fail parsing http response to JSON: " + e.Description;
                Log(string.Format("Response: Provider=DreamsPay; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                return depositResponse;
            }

            parseResponse(createRequest, depositResponse, createResponseObject);

            return depositResponse;
        }

        private void parseResponse(CreateRequest createRequest, DepositResponse depositResponse, CreateResponseObject createResponseObject)
        {
            try
            {
                if (createResponseObject.response.acs_url != null)
                {
                    // 3ds
                    if (createResponseObject.response.response_status == "success")
                    {
                        depositResponse.Is3DSecure = true;
                        depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        depositResponse.ErrorCode = "0";
                        depositResponse.ErrorDescripotion = "";
                        depositResponse.error.Description = "";

                        depositResponse.RedirectUrl = createResponseObject.response.acs_url;

                        depositResponse.RedirectParameters = new List<RedirectValues>();

                        RedirectValues pareq = new RedirectValues();
                        pareq.Key = "PaReq";
                        pareq.Value = createResponseObject.response.pareq;
                        depositResponse.RedirectParameters.Add(pareq);

                        RedirectValues md = new RedirectValues();
                        md.Key = "MD";
                        md.Value = createResponseObject.response.md;
                        depositResponse.RedirectParameters.Add(md);

                        RedirectValues termurl = new RedirectValues();
                        termurl.Key = "TermUrl";
                        termurl.Value = ReturnURL;
                        depositResponse.RedirectParameters.Add(termurl);

                        depositResponse.DataToSave = pareq.Value + "<SEP>";
                        depositResponse.DataToSave += createResponseObject.response.md + "<SEP>";
                        depositResponse.DataToSave += depositResponse.TransactionID + "<SEP>";
                        depositResponse.DataToSave += createRequest.request.signature + "<SEP>";
                        depositResponse.DataToSave += createRequest.request.client_ip + "<SEP>";                        
                        depositResponse.DataToSave += createRequest.request.amount.ToString() + "<SEP>";
                        depositResponse.DataToSave += createRequest.request.currency;                        
                    }
                    else
                    {
                        depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        depositResponse.ErrorCode = createResponseObject.response.error_code;
                        depositResponse.ErrorMessage = createResponseObject.response.response_status;
                        depositResponse.error.Description = string.Format("{0} - {1}", createResponseObject.response.error_code, createResponseObject.response.error_message);
                        depositResponse.ErrorDescripotion = depositResponse.error.Description;
                    }
                }
                else
                {
                    // non 3ds
                    if (createResponseObject.response.response_status == "success")
                    {
                        depositResponse.error.ErrorCode = createResponseObject.response.order_status == "approved" ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
                        depositResponse.ErrorMessage = createResponseObject.response.order_status;
                        depositResponse.error.Description = createResponseObject.response.response_description;
                        depositResponse.ErrorDescripotion = depositResponse.error.Description;
                        depositResponse.ErrorCode = createResponseObject.response.response_code;
                    }
                    else
                    {
                        // failed
                        depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        depositResponse.ErrorCode = createResponseObject.response.error_code;
                        depositResponse.ErrorMessage = createResponseObject.response.response_status;
                        depositResponse.error.Description = string.Format("{0} - {1}", createResponseObject.response.error_code, createResponseObject.response.error_message);
                        depositResponse.ErrorDescripotion = depositResponse.error.Description;
                    }
                }

            }
            catch (Exception exp)
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "parseResponse: Fail in parsing parameters: " + exp.Message;
                Log(string.Format("Provider=DreamsPay; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
            }
        }

        private CreateRequest BuildPurchaseRequestObject(DepositRequest request, string TransactionID)
        {
            string order_desc = "Deposit";
            string amount = request.Amount.ToString("0.00").Replace(".", "");
            if (request.CardExpYear.Length > 2)
            {
                request.CardExpYear = request.CardExpYear.Substring(request.CardExpYear.Length - 2);
            }
            string expiry_date = Convert.ToInt32(request.CardExpMonth).ToString("00") + Convert.ToInt32(request.CardExpYear).ToString("00");

            // build reservation data as requested by DreamsPay email ( no docs )
            //example: {  "phonemobile": "+12345678",  "customer_address": "15 gannet street elspark",  "customer_country": "US",  "customer_state": "NY",  "customer_name": "Brandon Nyathi",  "customer_city": "New York",  "customer_zip": "1401"}

            reservation_data res_data = new reservation_data();
            res_data.customer_name = string.Format("{0} {1}", request.FirstName, request.LastName);
            var jsonReservation = JSONSerializer<reservation_data>.Serialize(res_data).Replace("\\/", "/");
            string res_data_base64 = Base64Encode(jsonReservation);

            var data = new NameValueCollection
            {
                {ORDER_ID, TransactionID},
                {MERCHANT_ID, MerchantID},
                {ORDER_DESC, order_desc},
                {AMOUNT, amount},
                {CURRENCY, request.CurrencyISOCode},
                {CARD_NUMBER, request.CardNumber},
                {CVV2, request.CardCVV2},
                {EXPIRY_DATE, expiry_date },
                {CLIENT_IP, request.IpAddress},
                {RESERVATION_DATA, res_data_base64}
            };

            CreateRequest createRequest = new CreateRequest();
            createRequest.request = new Request();

            createRequest.request.order_id = TransactionID;
            createRequest.request.merchant_id = MerchantID;
            createRequest.request.order_desc = order_desc;
            createRequest.request.amount = amount;
            createRequest.request.currency = request.CurrencyISOCode;
            createRequest.request.card_number = request.CardNumber;
            createRequest.request.cvv2 = request.CardCVV2;
            createRequest.request.expiry_date = expiry_date;
            createRequest.request.client_ip = request.IpAddress;
            createRequest.request.reservation_data = res_data_base64;

            createRequest.request.signature = CalculateCheckSum(data, MerchantPassword);

            return createRequest;

        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        if ((we.Response is System.Net.HttpWebResponse))
                            resp.Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode);
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }


        /// <summary>
        /// The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
        /// Parameters must not be URL encoded before signature calculation.
        /// Please follow these steps:
        /// 1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
        /// 2. Append the corresponding values together according to the alphabetical sequence of parameter names.
        /// 3. Add the secret to the end of the string.
        /// 4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
        /// </summary>
        private string CalculateCheckSum(NameValueCollection data, string secretKey)
        {
            //The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
            //Parameters must not be URL encoded before signature calculation.
            //Please follow these steps:
            //1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
            //2. Append the corresponding values together according to the alphabetical sequence of parameter names.
            //3. Add the secret to the end of the string.
            //4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.

            //1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
            var keys = data.AllKeys.OrderBy(k => k);

            //2. Add the secret to the first of the string.
            var valuesRow = new StringBuilder();

            //3. Append the corresponding values together according to the alphabetical sequence of parameter names.
            valuesRow.Append(secretKey + "|");

            foreach (string key in keys)
            {
                valuesRow.Append(data[key] + "|");
            }

            //4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
            byte[] bytes = Encoding.UTF8.GetBytes(valuesRow.ToString().Substring(0, valuesRow.Length - 1));
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);

            var result = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

        private static string Base64Encode(string plainText)
        {
            byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(encbuff);
        }

    }
}