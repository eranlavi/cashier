﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    internal class ProviderAstech : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private string SoapAction;
        private string StoreId;
        private Dictionary<string, string> CurrencyToBrandId;

        public ProviderAstech(ProviderConfigBase config)
            : base(config)
        {
            var astechConfig = config as AstechProviderConfig;
            if (astechConfig == null)
            {
                throw new ArgumentException("AstechProvider expects to get AstechProviderConfig object!");
            }

            SoapAction = astechConfig.SoapAction;
            StoreId = astechConfig.StoreId;
            CurrencyToBrandId = astechConfig.BrandId.Split(',').Select(x => x.Split('=')).ToDictionary(x => x[0], x => x[1]);
        }

        protected override Globals.Providers ProviderType
        {
            get
            {
                return Globals.Providers.ASTECH;
            }
            set { }
        }

        private string ConvertCardTypeToAstechCardType(string Card)
        {
            switch (Card)
            {
                case "visa":
                case "Visa":
                    return "VISA";
                case "amex":
                case "American Express":
                case "american express":
                    return "AMEX";
                case "mastercard":
                case "Mastercard":
                    return "MASTERCARD";
                case "maestro":
                case "Maestro":
                    return "";
                case "dinersclubintl":
                case "Diners Club":
                case "dinersclub":
                    return "";
                default:
                    return string.Empty;
            }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            var _DepositResponse = new DepositResponse();
            var ci = new System.Globalization.CultureInfo("en-US");
            var OrderID = Guid.NewGuid().ToString().Replace("-", string.Empty);
            var CardExpiration = _DepositRequest.CardExpMonth + "/" + ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear));
            var ServiceName = (string.IsNullOrEmpty(_DepositRequest.Description)) ? ("Fund Deposit") : (_DepositRequest.Description);
            string billingState = (string.IsNullOrEmpty(_DepositRequest.StateISOCode)) ? ("NA") : (_DepositRequest.StateISOCode);
            string firstName = _DepositRequest.FirstName.Trim();
            string lastName = _DepositRequest.LastName.Trim();
            string phoneNumber = _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", "").Replace(" ", "");
            string amount = _DepositRequest.Amount.ToString("0.00");



            try
            {
                var req = string.Format(@"<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='https://www.astechprocessing.net/webservices/'>
                   <soapenv:Header/>
                   <soapenv:Body>
                      <web:ChargeCCAsync>
                         <web:pr>            
                            <web:BrandID>{0}</web:BrandID>
                            <web:MerchantID>{1}</web:MerchantID>
                            <web:MerchantPassword>{2}</web:MerchantPassword>
                            <web:ProductType>CREDIT</web:ProductType>
                            <web:StoreID>{3}</web:StoreID>            
                            <web:ClientFirstName>{4}</web:ClientFirstName>
                            <web:ClientLastName>{5}</web:ClientLastName>
                            <web:ClientEmail>{6}</web:ClientEmail>
                            <web:ClientPhone>{7}</web:ClientPhone>
                            <web:ClientAddress>{8}</web:ClientAddress>
                            <web:ClientCity>{9}</web:ClientCity>           
                            <web:ClientZip>{10}</web:ClientZip>
                            <web:ClientCountry>{11}</web:ClientCountry>
                            <web:ClientState>{12}</web:ClientState>
                            <web:ClientIP>{13}</web:ClientIP>
                            <web:ClientCreditCard>
                               <web:CardType>{14}</web:CardType>
                               <web:CardNumber>{15}</web:CardNumber>
                               <web:ExpirationDate>{16}</web:ExpirationDate>
                               <web:CVV>{17}</web:CVV>               
                               <web:NameOnCard>{18}</web:NameOnCard>
                               <web:TransactionType></web:TransactionType>
                            </web:ClientCreditCard>
                            <web:OrderNumber>{19}</web:OrderNumber>
                            <web:Currency>{20}</web:Currency>
                            <web:ServiceName>{21}</web:ServiceName>
                            <web:Amount>{22}</web:Amount>
                         </web:pr>
                      </web:ChargeCCAsync>
                   </soapenv:Body>
                </soapenv:Envelope>", CurrencyToBrandId[_DepositRequest.CurrencyISOCode], MerchantID, MerchantPassword, StoreId, firstName, lastName, _DepositRequest.Email,
                                        phoneNumber, _DepositRequest.Address, _DepositRequest.City, _DepositRequest.ZipCode, _DepositRequest.CountryISOCode, billingState, _DepositRequest.IpAddress,
                                        ConvertCardTypeToAstechCardType(_DepositRequest.CardType), _DepositRequest.CardNumber, CardExpiration, _DepositRequest.CardCVV2, _DepositRequest.FirstName + " " + _DepositRequest.LastName,
                                        OrderID, _DepositRequest.CurrencyISOCode, ServiceName, amount);

                Log(string.Format("Request: Provider=Astech; {0}{1}", req, Environment.NewLine));

                var header = new System.Collections.Specialized.NameValueCollection();
                header.Add("SOAPAction", SoapAction);

                var res = HttpRequest("POST", Url, req, header, "text/xml;charset=UTF-8", "text/xml", 10000, 30000);

                Log(string.Format("Response: Provider=Astech; {0}{1}", res.Description, Environment.NewLine));

                _DepositResponse.TransactionID = OrderID.ToString();
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ClearingUserID = MerchantID;

                if (res.Code != 0)
                {
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.error.Description = res.Description;
                    _DepositResponse.ErrorDescripotion = res.Description;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = Globals.Providers.ASTECH;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = _DepositResponse.TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = res.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);
                }
                else
                {
                    var xd = new XmlDocument();
                    xd.LoadXml(res.Description);
                    var xnl = xd.GetElementsByTagName("Status");
                    if (xnl == null || xnl[0] == null)
                    {
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        _DepositResponse.error.Description = "Astech returned invalid xml response. Missing Status attribute.";
                        _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description;
                        return _DepositResponse;
                    }
                    if (xnl[0].InnerText == "OK")
                    {
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        _DepositResponse.error.Description = string.Empty;
                        _DepositResponse.ErrorDescripotion = string.Empty;

                        xnl = xd.GetElementsByTagName("ASTECHReference");
                        if (xnl != null || xnl[0] != null)
                        {
                            _DepositResponse.Receipt = xnl[0].InnerText;
                        }
                        xnl = xd.GetElementsByTagName("SettledAmount");
                        if (xnl != null || xnl[0] != null)
                        {
                            _DepositResponse.Amount = xnl[0].InnerText;
                        }
                    }
                    else
                    {
                        var xnlCode = xd.GetElementsByTagName("StatusCode");
                        if (xnlCode == null || xnlCode[0] == null)
                        {
                            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                            _DepositResponse.error.Description = "Astech returned invalid xml response. Missing StatusCode attribute.";
                            _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description;
                            return _DepositResponse;
                        }

                        if (xnl[0].InnerText == "PENDING" && (xnlCode[0].InnerText == "3" || xnlCode[0].InnerText == "-3"))
                        {
                            _DepositResponse.Is3DSecure = true;

                            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                            _DepositResponse.ErrorCode = "0";
                            _DepositResponse.ErrorDescripotion = "";
                            _DepositResponse.error.Description = "";

                            var xnl1 = xd.GetElementsByTagName("RedirectURL");

                            if (xnl1 != null && xnl1[0] != null)
                            {
                                _DepositResponse.RedirectUrl = xnl1[0].InnerText;
                            }
                            else
                            {
                                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                                _DepositResponse.error.Description = "Description: missed RedirectURL field from Astech";
                                _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description;
                            }
                        }
                        else
                        {
                            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                            var Code = xnlCode[0].InnerText;
                            xnl = xd.GetElementsByTagName("StatusMessage");
                            if (xnl != null || xnl[0] != null)
                            {
                                _DepositResponse.error.Description = "Code: " + Code + ", Description: " + xnl[0].InnerText;
                                _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description;
                            }
                            else
                            {
                                _DepositResponse.error.Description = "Deposit failed with Code: " + Code;
                                _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.error.Description = "Cashier Exception: " + ex.ToString();
                _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description;

                _DepositResponse.TransactionID = OrderID.ToString();
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ClearingUserID = MerchantID;
            }
            finally
            {
                _DepositResponse.provider = _DepositRequest.provider;
            }

            return _DepositResponse;
        }

        private static Error HttpRequest(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec)
        {
            var resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                {
                    req.Headers.Add(Header);
                }
                if (!string.IsNullOrEmpty(ContentType))
                {
                    req.ContentType = ContentType;
                }
                if (!string.IsNullOrEmpty(Accept))
                {
                    req.Accept = Accept;
                }
                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (var stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (var stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                var webResponse = req.GetResponse();
                using (var rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                resp.Code = 1;
                resp.Description = we.Status.ToString() + ", " + we.Message;
            }
            catch (Exception ex)
            {
                resp.Code = 2;
                resp.Description = ex.Message;
            }

            return resp;
        }
    }
}
