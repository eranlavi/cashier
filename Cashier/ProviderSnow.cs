﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderSnow : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        public ProviderSnow(ProviderConfigBase config)
            : base(config)
		{
            
		}

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.SNOW; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");            
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = _DepositRequest.provider;

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();
            
            string req = BuildRequestXML(_DepositRequest, transactionID, ExpMonth, ExpYear);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for Snow";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;               
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=Snow; {0} {1}{2}", Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider=Snow; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to Snow failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
                ParseResponse(e.Description, _DepositResponse, _DepositRequest);

            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

            return _DepositResponse;
        }

        private string BuildRequestXML(DepositRequest _DepositRequest, string TransactionID, string CardExpMonth, string CardExpYear)
        {
            string req = string.Empty;
            
            string Amount = string.Empty;
            if (_DepositRequest.CurrencyISOCode.ToUpper() == "JPY")
                Amount = _DepositRequest.Amount.ToString();
            else
            {
                Amount = Math.Floor(_DepositRequest.Amount * 100.0).ToString();                
            }

            try
            {
                //req = "username=websitepromo2&password=Japadog!&transaction_id=" + TransactionID + "&transaction_type=sale&amount=" + Amount + "&currency=USD&ipaddress=1.1.1.1&email=" + Email + "&cc_number=4111111111111111&cc_expirym=01&cc_expiryy=2016&cc_cvv2=123&cc_nameoncard=" + CardHolderName + "&firstname=test&lastname=name";
                req = string.Format("username={0}&password={1}&transaction_id={2}&transaction_type={3}&amount={4}&currency={5}&ipaddress={6}&email={7}&cc_number={8}&cc_expirym={9}&cc_expiryy={10}&cc_cvv2={11}&cc_nameoncard={12}&firstname={13}&lastname={14}&address={15}&city={16}&country={17}&postcode={18}&phone={19}&demo={20}",
                    MerchantID, MerchantPassword, TransactionID, "sale", Amount, _DepositRequest.CurrencyISOCode, _DepositRequest.IpAddress, System.Web.HttpUtility.UrlEncode(_DepositRequest.Email), _DepositRequest.CardNumber, CardExpMonth, CardExpYear,
                    _DepositRequest.CardCVV2, System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName + " " + _DepositRequest.LastName),
                    System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName), System.Web.HttpUtility.UrlEncode(_DepositRequest.LastName), System.Web.HttpUtility.UrlEncode(_DepositRequest.Address),
                    System.Web.HttpUtility.UrlEncode(_DepositRequest.City), _DepositRequest.CountryISOCode, _DepositRequest.ZipCode, _DepositRequest.PhoneNumber.Replace("+", "").Replace("-", ""), (Mode == Configuration.Mode.LIVE) ? ("0") : ("1"));
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=Snow; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {               
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;
                
                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }                
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                            Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode).ToString() + " ";
                        resp.Code = -1;
                        resp.Description = Code + responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private void ParseResponse(string Xml, DepositResponse _DepositResponse, DepositRequest _DepositRequest)
        {
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.LoadXml(Xml);
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider=Snow; XML parse exception: {0}{1}", ex.Message, Environment.NewLine));

                _DepositResponse.ErrorDescripotion = "Invalid xml response. " + ex.Message;
                return;
            }

            try
            {
                XmlNode status = xDoc.SelectSingleNode("//response/status");
                if (status.InnerText == "3dsecure")
                {
                    XmlNode error = xDoc.SelectSingleNode("//response/error_code");
                    if (Convert.ToInt32(error.InnerText) != 0)
                    {
                        _DepositResponse.ErrorCode = error.InnerText;
                        error = xDoc.SelectSingleNode("//response/error_description");
                        _DepositResponse.ErrorDescripotion = "Code: " + _DepositResponse.ErrorCode + " , Description: " + error.InnerText;
                        return;
                    }

                    _DepositResponse.Is3DSecure = true;
                    XmlNode acsurl = xDoc.SelectSingleNode("//response/acsurl");
                    _DepositResponse.RedirectUrl = acsurl.InnerText;

                    _DepositResponse.RedirectParameters = new List<RedirectValues>();

                    XmlNode pareq = xDoc.SelectSingleNode("//response/pareq");
                    RedirectValues pareqVal = new RedirectValues();
                    pareqVal.Key = "PaReq";
                    pareqVal.Value = pareq.InnerText;
                    _DepositResponse.RedirectParameters.Add(pareqVal);

                    XmlNode request_id = xDoc.SelectSingleNode("//response/request_id");
                    RedirectValues md = new RedirectValues();
                    md.Key = "MD";
                    md.Value = request_id.InnerText;
                    _DepositResponse.RedirectParameters.Add(md);

                    RedirectValues termurl = new RedirectValues();
                    termurl.Key = "TermUrl";
                    termurl.Value = CallbackUrl;
                    _DepositResponse.RedirectParameters.Add(termurl);
                    
                    _DepositResponse.DataToSave = pareq.InnerText + "<SEP>";
                    _DepositResponse.DataToSave += _DepositResponse.TransactionID + "<SEP>";
                    _DepositResponse.DataToSave += _DepositRequest.CardCVV2 + "<SEP>";
                    _DepositResponse.DataToSave += _DepositRequest.IpAddress + "<SEP>";
                    _DepositResponse.DataToSave += request_id.InnerText + "<SEP>";
                    _DepositResponse.DataToSave += _DepositRequest.Amount.ToString() + "<SEP>";
                    _DepositResponse.DataToSave += _DepositRequest.CurrencyISOCode;

                    _DepositResponse.DataToSaveKey = request_id.InnerText;

                    _DepositResponse.ErrorDescripotion = "";
                    _DepositResponse.ErrorCode = "0";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                }
                else
                {
                    XmlNode node = xDoc.SelectSingleNode("//response/error_code");
                    if (node.InnerText == "0")
                    {
                        XmlNode receipt = xDoc.SelectSingleNode("//response/authcode");
                        _DepositResponse.Receipt = receipt.InnerText;

                        _DepositResponse.ErrorDescripotion = "";
                        _DepositResponse.ErrorCode = "0";
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                    }
                    else
                    {
                        XmlNode errorDesc = xDoc.SelectSingleNode("//response/error_description");
                        _DepositResponse.ErrorDescripotion = "Code: " + node.InnerText + " , Description: " + errorDesc.InnerText;
                    }
                }
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider=Snow; Parsing xml response exception: {0}{1}", ex.Message, Environment.NewLine));

                _DepositResponse.ErrorDescripotion = "Parsing xml response exception. " + ex.Message;
            }

        }


    }
}
