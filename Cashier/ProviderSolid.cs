﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Web;
using Cashier.Configuration.ProviderConfig;
using Cashier.Configuration;
using CashierUtils;

namespace Cashier
{
    internal class ProviderSolid : ProviderBase
    {
        #region Constants
        const string UNDEFINED = "Undefined";
        const string TRANSCTION_MODE_TEST = "INTEGRATOR_TEST";
        const string TRANSCTION_MODE_LIVE = "LIVE";

        const string POST = "POST";
        const string CONTENT_TYPE = "application/x-www-form-urlencoded;charset=UTF-8";
        const string SECURITY_HASH = "SecurityHash";
        const string TRANSACTION_ID = "TransactionID";
        const string AMOUNT = "Amount";
        const string CURRENCY = "Currency";
        const string STATUS = "Status";
        const string REASON = "Reason";
        const string TIMESTAMP = "Timestamp";
        const string RETURN = "Return";
        const string CODE = "code";

        const string RETURN_CODE_SUCCESS_1 = "000.000.000";
        const string RETURN_CODE_SUCCESS_2 = "000.100.110";
        const string RETURN_CODE_SUCCESS_3 = "000.100.111";
        const string RETURN_CODE_SUCCESS_4 = "000.100.112";
        const string STATUS_CODE_SUCCESS_1 = "90.00";
        const string STATUS_CODE_SUCCESS_2 = "90.70";

        private const string PROVIDER = "Provider";
        private const string ATTRIBUTE_NAME = "Name";
        private const string ATTRIBUTE_MODE = "Mode";
        private const string MODE_LIVE = "Live";
        private const string SENDER = "Sender";
        private const string CHANNEL = "Channel";

        private const string UNIQUE_ID = "UniqueID";

        #endregion

        #region Private members

        //private SQLiteDatabase db;
        private readonly CashierConfigurator configurator;
        private const string CARD_CODE = "fCardCode";
        private const string SQL_QUERY_CARD_TYPE_CODE = "SELECT  sct.fCardCode" +
                                                "FROM tSolidCardTypes sct" +
                                                "WHERE (c.fCardBrandName = '{0}'";

        private string sender;
        private string channel;



        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            {"visa", "VISA"},
            {"Visa", "VISA"},

            {"mastercard", "MASTER"},
            {"Mastercard", "MASTER"},

            {"diners club", "DINERS"},            
            {"dinersclubintl", "DINERS"},
			{"DinersClub", "DINERS"},
            {"dinersclub", "DINERS"},
            {"Diners Club", "DINERS"},

            {"american express", "AMEX"},            
            {"American Express", "AMEX"},
            {"amex", "AMEX"},
			{"AmEx", "AMEX"},

            {"maestro","MAESTRO"},
            {"Maestro","MAESTRO"},
        };

        #endregion

        #region Constructor

        public ProviderSolid(ProviderConfigBase config): base(config)
        {
            SolidProviderConfig solidConfig = config as SolidProviderConfig;
            if (solidConfig == null)
            {
                throw new ArgumentException("SolidProvider expects to get SolidProviderConfig object!");
            }

            sender = solidConfig.Sender;
            channel = solidConfig.Channel;

            /*string pathToCashierConfigFile = HttpContext.Current.Server.MapPath("~/Cashier.config");
            
            configurator = new CashierConfigurator(LogFile);
            
            try
            {
                db = new SQLiteDatabase(configurator.PathToCashierDB);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() + ": Cashier.ProviderSolid: ERROR creating sqlLite DB:  " + ex.Message + "\r\n");
                return;
            }*/
        }
        #endregion

        #region Public methods

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();

            if (_DepositRequest != null)
            {
                try
                {
                    string requestXml = CreateRequestXml(_DepositRequest);
                    Log(string.Format("Request: {0}", requestXml));
                    Log(string.Format("Request: UserName={0}; Currency={1}; Amount={2}; MerchantID={3}; Password={4}; Url={5}",
                     _DepositRequest.Email, _DepositRequest.CurrencyISOCode, _DepositRequest.Amount, MerchantID, MerchantPassword, Url));

                    _DepositResponse.CardType = _DepositRequest.CardType;

                    string responseXml = SendRequest(requestXml, Url);

                    Log(string.Format("Response: {0}", responseXml));

                    _DepositResponse = ParseResponse(responseXml);
                }
                catch (Exception ex)
                {
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.error.Description = ex.Message;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = ProviderType;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = _DepositResponse.TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = _DepositResponse.error.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);
                }
            }
            else
            {
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.error.Description = "SolidDepositRequest parameters can't be null";
            }

            _DepositResponse.provider = ProviderType;

            return _DepositResponse;
        }

        #endregion

        #region Private methods

        private string CreateRequestXml(DepositRequest request)
        {
            string transactionMode = Mode == Configuration.Mode.LIVE ? TRANSCTION_MODE_LIVE : TRANSCTION_MODE_TEST;
            /* string sqlQuery = string.Format(SQL_QUERY_CARD_TYPE_CODE, request.CardType.ToLower());
             string cardBrand = GetCardCode(db, sqlQuery);*/

            // 4 digits year
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = request.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(request.CardExpYear)).ToString();

            return new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request version=\"1.0\"><Header>")
                   .AppendFormat("<Security sender=\"{0}\" /></Header>", this.sender)
                   .AppendFormat("<Transaction mode=\"{0}\" response=\"SYNC\" channel=\"{1}\">",
                        transactionMode, this.channel)
                   .AppendFormat("<User login=\"{0}\" pwd=\"{1}\"/><Identification>", MerchantID, MerchantPassword)
                   .AppendFormat("<TransactionID>{0}</TransactionID></Identification><Payment code=\"CC.DB\"><Presentation>", Guid.NewGuid().ToString("n"))
                   .AppendFormat("<Amount>{0}</Amount>", request.Amount)
                   .AppendFormat("<Currency>{0}</Currency>", request.CurrencyISOCode)
                   .AppendFormat("<Usage>{0}</Usage></Presentation></Payment><Account>", request.Description)
                   .AppendFormat("<Holder>{0} {1}</Holder>", request.FirstName, request.LastName)
                   .AppendFormat("<Number>{0}</Number>", request.CardNumber)
                   .AppendFormat("<Brand>{0}</Brand>", GetSolidCardBrandByCardType(request.CardType))
                   .AppendFormat("<Expiry month=\"{0}\" year=\"{1}\"></Expiry>", request.CardExpMonth, ExpYear)
                   .AppendFormat("<Verification>{0}</Verification></Account><Customer><Name>", request.CardCVV2)
                   .AppendFormat("<Given>{0}</Given>", request.FirstName)
                   .AppendFormat("<Family>{0}</Family></Name><Address>", request.LastName)
                   .AppendFormat("<Street>{0}</Street>", request.Address)
                   .AppendFormat("<Zip>{0}</Zip>", request.ZipCode)
                   .AppendFormat("<City>{0}</City>", request.City)
                   .AppendFormat("<State>{0}</State>", request.StateISOCode)
                   .AppendFormat("<Country>{0}</Country></Address><Contact>", request.CountryISOCode)
                   .AppendFormat("<Email>{0}</Email>", request.Email)
                   .AppendFormat("<Ip>{0}</Ip>", request.IpAddress)
                   .Append("</Contact></Customer></Transaction></Request>")
                   .ToString();
        }

        private string GetCardCode(SQLiteDatabase dataBase, string sqlQuery)
        {
            string cardCode = string.Empty;
            if (dataBase == null)
            {
                //System.IO.File.AppendAllText(LogFile, DateTime.Now.ToString() +
                //    ": Cashier.ProviderSolid: GetCardCode DB is NULL\r\n");
                Log(DateTime.Now.ToString() + ": Cashier.ProviderSolid: GetCardCode DB is NULL");
            }


            //System.IO.File.AppendAllText(LogFile, DateTime.Now.ToString() +
                    //":  Query is: " + sqlQuery + "\r\n");
            Log(DateTime.Now.ToString() + ":  Query is: " + sqlQuery);

            DataTable dt = dataBase.GetDataTable(sqlQuery);

            //System.IO.File.AppendAllText(LogFile, DateTime.Now.ToString() +
                    //": Cashier.ProviderSolid: Cashier.ProviderSolid: GetCardCode: Result data table rows count: " + dt.Rows.Count + "\r\n");
            Log(DateTime.Now.ToString() + ": Cashier.ProviderSolid: Cashier.ProviderSolid: GetCardCode: Result data table rows count: " + dt.Rows.Count);

            if (dt == null || dt.Rows.Count == 0)
            {
                //System.IO.File.AppendAllText(LogFile, DateTime.Now.ToString() +
                    //"Cashier.ProviderSolid: Unable to read data from CashierDB (or there are no this card name)\nReturned 'Other' credit card type");
                Log(DateTime.Now.ToString() + "Cashier.ProviderSolid: Unable to read data from CashierDB (or there are no this card name)\nReturned 'Other' credit card type");
                return "Other";
            }
            DataRow row = dt.Rows[0];
            cardCode = row[CARD_CODE].ToString().ToUpper();
            //System.IO.File.AppendAllText(LogFile, DateTime.Now.ToString() +
                    //"Cashier.ProviderSolid: Card code " + cardCode);
            Log(DateTime.Now.ToString() + "Cashier.ProviderSolid: Card code " + cardCode);
            return cardCode;
        }

        private string SendRequest(string requestXml, string serviceUrl)
        {
            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create(serviceUrl);
            // Set the Method property of the request to POST.
            request.Method = POST;
            // Create POST data and convert it to a byte array.
            string postData = "load=" + requestXml;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = CONTENT_TYPE;
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            return responseFromServer;
        }

        private DepositResponse ParseResponse(string xmlResponse)
        {
            DepositResponse response = new DepositResponse();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlResponse);

                // Get the root node 
                XmlElement rootElm = doc.DocumentElement;

                response = new DepositResponse()
                {
                    AuthCode = GetXmlElementValue(rootElm, SECURITY_HASH),

                    TransactionID = GetXmlElementValue(rootElm, TRANSACTION_ID),
                    Amount = GetXmlElementValue(rootElm, AMOUNT),
                    CurrencyISOCode = GetXmlElementValue(rootElm, CURRENCY),
                    Status = GetXmlElementValue(rootElm, STATUS),
                    Reason = GetXmlElementValue(rootElm, REASON),
                    Time = GetXmlElementValue(rootElm, TIMESTAMP),
                    ClearingUserID = MerchantID,
                    ErrorDescripotion = "STATUS: " + GetXmlElementValue(rootElm, STATUS) + " REASON: " + GetXmlElementValue(rootElm, REASON) + " RETURN: " + GetXmlElementValue(rootElm, RETURN),

                };

                response.Receipt = GetXmlElementValue(rootElm, UNIQUE_ID);

                string statusCode = GetXmlElementAttributeValue(rootElm, STATUS, CODE);
                string reasonCode = GetXmlElementAttributeValue(rootElm, REASON, CODE);
                string returnCode = GetXmlElementAttributeValue(rootElm, RETURN, CODE);
                string returnValue = GetXmlElementValue(rootElm, RETURN);

                response.error.ErrorCode =
                    WasTransactionSuccessful(statusCode, reasonCode, returnCode) ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;

                response.error.Description = string.Format("Status: {0}; Reason: {1}; Return: {2}", response.Status, response.Reason, returnValue);
            }
            catch (Exception ex)
            {
                response.error.ErrorCode = Globals.ErrorCodes.FAIL;
                response.error.Description = "An error has occured while parsing Solid provider response: " + ex.Message;
            }

            response.provider = ProviderType;

            return response;
        }

        private string GetSolidCardBrandByCardType(string cardType)
        {
            string cardBrand = string.Empty;
            cardTypeMapping.TryGetValue(cardType, out cardBrand);
            return cardBrand;
        }

        //Returns Xml element value
        private string GetXmlElementValue(XmlElement rootElm, string elmName)
        {
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text
            string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
            return resultValue;
        }

        //Returns Xml element value
        private string GetXmlElementAttributeValue(XmlElement rootElm, string elmName, string attrName)
        {
            string resultValue = UNDEFINED;
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text

            if (nodes.Count > 0)
            {
                XmlNode node = nodes[0];
                XmlAttribute attrCode = node.Attributes[attrName];
                if (attrCode != null)
                {
                    resultValue = attrCode.Value;
                }
            }

            return resultValue;
        }

        private bool WasTransactionSuccessful(string statusCode, string reasonCode, string returnCode)
        {
            string status = string.Format("{0}.{1}", statusCode, reasonCode);

            return ((status == STATUS_CODE_SUCCESS_1 || status == STATUS_CODE_SUCCESS_2) &&
                    (returnCode == RETURN_CODE_SUCCESS_1 || returnCode == RETURN_CODE_SUCCESS_2 ||
                    returnCode == RETURN_CODE_SUCCESS_3 || returnCode == RETURN_CODE_SUCCESS_4));
        }

        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.SOLID; }
            set { }
        }

    }
}
