﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using CashierUtils;

namespace Cashier
{
    class ProviderWireCapital : ProviderBase
    {
        public class Error
        {
            public int Code;
            public string Description;
        }

        [DataContract]
        public class ProtocolVersion
        {
            [DataMember]
            public int majorNumber { get; set; }
            [DataMember]
            public int minorNumber { get; set; }
        }

        [DataContract]
        public class AuthToken
        {
            [DataMember]
            public ProtocolVersion protocolVersion { get; set; }
            [DataMember]
            public string signature { get; set; }
            [DataMember]
            public long creationTime { get; set; }
            [DataMember]
            public int validityTime { get; set; }
            [DataMember]
            public string userAccountId { get; set; }
        }

        [DataContract]
        public class ResponseError
        {
            [DataMember]
            public string code { get; set; }
            [DataMember]
            public string message { get; set; }
            [DataMember]
            public string advice { get; set; }
        }

        [DataContract]
        public class ResponseResult
        {
            [DataMember]
            public string code { get; set; }
            [DataMember]
            public string message { get; set; }
            [DataMember]
            public string errorId { get; set; }
            [DataMember]
            public List<ResponseError> error { get; set; }
        }

        #region Create Purchase
        //Request
        [DataContract]
        public class CreateBillingAddress
        {
            [DataMember]
            public string firstName { get; set; }
            [DataMember]
            public string lastName { get; set; }
            [DataMember]
            public string address1 { get; set; }
            [DataMember]
            public string address2 { get; set; }
            [DataMember]
            public string city { get; set; }
            [DataMember]
            public string zipcode { get; set; }
            [DataMember]
            public string stateCode { get; set; }
            [DataMember]
            public string countryCode { get; set; }
            [DataMember]
            public string mobile { get; set; }
            [DataMember]
            public string phone { get; set; }
            [DataMember]
            public string email { get; set; }
            [DataMember]
            public string fax { get; set; }
        }

        [DataContract]
        public class CreateOrderData
        {
            [DataMember]
            public string orderId { get; set; }
            [DataMember]
            public string orderDescription { get; set; }
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currencyCode { get; set; }
            [DataMember]
            public CreateBillingAddress billingAddress { get; set; }
            [DataMember]
            public object shippingAddress { get; set; }
            [DataMember]
            public object personalAddress { get; set; }
        }

        [DataContract]
        public class CreateTransactionInfo
        {
            [DataMember]
            public string apiVersion { get; set; }
            [DataMember]
            public string requestId { get; set; }
            [DataMember]
            public string txSource { get; set; }
            [DataMember]
            public string recurrentType { get; set; }
            [DataMember]
            public CreateOrderData orderData { get; set; }
            [DataMember]
            public string statementText { get; set; }
            [DataMember]
            public string cancelUrl { get; set; }
            [DataMember]
            public string returnUrl { get; set; }
            [DataMember]
            public string notificationUrl { get; set; }
        }

        [DataContract]
        public class CreateRequest
        {
            [DataMember]
            public string requestTime { get; set; }
            [DataMember]
            public string merchantIdHash { get; set; }
            [DataMember]
            public string merchantAccountIdHash { get; set; }
            [DataMember]
            public string encryptedAccountUsername { get; set; }
            [DataMember]
            public string encryptedAccountPassword { get; set; }
            [DataMember]
            public string signature { get; set; }
            [DataMember]
            public object lang { get; set; }
            [DataMember]
            public CreateTransactionInfo transactionInfo { get; set; }
        }
        //

        //Response     
        [DataContract]
        public class CreateResponseObject
        {
            [DataMember]
            public string responseTime { get; set; }
            [DataMember]
            public ResponseResult result { get; set; }
            [DataMember]
            public string signature { get; set; }
            [DataMember]
            public AuthToken authToken { get; set; }
            [DataMember]
            public string txId { get; set; }
            [DataMember]
            public string txTypeId { get; set; }
            [DataMember]
            public string requestId { get; set; }
            [DataMember]
            public string orderId { get; set; }
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currencyCode { get; set; }
            [DataMember]
            public object returnUrl { get; set; }
            [DataMember]
            public object failUrl { get; set; }
        }
        //
        #endregion

        #region Execute Purchase
        //Request  
        [DataContract]
        public class ExecuteCc
        {
            [DataMember]
            public string cvv { get; set; }
            [DataMember]
            public string expirationMonth { get; set; }
            [DataMember]
            public string expirationYear { get; set; }
            [DataMember]
            public string cardHolderName { get; set; }
            [DataMember]
            public string ccNumber { get; set; }
        }

        [DataContract]
        public class ExecuteObject
        {
            [DataMember]
            public AuthToken authToken { get; set; }
            [DataMember]
            public string txId { get; set; }
            [DataMember]
            public ExecuteCc cc { get; set; }
        }
        //

        //Response   
        [DataContract]
        public class ExecuteResponseObject
        {
            [DataMember]
            public string responseTime { get; set; }
            [DataMember]
            public ResponseResult result { get; set; }
            [DataMember]
            public string signature { get; set; }
            [DataMember]
            public AuthToken authToken { get; set; }
            [DataMember]
            public string txId { get; set; }
            [DataMember]
            public string txTypeId { get; set; }
            [DataMember]
            public string requestId { get; set; }
            [DataMember]
            public string orderId { get; set; }
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currencyCode { get; set; }
            [DataMember]
            public object returnUrl { get; set; }
            [DataMember]
            public object failUrl { get; set; }
            [DataMember]
            public string cardId { get; set; }
            [DataMember]
            public string pgId { get; set; }
        }
        //
        #endregion

        #region Get Status
        //Request   
        [DataContract]
        public class GetStatusRequestObject
        {
            [DataMember]
            public string requestTime { get; set; }
            [DataMember]
            public AuthToken authToken { get; set; }
            [DataMember]
            public string txId { get; set; }
        }
        //

        //Response   
        [DataContract]
        public class GetStatusResponseAmount
        {
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currencyCode { get; set; }
        }

        [DataContract]
        public class GetStatusResponseSourceAmount
        {
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currencyCode { get; set; }
        }

        [DataContract]
        public class GetStatusResponseObject
        {
            [DataMember]
            public string responseTime { get; set; }
            [DataMember]
            public ResponseResult result { get; set; }
            [DataMember]
            public string signature { get; set; }
            [DataMember]
            public string txId { get; set; }
            [DataMember]
            public string txStatusId { get; set; }
            [DataMember]
            public string auth3DUrl { get; set; }
            [DataMember]
            public string cancelUrl { get; set; }
            [DataMember]
            public string returnUrl { get; set; }
            [DataMember]
            public string failUrl { get; set; }
            [DataMember]
            public bool isShowResultMsgScreen { get; set; }
            [DataMember]
            public object detailMessage { get; set; }
            [DataMember]
            public string txType { get; set; }
            [DataMember]
            public string txTypeId { get; set; }
            [DataMember]
            public string recurrentTypeId { get; set; }
            [DataMember]
            public string requestId { get; set; }
            [DataMember]
            public string orderId { get; set; }
            [DataMember]
            public GetStatusResponseAmount amount { get; set; }
            [DataMember]
            public GetStatusResponseSourceAmount sourceAmount { get; set; }
            [DataMember]
            public string ccNumber { get; set; }
            [DataMember]
            public string cardId { get; set; }
        }
        //
        #endregion

        private string UrlExecute;
        private string UrlGetStatus;
        private string MerchantAccountUserName;
        private string MerchantAccountPassword;
        private string MerchantAccountID;
        private int GetStatusTimeoutSeconds;
        private string ReturnUrl;
        private string CancelUrl;
        //private string CallbackUrl;

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.WIRECAPITAL; }
            set { }
        }

        #region Constructor

        public ProviderWireCapital(ProviderConfigBase config)
            : base(config)
        {
            WireCapitalProviderConfig wireCapitalConfig = config as WireCapitalProviderConfig;
            if (wireCapitalConfig == null)
			{
                throw new ArgumentException("wireCapitalConfig expects to get WireCapitalProviderConfig object!");
			}

            UrlExecute = wireCapitalConfig.UrlExecute;
            UrlGetStatus = wireCapitalConfig.UrlGetStatus;
            MerchantAccountUserName = wireCapitalConfig.MerchantAccountUserName;
            MerchantAccountPassword = wireCapitalConfig.MerchantAccountPassword;
            MerchantAccountID = wireCapitalConfig.MerchantAccountID;
            ReturnUrl = wireCapitalConfig.ReturnUrl;
            CancelUrl = wireCapitalConfig.CancelUrl;
            
            if (!int.TryParse(wireCapitalConfig.GetStatusTimeoutSeconds, out GetStatusTimeoutSeconds) || GetStatusTimeoutSeconds <= 0)
                GetStatusTimeoutSeconds = 20 * 2; //default 20 seconds
		}       
        #endregion

        public override DepositResponse Deposit(DepositRequest depositRequest)
        {            
            DepositResponse depositResponse = new DepositResponse();
            //depositResponse.error = new Cashier.Error();

            depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            depositResponse.ErrorCode = "1";
            string transactionID = depositResponse.TransactionID = Guid.NewGuid().ToString().Replace("-", "");            
            depositResponse.Amount = depositRequest.Amount.ToString();
            depositResponse.CurrencyISOCode = depositRequest.CurrencyISOCode;
            depositResponse.provider = depositRequest.provider;
            depositResponse.ClearingUserID = MerchantID;
            depositResponse.TransactionID = transactionID;

            #region Trim            
            depositRequest.Address = depositRequest.Address.Trim();
            depositRequest.CardCVV2 = depositRequest.CardCVV2.Trim();
            depositRequest.CardExpMonth = depositRequest.CardExpMonth.Trim();
            depositRequest.CardExpYear = depositRequest.CardExpYear.Trim();            
            depositRequest.CardNumber = depositRequest.CardNumber.Trim();
            depositRequest.City = depositRequest.City.Trim();
            depositRequest.CountryISOCode = depositRequest.CountryISOCode.Trim();
            if (!string.IsNullOrEmpty(depositRequest.CountryISOCodeThreeLetters))
                depositRequest.CountryISOCodeThreeLetters = depositRequest.CountryISOCodeThreeLetters.Trim();
            depositRequest.CurrencyISOCode = depositRequest.CurrencyISOCode.Trim();
            if (!string.IsNullOrEmpty(depositRequest.Description))
                depositRequest.Description = depositRequest.Description.Trim();
            depositRequest.Email = depositRequest.Email.Trim();
            depositRequest.FirstName = depositRequest.FirstName.Trim();
            depositRequest.LastName = depositRequest.LastName.Trim();
            if (!string.IsNullOrEmpty(depositRequest.NameOnCard))
                depositRequest.NameOnCard = depositRequest.NameOnCard.Trim();
            depositRequest.PhoneNumber = depositRequest.PhoneNumber.Trim();
            if (!string.IsNullOrEmpty(depositRequest.StateISOCode))
                depositRequest.StateISOCode = depositRequest.StateISOCode.Trim();
            depositRequest.ZipCode = depositRequest.ZipCode.Trim();
            #endregion

            #region Purchase Phase
            CreateRequest createRequest = BuildPurchaseRequestObject(depositRequest, transactionID);

            depositResponse.TransactionID = createRequest.transactionInfo.orderData.orderId;

            var json = JSONSerializer<CreateRequest>.Serialize(createRequest).Replace("\\/", "/");

            Log(string.Format("Request [Purchase Phase]: Provider=WireCapital; {0} {1}{2}", Url, json, Environment.NewLine));

            Error e = HttpReq("POST", Url, json, null, "application/json", "", 20000, 20000);
            Log(string.Format("Response [Purchase Phase]: Provider=WireCapital; {0}{1}", e.Description, Environment.NewLine));
            if (e.Code != 0)
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "HTTP request for create PURCHASE failed: " + e.Description;
                Log(string.Format("Response [Purchase Phase]: Provider=WireCapital; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = depositRequest.provider;
                failedDeposit.Email = depositRequest.Email;
                failedDeposit.TransactionID = depositResponse.TransactionID;
                failedDeposit.Amount = depositRequest.Amount;
                failedDeposit.FirstName = depositRequest.FirstName;
                failedDeposit.LastName = depositRequest.LastName;
                failedDeposit.Description = e.Description;

                depositResponse.FailedDeposits.Add(failedDeposit);

                return depositResponse;
            }

            CreateResponseObject createResponseObject = null;
            try
            {
                createResponseObject = JSONSerializer<CreateResponseObject>.DeSerialize(e.Description);
            }
            catch
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "Fail parsing http PURCHASE response: " + e.Description;
                Log(string.Format("Response [Purchase Phase]: Provider=WireCapital; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                return depositResponse;
            }

            //depositResponse.TransactionID = createResponseObject.txId;

            if (createResponseObject.result.code != "3")
            {
                if (createResponseObject.result.error is List<ResponseError> && createResponseObject.result.error.Count > 0)
                {
                    depositResponse.ErrorCode = createResponseObject.result.error[0].code;
                    depositResponse.ErrorDescripotion = createResponseObject.result.error[0].code + " , " + createResponseObject.result.error[0].message;
                }
                else depositResponse.ErrorDescripotion = depositResponse.error.Description = "PURCHASE creation failed";
                Log(string.Format("Response [Purchase Phase]: Provider=WireCapital; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                // fill the class FailedDeposit with details in order to send email to support later
                /*FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = depositRequest.provider;
                failedDeposit.Email = depositRequest.Email;
                failedDeposit.TransactionID = depositResponse.TransactionID;
                failedDeposit.Amount = depositRequest.Amount;
                failedDeposit.FirstName = depositRequest.FirstName;
                failedDeposit.LastName = depositRequest.LastName;
                failedDeposit.Description = e.Description;

                depositResponse.FailedDeposits.Add(failedDeposit);*/

                return depositResponse;
            }
            #endregion


            #region Execute Phase            
            ExecuteObject executeObject = BuildExecuteRequestObject(depositRequest, createResponseObject);
            json = JSONSerializer<ExecuteObject>.Serialize(executeObject).Replace("\\/", "/");

            Log(string.Format("Request [Execute Phase]: Provider=WireCapital; {0} {1}{2}", UrlExecute, json, Environment.NewLine));
            e = HttpReq("POST", UrlExecute, json, null, "application/json", "", 20000, 20000);
            Log(string.Format("Response [Execute Phase]: Provider=WireCapital; {0}{1}", e.Description, Environment.NewLine));
            if (e.Code != 0)
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "HTTP request for EXECUTE purchase failed: " + e.Description;
                Log(string.Format("Response [Execute Phase]: Provider=WireCapital; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = depositRequest.provider;
                failedDeposit.Email = depositRequest.Email;
                failedDeposit.TransactionID = depositResponse.TransactionID;
                failedDeposit.Amount = depositRequest.Amount;
                failedDeposit.FirstName = depositRequest.FirstName;
                failedDeposit.LastName = depositRequest.LastName;
                failedDeposit.Description = e.Description;

                depositResponse.FailedDeposits.Add(failedDeposit);

                return depositResponse;
            }

            ExecuteResponseObject executeResponseObject = null;
            try
            {
                executeResponseObject = JSONSerializer<ExecuteResponseObject>.DeSerialize(e.Description);
            }
            catch
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "Fail parsing http EXECUTE response: " + e.Description;
                Log(string.Format("Response [Execute Phase]: Provider=WireCapital; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                return depositResponse;
            }

            if (executeResponseObject.result.code == "0" || executeResponseObject.result.code == "4" || executeResponseObject.result.code == "5")
            {
                if (executeResponseObject.result.error is List<ResponseError> && executeResponseObject.result.error.Count > 0)
                {
                    depositResponse.ErrorCode = executeResponseObject.result.error[0].code;
                    depositResponse.ErrorDescripotion = executeResponseObject.result.error[0].code + " , " + executeResponseObject.result.error[0].message;
                }
                else depositResponse.ErrorDescripotion = depositResponse.error.Description = "EXECUTE operation failed";
                Log(string.Format("Response [Execute Phase]: Provider=WireCapital; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                return depositResponse;
            }
            #endregion

            
            #region Get Status Phase            
            GetStatusRequestObject getStatusRequestObject = new GetStatusRequestObject();
            getStatusRequestObject.authToken = executeResponseObject.authToken;            
            getStatusRequestObject.txId = executeResponseObject.txId;

            GetStatusResponseObject getStatusResponseObject = null;
            json = JSONSerializer<GetStatusRequestObject>.Serialize(getStatusRequestObject).Replace("\\/", "/");
            int count = 1;            
            bool pendingTimeOut = true;
            while (count <= GetStatusTimeoutSeconds) //Wait 20 seconds for respnse other then "-1 - pending". Other wise consider it as rejected.
            {
                Log(string.Format("Request [Get Status Phase]: Provider=WireCapital; {0} {1}{2}", UrlGetStatus, json, Environment.NewLine));
                e = HttpReq("POST", UrlGetStatus, json, null, "application/json", "", 20000, 20000);
                Log(string.Format("Response [Get Status Phase]: Provider=WireCapital; {0}{1}", e.Description, Environment.NewLine));
                if (e.Code != 0)
                {
                    depositResponse.ErrorDescripotion = depositResponse.error.Description = "HTTP request for purchase GET STATUS failed: " + e.Description;
                    Log(string.Format("Response [Get Status Phase]: Provider=WireCapital; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = depositRequest.provider;
                    failedDeposit.Email = depositRequest.Email;
                    failedDeposit.TransactionID = depositResponse.TransactionID;
                    failedDeposit.Amount = depositRequest.Amount;
                    failedDeposit.FirstName = depositRequest.FirstName;
                    failedDeposit.LastName = depositRequest.LastName;
                    failedDeposit.Description = e.Description;

                    depositResponse.FailedDeposits.Add(failedDeposit);

                    return depositResponse;
                }

                try
                {
                    getStatusResponseObject = JSONSerializer<GetStatusResponseObject>.DeSerialize(e.Description);
                }
                catch(Exception ex)
                {                    
                    Log(string.Format("Response [Get Status Phase]: Provider=WireCapital; Fail parsing http GET STATUS response: {0}  {1}{2}", e.Description, ex.Message, Environment.NewLine));                    
                }
                finally
                {
                    count++;
                }

                try
                {
                    if (getStatusResponseObject is GetStatusResponseObject && getStatusResponseObject.result.code != "-1")
                    {
                        pendingTimeOut = false;
                        break;
                    }
                }
                catch { }

                System.Threading.Thread.Sleep(1000);
            }
            #endregion


            #region Check Final Result            
            if (pendingTimeOut)
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "Pending Timeout - Request is in pending stage too long or expected JSON was invalid";
                Log(string.Format("Response [Get Status Phase]: Provider=WireCapital; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                return depositResponse;
            }
            
            if (getStatusResponseObject.result.code == "-2")
            {
                //3DS
                depositResponse.Is3DSecure = true;
                depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                depositResponse.ErrorCode = "0";

                depositResponse.RedirectUrl = getStatusResponseObject.auth3DUrl;
            }
            else if (getStatusResponseObject.result.code == "1")
            {
                //success
                depositResponse.Receipt = getStatusResponseObject.txId;
                depositResponse.Type = getStatusResponseObject.txType;
                depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                depositResponse.ErrorCode = "0";
            }
            else
            {
                if (getStatusResponseObject.result.error is List<ResponseError> && getStatusResponseObject.result.error.Count > 0)
                {
                    depositResponse.ErrorCode = getStatusResponseObject.result.error[0].code;
                    depositResponse.ErrorDescripotion = getStatusResponseObject.result.error[0].code + " , " + getStatusResponseObject.result.error[0].message;
                }
                else
                {
                    depositResponse.ErrorCode = getStatusResponseObject.result.code;
                    depositResponse.ErrorDescripotion = getStatusResponseObject.result.code + " , " + getStatusResponseObject.result.message;
                }
            }
            #endregion

            depositResponse.error.Description = depositResponse.ErrorDescripotion;
            
            return depositResponse;
        }

        private ExecuteObject BuildExecuteRequestObject(DepositRequest depositRequest, CreateResponseObject createResponseObject)
        {            
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = depositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = (DateTime.Now.Year - (DateTime.Now.Year % 100) + Convert.ToInt32(ExpYear)).ToString();
            //ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(depositRequest.CardExpYear)).ToString();

            ExecuteObject executeObject = new ExecuteObject();
            executeObject.cc = new ExecuteCc();

            executeObject.authToken = createResponseObject.authToken;
            executeObject.txId = createResponseObject.txId;

            executeObject.cc.cardHolderName = depositRequest.FirstName + " " + depositRequest.LastName;
            executeObject.cc.ccNumber = depositRequest.CardNumber;
            executeObject.cc.cvv = depositRequest.CardCVV2;
            executeObject.cc.expirationMonth = Convert.ToInt32(depositRequest.CardExpMonth).ToString("00");
            executeObject.cc.expirationYear = ExpYear;

            return executeObject;
        }

        private CreateRequest BuildPurchaseRequestObject(DepositRequest depositRequest, string TransactionID)
        {
            string merchantIdHash = ToBase64(sha256(MerchantID));
            string merchantAccountIdHash = ToBase64(sha256(MerchantAccountID));
            string encryptedAccountUsername = Encrypt(MerchantAccountUserName, MerchantPassword);
            string encryptedAccountPassword = Encrypt(MerchantAccountPassword, MerchantPassword);

            CreateRequest createRequest = new CreateRequest();
            createRequest.transactionInfo = new CreateTransactionInfo();
            createRequest.transactionInfo.orderData = new CreateOrderData();
            createRequest.transactionInfo.orderData.billingAddress = new CreateBillingAddress();

            createRequest.encryptedAccountPassword = encryptedAccountPassword;
            createRequest.encryptedAccountUsername = encryptedAccountUsername;
            createRequest.lang = "EN";
            createRequest.merchantAccountIdHash = merchantAccountIdHash;
            createRequest.merchantIdHash = merchantIdHash;
            createRequest.requestTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            createRequest.transactionInfo.apiVersion = "1.0.0";
            createRequest.transactionInfo.cancelUrl = (string.IsNullOrEmpty(CancelUrl)) ? ("") : (CancelUrl);
            createRequest.transactionInfo.notificationUrl = (string.IsNullOrEmpty(CallbackUrl)) ? ("") : (CallbackUrl);
            createRequest.transactionInfo.recurrentType = "1";
            createRequest.transactionInfo.requestId = Guid.NewGuid().ToString().Replace("-", "");
            createRequest.transactionInfo.returnUrl = (string.IsNullOrEmpty(ReturnUrl)) ? ("") : (ReturnUrl);
            createRequest.transactionInfo.statementText = "Fund Deposit";
            createRequest.transactionInfo.txSource = "2";

            createRequest.transactionInfo.orderData.amount = depositRequest.Amount.ToString();
            createRequest.transactionInfo.orderData.currencyCode = depositRequest.CurrencyISOCode;
            createRequest.transactionInfo.orderData.orderDescription = "Fund Deposit";
            createRequest.transactionInfo.orderData.orderId = TransactionID;

            createRequest.transactionInfo.orderData.billingAddress.address1 = depositRequest.Address;
            createRequest.transactionInfo.orderData.billingAddress.address2 = "";
            createRequest.transactionInfo.orderData.billingAddress.city = depositRequest.City;
            createRequest.transactionInfo.orderData.billingAddress.countryCode = depositRequest.CountryISOCode;
            createRequest.transactionInfo.orderData.billingAddress.email = depositRequest.Email;
            createRequest.transactionInfo.orderData.billingAddress.firstName = depositRequest.FirstName;
            createRequest.transactionInfo.orderData.billingAddress.lastName = depositRequest.LastName;
            createRequest.transactionInfo.orderData.billingAddress.phone = depositRequest.PhoneNumber.Replace("+", "").Replace("-", "");
            createRequest.transactionInfo.orderData.billingAddress.zipcode = depositRequest.ZipCode;
            createRequest.transactionInfo.orderData.billingAddress.mobile = "";
            createRequest.transactionInfo.orderData.billingAddress.fax = "";
            createRequest.transactionInfo.orderData.billingAddress.stateCode = "";

            createRequest.signature = CreateSignature(createRequest);
            
            return createRequest;

        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        if ((we.Response is System.Net.HttpWebResponse))
                            resp.Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode);
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private byte[] sha256(string text)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            return crypt.ComputeHash(Encoding.UTF8.GetBytes(text), 0, Encoding.UTF8.GetByteCount(text));
        }
       
        private string ToBase64(byte[] input)
        {
            return Convert.ToBase64String(input);
        }

        private string CreateSignature(CreateRequest cr)
        {
            string sig = cr.requestTime + cr.merchantIdHash + cr.merchantAccountIdHash + cr.encryptedAccountUsername + cr.encryptedAccountPassword + cr.transactionInfo.apiVersion;
            sig += cr.transactionInfo.requestId + cr.transactionInfo.txSource + cr.transactionInfo.recurrentType + cr.transactionInfo.orderData.orderId + cr.transactionInfo.orderData.orderDescription;
            sig += cr.transactionInfo.orderData.amount + cr.transactionInfo.orderData.currencyCode + cr.transactionInfo.orderData.billingAddress.firstName + cr.transactionInfo.orderData.billingAddress.lastName;
            sig += cr.transactionInfo.orderData.billingAddress.address1 + cr.transactionInfo.orderData.billingAddress.address2 + cr.transactionInfo.orderData.billingAddress.city;
            sig += cr.transactionInfo.orderData.billingAddress.zipcode + cr.transactionInfo.orderData.billingAddress.stateCode + cr.transactionInfo.orderData.billingAddress.countryCode;
            sig += cr.transactionInfo.orderData.billingAddress.mobile + cr.transactionInfo.orderData.billingAddress.phone + cr.transactionInfo.orderData.billingAddress.email;
            sig += cr.transactionInfo.orderData.billingAddress.fax + cr.transactionInfo.statementText + cr.transactionInfo.cancelUrl + cr.transactionInfo.returnUrl + cr.transactionInfo.notificationUrl;

            sig = sig.Trim();

            var plainBytes = Encoding.UTF8.GetBytes(sig);
            byte[] b = Encrypt(plainBytes, GetRijndaelManaged(MerchantPassword));
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            b = crypt.ComputeHash(b, 0, b.Length);
            return Convert.ToBase64String(b);
        }

        private System.Security.Cryptography.RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new System.Security.Cryptography.RijndaelManaged
            {
                Mode = System.Security.Cryptography.CipherMode.ECB,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        private byte[] Encrypt(byte[] plainBytes, System.Security.Cryptography.RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);            
        }

        private byte[] Decrypt(byte[] encryptedData, System.Security.Cryptography.RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        /// <summary>
        /// Encrypts plaintext using AES 128bit key and a Chain Block Cipher and returns a base64 encoded string
        /// </summary>
        /// <param name="plainText">Plain text to encrypt</param>
        /// <param name="key">Secret key</param>
        /// <returns>Base64 encoded string</returns>
        private String Encrypt(String plainText, String key)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManaged(key)));
        }

        /// <summary>
        /// Decrypts a base64 encoded string using the given key (AES 128bit key and a Chain Block Cipher)
        /// </summary>
        /// <param name="encryptedText">Base64 Encoded String</param>
        /// <param name="key">Secret Key</param>
        /// <returns>Decrypted String</returns>
        private String Decrypt(String encryptedText, String key)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(key)));
        }

        
    }
}
