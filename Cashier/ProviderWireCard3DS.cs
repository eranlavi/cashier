﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderWireCard3DS : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private string businessCaseSignature;
        private string returnURL;
        private const string UNDEFINED = "undefined";
        private string providerName;

        #region Constructor

        public ProviderWireCard3DS(ProviderConfigBase config)
            : base(config)
        {
            MerchantsWorldWideProviderConfig WireCardConfig = config as MerchantsWorldWideProviderConfig;
            if (WireCardConfig == null)
            {
                throw new ArgumentException("WireCardConfig expects to get WireCardProviderConfig object!");
            }

            businessCaseSignature = WireCardConfig.PrivateKey;
            returnURL = WireCardConfig.CallbackUrl;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.WIRECARD3DS; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            //string transactionID = ((long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds).ToString();
            _DepositResponse.provider = _DepositRequest.provider;
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();

            providerName = _DepositRequest.provider.ToString();

            // build the request
            string req = BuildRequest(_DepositRequest, transactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for WireCard3DS";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=WireCard3DS; {0} {1}{2}", Url, req, Environment.NewLine));

            // build authorization Header
            string credHeader = "Basic " + Base64Encode(MerchantID + ":" + MerchantPassword + "\r\n");

            // send POST
            Error e = HttpReq("POST", Url, req, null, "text/xml", "", 20000, 20000, credHeader);

            Log(string.Format("Response: Provider=WireCard3DS; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to WireCard3DS failed. " + e.Description;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                try
                {
                    string result = e.Description;

                    // check if its xml format
                    int startOfXML = result.IndexOf("<?xml");
                    if (startOfXML > -1)
                    {
                        string xmlResponse = result.Substring(startOfXML);

                        // Now load the string into an XmlDocument 
                        XmlDocument responseDocument = new XmlDocument();
                        responseDocument.LoadXml(xmlResponse);

                        ParseResponseXml(responseDocument, _DepositResponse);

                        if (_DepositResponse.error.ErrorCode == Globals.ErrorCodes.NO_ERROR)
                        {
                            _DepositResponse.Is3DSecure = true;
                            ParseResponse3DSXml(responseDocument,_DepositRequest, _DepositResponse);
                        }
                    }
                    else
                    {
                        _DepositResponse.ErrorDescripotion = "WireCard3DS: response is invalid: " + result;
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.ErrorCode = "1";
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    }

                }
                catch (UriFormatException)
                {
                    _DepositResponse.ErrorDescripotion = "WireCard3DS: Couldn't parse URL return";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    return _DepositResponse;
                }
            }

            return _DepositResponse;
        }

        private string BuildRequest(DepositRequest _DepositRequest, string TransactionID)
        {
            string req = string.Empty;

            try
            {
                // 2 digits month
                string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");

                // 4 digits year
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                string ExpYear = _DepositRequest.CardExpYear;
                if (ExpYear.Length == 2)
                    ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

                string address = _DepositRequest.Address;
                string city = _DepositRequest.City;
                string email = _DepositRequest.Email;
                string firstName = _DepositRequest.FirstName.Trim();
                string lastName = _DepositRequest.LastName.Trim();
                string fullName = _DepositRequest.FirstName.Trim() + " " + _DepositRequest.LastName.Trim();
                string phoneNumber = _DepositRequest.PhoneNumber;

                // phone format is: '+xxx(yyy)zzz-zzzz-ppp'
                try
                {
                    string[] arr = phoneNumber.Split('-');
                    if (arr.Length == 1)
                        arr = phoneNumber.Split(' ');
                    phoneNumber = arr[0] + "(" + arr[1] + ")" + arr[2];
                }
                catch (Exception ex)
                {
                    Log("Exception in translating phone number " + phoneNumber + " to format +xxx(yyy)zzz-zzzz-ppp ( it should be 3 sections with - between them ), description: " + ex.Message);
                }
                if ( phoneNumber.IndexOf('+') < 0)
                {
                    phoneNumber = "+" + phoneNumber.Trim();
                }

                req = string.Format(@"<?xml version='1.0' encoding='UTF-8'?>
            <WIRECARD_BXML xmlns:xsi='http://www.w3.org/1999/XMLSchema-instance'>
	            <W_REQUEST>
		            <W_JOB>
			            <JobID>{0}</JobID>
			            <BusinessCaseSignature>{1}</BusinessCaseSignature>
			            <FNC_CC_ENROLLMENT_CHECK>
				            <FunctionID>authentication 1</FunctionID>
				            <CC_TRANSACTION>
					            <TransactionID>{2}</TransactionID>
					            <Amount>{3}</Amount>
					            <Currency>{4}</Currency>
					            <CountryCode>{5}</CountryCode>
					            <Usage>Order - Thank you</Usage>
					            <CREDIT_CARD_DATA>
						            <CreditCardNumber>{6}</CreditCardNumber>
                                    <CVC2>{7}</CVC2>
						            <ExpirationYear>{8}</ExpirationYear>
						            <ExpirationMonth>{9}</ExpirationMonth>
						            <CardHolderName>{10}</CardHolderName>
					            </CREDIT_CARD_DATA>
					            <CONTACT_DATA>
						            <IPAddress>{11}</IPAddress>
						            <BROWSER>
							            <AcceptHeader> application/xml,
            application/xhtml+xml, text/html;q=0.9,
            text/plain;q=0.8, image/png,
            */*;q=0.5</AcceptHeader>
							            <UserAgent>Mozilla/5.0 (iPhone; U; CPU like
            Mac OS X; en) AppleWebKit/420.1 (KHTML, like
            Gecko) Version/3.0 Mobile/3B48b
            Safari/419.3</UserAgent>
							            <DeviceCategory>0</DeviceCategory>
						            </BROWSER>
					            </CONTACT_DATA>
					            <CORPTRUSTCENTER_DATA>
						            <ADDRESS>
							            <FirstName>{12}</FirstName>
							            <LastName>{13}</LastName>
							            <Address1>{14}</Address1>
							            <Address2></Address2>
							            <City>{15}</City>
							            <ZipCode>{16}</ZipCode>
							            <State>{17}</State>
							            <Country>{18}</Country>
							            <Phone>{19}</Phone>
							            <Email>{20}</Email>
						            </ADDRESS>
					            </CORPTRUSTCENTER_DATA>
				            </CC_TRANSACTION>
			            </FNC_CC_ENROLLMENT_CHECK>
		            </W_JOB>
	            </W_REQUEST>
            </WIRECARD_BXML>",
                    "Job1", businessCaseSignature, TransactionID, (_DepositRequest.Amount * 100).ToString(),
                    _DepositRequest.CurrencyISOCode, _DepositRequest.CountryISOCodeThreeLetters,
                    _DepositRequest.CardNumber, _DepositRequest.CardCVV2, ExpYear, ExpMonth, fullName,
                    _DepositRequest.IpAddress, firstName, lastName,
                     address, city, _DepositRequest.ZipCode, _DepositRequest.StateISOCode, _DepositRequest.CountryISOCode,
                    phoneNumber, email);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=WireCard3DS; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                /*ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );*/

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }


            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        #region Response parsing funcs

        //Parses response Xml. Returns DataCashDepositResponseParameters object
        private void ParseResponseXml(XmlDocument docResponse, DepositResponse response)
        {
            // Get the root node 
            XmlElement rootElm = docResponse.DocumentElement;

            string errorCode = string.Empty;
            string result = string.Empty;
            string errorMessage = string.Empty;
            string errorDescription = string.Empty;
            string functionResult = string.Empty;
            string StatusType = string.Empty;

            if (GetXmlElementValue(rootElm, "ERROR") != "undefined")
            {
                errorCode = GetXmlElementValue(rootElm, "Number");
                result = GetXmlElementValue(rootElm, "Type");
                errorMessage = GetXmlElementValue(rootElm, "Message");
                errorDescription = GetXmlElementValue(rootElm, "Advice");
            }
            if (GetXmlElementValue(rootElm, "FunctionResult") != "undefined")
            {
                functionResult = GetXmlElementValue(rootElm, "FunctionResult");
            }

            if (GetXmlElementValue(rootElm, "StatusType") != "undefined")
            {
                StatusType = GetXmlElementValue(rootElm, "StatusType");
            }

            response.error.ErrorCode = StatusType == "Y" ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
            response.ErrorMessage = functionResult;
            response.error.Description = errorMessage == "" ? errorDescription : errorMessage;
            response.ErrorDescripotion = response.error.Description;
        }

        private void ParseResponse3DSXml(XmlDocument docResponse, DepositRequest request, DepositResponse response)
        {
            // Get the root node 
            XmlElement rootElm = docResponse.DocumentElement;

            // get redirect url
            string sACS = GetXmlElementValue(rootElm, "AcsUrl");
            response.RedirectUrl = sACS;

            response.RedirectParameters = new List<RedirectValues>();

            // get TermUrl
            RedirectValues termurl = new RedirectValues();
            termurl.Key = "TermUrl";
            termurl.Value = returnURL;
            response.RedirectParameters.Add(termurl);

            // get PAReq
            if (GetXmlElementValue(rootElm, "PAReq") != "undefined")
            {
                RedirectValues pareq = new RedirectValues();
                pareq.Key = "PaReq";
                pareq.Value = GetXmlElementValue(rootElm, "PAReq");
                response.RedirectParameters.Add(pareq);
            }

            // get MD
            RedirectValues md = new RedirectValues();
            md.Key = "MD";
            md.Value = response.TransactionID;
            response.RedirectParameters.Add(md);

            // get GuWID
            if (GetXmlElementValue(rootElm, "GuWID") != "undefined")
            {
                response.Receipt = GetXmlElementValue(rootElm, "GuWID");
            }

            // save params
            response.DataToSave += response.Receipt + "<SEP>";
            response.DataToSave += request.CardCVV2 + "<SEP>";
            response.DataToSaveKey = response.TransactionID;
        }

        //Returns Xml element value
        private string GetXmlElementValue(XmlElement rootElm, string elmName)
        {
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text
            string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
            return resultValue;
        }

        #endregion

        private static string Base64Encode(string plainText)
        {
            byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(encbuff);
        }

    }
}
