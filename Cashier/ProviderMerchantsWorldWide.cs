﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using CashierUtils;

namespace Cashier
{
    class ProviderMerchantsWorldWide : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private string TerminalId;
        private string PrivateKey;
        private string CallbackUrl;

        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            {"visa", "VI"},
            {"Visa", "VI"},
            {"mastercard", "MC"},
            {"Mastercard", "MC"},
            {"diners club", "DINERS"},
            {"dinersclub", "DINERS"},
            {"dinersclubintl", "DINERS"},
            {"Diners Club", "DINERS"},
            {"american express", "AMEX"},
            {"American Express", "AMEX"},
            {"amex", "AMEX"},
            {"maestro","MAESTRO"},
            {"Maestro","MAESTRO"}
        };

        #region Constructor

        public ProviderMerchantsWorldWide(ProviderConfigBase config)
            : base(config)
        {
            MerchantsWorldWideProviderConfig merchantsWorldWideConfig = config as MerchantsWorldWideProviderConfig;
            if (merchantsWorldWideConfig == null)
            {
                throw new ArgumentException("merchantsWorldWideProvider expects to get MerchantsWorldWideProviderConfig object!");
            }

            TerminalId = merchantsWorldWideConfig.TerminalId;
            PrivateKey = merchantsWorldWideConfig.PrivateKey;
            CallbackUrl = merchantsWorldWideConfig.CallbackUrl;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.MERCHANTSWORLDWIDE; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            _DepositResponse.provider = Globals.Providers.MERCHANTSWORLDWIDE;
            _DepositResponse.ClearingUserID = MerchantID;

            string TransactionID = ((long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds).ToString();
            _DepositResponse.TransactionID = TransactionID;

            string signature_key = (PrivateKey + MerchantPassword + TransactionID).Trim();
            string ApiPassword_encrypt = sha256(MerchantPassword);

            string req = BuildRequestXML(_DepositRequest, TransactionID, ApiPassword_encrypt);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ErrorDescripotion = "Fail building request XML";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            }
            else
            {
                Log(string.Format("Request [plaing xml]: Provider=MerchantsWorldWide; {0}{1}", req, Environment.NewLine));

                string signature = base64_hash_hmac(req, signature_key);
                string encodedMessage = Convert.ToBase64String(Encoding.UTF8.GetBytes(req));

                string parameters = "version=1.0&encodedMessage=" + System.Web.HttpUtility.UrlEncode(encodedMessage) + "&signature=" + System.Web.HttpUtility.UrlEncode(signature);

                Log(string.Format("Request [encoded data]: Provider=MerchantsWorldWide; {0}{1}", parameters, Environment.NewLine));

                Error e = HttpReq("POST", Url, parameters, null, "application/x-www-form-urlencoded", "*/*", 60000, 60000, "", _DepositRequest.UserAgent);

                Log(string.Format("Response: Provider=MerchantsWorldWide; {0}{1}", e.Description, Environment.NewLine));

                if (e.Code != 0)
                {
                    _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                    _DepositResponse.ErrorDescripotion = "Http request to MerchantsWorldWide failed. " + e.Description;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = Globals.Providers.MERCHANTSWORLDWIDE;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = _DepositResponse.TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = e.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);
                }
                else
                {
                    bool IsLiveMode = Configuration.Mode.LIVE == Mode ? true : false;
                    if (IsLiveMode)
                    {
                        ParseResponse(e.Description, _DepositResponse);
                    }
                    else
                    {
                        if (e.Description.Contains("<html>"))
                        {
                            Log("Provider=MerchantsWorldWide Test mode - Pending");
                            _DepositResponse.IsPending = true;
                            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;

                            try
                            {
                                File.Delete(@"c:\temp\test.html");
                            }
                            catch { }

                            try
                            {
                                System.IO.File.AppendAllText(@"c:\temp\test.html", e.Description);
                            }
                            catch { }

                        }
                        else
                            ParseResponse(e.Description, _DepositResponse);
                    }
                }

            }

            return _DepositResponse;
        }

        private XmlDocument StripNamespace(XmlDocument doc)
        {
            if (doc.DocumentElement.NamespaceURI.Length > 0)
            {
                doc.DocumentElement.SetAttribute("xmlns", "");
                // must serialize and reload for this to take effect
                XmlDocument newDoc = new XmlDocument();
                newDoc.LoadXml(doc.OuterXml);
                return newDoc;
            }
            else
            {
                return doc;
            }
        }

        private void ParseResponse(string XML, DepositResponse _DepositResponse)
        {
            XmlDocument xDoc = null;

            try
            {
                System.Collections.Specialized.NameValueCollection qscoll = System.Web.HttpUtility.ParseQueryString(XML);
                byte[] b = Convert.FromBase64String(qscoll["encodedMessage"]);
                XML = Encoding.UTF8.GetString(b);

                Log(string.Format("Response [decoded XML]: Provider=MerchantsWorldWide; {0}{1}", XML, Environment.NewLine));

                xDoc = new XmlDocument();
                xDoc.LoadXml(XML);
                xDoc = StripNamespace(xDoc);
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on parsing response (1): Provider=MerchantsWorldWide; {0}{1}", ex.Message, Environment.NewLine));
                _DepositResponse.ErrorDescripotion = ex.Message;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.ErrorCode = "1";
                return;
            }

            try
            {
                string Code = xDoc.SelectSingleNode("//TransactionResponse/Code").InnerText;
                int iCode = Convert.ToInt32(Code);
                if (iCode != 0)
                {
                    _DepositResponse.ErrorDescripotion = xDoc.SelectSingleNode("//TransactionResponse/Description").InnerText;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.ErrorCode = Code;
                }
                else
                {
                    _DepositResponse.IsPending = true;

                    _DepositResponse.ErrorDescripotion = "";
                    _DepositResponse.error.Description = "";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                    _DepositResponse.ErrorCode = "0";
                }
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on parsing response (2): Provider=MerchantsWorldWide; {0}{1}", ex.Message, Environment.NewLine));
                _DepositResponse.ErrorDescripotion = ex.Message;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.ErrorCode = "1";
            }
        }

        private string BuildRequestXML(DepositRequest _DepositRequest, string TransactionID, string ApiPassword_encrypt)
        {
            string req = string.Empty;

            try
            {
                //bool IsTestMode = (int)Configuration.Mode.LIVE == 1 ? true : false;

                string signature_key = (PrivateKey + MerchantPassword + TransactionID).Trim();

                string Amount = (Convert.ToDouble(_DepositRequest.Amount) * 100.0).ToString();

                string ExpYear = _DepositRequest.CardExpYear;
                if (ExpYear.Length > 2)
                    ExpYear = _DepositRequest.CardExpYear.Substring(2);

                req = string.Format(@"<?xml version='1.0' encoding='UTF-8'?>
<TransactionRequest>
   <Language>ENG</Language>
   <Credentials>
      <MerchantId>{0}</MerchantId>
      <TerminalId>{1}</TerminalId>
      <TerminalPassword>{2}</TerminalPassword>
   </Credentials>
   <TransactionType>LP001</TransactionType>
   <TransactionId>{3}</TransactionId>
   <ReturnUrl page='{4}'>
      <Param>
         <Key>data</Key>
         <Value>{22}</Value>         
      </Param>
   </ReturnUrl>
   <CurrencyCode>{5}</CurrencyCode>
   <TotalAmount>{6}</TotalAmount>
   <ProductDescription>Fund Deposit</ProductDescription>
   <CustomerDetails>
      <FirstName>{7}</FirstName>
      <LastName>{8}</LastName>
      <CustomerIP>{9}</CustomerIP>
      <Phone>{10}</Phone>
      <Email>{11}</Email>
      <Street>{12}</Street>
      <City>{13}</City>
      <Region>Region</Region>
      <Country>{14}</Country>
      <Zip>{15}</Zip>
   </CustomerDetails>
   <CardDetails>
      <CardHolderName>{16}</CardHolderName>
      <CardNumber>{17}</CardNumber>
      <CardExpireMonth>{18}</CardExpireMonth>
      <CardExpireYear>{19}</CardExpireYear>
      <CardType>{20}</CardType>
      <CardSecurityCode>{21}</CardSecurityCode>
   </CardDetails>
</TransactionRequest>", MerchantID, TerminalId, ApiPassword_encrypt, TransactionID, CallbackUrl, _DepositRequest.CurrencyISOCode, Amount, _DepositRequest.FirstName, _DepositRequest.LastName,
                          _DepositRequest.IpAddress, _DepositRequest.PhoneNumber, _DepositRequest.Email, _DepositRequest.Address, _DepositRequest.City, _DepositRequest.CountryISOCode,
                          _DepositRequest.ZipCode, _DepositRequest.FirstName + " " + _DepositRequest.LastName, _DepositRequest.CardNumber, _DepositRequest.CardExpMonth, ExpYear,
                          GetTrustPayCardBrandByCardType(_DepositRequest.CardType), _DepositRequest.CardCVV2, _DepositRequest.CallbackData);
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on building request XML string (2): Provider=MerchantsWorldWide; {0}{1}", ex.Message, Environment.NewLine));
                req = string.Empty;
            }
            return req;
        }

        string sha256(string password)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (byte bit in crypto)
            {
                hash.Append(bit.ToString("x2"));
            }
            return hash.ToString();
        }

        string base64_hash_hmac(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new System.Security.Cryptography.HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "", string UserAgent = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.UserAgent = UserAgent; //"Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0";
                //req.Referer = "https://my.g4trader.com";

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }

                //resp.Code = (int)webResponse.StatusCode;             
            }
            catch (WebException we)
            {
                if ((we.Response is System.Net.HttpWebResponse))
                    resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                else
                    resp.Code = (int)we.Status;
                resp.Description = resp.Code.ToString() + ", " + we.Message;
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private string GetTrustPayCardBrandByCardType(string cardType)
        {
            string cardBrand = string.Empty;
            cardTypeMapping.TryGetValue(cardType, out cardBrand);
            return cardBrand;
        }



    }
}
