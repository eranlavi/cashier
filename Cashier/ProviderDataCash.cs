﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Net;
using System.IO;
using Cashier.Configuration;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    internal class ProviderDataCash : ProviderBase
    {
        #region Constants

        private const string VERSION = "1.0";
        private const string ENCODING = "utf-8";
        private const string POST = "POST";
        private const string CONTENT_TYPE = "application/x-www-form-urlencoded";
        private const string REQUEST = "Request";
        private const string AUTHENTICATION = "Authentication";
        private const string CLIENT = "client";
        private const string PASSWORD = "password";
        private const string CARD_TXN = "CardTxn";
        private const string CARD = "Card";
        private const string METHOD = "method";
        private const string PAN = "pan";
        private const string EXPIRY_DATE = "expirydate";
        private const string STARTDATE = "startdate";
        private const string ISSUENUMBER = "issuenumber";
        private const string TRANSACTION = "Transaction";
        private const string TXN_DETAILS = "TxnDetails";
        private const string MERCHANT_REFERENCE = "merchantreference";
        private const string AMOUNT = "amount";
        private const string CURRENCY = "currency";
        private const string DATACASH_REFERENCE = "datacash_reference";
        private const string AUTH_CODE = "authcode";
        private const string CARD_SCHEME = "card_scheme";
        private const string COUNTRY = "country";
        private const string ISSUER = "issuer";
        private const string MODE = "mode";
        private const string REASON = "reason";
        private const string STATUS = "status";
        private const string TIME = "time";
        private const string UNDEFINED = "Undefined";
        private const string SUCCESS_STATUS = "1";
		        
        #endregion
		        
        public ProviderDataCash(ProviderConfigBase config): base(config)	
        {
            
		}
		        
        #region Public methods

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            try
            {
                XmlDocument docRequest = CreateRequestXml(_DepositRequest);


				Log(string.Format("Request: Provider={0}; UserName={1}; Currency={2}; Amount={3}; MerchantID={4}; Password={5}; Url={6}",
					   ProviderType.ToString(), _DepositRequest.Email, _DepositRequest.CurrencyISOCode,
					   _DepositRequest.Amount, MerchantID, MerchantPassword, Url));
				                				
                XmlDocument docResponse = SendRequest(docRequest, Url, Int32.Parse(_DepositRequest.Timeout));
                if (docResponse != null)
                {

					Log(string.Format("Response: {0}", docRequest.OuterXml));
					
                    _DepositResponse = ParseResponseXml(docResponse);
                    
                    _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                    _DepositResponse.CurrencyISOCode = _DepositRequest.CurrencyISOCode;
                }
            }
            catch (Exception ex)
            {
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.error.Description = ex.Message;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = ProviderType;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = _DepositResponse.TransactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = _DepositResponse.error.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }

            _DepositResponse.provider = ProviderType;
     
			return _DepositResponse; 
        }
        
        #endregion
        
        #region Private methods

        #region Request building funcs

        //Build request XML
        private XmlDocument CreateRequestXml(DepositRequest requestParams)
        {
            // Create the document, and add an XML declaration 
            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration(VERSION, ENCODING, null);
            doc.AppendChild(dec);

            // REQUEST Define the root (top-level) element 
            XmlElement root = doc.CreateElement(REQUEST);
            doc.AppendChild(root);

            //AUTHENTICATION
            XmlElement authenticationElm = BuildAuthenticationSection(doc, MerchantID, MerchantPassword);
            root.AppendChild(authenticationElm);

            // TRANSACTION
            XmlElement transactionElm =
                BuildTransactionSection(doc, CashierUtils.CalcTransactionID(requestParams.CustomerID), requestParams.CurrencyISOCode, requestParams.Amount.ToString(),
                requestParams.Method, requestParams.CardNumber, string.Format("{0}/{1}", requestParams.CardExpMonth, requestParams.CardExpYear.Substring(2, 2)), requestParams.CardStartDate, requestParams.CardIssueNumber);

            root.AppendChild(transactionElm);

            return doc;
        }

        //TRANSACTION
        private XmlElement BuildTransactionSection(XmlDocument doc, string merchantReference, string currencyCode, string amount, string method,
            string cardPan, string cardExpiryDate, string cardStartDate, string cardIssueNumber)
        {
            XmlElement transactionElm = doc.CreateElement(TRANSACTION);

            //CARD_TXN
            XmlElement cardTxnElm = BuildCardTxnSection(doc, method, cardPan, cardExpiryDate, cardStartDate, cardIssueNumber);
            transactionElm.AppendChild(cardTxnElm);

            //TXNDETAILS
            XmlElement txnDetailsElm = BuildTxnDetailsSection(doc, merchantReference, currencyCode, amount);
            transactionElm.AppendChild(txnDetailsElm);

            return transactionElm;
        }

        //AUTHENTICATION
        private XmlElement BuildAuthenticationSection(XmlDocument doc, string client, string password)
        {
            XmlElement authenticationElm = doc.CreateElement(AUTHENTICATION);

            //CLIENT
            XmlElement clientElm = doc.CreateElement(CLIENT);
            XmlText clientTxt = doc.CreateTextNode(client);
            clientElm.AppendChild(clientTxt);
            authenticationElm.AppendChild(clientElm);

            //PASSWORD
            XmlElement passwordElm = doc.CreateElement(PASSWORD);
            XmlText passwordTxt = doc.CreateTextNode(password);
            passwordElm.AppendChild(passwordTxt);
            authenticationElm.AppendChild(passwordElm);

            return authenticationElm;
        }

        //CARD_TXN
        private XmlElement BuildCardTxnSection(XmlDocument doc, string method, string cardPan, string cardExpiryDate,
            string cardStartDate, string cardIssueNumber)
        {
            //CARD_TXN
            XmlElement cardTxnElm = doc.CreateElement(CARD_TXN);

            //CARD
            XmlElement cardElm = BuildCardSection(doc, cardPan, cardExpiryDate, cardStartDate, cardIssueNumber);
            cardTxnElm.AppendChild(cardElm);

            //METHOD
            XmlElement methodElm = doc.CreateElement(METHOD);
            XmlText methodTxt = doc.CreateTextNode(method);
            methodElm.AppendChild(methodTxt);
            cardTxnElm.AppendChild(methodElm);

            return cardTxnElm;
        }

        //CARD
        private XmlElement BuildCardSection(XmlDocument doc, string cardPan, string cardExpiryDate, string cardStatrDate, string cardIssueNumber)
        {
            //CARD
            XmlElement cardElm = doc.CreateElement(CARD);

            //PAN
            XmlElement panElm = doc.CreateElement(PAN);
            XmlText panTxt = doc.CreateTextNode(cardPan);
            panElm.AppendChild(panTxt);
            cardElm.AppendChild(panElm);

            //EXPIRYDATE
            XmlElement expiryDateElm = doc.CreateElement(EXPIRY_DATE);
            XmlText expiryDateTxt = doc.CreateTextNode(cardExpiryDate);
            expiryDateElm.AppendChild(expiryDateTxt);
            cardElm.AppendChild(expiryDateElm);

            //STARTDATE
            XmlElement startDateElm = doc.CreateElement(STARTDATE);
            XmlText startDateTxt = doc.CreateTextNode(cardStatrDate);
            startDateElm.AppendChild(startDateTxt);
            cardElm.AppendChild(startDateElm);

            //ISSUENUMBER
            XmlElement issueNumberElm = doc.CreateElement(ISSUENUMBER);
            XmlText issueNumberTxt = doc.CreateTextNode(cardIssueNumber);
            issueNumberElm.AppendChild(issueNumberTxt);
            cardElm.AppendChild(issueNumberElm);

            return cardElm;
        }

        //TXN_DETAILS
        private XmlElement BuildTxnDetailsSection(XmlDocument doc, string merchantReference, string currencyCode, string amount)
        {
            XmlElement txnDetailsElm = doc.CreateElement(TXN_DETAILS);

            // MERCHANT_REFERENCE 
            XmlElement merchantRefElm = doc.CreateElement(MERCHANT_REFERENCE);
            XmlText mref = doc.CreateTextNode(merchantReference);
            merchantRefElm.AppendChild(mref);
            txnDetailsElm.AppendChild(merchantRefElm);

            // AMOUNT + CURRENCY ATTRIBUTE 
            XmlElement amountElm = doc.CreateElement(AMOUNT);
            amountElm.SetAttribute(CURRENCY, currencyCode);
            XmlText amt = doc.CreateTextNode(amount);
            amountElm.AppendChild(amt);
            txnDetailsElm.AppendChild(amountElm);

            return txnDetailsElm;
        }

        #endregion

        #region Response parsing funcs

        //Parses response Xml. Returns DataCashDepositResponseParameters object
        private DepositResponse ParseResponseXml(XmlDocument docResponse)
        {
            // Get the root node 
            XmlElement rootElm = docResponse.DocumentElement;

            DepositResponse responseParams = new DepositResponse()
            {
                AuthCode = GetXmlElementValue(rootElm, AUTH_CODE),
                CardType = GetXmlElementValue(rootElm, CARD_SCHEME),
                Country = GetXmlElementValue(rootElm, COUNTRY),
                Issuer = GetXmlElementValue(rootElm, ISSUER),
                Receipt = GetXmlElementValue(rootElm, DATACASH_REFERENCE),
                TransactionID = GetXmlElementValue(rootElm, MERCHANT_REFERENCE),
                Mode = GetXmlElementValue(rootElm, MODE),
                Reason = GetXmlElementValue(rootElm, REASON),
                Status = GetXmlElementValue(rootElm, STATUS),
                Time = GetXmlElementValue(rootElm, TIME),
                ClearingUserID = MerchantID
            };

            responseParams.error.ErrorCode  = responseParams.Status == SUCCESS_STATUS ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
            responseParams.error.Description = responseParams.Reason;

            return responseParams;
        }

        //Returns Xml element value
        private string GetXmlElementValue(XmlElement rootElm, string elmName)
        {
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text
            string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
            return resultValue;
        }

        #endregion

        //Send request to DataCash
        private XmlDocument SendRequest(XmlDocument request, string serviceUrl, int requestTimeout)
        {
            HttpWebRequest httpWebRequest = null;
            try
            {
                //Prepare web request
                httpWebRequest = (HttpWebRequest)WebRequest.Create(serviceUrl);
                httpWebRequest.Method = POST;
                httpWebRequest.ContentType = CONTENT_TYPE;
                httpWebRequest.KeepAlive = true;

                // int timeoutSeconds is your desired timeout value in seconds 
                httpWebRequest.Timeout = requestTimeout * 1000;

                // Content length in bytes 
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] xmlBytes = encoding.GetBytes(request.OuterXml);
                httpWebRequest.ContentLength = xmlBytes.Length;

                //Send request
                Stream str = null;
                try
                {
                    str = httpWebRequest.GetRequestStream();
                    str.Write(xmlBytes, 0, xmlBytes.Length);
                    str.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("DataCash: An error has occured during sending request: " + ex.Message);
                }

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string result = string.Empty;
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    try
                    {
                        result = streamReader.ReadToEnd();
                        streamReader.Close();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("DataCash: An error has occured during reading response: " + e.Message);
                    }
                }

                // We get the HTTP Headers to start with, so 
                // knock them off before creating the XML Document 
                int startOfXML = result.IndexOf("<?xml");

                // Make sure response is valid
                if (startOfXML < 0)
                {
                    throw new Exception("DataCash: XML response is invalid: " + result);
                }

                string xmlResponse = result.Substring(startOfXML);

                // Now load the string into an XmlDocument 
                XmlDocument responseDocument = new XmlDocument();
                responseDocument.LoadXml(xmlResponse);

                return responseDocument;
            }
            catch (UriFormatException e)
            {
                throw new Exception("DataCash: Couldn't parse URL return");
            }
        }

        #endregion

		protected override Globals.Providers ProviderType
		{
			get { return Globals.Providers.DATACASH; }
            set { }
        }
	}
}
