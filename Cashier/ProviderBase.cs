﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using Cashier.Configuration;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
	internal abstract class ProviderBase : ICashierProvider
	{
		internal ProviderBase(ProviderConfigBase config)
		{
			if (config == null)
			{
				throw new ArgumentNullException("Parameter 'config' cannot be null. It must be initialized!");
			}

			Url = config.Url;
			MerchantID = config.MerchantId;
			MerchantPassword = config.MerchantPassword;
			LogFile = config.PathToLogFile;
			Mode = config.Mode;
            Url2 = config.Url2;
            CallbackUrl = config.CallbackUrl;
            
		}

		public string Url { get; set; }
        public string Url2 { get; set; }
		public string MerchantID { get; set; }
		public string MerchantPassword { get; set; }
		public string LogFile { get; set; }
		public Mode Mode { get; set; }
        public string CallbackUrl { get; set; }

		protected abstract Globals.Providers ProviderType { get; set; }

		public abstract DepositResponse Deposit(DepositRequest _DepositRequest);
		
		/// <summary>
		/// Checks required field id not null or empty AND is not longer than allowed lenght
		/// </summary>
		/// <param name="value">Request value</param>
		/// <param name="fieldName">Field name</param>
		/// <param name="maxLenght">Maximal allowed field lenght</param>
		protected void ValidateRequiredField(string value, string fieldName, int maxLenght)
		{
			if (String.IsNullOrWhiteSpace(value))
			{
				throw new InvalidDataException(String.Format("Request validation Error. Field '{0}' is required", fieldName));
			}

			if (value.Length > maxLenght)
			{
				throw new InvalidDataException(String.Format("Request validation Error. Field '{0}' is longer than allowed lenght={1}", fieldName, maxLenght));
			}
		}

		/// <summary>
		/// Checks not required field is not longer than allowed lenght
		/// Does not check value if it is empty
		/// </summary>
		/// <param name="value">Request value</param>
		/// <param name="fieldName">Field name</param>
		/// <param name="maxLenght">Maximal allowed field lenght</param>
		protected void ValidateNotRequiredField(string value, string fieldName, int maxLenght)
		{
			if (!String.IsNullOrWhiteSpace(value) && value.Length > maxLenght)
			{
				throw new InvalidDataException(String.Format("Request validation Error. Field '{0}' is longer than allowed lenght={1}", fieldName, maxLenght));
			}
		}

		/// <summary>
		/// Builds request Url using name/value collection
		/// </summary>
		/// <param name="url">Server url</param>
		/// <param name="data">Name-value collection</param>
		/// <returns>Full request url</returns>
		protected string BuildRequestUrl(string url, NameValueCollection data)
		{
			var result = new StringBuilder(url.TrimEnd('?'));
			result.Append('?');

			foreach (string key in data.AllKeys)
			{
				string value = data[key];
				result.AppendFormat("{0}={1}&", key, HttpUtility.UrlEncode(value));
			}

			return result.ToString().TrimEnd('&');
		}

		protected string TryToGetXmlValue(XDocument doc, string elementName)
		{
			if (doc.Root != null)
			{
				var xElement = doc.Root.Element(elementName);
				if (xElement != null)
				{
					return xElement.Value;
				}
			}

			return null;
		}

		/// <summary>
		/// Log message to log file in format [Cashier {Provider Name} provider: {Date and Time}]   {Log Message}
		/// </summary>
		/// <param name="logMessage">Message to log</param>
		protected void Log(string logMessage)
		{
			CheckLogFileExists();

            try
            {
                File.AppendAllText(LogFile, String.Format("[Cashier '{0}' provider: {1:yyyy-MM-dd hh:mm:ss}]   {2}{3}", CashierUtils.GetName(ProviderType), DateTime.Now, logMessage, Environment.NewLine));
            }
            catch { }
		}

		protected void Log(NameValueCollection data)
		{
			CheckLogFileExists();

            try
            {
                using (var stream = new StreamWriter(LogFile, true))
                {
                    stream.WriteLine("");
                    stream.WriteLine(string.Format("   {0} REQUEST DATA:", ProviderType.ToString()));

                    var keys = data.AllKeys;
                    foreach (string key in keys)
                    {
                        stream.WriteLine("    {0}  =  '{1}'", key, data[key]);
                    }

                    stream.Flush();
                }
            }
            catch { }
		}

        protected void Log(NameValueCollection data, string ProviderName)
        {
            CheckLogFileExists();

            try
            {
                using (var stream = new StreamWriter(LogFile, true))
                {
                    stream.WriteLine("");
                    stream.WriteLine(string.Format("   {0} REQUEST DATA:", ProviderName));

                    var keys = data.AllKeys;
                    foreach (string key in keys)
                    {
                        stream.WriteLine("    {0}  =  '{1}'", key, data[key]);
                    }

                    stream.Flush();
                }
            }
            catch { }
        }

		private void CheckLogFileExists()
		{
			if (!File.Exists(LogFile))
			{
				FileInfo file = new FileInfo(LogFile);
				string directoryName = file.DirectoryName;

				if (!Directory.Exists(directoryName))
				{
					Directory.CreateDirectory(directoryName);
				}

				File.Create(LogFile);
			}
		}
	}
}
