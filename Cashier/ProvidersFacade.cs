﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using Cashier.Configuration;
using Cashier.Configuration.ProviderConfig;
using System.Web;
using CashierUtils;

namespace Cashier
{
    public class ProvidersFacade
    {
        #region Constants

        private const string PROVIDER_NAME = "fProviderName";

        private const string SQL_QUERY_WITH_CARD_TYPE_FORMAT = "SELECT cv.fPriority, p.fProviderName " +
                                                "FROM tCountries c, tCurrencies cur, tProviders p, tCardTypes ct, tCashierView cv " +
                                                "WHERE (c.fCountryCode = '{0}' AND c.fID = cv.fCountryID) AND (cur.fCurrencyCode = '{1}' AND cur.fID = cv.fCurrencyID) AND " +
                                                "(ct.fTypeCode = '{2}' AND ct.fID = cv.fCardTypeID) AND p.fMaxDepositAmount >= {3} AND p.fMinDepositAmount <= {4} AND cv.fIsActive = 1 AND cv.fIsBlocked = 0 " +
                                                "AND cv.fProviderID = p.fID ORDER BY cv.fPriority";

        private const string SQL_QUERY_WITHOUT_CARD_TYPE_FORMAT = "SELECT DISTINCT cv.fPriority, p.fProviderName " +
                                                "FROM tCountries c, tCurrencies cur, tProviders p, tCardTypes ct, tCashierView cv " +
                                                "WHERE (c.fCountryCode = '{0}' AND c.fID = cv.fCountryID) AND (cur.fCurrencyCode = '{1}' AND cur.fID = cv.fCurrencyID) AND " +
                                                "ct.fID = cv.fCardTypeID AND p.fMaxDepositAmount >= {2} AND p.fMinDepositAmount <= {3} AND cv.fIsActive = 1 AND cv.fIsBlocked = 0 AND cv.fProviderID = p.fID ORDER BY cv.fPriority";


        #endregion

        #region Private members

        CashierService.IService service;
        private SQLiteDatabase db;
        private CashierConfigurator configurator;

        #endregion

        #region Constructor


        /// <summary>
        /// REQUIREMENT: ProviderFacede should not expect any path parametrs. 
        /// CONVENTION: Cashier.config will be read from web application root dirctory
        /// </summary>
        public ProvidersFacade()
        {
            string pathToCashierConfigFile = HttpContext.Current.Server.MapPath("~/Cashier.config");

            configurator = new CashierConfigurator(pathToCashierConfigFile, true);

            try
            {
                db = new SQLiteDatabase(configurator.PathToCashierDB);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() + ": Cashier.ProviderFacade: ERROR creating sqlLite DB:  " + ex.Message + "\r\n");
                return;
            }
        }

        public ProvidersFacade(string pathToCashierConfigFile)
        {
            bool IsLocalFile = true;

            HandleConfiguration(pathToCashierConfigFile, IsLocalFile);
        }

        public ProvidersFacade(string pathToCashierConfigFile, bool IsLocalFile = true)
        {
            HandleConfiguration(pathToCashierConfigFile, IsLocalFile);
        }

        private void HandleConfiguration(string pathToCashierConfigFile, bool IsLocalFile)
        {
            configurator = new CashierConfigurator(pathToCashierConfigFile, IsLocalFile);

            try
            {
                db = new SQLiteDatabase(configurator.PathToCashierDB);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() + ": Cashier.ProviderFacade: ERROR creating sqlLite DB:  " + ex.Message + "\r\n");
                return;
            }
        }

        #endregion

        #region Public methods
        
        public DepositResponse Deposit(DepositRequest _DepositRequest, CashierService.IService service)
        {
            this.service = service;
            this.service.Message += Service_Message;
            DepositResponse dr = Deposit(_DepositRequest);
            this.service.Message -= Service_Message;
            return dr;
        }

        private void Service_Message(string obj)
        {            
            System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Service Message: {1}{2}",
                DateTime.Now.ToString(), obj, Environment.NewLine));
        }

        public DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = null;
           
            System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Try to read all active, not blocked, prioritized providers from CashierDB for CountryCode, CurrencyCode, CardType{1}",
                 DateTime.Now.ToString(), Environment.NewLine));
            

            List<CashierProvider> providers = null;
            if (service == null)
            {                
                string sqlQuery = !string.IsNullOrWhiteSpace(_DepositRequest.CardType) ?
                    string.Format(SQL_QUERY_WITH_CARD_TYPE_FORMAT, _DepositRequest.CountryISOCode, _DepositRequest.CurrencyISOCode, _DepositRequest.CardType.ToLower(), _DepositRequest.Amount.ToString(), _DepositRequest.Amount.ToString()) :
                    string.Format(SQL_QUERY_WITHOUT_CARD_TYPE_FORMAT, _DepositRequest.CountryISOCode, _DepositRequest.CurrencyISOCode, _DepositRequest.Amount.ToString(), _DepositRequest.Amount.ToString());

                System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] GetPrioritizedProviders SQL Query: {1}{2}",
                     DateTime.Now.ToString(), sqlQuery, Environment.NewLine));

                providers = GetPrioritizedProviders(db, sqlQuery);
            }
            else
            {                
                providers = GetPrioritizedProviders(_DepositRequest, service);
            }

            providers = providers.OrderBy(l => l.Level).ToList();
            System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] {1} providers were initialized and are available{2}",
                 DateTime.Now.ToString(), providers.Count, Environment.NewLine));

            System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] LastClearedBy from DepositRequest object = '{1}'{2}",
                DateTime.Now.ToString(), _DepositRequest.LastClearingCompany, Environment.NewLine));

            List<CashierProvider> tempProviders = new List<CashierProvider>();
            try
            {                
                CashierProvider cp = providers.Find(x => x.Level > 0);
                if (cp is CashierProvider)
                {                    
                    foreach(CashierProvider c in providers)
                        c.Percentage = Round(c.Percentage);

                    var levels = providers.Select(o => o.Level).Distinct().ToList();

                    //int index = 0;

                    for (int i = 0; i < levels.Count; i++)
                    {
                        List<CashierProvider> prov = providers.FindAll(element => element.Level == levels[i]);
                        prov.Sort((p, q) => p.Percentage.CompareTo(q.Percentage));
                        int total = prov.Sum(x => x.Percentage) / 10;
                        CashierProvider[] CashierProviders = new CashierProvider[total];

                        List<string> pattern = new List<string>();
                        for(int j=0, prct=10; j<total;prct +=10)
                        {                                    
                            for(int k=0;k< prov.Count;k++)
                            {
                                if (prct <= prov[k].Percentage)
                                {
                                    CashierProviders[j++] = prov[k];
                                    pattern.Add(prov[k].provider.ToString());
                                }
                            }                                                                                                      
                        }

                        string pat = string.Join(",", pattern);

                        int index = 0;
                        if (service == null)
                            index = GetRoundRobinIndex(db, levels[i], pat);
                        else
                            index = GetRoundRobinIndex(service, levels[i], pat, _DepositRequest.BrandID);

                        tempProviders.Add(CashierProviders[index]);

                        if (++index >= total)
                            index = 0;
                        if (service == null)
                            UpdateRoundRobinIndex(db, levels[i], pat, index);
                        else
                            service.UpdateRoundRobinIndex(_DepositRequest.BrandID, levels[i], pat, index);
                    }
                }
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Exception while arranging clearing order: {1}{2}", DateTime.Now.ToString(), ex.Message, Environment.NewLine));
            }

            providers = tempProviders;

            _DepositRequest.CardNumber = _DepositRequest.CardNumber.Trim();
            if (!string.IsNullOrWhiteSpace(_DepositRequest.LastClearingCompany))
            {
                CashierProvider lastProvider = providers.Find(x => x.provider.ToString() == _DepositRequest.LastClearingCompany.ToUpper());
                if (lastProvider != null)
                {
                    System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Last cleared provider '{1}' was found{2}",
                        DateTime.Now.ToString(), lastProvider.provider.ToString(), Environment.NewLine));

                    providers.Remove(lastProvider);

                    _DepositResponse = lastProvider.Deposit(_DepositRequest);
                }
                else
                {
                    System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Last cleared provider was not found among available{1}",
                        DateTime.Now.ToString(), Environment.NewLine));
                }
            }

            if (_DepositResponse == null ||
                _DepositResponse.error.ErrorCode != Globals.ErrorCodes.NO_ERROR)
            {

                System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Try to deposit via other available prioritized providers{1}",
                    DateTime.Now.ToString(), Environment.NewLine));


                System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] There are {1} providers that are ready for deposit {2}",
                            DateTime.Now.ToString(), providers.Count.ToString(), Environment.NewLine));
                // new list of failedDeposits
                List<FailedDeposit> failedDeposits = new List<FailedDeposit>();
                foreach (CashierProvider p in providers)
                {
                    System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Trying to deposit via '{1}'{2}",
                            DateTime.Now.ToString(), p.provider.ToString(), Environment.NewLine));

                    _DepositRequest.provider = p.provider;

                    _DepositResponse = p.Deposit(_DepositRequest);

                    System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Provider '{1}' responed '{2} - {3}'{4}",
                        DateTime.Now.ToString(), _DepositResponse.provider.ToString(), _DepositResponse.error.ErrorCode.ToString(),
                         _DepositResponse.error.Description, Environment.NewLine));

                    if (_DepositResponse.error.ErrorCode == Globals.ErrorCodes.NO_ERROR)
                    {
                        System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Managed to deposit via prioritized provider '{1}'{2}",
                            DateTime.Now.ToString(), _DepositResponse.provider.ToString(), Environment.NewLine));
                        break;
                    }
                    else
                    {
                        // add the FailedDeposits
                        if (_DepositResponse.FailedDeposits is List<FailedDeposit> && _DepositResponse.FailedDeposits.Count > 0)
                            failedDeposits.Add(_DepositResponse.FailedDeposits[0]);
                    }
                }

                _DepositResponse.FailedDeposits = failedDeposits;
                
                System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] num of Fail deposit: {1}{2}",
                        DateTime.Now.ToString(), _DepositResponse.FailedDeposits.Count.ToString(), Environment.NewLine));

                foreach (FailedDeposit failDepositp in _DepositResponse.FailedDeposits)
                {
                    System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] {1}: {2}{3}",
                        DateTime.Now.ToString(),
                        failDepositp.provider.ToString(),
                        failDepositp.Description,
                        Environment.NewLine));
                }
            }
            else
            {
                System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] Managed to deposit via LastClearedBy provider '{1}'{2}",
                    DateTime.Now.ToString(), _DepositResponse.provider.ToString(), Environment.NewLine));
            }

            return _DepositResponse;
        }
        
        #endregion

        #region Private methods

        int RoundUp(int toRound)
        {
            return (10 - toRound % 10) + toRound;
        }

        int RoundDown(int toRound)
        {
            return toRound - toRound % 10;
        }

        int Round(int Number)
        {
            if (Number < 10)
                return 10;

            if (Number < 50)
                return RoundDown(Number);
            else
                return RoundUp(Number);
        }
        
        int GetRoundRobinIndex(SQLiteDatabase dataBase, int Level, string Pattern)
        {
            int index = 0;

            try
            {
                using (DataTable dt = dataBase.GetDataTable("select fIndex from tRoundRobin where fLevel = " + Level.ToString() + " and fPattern = '" + Pattern + "'"))
                {
                    if (dt == null || dt.Rows.Count == 0)
                    {
                        dataBase.ExecuteNonQuery("REPLACE INTO tRoundRobin (fLevel, fPattern) VALUES (" + Level.ToString() + ", '" + Pattern + "');");
                        return index;
                    }
                    else
                    {
                        DataRow dr = dt.Rows[0];
                        index = (int)((long)dr["fIndex"]);
                    }
                }
            }
            catch(Exception ex)
            {
                System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] GetRoundRobinIndex exception  {1} {2}",
                    DateTime.Now.ToString(), ex.Message, Environment.NewLine));
            }

            return index;
        }

        void UpdateRoundRobinIndex(SQLiteDatabase dataBase, int Level, string Pattern, int Index)
        {
            try
            {
                int res = dataBase.ExecuteNonQuery("update tRoundRobin set fIndex = " + Index.ToString() + " where fLevel = " + Level.ToString() + " and fPattern = '" + Pattern + "'");
                if (res < 1)
                {
                    System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] UpdateRoundRobinIndex: Fail update next index in pattern {1}",
                        DateTime.Now.ToString(), Environment.NewLine));
                }
            }
            catch(Exception ex)
            {
                System.IO.File.AppendAllText(configurator.PathToLogFile, string.Format(": [Cashier: {0}] UpdateRoundRobinIndex exception  {1} {2}",
                    DateTime.Now.ToString(), ex.Message, Environment.NewLine));
            }
        }
                        
        List<CashierProvider> GetPrioritizedProviders(DepositRequest _DepositRequest, CashierService.IService service)
        {
            List<CashierProvider> prioritizedProviders = new List<CashierProvider>();

            Globals.Providers providerType;
            string providerName = "NO PROVIDER";
            ProviderConfigBase config = null;
            DataTable dt = null;

            try
            {                
                System.IO.File.AppendAllText(configurator.PathToLogFile, 
                    string.Format("{0} GetPrioritizedProviders - BrandID: {1}   CountryISOCode: {2}   CurrencyISOCode: {3}   CardType: {4}   Amount: {5} {6}", DateTime.Now.ToString(), _DepositRequest.BrandID, _DepositRequest.CountryISOCode, _DepositRequest.CurrencyISOCode, _DepositRequest.CardType, _DepositRequest.Amount, Environment.NewLine));

                dt = service.GetProviders(_DepositRequest.BrandID, _DepositRequest.CountryISOCode, _DepositRequest.CurrencyISOCode, _DepositRequest.CardType, _DepositRequest.Amount);

                if (dt == null || dt.Rows.Count == 0)
                {
                    throw new Exception("Cashier.ProviderFacade: Unable to read data from CashierDB (or there are no providers)");
                }

                System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                        ": Cashier.ProviderFacade: GetPrioritizedProviders: Result data table rows count: " + dt.Rows.Count + "\r\n");

                foreach (DataRow row in dt.Rows)
                {
                    providerName = row["title"].ToString().ToUpper();

                    if (!Enum.TryParse<Globals.Providers>(providerName, out providerType))
                    {
                        System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                            ": Cashier.ProviderFacade: Failed to recognize  '" + providerName + "' provider from CashierDB. It does not exist in Cashier\r\n");
                        continue;
                    }

                    config = configurator.GetProviderGonfig(providerType);

                    if (config == null)
                    {
                        System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                            ": Cashier.ProviderFacade: '" + providerName + "' provider must be configured in Cashier.config or deleted from CashierDB\r\n");
                        continue;
                    }
                    System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                        ": Cashier.ProviderFacade: Provider Name  " + providerType.ToString() + " " + config.Url + "\r\n");

                    prioritizedProviders.Add(new CashierProvider(providerType, config));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EXCEPTION: Method GetPrioritizedProviders, Provider Name = " + providerName + " issue: " + ex.Message);
            }
            finally
            {
                if (dt is DataTable)
                    dt.Dispose();
            }

            return prioritizedProviders;
        }

        int GetRoundRobinIndex(CashierService.IService service, int Level, string Pattern, int BrandID)
        {
            int index = service.GetRoundRobinIndex(BrandID, Level, Pattern);
            if (index == -1)
            {
                service.InsertRoundRobin(BrandID, Level, Pattern);
                index = 0;
            }

            return index;
        }

        List<CashierProvider> GetPrioritizedProviders(SQLiteDatabase dataBase, string sqlQuery)
        {
            List<CashierProvider> prioritizedProviders = new List<CashierProvider>();

            Globals.Providers providerType;
            string providerName = string.Empty;
            ProviderConfigBase config = null;

            try
            {
                if (dataBase == null)
                {
                    System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                        ": Cashier.ProviderFacade: GetPrioritizedProviders DB is NULL\r\n");
                }


                System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                        ": Query is: " + sqlQuery + "\r\n");

                DataTable dt = dataBase.GetDataTable(sqlQuery);

                System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                        ": Cashier.ProviderFacade: GetPrioritizedProviders: Result data table rows count: " + dt.Rows.Count + "\r\n");

                if (dt == null || dt.Rows.Count == 0)
                {
                    throw new Exception("Cashier.ProviderFacade: Unable to read data from CashierDB (or there are no providers)");
                }

                foreach (DataRow row in dt.Rows)
                {
                    providerName = row[PROVIDER_NAME].ToString().ToUpper();

                    if (!Enum.TryParse<Globals.Providers>(providerName, out providerType))
                    {
                        System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                            ": Cashier.ProviderFacade: Failed to recognize  '" + providerName + "' provider from CashierDB. It does not exist in Cashier\r\n");
                        continue;
                    }

                    config = configurator.GetProviderGonfig(providerType);
                
                    if (config == null)
                    {
                        System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                            ": Cashier.ProviderFacade: '" + providerName + "' provider must be configured in Cashier.config or deleted from CashierDB\r\n");
                        continue;
                    }
                    System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() +
                        ": Cashier.ProviderFacade: Provider Name  " + providerType.ToString() + " " + config.Url + "\r\n");

                    prioritizedProviders.Add(new CashierProvider(providerType, config));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EXCEPTION: Method GetPrioritizedProviders, Provider Name = " + providerName + " issue: " + ex.Message);
            }

            return prioritizedProviders;
        }

        #endregion
    }
}
