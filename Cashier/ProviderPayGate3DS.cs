﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using CashierUtils;

namespace Cashier
{
	class ProviderPayGate3DS : ProviderBase
	{
		private class Error
		{
			public int Code = 0;
			public string Description = string.Empty;
		}



		private string returnURL;
		private const string UNDEFINED = "undefined";
		private string providerName;

		#region Constructor

		public ProviderPayGate3DS(ProviderConfigBase config)
			: base(config)
		{

		}
		#endregion

		protected override Globals.Providers ProviderType
		{
			get { return Globals.Providers.PAYGATE3DS; }
            set { }
        }

		public override DepositResponse Deposit(DepositRequest _DepositRequest)
		{
			string transactionID = Guid.NewGuid().ToString().Replace("-", "");
			DepositResponse _DepositResponse = new DepositResponse();
			//_DepositResponse.error = new Cashier.Error();
			_DepositResponse.provider = _DepositRequest.provider;
			_DepositResponse.ClearingUserID = MerchantID;
			//_DepositResponse.TransactionID = transactionID;
			_DepositResponse.Amount = _DepositRequest.Amount.ToString();

			providerName = _DepositRequest.provider.ToString();

			// build the request
			string req = BuildRequest(_DepositRequest, transactionID);
			if (string.IsNullOrEmpty(req))
			{
				_DepositResponse.ErrorDescripotion = "Fail construct request string for PayGate3DS";
				_DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
				_DepositResponse.ErrorCode = "1";
				_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
				return _DepositResponse;
			}

			Log(string.Format("Request: Provider=PayGate3DS; {0} {1}{2}", Url, req, Environment.NewLine));

			// send POST
			Error e = HttpReq("POST", Url, req, null, "text/xml", "", 20000, 20000);

			Log(string.Format("Response: Provider=PayGate3DS; {0}{1}", e.Description, Environment.NewLine));

			if (e.Code != 0)
			{
				_DepositResponse.ErrorDescripotion = "Http request to PayGate3DS failed. " + e.Description;
				_DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
				_DepositResponse.ErrorCode = "1";
				_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

				// fill the class FailedDeposit with details in order to send email to support later
				FailedDeposit failedDeposit = new FailedDeposit();
				failedDeposit.provider = _DepositRequest.provider;
				failedDeposit.Email = _DepositRequest.Email;
				failedDeposit.TransactionID = transactionID;
				failedDeposit.Amount = _DepositRequest.Amount;
				failedDeposit.FirstName = _DepositRequest.FirstName;
				failedDeposit.LastName = _DepositRequest.LastName;
				failedDeposit.Description = e.Description;

				_DepositResponse.FailedDeposits.Add(failedDeposit);
			}
			else
			{
				try
				{
					string result = e.Description;

					// check if its xml format
					int startOfXML = result.IndexOf("<?xml");
					if (startOfXML > -1)
					{
						string xmlResponse = result.Substring(startOfXML);

						// Now load the string into an XmlDocument 
						XmlDocument responseDocument = new XmlDocument();
						responseDocument.LoadXml(xmlResponse);
						ParseResponseXml(responseDocument, _DepositRequest, _DepositResponse);
					}
					else
					{
						_DepositResponse.ErrorDescripotion = "PayGate3DS: response is invalid: " + result;
						_DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
						_DepositResponse.ErrorCode = "1";
						_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
					}

				}
				catch (UriFormatException)
				{
					_DepositResponse.ErrorDescripotion = "PayGate3DS: Couldn't parse URL return";
					_DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
					_DepositResponse.ErrorCode = "1";
					_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
					return _DepositResponse;
				}
			}

			return _DepositResponse;
		}

		private string BuildRequest(DepositRequest depositRequest, string transactionId)
		{

			string req = string.Empty;

			try
			{
				string accountID = MerchantID;
				string password = MerchantPassword;
				string orderID = transactionId;
				int amount = Convert.ToInt32(depositRequest.Amount * 100);
				string currency = depositRequest.CurrencyISOCode;
				string customerIP = depositRequest.IpAddress;
				string customerID = depositRequest.CustomerID;
				string cardHolderName = depositRequest.FirstName.Trim() + " " + depositRequest.LastName.Trim();
				string cardHolderAddress = depositRequest.Address;
				string cardHolderZipcode = depositRequest.ZipCode;
				string cardHolderCity = depositRequest.City;
				string cardHolderState = depositRequest.StateISOCode;
				string cardHolderCountryCode = depositRequest.CountryISOCode;
				string cardHolderPhone = depositRequest.PhoneNumber;
				string cardHolderEmail = depositRequest.Email;
				string cardNumber = depositRequest.CardNumber;
				string cardSecurityCode = depositRequest.CardCVV2;
				string cardExpireMonth = depositRequest.CardExpMonth;
				string cardExpireYear = depositRequest.CardExpYear;

				// phone format is: '+xxx(yyy)zzz-zzzz-ppp'
				try
				{
					string[] arr = cardHolderPhone.Split('-');
					if (arr.Length == 1)
						arr = cardHolderPhone.Split(' ');
					cardHolderPhone = arr[0] + "(" + arr[1] + ")" + arr[2];
				}
				catch (Exception ex)
				{
					Log("Exception in translating phone number " + cardHolderPhone + " to format +xxx(yyy)zzz-zzzz-ppp ( it should be 3 sections with - between them ), description: " + ex.Message);
				}
				if (cardHolderPhone.IndexOf('+') < 0)
				{
					cardHolderPhone = "+" + cardHolderPhone.Trim();
				}

				req = string.Format(@"<?xml version='1.0' encoding='UTF-8'?> <soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' 
			xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:Interface'>
			   <soapenv:Header/>
			   <soapenv:Body>
				  <urn:SaleTransaction soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>
					 <request xsi:type='ns1:DirectAuthorizationRequest'>
						<accountID xsi:type='xsd:string'>{0}</accountID>
						<password xsi:type='xsd:string'>{1}</password>
						<orderID xsi:type='xsd:string'>{2}</orderID>            
						<customerIP xsi:type='xsd:string'>{3}</customerIP>
						<amount xsi:type='xsd:int'>{4}</amount>
						<currency xsi:type='xsd:string'>{5}</currency>
						<customerID xsi:type='xsd:string'>{6}</customerID>
						<cardHolderName xsi:type='xsd: string'>{7}</cardHolderName>
						<cardHolderAddress xsi:type='xsd:string'>{8}</cardHolderAddress>
						<cardHolderZipcode xsi:type='xsd:string'>{9}</cardHolderZipcode>
						<cardHolderCity xsi:type='xsd:string'>{10}</cardHolderCity>
						<cardHolderState xsi:type='xsd:string'>{11}</cardHolderState>
						<cardHolderCountryCode xsi:type='xsd:string'>{12}</cardHolderCountryCode>
						<cardHolderPhone xsi:type='xsd:string'>{13}</cardHolderPhone>
						<cardHolderEmail xsi:type='xsd:string'>{14}</cardHolderEmail>
						<cardNumber xsi:type='xsd:string'>{15}</cardNumber>
						<cardSecurityCode xsi:type='xsd:string'>{16}</cardSecurityCode>
						<cardExpireMonth xsi:type='xsd:string'>{17}</cardExpireMonth>
						<cardExpireYear xsi:type='xsd:string'>{18}</cardExpireYear>         
					 </request>
				  </urn:SaleTransaction>
			   </soapenv:Body>
			</soapenv:Envelope>", accountID, password, orderID, customerIP, amount, currency, customerID, cardHolderName,
								  cardHolderAddress, cardHolderZipcode, cardHolderCity, cardHolderState, cardHolderCountryCode, cardHolderPhone,
								  cardHolderEmail, cardNumber, cardSecurityCode, cardExpireMonth, cardExpireYear);

			}
			catch (Exception ex)
			{
				Log(string.Format("Provider=PayGate3DS; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
			}

			return req;
		}

		private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
		{
			Error resp = new Error();

			try
			{
				System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
				/*ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );*/

				HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

				req.Method = Method.ToUpper();

				if (Header is System.Collections.Specialized.NameValueCollection)
					req.Headers.Add(Header);
				if (!string.IsNullOrEmpty(ContentType))
					req.ContentType = ContentType;
				if (!string.IsNullOrEmpty(Accept))
					req.Accept = Accept;
				if (!string.IsNullOrEmpty(Cred))
					req.Headers[HttpRequestHeader.Authorization] = Cred;

				req.Timeout = ConnectTimeoutMSec;
				req.ReadWriteTimeout = ReadWriteTimeoutMSec;

				if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
				{
					req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

					using (Stream stm = req.GetRequestStream())
					{
						stm.WriteTimeout = ReadWriteTimeoutMSec;
						using (StreamWriter stmw = new StreamWriter(stm))
						{
							stmw.Write(Parameters);
						}
					}
				}

				HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
				using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
				{
					resp.Description = rd.ReadToEnd();
				}


			}
			catch (WebException we)
			{
				if (we.Response != null)
				{
					using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
					{
						resp.Code = -1;
						resp.Description = responseReader.ReadToEnd();
					}
				}
				else
				{
					if ((we.Response is System.Net.HttpWebResponse))
						resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
					else
						resp.Code = (int)we.Status;
					resp.Description = resp.Code.ToString() + ", " + we.Message;
				}
			}
			catch (Exception ex)
			{
				resp.Code = 1;
				resp.Description = ex.Message;
			}

			return resp;
		}

		#region Response parsing funcs

		//Parses response Xml. Returns DataCashDepositResponseParameters object
		private void ParseResponseXml(XmlDocument docResponse, DepositRequest request, DepositResponse response)
		{
			// Get the root node 
			XmlElement rootElm = docResponse.DocumentElement;

			string transactionID = string.Empty;
			string orderID = string.Empty;
			string status = string.Empty;
			string errorCode = string.Empty;
			string errorMessage = string.Empty;
			string statementDescriptor = string.Empty;
			string acsUrl = string.Empty;
			string acsRequestMessage = string.Empty;

			transactionID = GetXmlElementValue(rootElm, "transactionID");
			orderID = GetXmlElementValue(rootElm, "orderID");
			status = GetXmlElementValue(rootElm, "status");
			errorCode = GetXmlElementValue(rootElm, "errorCode");
			errorMessage = GetXmlElementValue(rootElm, "errorMessage");
			statementDescriptor = GetXmlElementValue(rootElm, "statementDescriptor");

			response.TransactionID = transactionID;
			
			if (errorCode == "000")
			{
				response.error.ErrorCode = status == "Authorized" ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
				response.ErrorMessage = errorMessage;
				response.error.Description = errorMessage == "" ? statementDescriptor : errorMessage;
				response.ErrorDescripotion = response.error.Description;
				return;
			}
			if (errorCode == "655")
			{

				// get redirect url
				if (GetXmlElementValue(rootElm, "ACSUrl") != "undefined")
				{
					acsUrl = GetXmlElementValue(rootElm, "ACSUrl");
					response.RedirectUrl = acsUrl;
				}

				response.RedirectParameters = new List<RedirectValues>();

				// get TermUrl
				RedirectValues termurl = new RedirectValues();
				termurl.Key = "TermUrl";
				termurl.Value = CallbackUrl;
				response.RedirectParameters.Add(termurl);

				// get PAReq
				RedirectValues pareq = new RedirectValues();
				pareq.Key = "PaReq";
				pareq.Value = GetXmlElementValue(rootElm, "ACSRequestMessage");
				response.RedirectParameters.Add(pareq);

				// get MD
				RedirectValues md = new RedirectValues();
				md.Key = "MD";
				md.Value = transactionID;
				response.RedirectParameters.Add(md);
				Log("md.Value" + md.Value);

				response.Is3DSecure = true;
				response.error.ErrorCode = status == "Not authorized" ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
				response.ErrorMessage = errorMessage;
				response.error.Description = errorMessage == "" ? statementDescriptor : errorMessage;
				response.ErrorDescripotion = response.error.Description;
				return;
			}
			else 
			{
				response.error.ErrorCode =  Globals.ErrorCodes.FAIL;
				response.ErrorMessage = errorMessage;
				response.error.Description = errorMessage == "" ? statementDescriptor : errorMessage;
				response.ErrorDescripotion = response.error.Description;
				return;
			}


		}


		//Returns Xml element value
		private string GetXmlElementValue(XmlElement rootElm, string elmName)
		{
			// Find all descendants with the desired name 
			XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

			// Assume only one "hit" and return the contained text
			string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
			return resultValue;
		}

		#endregion

	}
}

