﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    class ProviderProcessingCom3DS : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        #region 3DS response
        [DataContract]
        private class ProcessingCom3DSResponseStatus
        {
            [DataMember]
            public int code { get; set; }
            [DataMember]
            public string message { get; set; }

            [DataMember]
            public string secure_hash { get; set; }
        }

        [DataContract]
        private class ProcessingCom3DSResponseResult
        {
            [DataMember]
            public string acs_url { get; set; }
            [DataMember]
            public string PaReq { get; set; }
            [DataMember]
            public string MD { get; set; }

            [DataMember]
            public string cavv { get; set; }
            [DataMember]
            public string xid { get; set; }
            [DataMember]
            public string eci { get; set; }
            [DataMember]
            public string secure_hash { get; set; }
            [DataMember]
            public string trans_id { get; set; }
        }

        [DataContract]
        private class ProcessingCom3DSResponse
        {
            [DataMember]
            public ProcessingCom3DSResponseStatus status { get; set; }
            [DataMember]
            public ProcessingCom3DSResponseResult result { get; set; }

            [DataMember]
            public int code { get; set; }
            [DataMember]
            public string message { get; set; }
        }
        #endregion

        private string Mid;
        private string MidQ;
        private string MidCombined;

        #region Constructor

        public ProviderProcessingCom3DS(ProviderConfigBase config)
            : base(config)
        {
            ProcessingComProviderConfig processingComConfig = config as ProcessingComProviderConfig;
            if (processingComConfig == null)
            {
                throw new ArgumentException("processingComConfig expects to get ProcessingComProviderConfig object!");
            }

            Mid = processingComConfig.Mid;
            MidQ = processingComConfig.MidQ;
            MidCombined = processingComConfig.MidCombined;

		

		}
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.PROCESSINGCOM3DS; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            string transactionID = ((int)((DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds)).ToString();

            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = ProviderType;

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string req = Build3DSRequest(_DepositRequest, ExpMonth, ExpYear, transactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for ProcessingCom3DS";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request [Request ACS URL phase]: Provider=ProcessingCom3DS; {0} {1}{2}", Url, req, Environment.NewLine));
            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded", "", 30000, 30000);
            Log(string.Format("Response [Request ACS URL phase]: Provider=ProcessingCom3DS; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to ProcessingCom failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                ProcessingCom3DSResponse processingCom3DSResponse = Parse3DSResponse(e.Description, _DepositRequest, _DepositResponse, ExpMonth, ExpYear);

				if (processingCom3DSResponse is ProcessingCom3DSResponse)
                {
					Log(string.Format("Request [PURCHASE phase N: 2]: processingCom3DSResponse is ProcessingCom3DSResponse; {0} {1}", processingCom3DSResponse, Environment.NewLine));
					req = BuildNon3DSRequest(_DepositRequest, ExpMonth, ExpYear, transactionID, processingCom3DSResponse);
                    Log(string.Format("Request [PURCHASE phase]: Provider=ProcessingCom3DS; {0} {1}{2}", Url2, req, Environment.NewLine));
                    e = HttpReq("POST", Url2, req, null, "application/x-www-form-urlencoded", "", 30000, 30000);
                    Log(string.Format("Response [PURCHASE phase]: Provider=ProcessingCom3DS; {0}{1}", e.Description, Environment.NewLine));
                    if (e.Code != 0)
                    {
                        _DepositResponse.ErrorDescripotion = "Http request to ProcessingCom failed. " + e.Description;

                        // fill the class FailedDeposit with details in order to send email to support later
                        FailedDeposit failedDeposit = new FailedDeposit();
                        failedDeposit.provider = _DepositRequest.provider;
                        failedDeposit.Email = _DepositRequest.Email;
                        failedDeposit.TransactionID = transactionID;
                        failedDeposit.Amount = _DepositRequest.Amount;
                        failedDeposit.FirstName = _DepositRequest.FirstName;
                        failedDeposit.LastName = _DepositRequest.LastName;
                        failedDeposit.Description = e.Description;

                        _DepositResponse.FailedDeposits.Add(failedDeposit);
                    }
                    else
                        ParseResponse(e.Description, _DepositResponse);
                }
            }

            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

            return _DepositResponse;
        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        if ((we.Response is System.Net.HttpWebResponse))
                            resp.Code = (int)((we.Response as System.Net.HttpWebResponse).StatusCode);
                        else
                            resp.Code = -1;
                        resp.Description = resp.Code.ToString() + " " + responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private string BuildNon3DSRequest(DepositRequest _DepositRequest, string CardExpMonth, string CardExpYear, string TransactionID, ProcessingCom3DSResponse processingCom3DSResponse)
        {
            string req = string.Empty;

            try
            {
                string m = "";
                string mq = "";
                if (!string.IsNullOrEmpty(MidCombined))
                {
                    //PROCESSINGCOM MidCombined: <CURRENCY=[MID ID];[MID QUAL]  SEPERATOR IS: @
                    var dict = MidCombined.Split(new[] { '@' }, StringSplitOptions.RemoveEmptyEntries)
                           .Select(part => part.Split('='))
                           .ToDictionary(split => split[0], split => split[1]);
                    string temp = dict[_DepositRequest.CurrencyISOCode.ToUpper()];
                    string[] cred = temp.Split(';');
                    m = cred[0];
                    mq = cred[1];
                }
                else
                {
                    m = Mid;
                    mq = MidQ;
                }

				req = string.Format("account_username={0}&account_password={1}&type=purchase&amount={2}&mid={3}&mid_q={4}&card_number={5}&card_expiry_month={6}&card_expiry_year={7}&card_cvv2={8}&ip_address={9}&first_name={10}&last_name={11}&email_address={12}&street_address_1={13}&zip={14}&city={15}&country={16}&phone_number={17}",
					MerchantID, MerchantPassword, _DepositRequest.Amount.ToString(), m, mq, _DepositRequest.CardNumber, CardExpMonth, CardExpYear, _DepositRequest.CardCVV2, _DepositRequest.IpAddress,
					_DepositRequest.FirstName, _DepositRequest.LastName, _DepositRequest.Email, _DepositRequest.Address, _DepositRequest.ZipCode, _DepositRequest.City, _DepositRequest.CountryISOCode, _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", ""));

				if (processingCom3DSResponse.result is ProcessingCom3DSResponseResult)
                {
                    if (!string.IsNullOrEmpty(processingCom3DSResponse.result.cavv))
                        req += "&cavv=" + processingCom3DSResponse.result.cavv;
                    if (!string.IsNullOrEmpty(processingCom3DSResponse.result.eci))
                        req += "&eci=" + processingCom3DSResponse.result.eci;
                    if (!string.IsNullOrEmpty(processingCom3DSResponse.result.secure_hash))
                        req += "&secure_hash=" + processingCom3DSResponse.result.secure_hash;
                    if (!string.IsNullOrEmpty(processingCom3DSResponse.result.xid))
                        req += "&xid=" + processingCom3DSResponse.result.xid;
                }
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=ProcessingCom3DS; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private void ParseResponse(string data, DepositResponse _DepositResponse)
        {
            NameValueCollection qscoll = HttpUtility.ParseQueryString(data);

            if (qscoll.Count == 0)
            {
                Log(string.Format("Response: Provider=ProcessingCom; Invalid http response"));
                _DepositResponse.ErrorDescripotion = "ProcessingCom returned empty response for deposit request";
                return;
            }

            //success
            if (qscoll["code"] == "00" || qscoll["code"] == "0")
            {
                _DepositResponse.Receipt = qscoll["transaction_id"];
                _DepositResponse.Amount = qscoll["amount"];
                _DepositResponse.ErrorDescripotion = "";
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
            }
            else
            {
                _DepositResponse.ErrorDescripotion = "Code: " + qscoll["code"] + " , Message: " + qscoll["message"];
                if (!string.IsNullOrEmpty(qscoll["sub_code"]) && !string.IsNullOrEmpty(qscoll["sub_message"]))
                    _DepositResponse.ErrorDescripotion += " , SubCode: " + qscoll["sub_code"] + " , SubMessage: " + qscoll["sub_message"];
            }
        }

        private string Build3DSRequest(DepositRequest _DepositRequest, string CardExpMonth, string CardExpYear, string TransactionID)
        {
            string req = string.Empty;

            try
            {
                string m = "";
                if (!string.IsNullOrEmpty(MidCombined))
                {
                    //PROCESSINGCOM MidCombined: <CURRENCY=[MID ID];[MID QUAL]  SEPERATOR IS: @
                    var dict = MidCombined.Split(new[] { '@' }, StringSplitOptions.RemoveEmptyEntries)
                           .Select(part => part.Split('='))
                           .ToDictionary(split => split[0], split => split[1]);
                    string temp = dict[_DepositRequest.CurrencyISOCode.ToUpper()];
                    string[] cred = temp.Split(';');
                    m = cred[0];
                }
                else
                {
                    m = "34030";
                }

                req = string.Format("mid={0}&trans_amount={1}&trans_id={2}&cc_num={3}&cc_exp_yr={4}&cc_exp_mth={5}", m, _DepositRequest.Amount.ToString(), TransactionID, _DepositRequest.CardNumber, CardExpYear, CardExpMonth);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=ProcessingCom3DS; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private ProcessingCom3DSResponse Parse3DSResponse(string resp, DepositRequest _DepositRequest, DepositResponse _DepositResponse, string CardExpMonth, string CardExpYear)
        {
            ProcessingCom3DSResponse processingCom3DSResponse = null;
			try
            {
                processingCom3DSResponse = JSONSerializer<ProcessingCom3DSResponse>.DeSerialize(resp);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=ProcessingCom3DS; Fail parsing response: {0}{1}", ex.Message, Environment.NewLine));
                return null;
            }
			if (processingCom3DSResponse.status is ProcessingCom3DSResponseStatus)
            {
                switch (processingCom3DSResponse.status.code)
                {
                    case 200:
                        {
                            if (!string.IsNullOrEmpty(processingCom3DSResponse.result.acs_url))
                            {
								
								_DepositResponse.Is3DSecure = true;
                                _DepositResponse.RedirectUrl = processingCom3DSResponse.result.acs_url;
                                _DepositResponse.RedirectParameters = new List<RedirectValues>();

                                RedirectValues pareqVal = new RedirectValues();
                                pareqVal.Key = "PaReq";
                                pareqVal.Value = processingCom3DSResponse.result.PaReq;
                                _DepositResponse.RedirectParameters.Add(pareqVal);
								
								RedirectValues md = new RedirectValues();
                                md.Key = "MD";
                                md.Value = processingCom3DSResponse.result.MD;
                                _DepositResponse.RedirectParameters.Add(md);
								
								RedirectValues termurl = new RedirectValues();
                                termurl.Key = "TermUrl";
                                termurl.Value = CallbackUrl;
                                _DepositResponse.RedirectParameters.Add(termurl);
								
								_DepositResponse.DataToSave = _DepositRequest.CurrencyISOCode + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.Amount.ToString() + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.CardNumber + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.CardCVV2 + "<SEP>";
                                _DepositResponse.DataToSave += CardExpMonth + "<SEP>";
                                _DepositResponse.DataToSave += CardExpYear + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.IpAddress + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.Email + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.FirstName + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.LastName + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.Address + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.City + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.CountryISOCode + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.ZipCode + "<SEP>";
                                _DepositResponse.DataToSave += _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", "") + "<SEP>";

                                _DepositResponse.DataToSave = _DepositResponse.DataToSave.Replace("'", "");
                                _DepositResponse.DataToSaveKey = _DepositResponse.TransactionID;

                                _DepositResponse.ErrorDescripotion = "";
                                _DepositResponse.ErrorCode = "0";
                                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
								Log(string.Format("Provider=ProcessingCom3DS; Add 3DS parsing response to HTTP REQUEST: {0}{1}", _DepositResponse.DataToSave, Environment.NewLine));
							}
                        }
                        break;

                    case 404:
                        {
                            return processingCom3DSResponse;
                        }

                    case 500:
                        {
                            _DepositResponse.ErrorDescripotion = "Code: " + processingCom3DSResponse.status.code.ToString() + " , Message: " + processingCom3DSResponse.status.message;
                        }
                        break;
                }

            }
            else if (processingCom3DSResponse.code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Code: " + processingCom3DSResponse.code.ToString() + " , Message: " + processingCom3DSResponse.message;
            }

            return null;
        }

    }
}
