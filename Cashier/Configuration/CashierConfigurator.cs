﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using Cashier.Configuration.Parsing;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier.Configuration
{
	internal class CashierConfigurator
	{
		#region Constants

		private const string PROVIDER = "Provider";
		private const string ATTRIBUTE_NAME = "Name";
		private const string ATTRIBUTE_MODE = "Mode";
		private const string ATTRIBUTE_PATH_TO_DB = "PathToCashierDB";
		private const string ATTRIBUTE_PATH_TO_LOG = "PathToLogFile";
		private const string ELEMENT_CASHIER_CONFIG = "CashierConfig";

		#endregion

		private readonly Dictionary<Globals.Providers, ProviderConfigBase> _settings;
		//private string _pathToConfigFile;

		public string PathToLogFile { get; private set; }
		public string PathToCashierDB { get; private set; }
		
		public CashierConfigurator(string pathToCashierConfig, bool IsLocalFile)
		{            
			if(string.IsNullOrWhiteSpace(pathToCashierConfig))
			{
				throw new ArgumentNullException("pathToCashierConfig parameter cannot be null or empty");
			}

            if (IsLocalFile)
            {
                if (!File.Exists(pathToCashierConfig))
                {
                    throw new ArgumentException(string.Format("'{0}' file doe not exist. Check path to Cashier.config file", pathToCashierConfig));
                }
            }
			
			//_pathToConfigFile = pathToCashierConfig;

			_settings = ParseConfiguration(pathToCashierConfig, IsLocalFile);
		}
		
		//This function will return NULL if there is no configuratuion for passed providerType
		public ProviderConfigBase GetProviderGonfig(Globals.Providers providerType)
		{
			ProviderConfigBase config = null;
			_settings.TryGetValue(providerType, out config);
		
			return config;	
		}
			
		private Dictionary<Globals.Providers, ProviderConfigBase> ParseConfiguration(string pathToConfigFile, bool IsLocalFile)
		{
			var config = new Dictionary<Globals.Providers, ProviderConfigBase>();
								  		
			//XmlAttribute attr;
			XmlDocument doc = new XmlDocument();
			
			Globals.Providers providerType;
			
			try
			{
                if (IsLocalFile)
                    doc.Load(pathToConfigFile);
                else
                    doc.LoadXml(pathToConfigFile);
															
				foreach(XmlNode node in doc.GetElementsByTagName(ELEMENT_CASHIER_CONFIG))
				{
					PathToCashierDB = node.Attributes[ATTRIBUTE_PATH_TO_DB].Value;
					PathToLogFile = node.Attributes[ATTRIBUTE_PATH_TO_LOG].Value;
				}
                System.IO.File.AppendAllText(PathToLogFile, DateTime.Now.ToString() + ": Cashier.CashierConfigurator: Path To Cashier DB:  " + PathToCashierDB + "\r\n");				
				XmlNodeList providers = doc.DocumentElement.GetElementsByTagName(PROVIDER);
								
				ProviderConfigParserFactory parserFactory = new ProviderConfigParserFactory();
				ProviderConfigParserBase parser = null;
				ProviderConfigBase configItem;

				foreach (XmlNode providerNode in providers)
				{
					providerType = 
						 (Globals.Providers)Enum.Parse(typeof(Globals.Providers), providerNode.Attributes[ATTRIBUTE_NAME].Value);
                    System.IO.File.AppendAllText(PathToLogFile, DateTime.Now.ToString() + ": Cashier.CashierConfigurator: providerType:  " + providerType.ToString() + "\r\n");		
					
					parser = parserFactory.GetParser(providerType);

					configItem = parser.ParseProviderConfig(providerNode);
										
					configItem.PathToLogFile = PathToLogFile; 
		
					config.Add(providerType, configItem);
                    System.IO.File.AppendAllText(PathToLogFile, DateTime.Now.ToString() + ": Cashier.CashierConfigurator: configItem:  " + configItem.ToString() + "\r\n");		

				}
				
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("An error has occured while parsing '{0}' {1}", pathToConfigFile, ex.Message + " " + ex.StackTrace));	
			}

			return config;
		}
	}
}
