﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cashier.Configuration.ProviderConfig
{
    internal class TrustPayProviderConfig : ProviderConfigBase
    {
        public string Sender { get; set; }
        public string Channel { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; Sender: {0}; Channel: {1}", Sender, Channel);
        }
    }
}
