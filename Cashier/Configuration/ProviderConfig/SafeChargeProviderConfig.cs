﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cashier.Configuration.ProviderConfig
{
	class SafeChargeProviderConfig: ProviderConfigBase
	{
		public string SiteId { get; set; }

		public override string ToString()
		{
			return base.ToString() + string.Format("; SiteId: {0}", SiteId);
		}

	}
}
