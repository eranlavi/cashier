﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cashier.Configuration.ProviderConfig
{
    internal class SolidProviderConfig : ProviderConfigBase
    {
        public string Sender { get; set; }
        public string Channel { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; Sender: {0}; Channel: {1}", Sender, Channel);
        }
    }
}
