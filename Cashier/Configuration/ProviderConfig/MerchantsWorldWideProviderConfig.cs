﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cashier.Configuration.ProviderConfig
{
    internal class MerchantsWorldWideProviderConfig : ProviderConfigBase
    {
        public string TerminalId { get; set; }
        public string PrivateKey { get; set; }
        public string CallbackUrl { get; set; }
        public string TrmCode { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; TerminalId: {0}; PrivateKey: {1}; CallbackUrl: {2}; TrmCode: {3}", TerminalId, PrivateKey, CallbackUrl, TrmCode);
        }
    }
}
