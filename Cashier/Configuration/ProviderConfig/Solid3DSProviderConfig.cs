﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cashier.Configuration.ProviderConfig
{
    internal class Solid3DSProviderConfig : ProviderConfigBase
    {
        public string Sender { get; set; }
        public string Channel { get; set; }
        public string ReturnURL { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; ReturnURL: {0}; ReturnURL: {1}; ReturnURL: {2}", Sender, Channel, ReturnURL);
        }
    }
}
