﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cashier.Configuration.ProviderConfig
{
    class Paydelta3DSProviderConfig : ProviderConfigBase
    {       
        public string ProduceName { get; set; }
        public string ProduceURL { get; set; }
        public string CallbackUrl { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; ProduceName: {0}; ProduceURL: {1}; CallbackUrl: {2}", ProduceName, ProduceURL, CallbackUrl);
        }
    }
}
