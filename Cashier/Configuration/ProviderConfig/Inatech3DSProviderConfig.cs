﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cashier.Configuration.ProviderConfig
{
    internal class Inatech3DSProviderConfig : ProviderConfigBase
    {
        public string ReturnURL { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; ReturnURL: {0}", ReturnURL);
        }
    }
}
