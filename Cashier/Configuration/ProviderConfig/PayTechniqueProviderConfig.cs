﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cashier.Configuration.ProviderConfig
{
    internal class PayTechniqueProviderConfig : ProviderConfigBase
    {
        public string ChannelID { get; set; }
        public string SoapAction { get; set; }
        public string ReturnUrl { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; ChannelID: {0}; SoapAction: {1}; ReturnUrl: {2}", ChannelID, SoapAction, ReturnUrl);
        }
    }
}
