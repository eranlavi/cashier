﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cashier.Configuration.ProviderConfig
{
    internal class AstechProviderConfig : ProviderConfigBase
    {
        public string SoapAction { get; set; }
        public string StoreId { get; set; }
        public string BrandId { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; SoapAction: {0}; StoreId: {1}; BrandId: {2}", SoapAction, StoreId, BrandId);
        }
    }
}
