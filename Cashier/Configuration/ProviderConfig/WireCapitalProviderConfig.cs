﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cashier.Configuration.ProviderConfig
{
    class WireCapitalProviderConfig : ProviderConfigBase
    {
        public string UrlExecute { get; set; }
        public string UrlGetStatus { get; set; }
        public string MerchantAccountID { get; set; }
        public string MerchantAccountUserName { get; set; }
        public string MerchantAccountPassword { get; set; }
        public string GetStatusTimeoutSeconds { get; set; }
        public string ReturnUrl { get; set; }
        public string CancelUrl { get; set; }
        //public string CallbackUrl { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; UrlExecute: {0}; UrlGetStatus: {1}; MerchantAccountUserName: {2}; MerchantAccountPassword: {3}; MerchantAccountID: {4}; GetStatusTimeoutSeconds: {5}; ReturnUrl: {6}; CancelUrl: {6}; CallbackUrl: {6};", UrlExecute, UrlGetStatus, MerchantAccountUserName, MerchantAccountPassword, MerchantAccountID, GetStatusTimeoutSeconds, ReturnUrl, CancelUrl, CallbackUrl);
        }
    }
}
