﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cashier.Configuration.ProviderConfig
{
    internal class FibonatixProviderConfig : ProviderConfigBase
	{
        public string CONTROLKEY { get; set; }
        public string ENDPOINT { get; set; }
        public string CallbackUrl { get; set; }
        public string ReturnUrl { get; set; }
        public string SaleUrl { get; set; }
        public string StatusUrl { get; set; }

		public override string ToString()
		{
            return base.ToString() + string.Format("; Control Key: {0}; End Point: {1}; CallbackUrl: {2}; ReturnUrl: {3}; SaleUrl: {4}; StatusUrl: {5}", CONTROLKEY, ENDPOINT, CallbackUrl, ReturnUrl, SaleUrl, StatusUrl);
		}
	}
}
