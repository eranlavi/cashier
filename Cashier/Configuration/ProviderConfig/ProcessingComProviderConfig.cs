﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cashier.Configuration.ProviderConfig
{
    internal class ProcessingComProviderConfig : ProviderConfigBase
    {
        public string Mid { get; set; }
        public string MidQ { get; set; }
        public string MidCombined { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format("; Mid: {0}; MidQ: {1}; MidCombined: {2}", Mid, MidQ, MidCombined);
        }

    }
}
