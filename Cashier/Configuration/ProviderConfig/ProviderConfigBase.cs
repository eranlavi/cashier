﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cashier.Configuration.ProviderConfig
{
    internal class ProviderConfigBase
    {
        public Mode Mode { get; set; }
        public int Percentage { get; set; }
        public int Level { get; set; }
        public string Url { get; set; }
        public string Url2 { get; set; }
        public string MerchantId { get; set; }
        public string MerchantPassword { get; set; }
        public string PathToLogFile { get; set; }
        public string CallbackUrl { get; set; }
        
        public override string ToString()
        {
            return string.Format("Mode: {0}; Percentage: {1}; Level: {2}; Url: {3}; MerchatId: {4}; MerchantPassword: {5}; CallbackUrl: {6}; Url2: {7}; PathToLogFile: {8}",
                Mode.ToString(), Percentage.ToString(), Level.ToString(), Url, MerchantId, MerchantPassword, CallbackUrl, Url2, PathToLogFile);
        }
    }
}
