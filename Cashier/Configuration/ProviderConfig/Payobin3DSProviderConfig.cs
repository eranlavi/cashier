﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cashier.Configuration.ProviderConfig
{
    internal class Payobin3DSProviderConfig : ProviderConfigBase
    {
        public string Servicekey { get; set; }
        public string Routingkey { get; set; }
        public string RedirectUrl { get; set; }
        public string CallbackUrl { get; set; }
        public string RoutingKeyCombined { get; set; }
        
        public override string ToString()
        {
            return base.ToString() + string.Format("; Servicekey: {0}; Routingkey: {1}; RoutingKeyCombined: {2}; RedirectUrl: {3}; CallbackUrl: {4}", Servicekey, Routingkey, RoutingKeyCombined, RedirectUrl, CallbackUrl);
        }
    }
}
