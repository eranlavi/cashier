﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.ProviderConfig;
using System.Xml;

namespace Cashier.Configuration.Parsing
{
    class AstechProviderConfigParser : ProviderConfigParserBase
    {
        #region Const
        private const string ATTRIBUTE_SOAPACTION = "SoapAction";
        private const string ATTRIBUTE_STOREID = "StoreId";
        private const string ATTRIBUTE_BRANDID = "BrandId";
        #endregion

        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);
            AstechProviderConfig config = new AstechProviderConfig
            {
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            XmlNode settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_SOAPACTION];
                config.SoapAction = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_STOREID];
                config.StoreId = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_BRANDID];
                config.BrandId = attr != null ? attr.Value : string.Empty;
            }

            return config;
        }
    }
}
