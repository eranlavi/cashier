﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cashier.Configuration.ProviderConfig;
using System.Xml;

namespace Cashier.Configuration.Parsing
{
    internal class FibonatixProviderConfigParser : ProviderConfigParserBase
    {
        protected const string ATTRIBUTE_CONTROLKEY = "CONTROLKEY";
        protected const string ATTRIBUTE_ENDPOINT = "ENDPOINT";
        protected const string ATTRIBUTE_CALLBACKURL = "CallbackUrl";
        protected const string ATTRIBUTE_RETURNURL = "ReturnUrl";

        protected const string ATTRIBUTE_SALEURL = "SaleUrl";
        protected const string ATTRIBUTE_STATUSURL = "StatusUrl";

        
        public override ProviderConfigBase ParseProviderConfig(XmlNode providerNode)
        {
            var baseConfig = base.ParseProviderConfig(providerNode);

            var config = new FibonatixProviderConfig
            { 
                Mode = baseConfig.Mode,
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            var strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            var settingNode = providerNode[strMode];
            XmlNode attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_CONTROLKEY];
                config.CONTROLKEY = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_ENDPOINT];
                config.ENDPOINT = attr != null ? attr.Value : string.Empty;

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_CALLBACKURL];
                    config.CallbackUrl = attr != null ? attr.Value : string.Empty;
                }
                catch { config.CallbackUrl = string.Empty; }

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_RETURNURL];
                    config.ReturnUrl = attr != null ? attr.Value : string.Empty;
                }
                catch { config.ReturnUrl = string.Empty; }

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_SALEURL];
                    config.SaleUrl = attr != null ? attr.Value : string.Empty;
                }
                catch { config.SaleUrl = string.Empty; }

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_STATUSURL];
                    config.StatusUrl = attr != null ? attr.Value : string.Empty;
                }
                catch { config.StatusUrl = string.Empty; }
            }

            return config;
        }
    }
}
