﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.ProviderConfig;
using System.Xml;


namespace Cashier.Configuration.Parsing
{
    class MerchantsWorldWideProviderConfigParser : ProviderConfigParserBase
    {
        #region Const
        private const string ATTRIBUTE_TERMINALID = "TerminalId";
        private const string ATTRIBUTE_PRIVATEKEY = "PrivateKey";
        private const string ATTRIBUTE_CALLBACKURL = "CallbackUrl";
        private const string ATTRIBUTE_TRMCODE = "TrmCode";
        #endregion

        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);
            MerchantsWorldWideProviderConfig config = new MerchantsWorldWideProviderConfig
            {
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            XmlNode settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_TERMINALID];
                config.TerminalId = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_PRIVATEKEY];
                config.PrivateKey = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_CALLBACKURL];
                config.CallbackUrl = attr != null ? attr.Value : string.Empty;

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_TRMCODE];
                    config.TrmCode = attr != null ? attr.Value : string.Empty;
                }
                catch
                {
                    config.TrmCode = "";
                }
            }

            return config;
        }
    }
}
