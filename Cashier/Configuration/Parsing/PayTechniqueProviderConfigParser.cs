﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Xml;

namespace Cashier.Configuration.Parsing
{
    class PayTechniqueProviderConfigParser : ProviderConfigParserBase
    {
        #region Const
        private const string ATTRIBUTE_CHANNELID = "ChannelID";
        private const string ATTRIBUTE_SOAPACTION = "SoapAction";
        private const string ATTRIBUTE_RETURNURL = "ReturnUrl";     
        #endregion
        
        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);
            PayTechniqueProviderConfig config = new PayTechniqueProviderConfig
            {
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            XmlNode settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_CHANNELID];
                config.ChannelID = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_SOAPACTION];
                config.SoapAction = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_RETURNURL];
                config.ReturnUrl = attr != null ? attr.Value : string.Empty;
            }

            return config;
        }
    }
}
