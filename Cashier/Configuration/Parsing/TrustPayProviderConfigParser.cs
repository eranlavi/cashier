﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cashier.Configuration.ProviderConfig;
using System.Xml;

namespace Cashier.Configuration.Parsing
{
    internal class TrustPayProviderConfigParser : ProviderConfigParserBase
    {
        private const string ATTRIBUTE_SENDER = "Sender";
        private const string ATTRIBUTE_CHANNEL = "Channel";

        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            var baseConfig = base.ParseProviderConfig(providerNode);
            var config = new TrustPayProviderConfig
            { 
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            var strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            var settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_SENDER];
                config.Sender = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_CHANNEL];
                config.Channel = attr != null ? attr.Value : string.Empty;
            }

            return config;
        }
    }
}
