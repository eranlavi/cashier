﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Cashier.Configuration.ProviderConfig;

namespace Cashier.Configuration.Parsing
{
	internal class SafeChargeProviderConfigParser: ProviderConfigParserBase
	{
		#region Const
		
		protected const string ATTRIBUTE_SITE_ID = "SiteId";
		
		#endregion
		
		public override ProviderConfigBase ParseProviderConfig(XmlNode providerNode)
		{
			ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);

			SafeChargeProviderConfig config = new SafeChargeProviderConfig { 
				Mode = baseConfig.Mode,
 				Url = baseConfig.Url,
				MerchantId = baseConfig.MerchantId,
				MerchantPassword = baseConfig.MerchantPassword,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
			};
						
			string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

			XmlNode settingNode = providerNode[strMode];
			XmlNode attr;
			if (settingNode != null)
			{
				attr = settingNode.Attributes[ATTRIBUTE_SITE_ID];
				config.SiteId = attr != null ? attr.Value : string.Empty;
			}

			return config;
		}
	}
}
