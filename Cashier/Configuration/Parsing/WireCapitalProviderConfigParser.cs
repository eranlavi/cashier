﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Xml;

namespace Cashier.Configuration.Parsing
{
    class WireCapitalProviderConfigParser : ProviderConfigParserBase
    {
        #region Const
        private const string ATTRIBUTE_URLEXECUTE = "UrlExecute";
        private const string ATTRIBUTE_URLGETSTATUS = "UrlGetStatus";
        private const string ATTRIBUTE_MERCHANTACCOUNTUSERNAME = "MerchantAccountUserName";
        private const string ATTRIBUTE_MERCHANTACCOUNTPASSWORD = "MerchantAccountPassword";
        private const string ATTRIBUTE_MERCHANTACCOUNTID = "MerchantAccountID";
        private const string ATTRIBUTE_GETSTATUSTIMEOUTSECONDS = "GetStatusTimeoutSeconds";
        private const string ATTRIBUTE_RETURNURL = "ReturnUrl";
        private const string ATTRIBUTE_CANCELURL = "CancelUrl";
        private const string ATTRIBUTE_CALLBACKURL = "CallbackUrl";
        #endregion

        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);
            WireCapitalProviderConfig config = new WireCapitalProviderConfig
            {
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            XmlNode settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_URLEXECUTE];
                config.UrlExecute = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_URLGETSTATUS];
                config.UrlGetStatus = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_MERCHANTACCOUNTUSERNAME];
                config.MerchantAccountUserName = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_MERCHANTACCOUNTPASSWORD];
                config.MerchantAccountPassword = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_MERCHANTACCOUNTID];
                config.MerchantAccountID = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_GETSTATUSTIMEOUTSECONDS];
                config.GetStatusTimeoutSeconds = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_RETURNURL];
                config.ReturnUrl = attr != null ? attr.Value : string.Empty;

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_CANCELURL];
                    config.CancelUrl = attr != null ? attr.Value : string.Empty;
                }
                catch { config.CancelUrl = ""; }

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_CALLBACKURL];
                    config.CallbackUrl = attr != null ? attr.Value : string.Empty;
                }
                catch { config.CallbackUrl = ""; }
            }

            return config;
        }
    }
}
