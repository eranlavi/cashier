﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Xml;

namespace Cashier.Configuration.Parsing
{
    class Payobin3DSProviderConfigParser : ProviderConfigParserBase
    {
        #region Const
        private const string ATTRIBUTE_SERVICEKEY = "Servicekey";
        private const string ATTRIBUTE_ROUTINGKEY = "Routingkey";
        private const string ATTRIBUTE_REDIRECTURL = "RedirectUrl";
        private const string ATTRIBUTE_CALLBACKURL = "CallbackUrl";
        private const string ATTRIBUTE_ROUTINGKEYCOMBINED = "RoutingKeyCombined"; 
        #endregion

        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);
            Payobin3DSProviderConfig config = new Payobin3DSProviderConfig
            {
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            XmlNode settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_SERVICEKEY];
                config.Servicekey = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_ROUTINGKEY];
                config.Routingkey = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_REDIRECTURL];
                config.RedirectUrl = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_CALLBACKURL];
                config.CallbackUrl = attr != null ? attr.Value : string.Empty;

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_ROUTINGKEYCOMBINED];
                    config.RoutingKeyCombined = attr != null ? attr.Value : string.Empty;
                }
                catch { }
            }

            return config;
        }
    }
}
