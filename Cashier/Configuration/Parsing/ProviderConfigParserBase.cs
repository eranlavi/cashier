﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Cashier.Configuration.ProviderConfig;


namespace Cashier.Configuration.Parsing
{
	internal class ProviderConfigParserBase
	{
		#region Const
		
		protected const string ATTRIBUTE_MODE = "Mode";
		protected const string ATTRIBUTE_URL = "Url";
		protected const string ATTRIBUTE_MERCHANT_ID = "MerchantId";
		protected const string ATTRIBUTE_MERCHANT_PASSWORD = "MerchantPassword";
		protected const string ELEMENT_TEST = "Test";
		protected const string ELEMENT_LIVE = "Live";
        protected const string ELEMENT_PERCENTAGE = "percentage";
        protected const string ATTRIBUTE_CALLBACK_URL = "CallbackUrl";
        protected const string ATTRIBUTE_URL2 = "Url2";
	
		#endregion
		
		public virtual ProviderConfigBase ParseProviderConfig(XmlNode providerNode)
		{
			var providerConfig  = new ProviderConfigBase();

			if (providerNode == null)
			{
				throw new ArgumentNullException("'providerNode' parameter cannot be null");
			}
						
			string strMode = providerNode.Attributes[ATTRIBUTE_MODE].Value;
            //System.IO.File.AppendAllText("C:\\logs\\Cashier\\CashierLog.txt", DateTime.Now.ToString() + ": Cashier.CashierConfigurator: providerType:  " + strMode + "\r\n");							
			providerConfig.Mode = (Mode) Enum.Parse(typeof(Mode), strMode.ToUpper());

            if (providerNode.Attributes["percentage"] is XmlAttribute)
                providerConfig.Percentage = Convert.ToInt32(providerNode.Attributes["percentage"].Value);
            else
                providerConfig.Percentage = 0;

            if (providerNode.Attributes["level"] is XmlAttribute)
                providerConfig.Level = Convert.ToInt32(providerNode.Attributes["level"].Value);
            else
                providerConfig.Level = 0;
           
			XmlNode settingsNode = providerNode[strMode];
			XmlAttribute configItemAttr;

			if (settingsNode != null)
			{
				configItemAttr = settingsNode.Attributes[ATTRIBUTE_URL];
				providerConfig.Url = configItemAttr != null ? configItemAttr.Value : string.Empty;

				configItemAttr = settingsNode.Attributes[ATTRIBUTE_MERCHANT_ID];
				providerConfig.MerchantId = configItemAttr != null ? configItemAttr.Value : string.Empty;

				configItemAttr = settingsNode.Attributes[ATTRIBUTE_MERCHANT_PASSWORD];
				providerConfig.MerchantPassword = configItemAttr != null ? configItemAttr.Value : string.Empty;

                try
                {
                    configItemAttr = settingsNode.Attributes[ATTRIBUTE_CALLBACK_URL];
                    providerConfig.CallbackUrl = configItemAttr != null ? configItemAttr.Value : string.Empty;
                }
                catch
                {
                    providerConfig.CallbackUrl = string.Empty;
                }

                try
                {
                    configItemAttr = settingsNode.Attributes[ATTRIBUTE_URL2];
                    providerConfig.Url2 = configItemAttr != null ? configItemAttr.Value : string.Empty;
                }
                catch
                {
                    providerConfig.Url2 = string.Empty;
                }
			}
						
			return providerConfig;
		}
	}
}
