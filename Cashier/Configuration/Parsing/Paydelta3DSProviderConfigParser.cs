﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Xml;

namespace Cashier.Configuration.Parsing
{
    class Paydelta3DSProviderConfigParser : ProviderConfigParserBase
    {
        #region Const
        private const string ATTRIBUTE_PRODUCTNAME = "ProduceName";
        private const string ATTRIBUTE_PRODUCTURL = "ProduceURL";        
        private const string ATTRIBUTE_CALLBACKURL = "CallbackUrl";
        #endregion

        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);
            Paydelta3DSProviderConfig config = new Paydelta3DSProviderConfig
            {
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            XmlNode settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {                
                attr = settingNode.Attributes[ATTRIBUTE_PRODUCTNAME];
                config.ProduceName = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_PRODUCTURL];
                config.ProduceURL = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_CALLBACKURL];
                config.CallbackUrl = attr != null ? attr.Value : string.Empty;
            }

            return config;
        }
    }
}
