﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.Parsing;
using CashierUtils;

namespace Cashier.Configuration.Parsing
{
    internal class ProviderConfigParserFactory
    {
        public ProviderConfigParserBase GetParser(Globals.Providers providerType)
        {
            switch (providerType)
            {
                case Globals.Providers.SAFECHARGE:
                    return new SafeChargeProviderConfigParser();
                case Globals.Providers.SOLID:
                case Globals.Providers.SOLID_BURGON:
                case Globals.Providers.SWISH:
                case Globals.Providers.ACAPTURE:
                    return new SolidProviderConfigParser();
                case Globals.Providers.ASTECH:
                case Globals.Providers.ASTECH3DS:
                    return new AstechProviderConfigParser();

                case Globals.Providers.ALLCHARGE:
                case Globals.Providers.DATACASH:
                case Globals.Providers.EMERCHANTPAY:
                case Globals.Providers.EMERCHANTPAY3DS:
                case Globals.Providers.EMERCHANTPAYTE:
                case Globals.Providers.EMERCHANTPAYTE3DS:
				case Globals.Providers.EMP_KOR:
                case Globals.Providers.EMP_KOR_3D:
                case Globals.Providers.EMP_KOR1:
                case Globals.Providers.EMP_KOR1_3D:
                case Globals.Providers.EMP_TE:
                case Globals.Providers.EMP_TE_3D:
                case Globals.Providers.INATECH:
                case Globals.Providers.INTRX:
                case Globals.Providers.GLOBALCOLLECT:
                case Globals.Providers.SNOW:
                case Globals.Providers.SNOW3DS:
                case Globals.Providers.IMGLOBAL:
                case Globals.Providers.WONDERLANDPAY:
                case Globals.Providers.COALA:
                case Globals.Providers.EMP_BORGUN:
                case Globals.Providers.EMP_BORGUN_3D:
                case Globals.Providers.EMP_TE1:
                case Globals.Providers.EMP_TE1_3D:
                case Globals.Providers.EMP_ECP:
                case Globals.Providers.EMP_ECP_3D:
				case Globals.Providers.ECOMMPAY3DS:
				case Globals.Providers.PAYGATE3DS:
					return new ProviderConfigParserBase();
                case Globals.Providers.ZOTAPAY:
                case Globals.Providers.ZOTAPAY3DS:
                case Globals.Providers.FIBONATIX:
                case Globals.Providers.FIBONATIX3DS:
                case Globals.Providers.ZOTAPAYMYR:
                case Globals.Providers.SOLIDPAYNETEASY:
                case Globals.Providers.SOLIDPAYNETEASY3DS:
                case Globals.Providers.ORANGEPAY:
                case Globals.Providers.ORANGEPAY3DS:
                case Globals.Providers.NASPAY:
                case Globals.Providers.NASPAY3DS:
                case Globals.Providers.NOPROBLEMPAY:
                case Globals.Providers.NOPROBLEMPAY3DS:
                    return new FibonatixProviderConfigParser();
                case Globals.Providers.YOUPAY:
                    return new YouPayProviderConfigParser();

                case Globals.Providers.TRUSTPAY:
                    return new TrustPayProviderConfigParser();
                case Globals.Providers.MERCHANTSWORLDWIDE:
                case Globals.Providers.MEIKOPAY:
                case Globals.Providers.MEIKOPAY3DS:
                case Globals.Providers.GPN:
                case Globals.Providers.GPN3DS:
                case Globals.Providers.NETPAY:
                case Globals.Providers.NETPAY3DS:
                case Globals.Providers.DREAMSPAY:
                case Globals.Providers.WIRECARD:
                case Globals.Providers.WIRECARD3DS:
                case Globals.Providers.NETPAY_PAYOBIN:
                case Globals.Providers.NETPAY_PAYOBIN_3D:
                    return new MerchantsWorldWideProviderConfigParser();
                case Globals.Providers.PAYOBIN:
                case Globals.Providers.PAYOBIN3DS:
                case Globals.Providers.WIROPAY:
                case Globals.Providers.WIROPAY3DS:
                    return new Payobin3DSProviderConfigParser();
                case Globals.Providers.PAYTECHNIQUE:
                case Globals.Providers.PAYTECHNIQUE3DS:
                    return new PayTechniqueProviderConfigParser();
                case Globals.Providers.INATECH3DS:
                    return new Inatech3DSProviderConfigParser();
                case Globals.Providers.S4UCASH3DS:
                case Globals.Providers.SOLID3DS:
                case Globals.Providers.SWISH3DS:
				case Globals.Providers.TRUEVO3DS:
				case Globals.Providers.ACAPTURE3DS:
                case Globals.Providers.IPAYMENT:
                case Globals.Providers.IPAYMENT3DS:
                    return new Solid3DSProviderConfigParser();
                case Globals.Providers.PAYDELTA3DS:
                case Globals.Providers.PAYDELTA:
                    return new Paydelta3DSProviderConfigParser();
                case Globals.Providers.PROCESSINGCOM:
                case Globals.Providers.PROCESSINGCOM3DS:
                    return new ProcessingComProviderConfigParser();
                case Globals.Providers.WIRECAPITAL:
                case Globals.Providers.CERTUS:
                case Globals.Providers.MASTERPAY:
				case Globals.Providers.EMERCHANTPAYGENESIS:
				case Globals.Providers.EMERCHANTPAYGENESIS3DS:
					return new WireCapitalProviderConfigParser();


                default:
                    throw new ArgumentOutOfRangeException(providerType.ToString(),
                        string.Format("Parser for {0} provider type has not been implemented yet!", providerType.ToString()));
            }
        }
    }
}
