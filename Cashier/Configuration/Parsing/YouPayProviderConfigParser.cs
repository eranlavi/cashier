﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.ProviderConfig;
using System.Xml;


namespace Cashier.Configuration.Parsing
{
    class YouPayProviderConfigParser : ProviderConfigParserBase
    {
        #region Const
        private const string ATTRIBUTE_SENDER = "Sender";
        private const string ATTRIBUTE_CHANNEL = "Channel";
        #endregion

        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);
            YouPayProviderConfig config = new YouPayProviderConfig
            {
                Url = baseConfig.Url,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            XmlNode settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_SENDER];
                config.Sender = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_CHANNEL];
                config.Channel = attr != null ? attr.Value : string.Empty;
            }

            return config;
        }

    }
}
