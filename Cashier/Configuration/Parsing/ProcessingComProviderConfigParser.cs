﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Xml;

namespace Cashier.Configuration.Parsing
{
    class ProcessingComProviderConfigParser : ProviderConfigParserBase
    {
        #region Const
        private const string ATTRIBUTE_MID = "mid";
        private const string ATTRIBUTE_MIDQ = "mid_q";
        private const string ATTRIBUTE_MIDCOMBINED = "MidCombined";
        private const string ATTRIBUTE_CALLBACKURL = "CallbackUrl";
        #endregion
        
        public override ProviderConfig.ProviderConfigBase ParseProviderConfig(System.Xml.XmlNode providerNode)
        {
            ProviderConfigBase baseConfig = base.ParseProviderConfig(providerNode);
            ProcessingComProviderConfig config = new ProcessingComProviderConfig
            {
                Url = baseConfig.Url,
                Url2 = baseConfig.Url2,
                MerchantId = baseConfig.MerchantId,
                MerchantPassword = baseConfig.MerchantPassword,
                PathToLogFile = baseConfig.PathToLogFile,
                Mode = baseConfig.Mode,
                Percentage = baseConfig.Percentage,
                Level = baseConfig.Level
            };

            string strMode = config.Mode == Mode.LIVE ? ELEMENT_LIVE : ELEMENT_TEST;

            XmlNode settingNode = providerNode[strMode];
            XmlAttribute attr;
            if (settingNode != null)
            {
                attr = settingNode.Attributes[ATTRIBUTE_MID];
                config.Mid = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_MIDQ];
                config.MidQ = attr != null ? attr.Value : string.Empty;

                attr = settingNode.Attributes[ATTRIBUTE_MIDCOMBINED];
                config.MidCombined = attr != null ? attr.Value : string.Empty;

                try
                {
                    attr = settingNode.Attributes[ATTRIBUTE_CALLBACKURL];
                    config.CallbackUrl = attr != null ? attr.Value : string.Empty;
                }
                catch { }
            }

            return config;
        }
    }
}
