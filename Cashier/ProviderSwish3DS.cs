﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using System.Xml;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using CashierUtils;

namespace Cashier
{
    class ProviderSwish3DS : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        //private Dictionary<string, string> ResultCodeTable { get; set; }

        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            {"visa", "VISA"},
            {"Visa", "VISA"},
            {"VISA", "VISA"},
            {"mastercard", "MASTER"},
            {"Mastercard", "MASTER"},
            {"MasterCard", "MASTER"},
            { "ELO","ELO"},
            { "JCB","JB"},
            { "amex", "AMEX"},
            { "american express", "AMEX"},
            {"American Express", "AMEX"},
            {"VPAY", "VPAY"},
            {"AXESS", "AXESS"},
            {"BONUS", "BONUS"},
            {"CABAL", "CABAL"},
            {"WORLD", "WORLD"},
            {"diners club", "DINERS"},
            {"dinersclub", "DINERS"},
            {"dinersclubintl", "DINERS"},
            {"Diners Club", "DINERS"},
            {"BITCOIN", "BITCOIN"},
            {"DANKORT", "DANKORT"},
            {"maestro","MAESTRO"},
            {"Maestro","MAESTRO"},
            {"MAXIMUM", "MAXIMUM"},
            {"AFTERPAY", "AFTERPAY"},
            {"ASYACARDb", "ASYACARD"},
            {"Discover","DISCOVER"},
            {"discover","DISCOVER"},
            {"POSTEPAY", "POSTEPAY"},
            {"SERVIRED", "SERVIRED"},
            {"HIPERCARD", "HIPERCARD"},
            {"VISADEBIT", "VISADEBIT"},
            {"CARDFINANS", "CARDFINANS"},
            {"CARTEBLEUE", "CARTEBLEUE"},
            {"VISAELECTRON", "VISAELECTRON"},
            {"CARTEBANCAIRE", "CARTEBANCAIRE"},
            {"KLARNA_INVOICE", "KLARNA_INVOICE"},
            {"DIRECTDEBIT_SEPA", "DIRECTDEBIT_SEPA"}
        };
        private const string UNDEFINED = "undefined";
        private string channel;
        private string returnUrl;
        private string sender;

        #region Constructor

        public ProviderSwish3DS(ProviderConfigBase config)
            : base(config)
        {
            Solid3DSProviderConfig swish3DSConfig = config as Solid3DSProviderConfig;
            if (swish3DSConfig == null)
            {
                throw new ArgumentException("Swish3DSProvider expects to get Solid3DSProviderConfig object!");
            }
            channel = swish3DSConfig.Channel;
            returnUrl = swish3DSConfig.ReturnURL;
            sender = swish3DSConfig.Sender;
        }
        #endregion
        private Globals.Providers providerType;

        protected override Globals.Providers ProviderType
        {
            get { return providerType; }
            set { providerType = value; }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            ProviderType = _DepositRequest.provider;

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = ProviderType;

            string TransactionID = Guid.NewGuid().ToString().Replace("-", "");
            _DepositResponse.TransactionID = TransactionID;

            string req = BuildRequest(_DepositRequest, TransactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for " + _DepositRequest.provider;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider={0}; {1} {2}{3}", _DepositRequest.provider, Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider={0}; {1}{2}", _DepositRequest.provider, e.Description, Environment.NewLine));

            if (e.Code < 100)
            {
                _DepositResponse.ErrorDescripotion = string.Format("Http request to {0} failed. Description: {1}", _DepositRequest.provider, e.Description);

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = _DepositResponse.TransactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }

            ResponseObject ResponseObject = null;
            try
            {
                ResponseObject = JSONSerializer<ResponseObject>.DeSerialize(e.Description);
            }
            catch
            {
                _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description = "Fail parsing http PAYMENT response: " + e.Description;
                Log(string.Format("Response Deposit: Provider={0}; {1}{2}", _DepositRequest.provider, _DepositResponse.ErrorDescripotion, Environment.NewLine));
                return _DepositResponse;
            }

            var success_regex = @"^(000\.000\.|000\.100\.1|000\.[36])";
            var success_match = Regex.Match(ResponseObject.result.code, success_regex, RegexOptions.IgnoreCase);

            //if (ResponseObject.result.code == "000.000.000" || ResponseObject.result.code == "000.300.000")

            if (success_match.Success)
            {
                //success                
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.ErrorDescripotion = ResponseObject.result.description;
            }
            else
            {
                // if (ResponseObject.result.code.StartsWith("000.200"))
                var regex = @"^(000\.200)";
                var match = Regex.Match(ResponseObject.result.code, regex, RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    try
                    {
                        _DepositResponse.Is3DSecure = true;
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        _DepositResponse.ErrorDescripotion = "";
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                        _DepositResponse.RedirectUrl = ResponseObject.redirect.url;

                        if (ResponseObject.redirect == null)
                        {
                            _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description = "Fail parsing http 3DS PAYMENT response: no redirect params ";
                            Log(string.Format("Response Deposit: Provider={0}; {1}{2}", _DepositRequest.provider, _DepositResponse.ErrorDescripotion, Environment.NewLine));
                            return _DepositResponse;
                        }

                        _DepositResponse.RedirectParameters = new List<RedirectValues>();

                        RedirectValues pareq = new RedirectValues();
                        pareq.Key = "PaReq";
                        pareq.Value = ResponseObject.redirect.parameters.Find(x => x.name.Contains(pareq.Key)).value;
                        _DepositResponse.RedirectParameters.Add(pareq);

                        RedirectValues md = new RedirectValues();
                        md.Key = "MD";
                        md.Value = ResponseObject.redirect.parameters.Find(x => x.name.Contains(md.Key)).value;
                        _DepositResponse.RedirectParameters.Add(md);

                        RedirectValues termurl = new RedirectValues();
                        termurl.Key = "TermUrl";
                        termurl.Value = ResponseObject.redirect.parameters.Find(x => x.name.Contains(termurl.Key)).value;
                        _DepositResponse.RedirectParameters.Add(termurl);

                        // Acapture test gateway will not work if there is no connector parameter!
                        if (_DepositRequest.provider == Globals.Providers.ACAPTURE3DS)
                        {
                            RedirectValues connector = new RedirectValues();
                            connector.Key = "connector";
                            // only in test the response contains the connector parameter.
                            var val = ResponseObject.redirect.parameters.Find(x => x.name.Contains(connector.Key));
                            if (val != null)
                            {
                                connector.Value = ResponseObject.redirect.parameters.Find(x => x.name.Contains(connector.Key)).value;
                                _DepositResponse.RedirectParameters.Add(connector);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _DepositResponse.ErrorCode = "1";
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description = "Fail parsing http 3DS PAYMENT response: " + ex.Message;
                        Log(string.Format("Response Deposit: Provider={0}; {1}{2}", _DepositRequest.provider, _DepositResponse.ErrorDescripotion, Environment.NewLine));
                        return _DepositResponse;
                    }
                }
                else
                {
                    _DepositResponse.ErrorCode = ResponseObject.result.code;
                    _DepositResponse.ErrorDescripotion = ResponseObject.result.code + " , " + ResponseObject.result.description;

                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = _DepositRequest.provider;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = _DepositResponse.TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = _DepositResponse.ErrorDescripotion;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);
                }
            }
            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
            return _DepositResponse;
        }

        //yyyy-MM-dd
        private string GenerateRandomDob()
        {
            Random r = new Random(DateTime.Now.Millisecond + 1);
            int year = DateTime.Now.Year - r.Next(19, 61);
            int month = r.Next(1, 13);
            int day = r.Next(1, 28);

            return string.Format("{0}-{1}-{2}", year, month.ToString("00"), day.ToString("00"));
        }

        private string BuildRequest(DepositRequest _DepositRequest, string TransactionID)
        {
            string amount = _DepositRequest.Amount.ToString("F2");
            string paymentBrand = GetCardBrandByCardType(_DepositRequest.CardType);
            string cardHolderName = System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName + _DepositRequest.LastName);
            string firstName = System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName);
            string lastName = System.Web.HttpUtility.UrlEncode(_DepositRequest.LastName);

            string billingAddress1 = System.Web.HttpUtility.UrlEncode(_DepositRequest.Address);
            string billingCity = System.Web.HttpUtility.UrlEncode(_DepositRequest.City);
            string billingState = (string.IsNullOrEmpty(_DepositRequest.StateISOCode)) ? ("NA") : (_DepositRequest.StateISOCode);

            string cardExpiryMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");

            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string cardExpiryYear = _DepositRequest.CardExpYear;
            if (cardExpiryYear.Length == 2)
                cardExpiryYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string shopperResultUrl = HttpUtility.UrlEncode(returnUrl);

            string req = string.Empty;

            try
            {
                //req = "authentication.userId = 8a8294184e01a2c3014e06888fb10ea7&authentication.password=TGxDp3Mjat" +
                //        "&authentication.entityId=8a8294184e01a2c3014e06888fa30ea3&amount=92.00" +
                //        "&currency=EUR&paymentBrand=VISA&paymentType=DB" +
                //        "&card.number=4200000000000000&card.holder=Jane Jones&card.expiryMonth=05" +
                //        "&card.expiryYear=2018&card.cvv=123";

                req = string.Format(@"authentication.userId={0}&authentication.password={1}&authentication.entityId={2}&amount={3}&currency={4}&paymentBrand={5}&paymentType={6}&card.number={7}&card.holder={8}&card.expiryMonth={9}&card.expiryYear={10}&card.cvv={11}&merchantTransactionId={12}&customer.merchantCustomerId={13}&customer.givenName={14}&customer.surname={15}&customer.phone={16}&customer.email={17}&customer.ip={18}&billing.street1={19}&billing.city={20}&billing.state={21}&billing.postcode={22}&billing.country={23}&shopperResultUrl={24}&customer.middleName={25}&customer.birthDate={26}&risk.merchantWebsite={27}",
                                    MerchantID, MerchantPassword, channel,
                                    amount, _DepositRequest.CurrencyISOCode, paymentBrand,
                                    "DB", _DepositRequest.CardNumber, cardHolderName,
                                    cardExpiryMonth, cardExpiryYear, _DepositRequest.CardCVV2,
                                    TransactionID, _DepositRequest.CustomerID, firstName,
                                    lastName, _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", ""), _DepositRequest.Email,
                                    _DepositRequest.IpAddress, billingAddress1, billingCity,
                                    billingState, _DepositRequest.ZipCode, _DepositRequest.CountryISOCode, shopperResultUrl,
                                     "none", GenerateRandomDob(), sender);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider={0}; Fail in building request string: {1}{2}", _DepositRequest.provider, ex.Message, Environment.NewLine));
            }

            return req;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = new
                System.Net.Security.RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                    resp.Code = 200;
                }

            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        //string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                            resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                        //Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode).ToString() + " ";
                        else
                            resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        #region Response
        [DataContract]
        public class ResponseObject
        {
            [DataMember]
            public string id { get; set; }
            [DataMember]
            public string paymentType { get; set; }
            [DataMember]
            public string paymentBrand { get; set; }
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currency { get; set; }
            [DataMember]
            public string descriptor { get; set; }
            [DataMember]
            public ResponseResult result { get; set; }
            [DataMember]
            public ResponseCard card { get; set; }
            [DataMember]
            public ResponseRisk risk { get; set; }
            [DataMember]
            public ResponseRedirect redirect { get; set; }
            [DataMember]
            public string buildNumber { get; set; }
            [DataMember]
            public string timestamp { get; set; }
            [DataMember]
            public string ndc { get; set; }
        }

        [DataContract]
        public class ResponseResult
        {
            [DataMember]
            public string code { get; set; }
            [DataMember]
            public string description { get; set; }
        }

        [DataContract]
        public class ResponseCard
        {
            [DataMember]
            public string bin { get; set; }
            [DataMember]
            public string last4Digits { get; set; }
            [DataMember]
            public string holder { get; set; }
            [DataMember]
            public string expiryMonth { get; set; }
            [DataMember]
            public string expiryYear { get; set; }
        }

        [DataContract]
        public class ResponseRisk
        {
            [DataMember]
            public string risk { get; set; }
        }

        [DataContract]
        public class ResponseRedirect
        {
            [DataMember]
            public string url { get; set; }
            [DataMember]
            public List<ResponseParam> parameters { get; set; }

        }
        [DataContract]
        public class ResponseParam
        {
            [DataMember]
            public string name { get; set; }
            [DataMember]
            public string value { get; set; }
        }

        #endregion

        private string GetCardBrandByCardType(string cardType)
        {
            string cardBrand = string.Empty;
            cardTypeMapping.TryGetValue(cardType, out cardBrand);
            return cardBrand;
        }
    }
}
