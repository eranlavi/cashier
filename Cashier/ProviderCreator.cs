﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    internal class ProviderCreator : ProviderFactory
    {
        public override ICashierProvider GetProvider(Globals.Providers provider, ProviderConfigBase config)
        {
            switch (provider)
            {
                case Globals.Providers.ALLCHARGE:
                    return new ProviderAllcharge(config);

                case Globals.Providers.NETPAY:
                case Globals.Providers.NETPAY3DS:
                case Globals.Providers.NETPAY_PAYOBIN:
                case Globals.Providers.NETPAY_PAYOBIN_3D:
                    return new ProviderNetpay(config);

                case Globals.Providers.DATACASH:
                    return new ProviderDataCash(config);

                case Globals.Providers.EMERCHANTPAY:
                case Globals.Providers.EMERCHANTPAYTE:
                case Globals.Providers.EMP_KOR:
                case Globals.Providers.EMP_KOR1:
                case Globals.Providers.EMP_TE:
                case Globals.Providers.EMP_BORGUN:
                case Globals.Providers.EMP_TE1:
                case Globals.Providers.EMP_ECP:
                    return new ProviderEMerchantPay(config);

                case Globals.Providers.EMERCHANTPAY3DS:
                case Globals.Providers.EMERCHANTPAYTE3DS:
                case Globals.Providers.EMP_KOR_3D:
                case Globals.Providers.EMP_KOR1_3D:
                case Globals.Providers.EMP_TE_3D:
                case Globals.Providers.EMP_BORGUN_3D:
                case Globals.Providers.EMP_TE1_3D:
                case Globals.Providers.EMP_ECP_3D:
                    return new ProviderEMerchantPay3DS(config);

                case Globals.Providers.SOLID:
                    return new ProviderSolid(config);

                case Globals.Providers.SOLID_BURGON:
                    return new ProviderSolid(config);

                case Globals.Providers.SAFECHARGE:
                    return new ProviderSafeCharge(config);

                case Globals.Providers.INATECH:
                case Globals.Providers.INTRX:
                    return new ProviderInatech(config);

                case Globals.Providers.GLOBALCOLLECT:
                    return new ProviderGlobalCollect(config);

                case Globals.Providers.ASTECH:
                case Globals.Providers.ASTECH3DS:
                    return new ProviderAstech(config);

                case Globals.Providers.ZOTAPAY:
                case Globals.Providers.ZOTAPAY3DS:
                case Globals.Providers.FIBONATIX:
                case Globals.Providers.FIBONATIX3DS:
                case Globals.Providers.ZOTAPAYMYR:
                case Globals.Providers.NOPROBLEMPAY:
                case Globals.Providers.NOPROBLEMPAY3DS:
                    return new ProviderFibonatix(config);

                case Globals.Providers.YOUPAY:
                    return new ProviderYouPay(config);

                case Globals.Providers.TRUSTPAY:
                    return new ProviderTrustPay(config);

                case Globals.Providers.MERCHANTSWORLDWIDE:
                    return new ProviderMerchantsWorldWide(config);

                case Globals.Providers.PAYOBIN:
                case Globals.Providers.PAYOBIN3DS:
                case Globals.Providers.WIROPAY:
                case Globals.Providers.WIROPAY3DS:
                    return new ProviderPayobin3DS(config);

                case Globals.Providers.PAYTECHNIQUE:
                case Globals.Providers.PAYTECHNIQUE3DS:
                    return new ProviderPayTechnique(config);

                case Globals.Providers.INATECH3DS:
                    return new ProviderInatech3DS(config);

                case Globals.Providers.S4UCASH3DS:
                case Globals.Providers.SOLID3DS:
                    return new ProviderSolid3DS(config);

                case Globals.Providers.SNOW:
                case Globals.Providers.SNOW3DS:
                    return new ProviderSnow(config);

                case Globals.Providers.PAYDELTA3DS:
                case Globals.Providers.PAYDELTA:
                    return new ProviderPaydelta3DS(config);

                case Globals.Providers.PROCESSINGCOM:
                    return new ProviderProcessingCom(config);

                case Globals.Providers.PROCESSINGCOM3DS:
                    return new ProviderProcessingCom3DS(config);

                case Globals.Providers.WIRECAPITAL:
                case Globals.Providers.MASTERPAY:
                    return new ProviderWireCapital(config);

                case Globals.Providers.IMGLOBAL:
                    return new ProviderIMGlobal(config);

                case Globals.Providers.CERTUS:
                    return new ProviderCertus(config);

                case Globals.Providers.ACAPTURE:
                case Globals.Providers.SWISH:
                    return new ProviderAcapture(config);

                case Globals.Providers.COALA:
                    return new ProviderCoala(config);

                case Globals.Providers.WONDERLANDPAY:
                    return new ProviderWonderlandPay(config);

                case Globals.Providers.MEIKOPAY:
                case Globals.Providers.MEIKOPAY3DS:
                    return new ProviderMeikoPay(config);

                case Globals.Providers.GPN:
                case Globals.Providers.GPN3DS:
                    return new ProviderGPN(config);

                case Globals.Providers.DREAMSPAY:
                    return new ProviderDreamsPay(config);

                case Globals.Providers.SWISH3DS:
                case Globals.Providers.ACAPTURE3DS:
                    return new ProviderSwish3DS(config);

                case Globals.Providers.WIRECARD:
                    return new ProviderWireCard(config);

                case Globals.Providers.WIRECARD3DS:
                    return new ProviderWireCard3DS(config);

                case Globals.Providers.ECOMMPAY3DS:
                    return new ProviderEcommPay3DS(config);

                case Globals.Providers.PAYGATE:
                case Globals.Providers.PAYGATE3DS:
                    return new ProviderPayGate3DS(config);


                case Globals.Providers.SOLIDPAYNETEASY:
                case Globals.Providers.SOLIDPAYNETEASY3DS:
                    return new ProviderPayNetEasy(config);

                case Globals.Providers.ORANGEPAY:
                case Globals.Providers.ORANGEPAY3DS:
                    return new ProviderOrangePay3DS(config);

                case Globals.Providers.NASPAY:
                case Globals.Providers.NASPAY3DS:
                    return new ProviderNasPay3DS(config);

                case Globals.Providers.IPAYMENT3DS:
                case Globals.Providers.IPAYMENT:
                case Globals.Providers.TRUEVO3DS:
                    return new ProviderTruevo3DS(config);

                case Globals.Providers.EMERCHANTPAYGENESIS:
                case Globals.Providers.EMERCHANTPAYGENESIS3DS:
                    return new ProviderEMerchantPayGenesis3DS(config);

                default:
                    throw new ArgumentOutOfRangeException(provider.ToString(), "Not implemented Provider type");
            }

        }
    }
}
