﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderWonderlandPay : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        public ProviderWonderlandPay(ProviderConfigBase config)
            : base(config)
        {

        }

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.WONDERLANDPAY; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            //_DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = _DepositRequest.provider;

            var dict = MerchantID.Split(new[] { '@' }, StringSplitOptions.RemoveEmptyEntries)
                           .Select(part => part.Split('='))
                           .ToDictionary(split => split[0], split => split[1]);
            string temp = dict[_DepositRequest.CurrencyISOCode.ToUpper()];
            if (!dict.ContainsKey(_DepositRequest.CurrencyISOCode.ToUpper()))
            {
                _DepositResponse.ErrorDescripotion = "The gateway not support currency " + _DepositRequest.CurrencyISOCode;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }
            // Merchant NO;Gateway NO;Key
            string[] cred = temp.Split(';');
            string MerchantNO = cred[0];
            string GatewayNO = cred[1];
            string key = cred[2];

            _DepositResponse.ClearingUserID = MerchantNO;

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string req = BuildRequest(_DepositRequest, transactionID, ExpMonth, ExpYear, MerchantNO, GatewayNO, key);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for: " + _DepositRequest.provider.ToString();
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider={3}; {0} {1}{2}", Url, req, Environment.NewLine, _DepositRequest.provider.ToString()));
            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 30000, 30000);
            Log(string.Format("Response: Provider={2}; {0}{1}", e.Description, Environment.NewLine, _DepositRequest.provider.ToString()));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to " + _DepositRequest.provider.ToString() + " failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
                ParseResponse(e.Description, _DepositResponse, _DepositRequest);

            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

            return _DepositResponse;
        }

        private void ParseResponse(string Xml, DepositResponse _DepositResponse, DepositRequest _DepositRequest)
        {
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.LoadXml(Xml);
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider={2}; XML parse exception: {0}{1}", ex.Message, Environment.NewLine, _DepositRequest.provider.ToString()));

                _DepositResponse.ErrorDescripotion = "Invalid xml response. " + ex.Message;
                return;
            }

            XmlNode receipt = xDoc.SelectSingleNode("//respon/tradeNo");
            if (receipt is XmlNode)
                _DepositResponse.Receipt = receipt.InnerText;

            XmlNode status = xDoc.SelectSingleNode("//respon/orderStatus");
            if (status is XmlNode && status.InnerText == "1") //success
            {                
                _DepositResponse.ErrorDescripotion = "";
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
            }
            else
            {
                XmlNode info = xDoc.SelectSingleNode("//respon/orderInfo");
                if (info is XmlNode)
                    _DepositResponse.ErrorDescripotion = info.InnerText;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            }

        }

        string sha256(string data)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(data), 0, Encoding.UTF8.GetByteCount(data));
            foreach (byte bit in crypto)
            {
                hash.Append(bit.ToString("x2"));
            }
            return hash.ToString();
        }

        private string BuildRequest(DepositRequest _DepositRequest, string transactionID, string ExpMonth, string ExpYear, string MerchantNO, string GatewayNO, string key)
        {            
            try
            {
                string phone = _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", "").Replace(" ", "");

                string signInfo = sha256(MerchantNO + GatewayNO + transactionID + _DepositRequest.CurrencyISOCode.ToUpper() + _DepositRequest.Amount.ToString() + _DepositRequest.CardNumber + ExpYear + ExpMonth + _DepositRequest.CardCVV2 + key);

               var  req = string.Format("merNo={0}&gatewayNo={1}&orderNo={2}&orderCurrency={3}&orderAmount={4}&cardNo={5}&cardExpireMonth={6}&cardExpireYear={7}&cardSecurityCode={8}&firstName={9}&lastName={10}&email={11}&ip={12}&phone={13}&country={14}&city={15}&address={16}&zip={17}&signInfo={18}",
                    MerchantNO, GatewayNO, transactionID, _DepositRequest.CurrencyISOCode.ToUpper(), _DepositRequest.Amount.ToString(), _DepositRequest.CardNumber, ExpMonth, ExpYear, _DepositRequest.CardCVV2,
                    System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName), System.Web.HttpUtility.UrlEncode(_DepositRequest.LastName), System.Web.HttpUtility.UrlEncode(_DepositRequest.Email),
                    _DepositRequest.IpAddress, phone, _DepositRequest.CountryISOCode, System.Web.HttpUtility.UrlEncode(_DepositRequest.City), System.Web.HttpUtility.UrlEncode(_DepositRequest.Address),
                    System.Web.HttpUtility.UrlEncode(_DepositRequest.ZipCode), signInfo);


                //var req = $"merNo={MerchantNO}&gatewayNo={GatewayNO}&orderNo={transactionID}&orderCurrency={_DepositRequest.CurrencyISOCode.ToUpper()}&orderAmount={_DepositRequest.CurrencyISOCode.ToUpper()}&cardNo={_DepositRequest.CardNumber}&cardExpireMonth={ExpMonth}&cardExpireYear={ExpYear}&cardSecurityCode={_DepositRequest.CardCVV2}&firstName={System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName)}&lastName={System.Web.HttpUtility.UrlEncode(_DepositRequest.LastName)}&email={System.Web.HttpUtility.UrlEncode(_DepositRequest.Email)}&ip={_DepositRequest.IpAddress}&phone={phone}&country={_DepositRequest.CountryISOCode}&city={System.Web.HttpUtility.UrlEncode(_DepositRequest.City)}&address={System.Web.HttpUtility.UrlEncode(_DepositRequest.Address)}&zip={System.Web.HttpUtility.UrlEncode(_DepositRequest.ZipCode)}&signInfo={signInfo}";
                return req;
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider={2}; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine, _DepositRequest.provider.ToString()));
                return string.Empty;
            }
            
        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                            Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode).ToString() + " ";
                        resp.Code = -1;
                        resp.Description = Code + responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }
    }

}
