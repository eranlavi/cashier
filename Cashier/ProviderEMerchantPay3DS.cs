﻿using Cashier.Configuration.ProviderConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderEMerchantPay3DS : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }
        
        public ProviderEMerchantPay3DS(ProviderConfigBase config)
            : base(config)
        {
            
        }

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.EMERCHANTPAY3DS; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {            
            string test_transaction = (Mode == Configuration.Mode.TEST) ? ("1") : ("0");

            DepositResponse _DepositResponse = new DepositResponse();

            //_DepositResponse.error = new Cashier.Error();

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = _DepositRequest.provider;

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");            
            string ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString().Substring(2);

            string req = Build3DSRequest(test_transaction, _DepositRequest, transactionID, ExpMonth, ExpYear);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for EMerchantPay3DS";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=EMerchantPay3DS; {0} {1}{2}", Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, _DepositRequest.Host, _DepositRequest.UserAgent, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider=EMerchantPay3DS; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to EMerchantPay3DS failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                if (Parse3DSResponse(e.Description, _DepositResponse, transactionID, _DepositRequest, ExpMonth, ExpYear))
                {
                    req = BuildOrderSubmitRequest(test_transaction, transactionID, _DepositRequest, ExpMonth, ExpYear);

                    Log(string.Format("Request OrderSubmit: Provider=EMerchantPay3DS; {0} {1}{2}", Url2, req, Environment.NewLine));

                    e = HttpReq("POST", Url2, req, _DepositRequest.Host, _DepositRequest.UserAgent, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

                    Log(string.Format("Response OrderSubmit: Provider=EMerchantPay3DS; {0}{1}", e.Description, Environment.NewLine));

                    if (e.Code != 0)
                    {
                        _DepositResponse.ErrorDescripotion = "Http request to EMerchantPay3DS failed. " + e.Description;

                        // fill the class FailedDeposit with details in order to send email to support later
                        FailedDeposit failedDeposit = new FailedDeposit();
                        failedDeposit.provider = _DepositRequest.provider;
                        failedDeposit.Email = _DepositRequest.Email;
                        failedDeposit.TransactionID = transactionID;
                        failedDeposit.Amount = _DepositRequest.Amount;
                        failedDeposit.FirstName = _DepositRequest.FirstName;
                        failedDeposit.LastName = _DepositRequest.LastName;
                        failedDeposit.Description = e.Description;

                        _DepositResponse.FailedDeposits.Add(failedDeposit);
                    }
                    else
                    {
                        ParseOrderSubmitResponseXml(e.Description, _DepositResponse);
                    }
                }
            }

            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

            return _DepositResponse;
        }

        private void ParseOrderSubmitResponseXml(string XML, DepositResponse response)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(XML);
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider=EMerchantPay3DS; Load XML exception: {0}{1}", ex.Message, Environment.NewLine));

                response.ErrorDescripotion = "Invalid xml response. " + ex.Message;
                return;
            }

            XmlElement root = doc.DocumentElement;
                       
            switch (root.LocalName)
            {
                case "order":
                    {
                        response.Amount = GetXmlElementValue(root, "order_total");
                        response.Status = GetXmlElementValue(root, "response_text");
                        response.RetCode = GetXmlElementValue(root, "response_code");
                        //response.TransactionID = transactionID;
                        response.TransactionType = GetXmlElementValue(root, "type");
                        response.Receipt = GetXmlElementValue(root, "trans_id") == "undefined" ? "" : GetXmlElementValue(root, "trans_id");
                        response.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;                        
                    }
                    break;
                case "failure":
                    {
                        response.RetCode = GetXmlElementValue(root, "code");
                        response.ErrorDescripotion = GetXmlElementValue(root, "text");
                        //response.TransactionID = transactionID;
                        response.Receipt = GetXmlElementValue(root, "trans_id") == "undefined" ? "" : GetXmlElementValue(root, "trans_id");
                        //response.error.Description = response.ErrorDescripotion;
                        //response.error.ErrorCode = Globals.ErrorCodes.FAIL;                        
                    }
                    break;
                case "decline":
                    {
                        response.TransactionType = GetXmlElementValue(root, "type");
                        response.RetCode = GetXmlElementValue(root, "response_code");
                        response.ErrorDescripotion = GetXmlElementValue(root, "response_text");
                        //response.TransactionID = transactionID;
                        response.Receipt = GetXmlElementValue(root, "trans_id") == "undefined" ? "" : GetXmlElementValue(root, "trans_id");
                        //response.error.Description = response.ErrorDescripotion;
                        //response.error.ErrorCode = Globals.ErrorCodes.FAIL;                        
                    }
                    break;
                default:
                    {                        
                        response.ErrorDescripotion = "An error has occured when parsing OrderSubmit server response";
                    }
                    break;
            }
           
        }

        private string GetXmlElementValue(XmlElement rootElm, string elmName)
        {
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text
            string resultValue = nodes.Count > 0 ? nodes[0].InnerText : "undefined";
            return resultValue;
        }

        private string BuildOrderSubmitRequest(string test_transaction, string TransactionID, DepositRequest _DepositRequest, string CardExpMonth, string CardExpYear)
        {
            string req = string.Empty;
                        
            try
            {
                string eci = ConvertCardTypeToEci(_DepositRequest.CardType);

                req = string.Format("client_id={0}&api_key={1}&notify=0&payment_type=creditcard&order_reference={2}&order_currency={3}&test_transaction={4}&ip_address={5}&customer_first_name={6}&customer_last_name={7}&customer_address={8}&customer_city={9}&customer_country={10}&customer_postcode={11}&customer_email={12}&customer_phone={13}&item_1_code=Deposit&item_1_qty=1&item_1_predefined=0&item_1_name=Deposit&item_1_digital=1&item_1_unit_price_{14}={15}&card_holder_name={16}&card_number={17}&cvv={18}&exp_month={19}&exp_year={20}&credit_card_trans_type=sale",
                    MerchantID, MerchantPassword, TransactionID, _DepositRequest.CurrencyISOCode, test_transaction, _DepositRequest.IpAddress, _DepositRequest.FirstName, _DepositRequest.LastName, _DepositRequest.Address, _DepositRequest.City, _DepositRequest.CountryISOCode, _DepositRequest.ZipCode, _DepositRequest.Email, _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", ""),
                    _DepositRequest.CurrencyISOCode, _DepositRequest.Amount.ToString(), _DepositRequest.FirstName + " " + _DepositRequest.LastName, _DepositRequest.CardNumber, _DepositRequest.CardCVV2, CardExpMonth, CardExpYear);

                if (!string.IsNullOrEmpty(eci))
                    req = req + "&eci=" + eci;
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on building OrderSubmit request string. Provider=EMerchantPay3DS; {0}{1}", ex.Message, Environment.NewLine));
                req = string.Empty;
            }
            return req;
        }

        private string Build3DSRequest(string test_transaction, DepositRequest _DepositRequest, string TransactionID, string CardExpMonth, string CardExpYear)
        {
            string req = string.Empty;

            try
            {
                req = string.Format("client_id={0}&api_key={1}&reference={2}&callback_url={3}&cardnumber={4}&expdate={5}&amount={6}&currency={7}&test_transaction={8}&browser_useragent={9}&browser_accept={10}",
                    MerchantID, MerchantPassword, TransactionID, CallbackUrl, _DepositRequest.CardNumber, CardExpYear + CardExpMonth, _DepositRequest.Amount.ToString("F2"), _DepositRequest.CurrencyISOCode,
                    test_transaction, _DepositRequest.UserAgent, _DepositRequest.Accept);

                /*req = string.Format("client_id={0}&api_key={1}&reference={2}&callback_url={3}&cardnumber={4}&expdate={5}&amount={6}&currency={7}&test_transaction={8}",
                    MerchantID, MerchantPassword, TransactionID, CallbackUrl, _DepositRequest.CardNumber, CardExpYear + CardExpMonth, _DepositRequest.Amount.ToString(), _DepositRequest.CurrencyISOCode,
                    test_transaction);*/
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on building 3DS request string. Provider=EMerchantPay3DS; {0}{1}", ex.Message, Environment.NewLine));
                req = string.Empty;
            }
            return req;
        }

        private Error HttpReq(string Method, string Url, string Parameters, string Host, string UserAgent, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();
            resp.Code = 0;

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = new
                System.Net.Security.RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;
                if (!string.IsNullOrEmpty(Host))
                    req.Host = Host;
                if (!string.IsNullOrEmpty(UserAgent))
                    req.UserAgent = UserAgent;
                
                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                        {
                            resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                            Code = resp.Code.ToString() + " ";
                        }
                        else
                            resp.Code = -1;

                        resp.Description = Code + responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private bool Parse3DSResponse(string Xml, DepositResponse _DepositResponse, string TransactionID, DepositRequest _DepositRequest, string CardExpMonth, string CardExpYear)
        {
            bool ret = false;
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.LoadXml(Xml);
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider=EMerchantPay3DS; Load XML exception: {0}{1}", ex.Message, Environment.NewLine));

                _DepositResponse.ErrorDescripotion = "Invalid xml response. " + ex.Message;
                return ret;
            }
            
            try
            {
                //<failure><errors><error><code>OP903</code><text>Authentication failure</text></error></errors></failure>
                if (Xml.Contains("<failure><errors>"))
                {
                    XmlNode nodeError = xDoc.SelectSingleNode("//failure/errors/error/text");
                    if (nodeError is XmlNode && !string.IsNullOrEmpty(nodeError.InnerText))
                        _DepositResponse.ErrorDescripotion = nodeError.InnerText;
                    else
                        _DepositResponse.ErrorDescripotion = Xml;
                    return ret;
                }

                if (Xml.Contains("<error><description>"))
                {
                    XmlNode nodeError = xDoc.SelectSingleNode("//error/description");
                    if (nodeError is XmlNode && !string.IsNullOrEmpty(nodeError.InnerText))
                        _DepositResponse.ErrorDescripotion = nodeError.InnerText;
                    else
                        _DepositResponse.ErrorDescripotion = Xml;
                    return ret;
                }

                XmlNode node = xDoc.SelectSingleNode("//response/enrollmentstatus");
                string status = node.InnerText.Trim();
                switch (status)
                {
                    case "Y":
                        {
                            node = xDoc.SelectSingleNode("//response/bouncerURL");
                            if (node is XmlNode && !string.IsNullOrEmpty(node.InnerText))
                            {
                                string bounceURL = node.InnerText;

                                node = xDoc.SelectSingleNode("//response/requestid");
                                if (node is XmlNode && !string.IsNullOrEmpty(node.InnerText))
                                {
                                    _DepositResponse.ErrorDescripotion = "";
                                    _DepositResponse.ErrorCode = "0";
                                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                                    _DepositResponse.Is3DSecure = true;
                                    _DepositResponse.RedirectUrl = bounceURL;

                                    _DepositResponse.DataToSave = _DepositRequest.CurrencyISOCode + "<SEP>";
                                    _DepositResponse.DataToSave += TransactionID + "<SEP>";
                                    _DepositResponse.DataToSave += node.InnerText + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.Email + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.Amount.ToString() + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.IpAddress + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.FirstName + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.LastName + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.Address + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.City + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.CountryISOCode + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.ZipCode + "<SEP>";                                    
                                    _DepositResponse.DataToSave += _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", "") + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.CardNumber + "<SEP>";
                                    _DepositResponse.DataToSave += _DepositRequest.CardCVV2 + "<SEP>";
                                    _DepositResponse.DataToSave += CardExpMonth + "<SEP>";
                                    _DepositResponse.DataToSave += CardExpYear;
                                    _DepositResponse.DataToSave = _DepositResponse.DataToSave.Replace("'", "");
                                }
                                else
                                {
                                    _DepositResponse.ErrorDescripotion = "Card is enrolled for 3DS but requestid tag is missing";
                                }
                            }
                            else
                            {
                                _DepositResponse.ErrorDescripotion = "Card is enrolled for 3DS but bouncerURL tag is missing";
                            }
                        }
                        break;
                    case "N":
                        {
                            ret = true;
                        }
                        break;
                    case "U":
                        {
                            _DepositResponse.ErrorDescripotion = "Unable to Authenticate or Card Not Eligible for Attempts";
                        }
                        break;
                    case "E":
                        {
                            _DepositResponse.ErrorDescripotion = "Enrolment status could not be retrieved";
                        }
                        break;
                }                                
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider=EMerchantPay3DS; Parsing xml response exception: {0}{1}", ex.Message, Environment.NewLine));

                _DepositResponse.ErrorDescripotion = "Parsing xml response exception. " + ex.Message;
            }

            return ret;

        }


        private string ConvertCardTypeToEci(string Card)
        {
            switch (Card)
            {
                case "visa":
                case "Visa":
                case "VISA":
                    return "06";               
                case "mastercard":
                case "Mastercard":
                case "MASTERCARD":
                    return "01";                                
                default:
                    return string.Empty;
            }
        }


    }
}
