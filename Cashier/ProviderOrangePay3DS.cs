﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using System.Xml;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using CashierUtils;

namespace Cashier
{
    class ProviderOrangePay3DS : ProviderBase
    {
        public class Error
        {
            public int Code;
            public string Description;
        }

        #region Create Charge
        //Request

        [DataContract]
        public class ChargeDirectRequest
        {
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currency { get; set; }
            [DataMember]
            public string description { get; set; }
            [DataMember]
            public string email { get; set; }
            [DataMember]
            public string pay_method { get; set; }
            [DataMember]
            public string ip_address { get; set; }
            [DataMember]
            public string name { get; set; }
            [DataMember]
            public string card_number { get; set; }
            [DataMember]
            public string card_expiry_month { get; set; }
            [DataMember]
            public string card_expiry_year { get; set; }
            [DataMember]
            public string card_csc { get; set; }
            [DataMember]
            public string reference_id { get; set; }
        }

        //Response 
        [DataContract]
        public class ChargeDirectResponse
        {            
            [DataMember]
            public DataMembers data { get; set; }
            [DataMember]
            public Meta meta { get; set; }
        }
        [DataContract]
        public class DataMembers
        {
            [DataMember]
            public Charge charge { get; set; }
            [DataMember]
            public Links links { get; set; }
        }
        [DataContract]
        public class Charge
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string id { get; set; }
            [DataMember]
            public ChargeAttributes attributes { get; set; }
        }
        [DataContract]
        public class ChargeAttributes
        {
            [DataMember]
            public string livemode { get; set; }
            [DataMember]
            public string status { get; set; }
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string fee { get; set; }
            [DataMember]
            public string total_amount { get; set; }
            [DataMember]
            public string currency { get; set; }
            [DataMember]
            public string pay_method { get; set; }
            [DataMember]
            public string description { get; set; }
            [DataMember]
            public AttributeSource source { get; set; }
            [DataMember]
            public AttributeFailure failure { get; set; }
            [DataMember]
            public string reference_id { get; set; }
            [DataMember]
            public int created_at { get; set; }
            [DataMember]
            public int updated_at { get; set; }
            [DataMember]
            public int valid_till { get; set; }
        }
        [DataContract]
        public class AttributeSource
        {
            [DataMember]
            public string email { get; set; }
            [DataMember]
            public string ip_address { get; set; }
            [DataMember]
            public string name { get; set; }
            [DataMember]
            public string card_number { get; set; }
        }
        [DataContract]
        public class AttributeFailure
        {
            [DataMember]
            public string code { get; set; }
            [DataMember]
            public string message { get; set; }
        }
        [DataContract]
        public class Links
        {
            [DataMember]
            public string redirect_uri { get; set; }
        }
        [DataContract]
        public class Meta
        {
            [DataMember]
            public int server_time { get; set; }
            [DataMember]
            public string server_timezone { get; set; }
            [DataMember]
            public string api_version { get; set; }
        }


        public class OrangePayResponseError
        {
            public string type { get; set; }
            public string code { get; set; }
            public string errors { get; set; }
            public Meta meta { get; set; }
        }

        #endregion

        private string apiKey;

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.ORANGEPAY3DS; }
            set { }
        }

        #region Constructor

        public ProviderOrangePay3DS(ProviderConfigBase config)
            : base(config)
        {
            FibonatixProviderConfig orangePayConfig = config as FibonatixProviderConfig;
            if (orangePayConfig == null)
            {
                throw new ArgumentException("orangePayConfig expects to get FibonatixProviderConfig object!");
            }

            apiKey = orangePayConfig.CONTROLKEY;
        }
        #endregion

        public override DepositResponse Deposit(DepositRequest depositRequest)
        {
            DepositResponse depositResponse = new DepositResponse();
            //depositResponse.error = new Cashier.Error();

            depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            depositResponse.ErrorCode = "1";
            depositResponse.Amount = depositRequest.Amount.ToString();
            depositResponse.CurrencyISOCode = depositRequest.CurrencyISOCode;
            depositResponse.provider = depositRequest.provider;
            depositResponse.ClearingUserID = MerchantID;
            depositResponse.Receipt = Guid.NewGuid().ToString().Replace("-", "");

            #region Trim           
            depositRequest.CardCVV2 = depositRequest.CardCVV2.Trim();
            depositRequest.CardExpMonth = depositRequest.CardExpMonth.Trim();
            depositRequest.CardExpYear = depositRequest.CardExpYear.Trim();
            depositRequest.CardNumber = depositRequest.CardNumber.Trim();
            depositRequest.CurrencyISOCode = depositRequest.CurrencyISOCode.Trim();
            if (!string.IsNullOrEmpty(depositRequest.Description))
                depositRequest.Description = depositRequest.Description.Trim();
            depositRequest.Email = depositRequest.Email.Trim();
            depositRequest.FirstName = depositRequest.FirstName.Trim();
            depositRequest.LastName = depositRequest.LastName.Trim();

            #endregion

            ChargeDirectRequest chargeDirectRequest = BuildChargeRequestObject(depositRequest, depositResponse);

            var json = JSONSerializer<ChargeDirectRequest>.Serialize(chargeDirectRequest).Replace("\\/", "/");

            string auth = "Bearer " + apiKey;
            
            Log(string.Format("Request: Provider=OrangePay3DS; Url:{0} Header:{1} Body:{2}{3}", Url, auth, json, Environment.NewLine));
            Error e = HttpReq("POST", Url, json, null, "application/json", "application/json", 20000, 20000, auth);
            Log(string.Format("Response : Provider=OrangePay3DS; {0}{1}", e.Description, Environment.NewLine));
            if (e.Code != 0)
            {
                string errorDesc = e.Description;
                try
                {
                    errorDesc = JSONSerializer<OrangePayResponseError>.DeSerialize(e.Description).errors;
                }
                catch
                {
                    Log($"Fail parsing http response to JSON: {e.Description} , save all response text.");
                }
                depositResponse.TransactionID = depositResponse.Receipt;
                depositResponse.ErrorDescripotion = "HTTP request for create PURCHASE failed: " + errorDesc;
                depositResponse.ErrorMessage = depositResponse.ErrorDescripotion;
                depositResponse.error.Description = depositResponse.ErrorDescripotion;
                
                Log(string.Format("Response: Provider=OrangePay3DS; Http response code: {0} {1}{2}", e.Code, depositResponse.ErrorDescripotion, Environment.NewLine));

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = depositRequest.provider;
                failedDeposit.Email = depositRequest.Email;
                failedDeposit.TransactionID = depositResponse.TransactionID;
                failedDeposit.Amount = depositRequest.Amount;
                failedDeposit.FirstName = depositRequest.FirstName;
                failedDeposit.LastName = depositRequest.LastName;
                failedDeposit.Description = errorDesc;

                depositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                // Deserialize the json
                ChargeDirectResponse chargeDirectResponse = null;
                try
                {
                    chargeDirectResponse = JSONSerializer<ChargeDirectResponse>.DeSerialize(e.Description);
                }
                catch (Exception exp)
                {
                    depositResponse.ErrorDescripotion = depositResponse.error.Description = "Fail parsing http response to JSON: " + e.Description;
                    Log(string.Format("Exception Error:{0}", exp.Message, Environment.NewLine));
                    Log(string.Format("Response: Provider=OrangePay3DS; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                    return depositResponse;
                }

                parseResponse(chargeDirectRequest, depositResponse, chargeDirectResponse);
            }

            depositResponse.error.Description = depositResponse.ErrorDescripotion;

            return depositResponse;
        }

        private void parseResponse(ChargeDirectRequest chargeDirectRequest, DepositResponse depositResponse, ChargeDirectResponse chargeDirectResponse)
        {
            try
            {                
                string status = chargeDirectResponse.data.charge.attributes.status;
                depositResponse.TransactionID = chargeDirectResponse.data.charge.id;
                if (!string.IsNullOrEmpty(status) && status == "pending" && !string.IsNullOrEmpty(chargeDirectResponse.data.links.redirect_uri))
                {
                    // 3ds                    
                    depositResponse.Is3DSecure = true;
                    depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                    depositResponse.ErrorCode = "0";
                    depositResponse.ErrorDescripotion = "";
                    depositResponse.error.Description = "";

                    depositResponse.RedirectUrl = chargeDirectResponse.data.links.redirect_uri;
                }
                else
                {
                    // non 3ds
                    if (status == "successful")
                    {
                        depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        depositResponse.ErrorMessage = "";
                        depositResponse.error.Description = "";
                        depositResponse.ErrorDescripotion = depositResponse.error.Description;
                        depositResponse.ErrorCode = "0";
                    }
                    else
                    {
                        // failed
                        depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        depositResponse.ErrorCode = "1";
                        depositResponse.ErrorMessage = chargeDirectResponse.data.charge.attributes.status;
                        depositResponse.error.Description = string.Format("{0} - {1}", chargeDirectResponse.data.charge.attributes.failure.code, chargeDirectResponse.data.charge.attributes.failure.message);
                        depositResponse.ErrorDescripotion = depositResponse.error.Description;
                    }
                }
            }
            catch (Exception exp)
            {
                depositResponse.ErrorDescripotion = "parseResponse: Fail in parsing parameters: " + exp.Message;
                Log(string.Format("Provider=OrangePay3DS; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
            }

        }

        private ChargeDirectRequest BuildChargeRequestObject(DepositRequest depositRequest, DepositResponse depositResponse)
        {
            string amount = depositRequest.Amount.ToString("0.00");
            if (depositRequest.CardExpYear.Length > 2)
            {
                depositRequest.CardExpYear = depositRequest.CardExpYear.Substring(depositRequest.CardExpYear.Length - 2);
            }
            string expireMonth = Convert.ToInt32(depositRequest.CardExpMonth).ToString("00");
            string expireYear = Convert.ToInt32(depositRequest.CardExpYear).ToString("00");

            ChargeDirectRequest createRequest = new ChargeDirectRequest();
            createRequest.amount = amount;
            createRequest.currency = depositRequest.CurrencyISOCode;
            createRequest.description = depositRequest.Description;
            createRequest.email = depositRequest.Email;
            createRequest.pay_method = "card";
            createRequest.ip_address = depositRequest.IpAddress;
            createRequest.name = depositRequest.FirstName + depositRequest.LastName;
            createRequest.card_number = depositRequest.CardNumber;
            createRequest.card_expiry_month = expireMonth;
            createRequest.card_expiry_year = expireYear;
            createRequest.card_csc = depositRequest.CardCVV2;
            createRequest.reference_id = depositResponse.Receipt; // for now we will use it just in CRM as a receipt.

            return createRequest;
        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        if ((we.Response is System.Net.HttpWebResponse))
                            resp.Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode);
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }


        /// <summary>
        /// The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
        /// Parameters must not be URL encoded before signature calculation.
        /// Please follow these steps:
        /// 1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
        /// 2. Append the corresponding values together according to the alphabetical sequence of parameter names.
        /// 3. Add the secret to the end of the string.
        /// 4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
        /// </summary>
        private string CalculateCheckSum(NameValueCollection data, string secretKey)
        {
            //The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
            //Parameters must not be URL encoded before signature calculation.
            //Please follow these steps:
            //1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
            //2. Append the corresponding values together according to the alphabetical sequence of parameter names.
            //3. Add the secret to the end of the string.
            //4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.

            //1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
            var keys = data.AllKeys.OrderBy(k => k);

            //2. Add the secret to the first of the string.
            var valuesRow = new StringBuilder();

            //3. Append the corresponding values together according to the alphabetical sequence of parameter names.
            valuesRow.Append(secretKey + "|");

            foreach (string key in keys)
            {
                valuesRow.Append(data[key] + "|");
            }

            //4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
            byte[] bytes = Encoding.UTF8.GetBytes(valuesRow.ToString().Substring(0, valuesRow.Length - 1));
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);

            var result = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

        private static string Base64Encode(string plainText)
        {
            byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(encbuff);
        }

    }
}
