﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderSolid3DS : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private class SolidRedirectValues
        {
            public string Key;
            public string Value;
        }

        private class SolidResponse
        {
            public string TransactionID;
            public string Amount;
            public string CurrencyISOCode;
            public string Status;
            public string Reason;
            public string Receipt;
            public string Return;
            public string Description;
            public string ResponseType;
            public string RedirectUrl;
            public List<SolidRedirectValues> RedirectParameters = new List<SolidRedirectValues>();

        }

        private string RETURN_CODE_SUCCESS_1 = "000.000.000";
        private string RETURN_CODE_SUCCESS_2 = "000.100.110";
        private string RETURN_CODE_SUCCESS_3 = "000.100.111";
        private string RETURN_CODE_SUCCESS_4 = "000.100.112";
        private string STATUS_CODE_SUCCESS_1 = "90.00";
        private string STATUS_CODE_SUCCESS_2 = "90.70";
        private string STATUS_CODE_TIMEOUT = "100.380.501";

        private string Sender;
        private string Channel;
        private string ReturnURL;

        #region Constructor

        public ProviderSolid3DS(ProviderConfigBase config)
            : base(config)
        {
            Solid3DSProviderConfig solid3DSConfig = config as Solid3DSProviderConfig;
            if (solid3DSConfig == null)
            {
                throw new ArgumentException("solid3DSConfig expects to get Solid3DSProviderConfig object!");
            }

            Sender = solid3DSConfig.Sender;
            Channel = solid3DSConfig.Channel;
            ReturnURL = solid3DSConfig.ReturnURL;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.SOLID3DS; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            string SessionID = Guid.NewGuid().ToString().Replace("-", "");

            //_DepositResponse.provider = Globals.Providers.SOLID3DS;
            _DepositResponse.provider = _DepositRequest.provider;
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            //_DepositResponse.provider = ProviderType;

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string req = BuildRequestXML(_DepositRequest, transactionID, SessionID, ExpMonth, ExpYear);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for " + _DepositRequest.provider.ToString();
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider={0}; {1} {2}{3}", _DepositRequest.provider.ToString(), Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, "load=" + req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider={0}; {1}{2}", _DepositRequest.provider.ToString(), e.Description, Environment.NewLine));
            
            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to " + _DepositRequest.provider.ToString() + " failed. " + e.Description;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                SolidResponse solidResponse = ParseResponse(e.Description, _DepositRequest);
                if (solidResponse.ResponseType == "ASYNC" && solidResponse.Status == "80" && solidResponse.Return == "000.200.000") // status waiting
                {
                    _DepositResponse.Is3DSecure = true;

                    _DepositResponse.RedirectUrl = solidResponse.RedirectUrl;

                    _DepositResponse.RedirectParameters = new List<RedirectValues>();
                    for (int i = 0; i < solidResponse.RedirectParameters.Count; i++)
                    {
                        RedirectValues s = new RedirectValues();
                        s.Key = solidResponse.RedirectParameters[i].Key;
                        s.Value = solidResponse.RedirectParameters[i].Value;

                        _DepositResponse.RedirectParameters.Add(s);
                    }
                }
                else
                {
                    if (WasTransactionSuccessful(solidResponse.Status, solidResponse.Reason, solidResponse.Return))
                    {
                        _DepositResponse.ErrorDescripotion = "";
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        _DepositResponse.ErrorCode = "0";
                    }
                    else
                    {
                        _DepositResponse.ErrorDescripotion = string.Format("Status: {0}; Reason: {1}; Return: {2}", solidResponse.Status, solidResponse.Reason, solidResponse.Description);
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        _DepositResponse.ErrorCode = "1";

                        FailedDeposit failedDeposit = new FailedDeposit();
                        failedDeposit.provider = _DepositRequest.provider;
                        failedDeposit.Email = _DepositRequest.Email;
                        failedDeposit.TransactionID = _DepositResponse.TransactionID;
                        failedDeposit.Amount = _DepositRequest.Amount;
                        failedDeposit.FirstName = _DepositRequest.FirstName;
                        failedDeposit.LastName = _DepositRequest.LastName;
                        failedDeposit.Description = _DepositResponse.error.Description;

                        _DepositResponse.FailedDeposits.Add(failedDeposit);
                    }
                }
            }

            return _DepositResponse;
        }

        private string BuildRequestXML(DepositRequest _DepositRequest, string TransactionID, string SessionID, string CardExpMonth, string CardExpYear)
        {
            string req = string.Empty;

            try
            {
                //3DS can only be checked on live mode
                string Mode = "LIVE";
                //string Mode = "CONNECTOR_TEST";
                //string Mode = "TEST";

                Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
            {
                {"visa", "VISA"},
                {"Visa", "VISA"},
                {"mastercard", "MASTER"},
                {"Mastercard", "MASTER"},
                {"MasterCard", "MASTER"},
                {"Master Card", "MASTER"},
                {"master card", "MASTER"},
                {"Master card", "MASTER"},
                {"diners club", "DINERS"},
                {"dinersclubintl", "DINERS"},
                {"DinersClub", "DINERS"},
                {"dinersclub", "DINERS"},
                {"Diners Club", "DINERS"},
                {"american express", "AMEX"},
                {"americanexpress", "AMEX"},
                {"AmericanExpress", "AMEX"},
                {"American Express", "AMEX"},
                {"amex", "AMEX"},
                {"AmEx", "AMEX"},
                {"maestro","MAESTRO"},
                {"Maestro","MAESTRO"},
                {"Discover","DISCOVER"},
                {"JCB","JCB"},
                {"Solo","SOLO"},
                {"Switch","SWITCH"},
                {"VisaElectron","VISAELECTRON"},
                {"visaelectron","VISAELECTRON"},
                {"visa electron","VISAELECTRON"},
                {"Visa Electron","VISAELECTRON"},
                {"LaserCard","LASER"},
                {"lasercard","LASER"},
                {"Laser Card","LASER"},
                {"laser card","LASER"}
            };

                string ccType;
                if (!cardTypeMapping.TryGetValue(_DepositRequest.CardType, out ccType))
                    ccType = _DepositRequest.CardType;

                req = string.Format(@"<?xml version='1.0' encoding='UTF-8'?>
                <Request version='1.0'>
	                <Header>
		                <Security sender='{0}'/>
	                </Header>
	                <Transaction mode='{1}' channel='{2}'>
		                <User login='{3}' pwd='{4}'/>
		                <Identification>
			                <TransactionID>{5}</TransactionID>
		                </Identification>
		                <Payment code='CC.DB'>
			                <Presentation>
				                <Amount>{6}</Amount>
				                <Currency>{7}</Currency>
				                <Usage>{8}</Usage>
			                </Presentation>
		                </Payment>
		                <Account>
			                <Holder>{9}</Holder>
			                <Number>{10}</Number>
			                <Brand>{11}</Brand>
			                <Month>{12}</Month>
			                <Year>{13}</Year>
			                <Verification>{14}</Verification>
		                </Account>
		                <Customer>
			                <Name>				
				                <Given>{15}</Given>
				                <Family>{16}</Family>				
			                </Name>
			                <Address>
				                <Street>{17}</Street>
				                <Zip>{18}</Zip>
				                <City>{19}</City>
				                <State></State>
				                <Country>{20}</Country>
			                </Address>
			                <Contact>				
				                <Email>{21}</Email>
				                <Ip>{22}</Ip>
			                </Contact>
		                </Customer>
		                <RiskManagement process='AUTO'/>		
		                <Frontend>
                            <Mode>ASYNC</Mode>
			                <ResponseUrl>{23}</ResponseUrl>
			                <SessionID>{24}</SessionID>
		                </Frontend>
	                </Transaction>
                </Request>", Sender, Mode, Channel, MerchantID,
                               MerchantPassword, TransactionID, _DepositRequest.Amount.ToString(), _DepositRequest.CurrencyISOCode, "Deposit", _DepositRequest.FirstName + " " + _DepositRequest.LastName, _DepositRequest.CardNumber, ccType, CardExpMonth, CardExpYear, _DepositRequest.CardCVV2,
                               _DepositRequest.FirstName, _DepositRequest.LastName, _DepositRequest.Address, _DepositRequest.ZipCode, _DepositRequest.City, _DepositRequest.CountryISOCode, _DepositRequest.Email, _DepositRequest.IpAddress,
                               ReturnURL, SessionID);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider={0}; Fail construct request string: {1}{2}", _DepositRequest.provider.ToString(), ex.Message, Environment.NewLine));
            }

            return req;
        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
                    req.ContentLength = byteArray.Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        stm.Write(byteArray, 0, byteArray.Length);
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if ((we.Response is System.Net.HttpWebResponse))
                    resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                else
                    resp.Code = (int)we.Status;
                resp.Description = resp.Code.ToString() + ", " + we.Message;
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private bool WasTransactionSuccessful(string statusCode, string reasonCode, string returnCode)
        {
            string status = string.Format("{0}.{1}", statusCode, reasonCode);

            return ((status == STATUS_CODE_SUCCESS_1 || status == STATUS_CODE_SUCCESS_2) &&
                    (returnCode == RETURN_CODE_SUCCESS_1 || returnCode == RETURN_CODE_SUCCESS_2 ||
                    returnCode == RETURN_CODE_SUCCESS_3 || returnCode == RETURN_CODE_SUCCESS_4));
        }

        private SolidResponse ParseResponse(string Xml, DepositRequest _DepositRequest)
        {
            SolidResponse solidResponse = new SolidResponse();

            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(Xml);

                XmlNode node = xDoc.SelectSingleNode("//Response/Transaction/Processing/Status");
                solidResponse.Status = node.Attributes["code"].Value;
                solidResponse.Description = "Status: " + node.InnerText;

                node = xDoc.SelectSingleNode("//Response/Transaction/Processing/Reason");
                solidResponse.Reason = node.Attributes["code"].Value;
                solidResponse.Description += " Reason: " + node.InnerText;

                node = xDoc.SelectSingleNode("//Response/Transaction/Processing/Return");
                solidResponse.Return = node.Attributes["code"].Value;
                solidResponse.Description += " Return: " + node.InnerText;

                try
                {
                    node = xDoc.SelectSingleNode("//Response/Transaction");
                    solidResponse.ResponseType = node.Attributes["response"].Value.ToUpper();
                }
                catch { }

                node = xDoc.SelectSingleNode("//Response/Transaction/Identification/TransactionID");
                solidResponse.TransactionID = node.InnerText;

                node = xDoc.SelectSingleNode("//Response/Transaction/Identification/UniqueID");
                solidResponse.Receipt = node.InnerText;


                node = xDoc.SelectSingleNode("//Response/Transaction/Payment/Clearing/Amount");
                if (node != null && !string.IsNullOrEmpty(node.InnerText))
                    solidResponse.Amount = node.InnerText;
                else
                    solidResponse.Amount = "-1";

                node = xDoc.SelectSingleNode("//Response/Transaction/Payment/Clearing/Currency");
                if (node != null && !string.IsNullOrEmpty(node.InnerText))
                    solidResponse.CurrencyISOCode = node.InnerText;


                node = xDoc.SelectSingleNode("//Response/Transaction/Processing/Redirect");
                if (node != null)
                {
                    /*if (_DepositRequest.provider == Globals.Providers.SOLID3DS)
                        solidResponse.RedirectUrl = node.Attributes["url"].Value.ToUpper();
                    else*/
                    solidResponse.RedirectUrl = node.Attributes["url"].Value;

                    XmlNodeList nodes = xDoc.SelectNodes("//Response/Transaction/Processing/Redirect/Parameter");
                    if (nodes != null && nodes.Count > 0)
                    {
                        foreach (XmlNode n in nodes)
                        {
                            SolidRedirectValues s = new SolidRedirectValues();
                            s.Key = n.Attributes["name"].Value;
                            s.Value = n.InnerText;
                            //solidResponse.RedirectParameters += n.Attributes["name"].Value + "&" + n.InnerText;
                            solidResponse.RedirectParameters.Add(s);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider={0}; EXCEPTION - on parsing xml response: {1}{2}", _DepositRequest.provider.ToString(), ex.Message, Environment.NewLine));
            }

            return solidResponse;
        }
    }
}
