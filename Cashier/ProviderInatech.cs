﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml.Linq;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
	internal class ProviderInatech : ProviderBase
	{
		#region Constants

		#region Request Parameters

		/// <summary>
		/// alphanumeric	40 characters merchant identification 
		/// 
		/// It is created when the merchant is created. 
		/// The merchantID will be communicated to the merchant by the inatec administration. 
		/// If a request contains no or an invalid merchantID, the inatec system will instantly reject the request.
		/// </summary>
		private const string MERCHANT_ID = "merchantid";

		/// <summary>
		/// alphanumeric	max. 30 characters orderid in the merchant shop system 
		/// 
		/// The field orderid is optional and exclusively for the merchant’s convenience. 
		/// It can be used to assign order number, customer number, item number or other identifiers to a transaction, so the merchant will be able to identifiy every single transaction within the system. 
		/// The orderid will be saved within the inatec system. 
		/// If your system does not generate an orderid, powercash is able to create one in the format YYY-MM-DD-HH-MM-SS (year-month-day-hours-minutes-seconds).
		/// </summary>
		private const string ORDER_ID = "orderid";

		/// <summary>
		/// float	numeric 8.2 8 12345678.90 12345678 transaction amount 
		/// 
		/// The amount parameter can be in one of these two formats:
		/// 1: float: eight-digit number with two decimal places, separated by „.“(point).
		/// 2: numeric: eight-digit number without decimal places
		/// The „float“ format is the preferred format.
		/// </summary>
		private const string AMOUNT = "amount";

		/// <summary>
		/// char 3 ISO 4217 i.e. “EUR” or “USD” see Annex C: currency 
		/// </summary>
		private const string CURRENCY = "currency";

		/// <summary>
		/// payment_method numeric valid payment method id 1,11 payment method, 11 for Amex and 1 for every other credit card 
		/// </summary>
		private const string PAYMENT_METHOD = "payment_method";

		/// <summary>
		/// hex 40 characters SHA-1 hash value (hexadecimal) see Annex A: checksum for validation of request 
		/// </summary>
		private const string SIGNATURE = "signature";

		//url_return char 256 http://www.result.com Used for 3DSecure transactions only. Section 12 provides a full description of the 3DSecure transaction procedure. 


		/// <summary>
		/// language char 2 ISO-639-1 alpha 2 The language in which the error messages will be displayed. 
		/// 
		/// The language field contains the two-letter value for the selected language. 
		/// System messages will be displayed in the selected language, if available. 
		/// Currently, inatec supports German (de) and English (en) language options. 
		/// The language field is optional.
		/// </summary>
		private const string LANGUAGE = "language";

		//param_3d alphanumeric max. 8 non3d, try3d, always3d Used only in 3DSecure. Section 12 provides a full description of the 3DSecure transaction procedure. 
		//mpi_eci numeric 1 Valid values are: 1 – MasterCard attempted 2 – MasterCard verified 5 – Visa verified 6 – Visa attempted 7 – Card not participating or enrolment service is unavailable. 
		//mpi_avv char 28 AVV for MasterCard or CAVV for Visa. Base64 encoded string. 
		//mpi_xid char 28 XID for 3D Transaction. Only for Visa. Base64 encoded string. 

		/// <summary>
		/// ccn 1 numeric Max. 16 digits credit card number, debit card number 
		/// </summary>
		private const string CARD_NUMBER = "ccn";

		/// <summary>
		/// 1 numeric 2 digits valid through: month 
		/// </summary>
		private const string EXP_MONTH = "exp_month";

		/// <summary>
		/// exp_year 1 numeric 4 digits valid through: year 
		/// </summary>
		private const string EXP_YEAR = "exp_year";

		/// <summary>
		/// 1 numeric max. 4 digits card validation code 
		/// </summary>
		private const string CVC_CODE = "cvc_code";

		/// <summary>
		/// 1 alphanumeric max. 50 characters card holder
		/// </summary>
		private const string CARDHOLDER_NAME = "cardholder_name";


		//salutation alphanumeric* max. 10 characters Mr., Mrs. salutation
		//title alphanumeric max. 20 characters Dr., Prof. title 
		//gender char 1 M – male F - female gender 

		/// <summary>
		/// alphanumeric max. 30 characters first name 
		/// </summary>
		private const string FIRST_NAME = "firstname";

		/// <summary>
		/// alphanumeric max. 30 characters last name 
		/// </summary>
		private const string LAST_NAME = "lastname";

		//birthday numeric 8 digits Ddmmyyyy birthday 


		/// <summary>
		/// alphanumeric max. 100 characters street 
		/// </summary>
		private const string STREET = "street";

		//house alphanumeric max. 10 characters house number 
		//postbox alphanumeric max. 20 characters post office box 


		/// <summary>
		/// alphanumeric max. 10 characters postal code (USA and Canada only) 
		/// </summary>
		private const string ZIP = "zip";

		/// <summary>
		/// alphanumeric max. 40 characters city 
		/// </summary>
		private const string CITY = "city";

		/// <summary>
		/// alphanumeric max. 30 characters see Annex C: state/province (USA and Canada only) 
		/// </summary>
		private const string STATE = "state";

		/// <summary>
		///  char 3 country (ISO 3166 alpha3) see Annex C: country 
		/// </summary>
		private const string COUNTRY = "country";

		/// <summary>
		///  RFC 822 max. 50 characters e-mail-address 
		/// </summary>
		private const string EMAIL = "email";

		/// <summary>
		/// phone numeric max. 20 characters (blank, +, -, (, )) phone number 
		/// </summary>
		private const string PHONE = "phone";

		//fax numeric max. 20 characters (blank, +, -, (, )) fax number 
		//mobile numeric max. 20 characters (blank, +, -, (, ))  mobile number 
		//company alphanumeric max. 40 characters  company 
		//customerid alphanumeric max. 20 characters internal customer id in the merchant’s shop system 


		/// <summary>
		///  alphanumeric max. 15 characters NNN.NNN.NNN.NNN customer IP (IPv4) 
		/// </summary>
		private const string CUSTOMER_IP = "customerip";

        private const string PARAM_3D = "param_3d";

        #endregion

        #region Response Data

        /// <summary>
        /// numeric status, mandatory 0 = no error, 2000 = transaction is pending, any other value is an error code. 
        /// </summary>
        private const string RESPONSE_STATUS = "status";

		/// <summary>
		/// alphanumeric empty or error message error messages of the inatec system or empty 
		/// </summary>
		private const string RESPONSE_ERROR_MESSAGE = "errormessage";

		/// <summary>
		/// numeric transactionid, mandatory inatec transaction identification for interfaces preauthorize and authorize 
		/// </summary>
		private const string RESPONSE_TRANSACTION_ID = "transactionid";

		/// <summary>
		/// numeric transactionid, mandatory inatec transaction identification for interfaces preauthorize and authorize (deprecated) 
		/// </summary>
		private const string RESPONSE_TRANS_ID = "transid";

		/// <summary>
		/// numeric empty or amount value amount 
		/// </summary>
		private const string RESPONSE_AMOUNT = "amount";

		/// <summary>
		/// numeric empty or amount value price (deprecated) 
		/// </summary>
		private const string RESPONSE_PRICE = "price";

		/// <summary>
		/// char empty or currency value currency 
		/// </summary>
		private const string RESPONSE_CURRENCY = "currency";

		/// <summary>
		/// alphanumeric empty or orderid value orderid in the merchant shop system 
		/// </summary>
		private const string RESPONSE_ORDER_ID = "orderid";

		/// <summary>
		/// URL URL to redirect customer Used only in 3D-Secure Transactions. Chapter 11 provides the full description of the 3DSecure procedure. 
		/// </summary>
		private const string RESPONSE_URL_3DS = "url_3ds";

		/// <summary>
		/// numeric User_id user_id
		/// </summary>
		private const string RESPONSE_USER_ID = "user_id";


		#endregion

		#endregion

        
		public ProviderInatech(ProviderConfigBase config): base(config)
		{
            
		}

		protected override Globals.Providers ProviderType
		{
			get { return Globals.Providers.INATECH; }
            set { }
        }
		
		public override DepositResponse Deposit(DepositRequest _DepositRequest)
		{
			DepositResponse _DepositResponse = new DepositResponse();

			if (_DepositRequest != null)
			{
				try
				{                    
					NameValueCollection requestCollection = CreateRequestDataCollection(_DepositRequest);

					Log(string.Format("Request: Provider={6}; UserName={0}; Currency={1}; Amount={2}; MerchantID={3}; Password={4}; Url={5}",
									_DepositRequest.Email, _DepositRequest.CurrencyISOCode, _DepositRequest.Amount, MerchantID, MerchantPassword, Url, CashierUtils.GetName(ProviderType)));

					string request = BuildRequestUrl(Url, requestCollection);
					Log(string.Format("Authorize RequesetUrl: {0}", request));

					Log(requestCollection);

					string response = SendRequest(Url, requestCollection);
					Log(string.Format("Authorize Response: {0}", response));

					_DepositResponse = ParseResponse(response);

					if (string.IsNullOrWhiteSpace(_DepositResponse.Amount))
					{
						_DepositResponse.Amount = _DepositRequest.Amount.ToString();
					}

					if (string.IsNullOrWhiteSpace(_DepositResponse.CurrencyISOCode))
					{
						_DepositResponse.CurrencyISOCode = _DepositRequest.CurrencyISOCode;
					}
				}
				catch (Exception ex)
				{
					_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
					_DepositResponse.error.Description = ex.Message;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = _DepositRequest.provider;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = _DepositResponse.TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = _DepositResponse.error.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);

                }
            }
			else
			{
				_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
				_DepositResponse.error.Description = "Inatech DepositRequest parameters can't be null";
			}

			_DepositResponse.provider = _DepositRequest.provider;

			return _DepositResponse;
		}



		#region Private methods

		private string SendRequest(string url, NameValueCollection data)
		{
			using (var webClient = new WebClient())
			{
				try
				{                    
					byte[] response = webClient.UploadValues(url, "POST", data);
                    
					Encoding iso = Encoding.GetEncoding("ISO-8859-1");
					string responseDecoded = HttpUtility.UrlDecode(response, iso);
                   
					return responseDecoded;
				}
				catch (Exception ex)
				{
					string errorMessage = String.Format("An error has occured during sending request: {0}", ex.Message);
					Log(errorMessage);
					throw new WebException(errorMessage, ex);
				}
			}
		}

		#region Parse response

		private DepositResponse ParseResponse(string response)
		{
			try
			{
				NameValueCollection responseData = HttpUtility.ParseQueryString(response);

				DepositResponse responseParams = new DepositResponse()
				{
					TransactionID = responseData[RESPONSE_TRANSACTION_ID],
					Status = responseData[RESPONSE_STATUS],
					ErrorDescripotion = responseData[RESPONSE_ERROR_MESSAGE],
					Amount =  !string.IsNullOrWhiteSpace(responseData[RESPONSE_AMOUNT]) ? Double.Parse(responseData[RESPONSE_AMOUNT]).ToString()  : string.Empty,
					CurrencyISOCode = responseData[RESPONSE_CURRENCY],
					ClearingUserID = responseData[RESPONSE_USER_ID]
				};

				responseParams.Receipt = responseParams.TransactionID;

				responseParams.error.ErrorCode = responseParams.Status == "0" ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
				responseParams.error.Description = responseParams.ErrorDescripotion;

				return responseParams;
			}
			catch (Exception ex)
			{
				string errorMessage = string.Format("An error has occured during parsing response '{0}'. Exception: {1}", response, ex.Message);
				Log(errorMessage);
				throw new InvalidDataException(errorMessage, ex);
			}
		}

		#endregion

		#region Create Request

		private NameValueCollection CreateRequestDataCollection(DepositRequest request)
		{
			if (string.IsNullOrWhiteSpace(request.NameOnCard))
			{
				request.NameOnCard = string.Format("{0} {1}", request.FirstName, request.LastName);		
			}

			if (request.CardExpYear.Length < 4)
			{
				request.CardExpYear = "20" + request.CardExpYear;
			}

            
			// Validate fields lenght
			ValidateRequestData(request);

            if (string.IsNullOrWhiteSpace(request.StateISOCode))
            {
                if (request.CountryISOCode == "US") // united states
                    request.StateISOCode = "MI";
                else if (request.CountryISOCode == "CA") // canada
                    request.StateISOCode = "ON";
                else if (request.CountryISOCode == "AU") // australia
                    request.StateISOCode = "NSW";
            }

            var data = new NameValueCollection
			{
				{MERCHANT_ID, MerchantID},
				{ORDER_ID, DateTime.Now.ToString("yyy-MM-dd-HH-mm-ss")},
				{AMOUNT, request.Amount.ToString("00000000.00")},
				{CURRENCY, request.CurrencyISOCode},
				
				{PAYMENT_METHOD, request.CardType.Equals("AMEX", StringComparison.InvariantCultureIgnoreCase) ? "11" : "1"},

                { PARAM_3D, "non3d"},
                
                {LANGUAGE, "EN"},
				{CARD_NUMBER, request.CardNumber},
				{EXP_MONTH, request.CardExpMonth},
				{EXP_YEAR, request.CardExpYear},
				{CVC_CODE, request.CardCVV2},
				{CARDHOLDER_NAME, request.NameOnCard },
				{FIRST_NAME, request.FirstName},
				{LAST_NAME, request.LastName},
				{STREET, request.Address},
				{ZIP, request.ZipCode},
				{CITY, request.City},
				{STATE, request.StateISOCode},
				{COUNTRY, request.CountryISOCodeThreeLetters},
				{EMAIL, request.Email},
				{CUSTOMER_IP, request.IpAddress},
			};

            if (Mode == Configuration.Mode.TEST)
                data.Add("custom1", "123456");


            string signature = CalculateCheckSum(data, MerchantPassword);
			data.Add(SIGNATURE, signature);

			return data;
		}

		/// <summary>
		/// The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
		/// Parameters must not be URL encoded before signature calculation.
		/// Please follow these steps:
		/// 1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
		/// 2. Append the corresponding values together according to the alphabetical sequence of parameter names.
		/// 3. Add the secret to the end of the string.
		/// 4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
		/// </summary>
		private string CalculateCheckSum(NameValueCollection data, string secretKey)
		{
			//The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
			//Parameters must not be URL encoded before signature calculation.
			//Please follow these steps:
			//1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
			//2. Append the corresponding values together according to the alphabetical sequence of parameter names.
			//3. Add the secret to the end of the string.
			//4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.

			//1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
			var keys = data.AllKeys.OrderBy(k => k);

			//2. Append the corresponding values together according to the alphabetical sequence of parameter names.
			var valuesRow = new StringBuilder();
			foreach (string key in keys)
			{
				valuesRow.Append(data[key]);
			}

			//3. Add the secret to the end of the string.
			valuesRow.Append(secretKey);

			//4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
			byte[] bytes = Encoding.UTF8.GetBytes(valuesRow.ToString());
			var sha1 = SHA1.Create();
			byte[] hashBytes = sha1.ComputeHash(bytes);

			var result = new StringBuilder();
			foreach (byte b in hashBytes)
			{
				result.Append(b.ToString("x2"));
			}

			return result.ToString();
		}

		private void ValidateRequestData(DepositRequest request)
		{
			// Validate Mandatory fields
			ValidateRequiredField(request.FirstName, FIRST_NAME, 30);
			ValidateRequiredField(request.LastName, LAST_NAME, 30);
			ValidateRequiredField(request.Address, STREET, 100);
			ValidateRequiredField(request.City, CITY, 40);
			ValidateRequiredField(request.ZipCode, ZIP, 10);
			ValidateRequiredField(request.CountryISOCodeThreeLetters, COUNTRY, 3);

			ValidateNotRequiredField(request.PhoneNumber, PHONE, 20);

			ValidateRequiredField(request.IpAddress, CUSTOMER_IP, 15);
			ValidateRequiredField(request.Email, EMAIL, 50);
			ValidateNotRequiredField(request.StateISOCode, STATE, 30);

			ValidateRequiredField(request.NameOnCard, CARDHOLDER_NAME, 50);
			ValidateRequiredField(request.CardNumber, CARD_NUMBER, 16);
			ValidateRequiredField(request.CardExpMonth, EXP_MONTH, 2);
			ValidateRequiredField(request.CardExpYear, EXP_YEAR, 4);
			ValidateRequiredField(request.CardCVV2, CVC_CODE, 4);
			//ValidateNotRequiredField(request.CardIssueNumber, "CardIssueNumber", 2);	

			ValidateRequiredField(request.CurrencyISOCode, CURRENCY, 3);
			//ValidateNotRequiredField(request.AuthorizationCode, "AuthorizationCode", 10);
			ValidateRequiredField(MerchantID, MERCHANT_ID, 40);
			//ValidateRequiredField(MerchantPassword, "MerchantPassword", 24);			
			//ValidateNotRequiredField(request.SecretKey, "SecretKey", 32);				
			//ValidateNotRequiredField(request.CardType, TYPE, 1);			
		}

		#endregion

		#endregion

	}

}
