﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using System.Xml;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using CashierUtils;

namespace Cashier
{
    class ProviderNasPay3DS : ProviderBase
    {
        public class Error
        {
            public int Code;
            public string Description;
        }

        #region Create token
        [DataContract]
        public class NasPayToken
        {
            [DataMember]
            public string access_token { get; set; }
            [DataMember]
            public string token_type { get; set; }
        }

        #endregion

        #region SALE

        [DataContract]
        private class NasPayRequest
        {
            [DataMember]
            public double amount;
            [DataMember]
            public Card card;
            [DataMember]
            public string currency;
            [DataMember]
            public Customer customer;
            [DataMember]
            public string description;
            [DataMember]
            public string intent;
            [DataMember]
            public string merchantTransactionId;
            // [DataMember]
            // public string paymentMethod;  // optional
            // [DataMember]
            // public shippingAddress shippingAddress; // optional

        }
        [DataContract]
        public class Card
        {
            [DataMember]
            public BillingAddress billingAddress { get; set; }
            [DataMember]
            public string cardholder { get; set; }
            [DataMember]
            public string cvv { get; set; }
            [DataMember]
            public int expiryMonth { get; set; }
            [DataMember]
            public int expiryYear { get; set; }
            [DataMember]
            public string pan { get; set; }
        }

        [DataContract]
        public class BillingAddress
        {
            [DataMember]
            public string city { get; set; }
            [DataMember]
            public string countryCode { get; set; }
            [DataMember]
            public string line1 { get; set; }
            [DataMember]
            public string line2 { get; set; }  // optional
            [DataMember]
            public string phone { get; set; } // optional
            [DataMember]
            public string postalCode { get; set; } // optional
            [DataMember]
            public string state { get; set; } // optional
        }

        [DataContract]
        public class Customer
        {
            [DataMember]
            public string email { get; set; }
            [DataMember]
            public string firstName { get; set; } // optional
            [DataMember]
            public string ip { get; set; } // optional
            [DataMember]
            public string lastName { get; set; } // optional
            [DataMember]
            public string locale { get; set; } // optional
            [DataMember]
            public string merchantCustomerId { get; set; } // optional
            [DataMember]
            public string phone { get; set; } // optional            
        }

        [DataContract]
        public class ShippingAddress
        {
            [DataMember]
            public string city { get; set; }
            [DataMember]
            public string countryCode { get; set; }
            [DataMember]
            public string line1 { get; set; }
            [DataMember]
            public string line2 { get; set; }  // optional
            [DataMember]
            public string phone { get; set; } // optional
            [DataMember]
            public string postalCode { get; set; } // optional
            [DataMember]
            public string recipientName { get; set; }
            [DataMember]
            public string state { get; set; } // optional
        }

        #endregion

        #region Response

        [DataContract]
        public class NasPayResponse
        {
            [DataMember]
            public double amount { get; set; } // optional
            [DataMember]
            public string created { get; set; }
            [DataMember]
            public string currency { get; set; } // optional
            [DataMember]
            public string description { get; set; } // optional
            [DataMember]
            public string id { get; set; }
            [DataMember]
            public List<Link> links { get; set; }
            [DataMember]
            public string merchantTransactionId { get; set; } // optional
            [DataMember]
            public string state { get; set; }
            [DataMember]
            public string updated { get; set; }
            [DataMember]
            public NasPayError error { get; set; }
        }

        [DataContract]
        public class NasPayError
        {
            [DataMember]
            public int status { get; set; }
            [DataMember]
            public string code { get; set; }
            [DataMember]
            public string message { get; set; }
            [DataMember]
            public List<FieldError> fieldErrors { get; set; }
        }

        [DataContract]
        public class FieldError
        {
            [DataMember]
            public string field { get; set; }
            [DataMember]
            public string message { get; set; }
        }
        [DataContract]
        public class Link
        {
            [DataMember]
            public string href { get; set; }
            [DataMember]
            public string rel { get; set; }
        }

        #endregion

        #region ResultCodes

        private string resultCodesPath;

        [DataContract]
        public class ResultCode
        {
            [DataMember]
            public string code { get; set; }
            [DataMember]
            public string description { get; set; }
        }

        [DataContract]
        public class ResultCodeRootObject
        {
            [DataMember]
            public List<ResultCode> resultCodes { get; set; }
        }
        #endregion

        private string saleUrl;

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.NASPAY3DS; }
            set { }
        }

        #region Constructor

        public ProviderNasPay3DS(ProviderConfigBase config)
            : base(config)
        {
            FibonatixProviderConfig nasPayConfig = config as FibonatixProviderConfig;
            if (nasPayConfig == null)
            {
                throw new ArgumentException("nasPayConfig expects to get FibonatixProviderConfig object!");
            }

            resultCodesPath = nasPayConfig.CONTROLKEY;
            saleUrl = nasPayConfig.SaleUrl;
        }
        #endregion

        public override DepositResponse Deposit(DepositRequest depositRequest)
        {
            DepositResponse depositResponse = new DepositResponse();
            //depositResponse.error = new Cashier.Error();

            depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            depositResponse.ErrorCode = "1";
            depositResponse.Amount = depositRequest.Amount.ToString();
            depositResponse.CurrencyISOCode = depositRequest.CurrencyISOCode;
            depositResponse.provider = depositRequest.provider;
            depositResponse.ClearingUserID = MerchantID;
            depositResponse.TransactionID = Guid.NewGuid().ToString().Replace("-", "");

            #region Trim           
            depositRequest.CardCVV2 = depositRequest.CardCVV2.Trim();
            depositRequest.CardExpMonth = depositRequest.CardExpMonth.Trim();
            depositRequest.CardExpYear = depositRequest.CardExpYear.Trim();
            depositRequest.CardNumber = depositRequest.CardNumber.Trim();
            depositRequest.CurrencyISOCode = depositRequest.CurrencyISOCode.Trim();
            if (!string.IsNullOrEmpty(depositRequest.Description))
                depositRequest.Description = depositRequest.Description.Trim();
            depositRequest.Email = depositRequest.Email.Trim();
            depositRequest.FirstName = depositRequest.FirstName.Trim();
            depositRequest.LastName = depositRequest.LastName.Trim();

            #endregion

            NasPayToken nt = NasPayGetToken();

            if (nt != null && nt.access_token != null)
            {
                NasPayRequest nasPayRequest = BuildNasPayRequestObject(depositRequest, depositResponse);
                if (nasPayRequest != null)
                {
                    var json = JSONSerializer<NasPayRequest>.Serialize(nasPayRequest).Replace("\\/", "/");

                    string auth = "Bearer " + nt.access_token;

                    Log(string.Format("Request: Provider=NasPay3DS; Url:{0} Header:{1} Body:{2}{3}", Url, auth, json, Environment.NewLine));
                    Error e = HttpReq("POST", saleUrl, json, null, "application/json", "application/json", 20000, 20000, auth);
                    Log(string.Format("Response : Provider=NasPay3DS; {0}{1}", e.Description, Environment.NewLine));

                    // Deserialize the json
                    NasPayResponse nasPayResponse = null;
                    try
                    {
                        nasPayResponse = JSONSerializer<NasPayResponse>.DeSerialize(e.Description);
                    }
                    catch (Exception exp)
                    {
                        depositResponse.ErrorDescripotion = depositResponse.error.Description = "Fail parsing http response to JSON: " + e.Description;
                        Log(string.Format("Exception Error:{0}", exp.Message, Environment.NewLine));
                        Log(string.Format("Response: Provider=NasPay3DS; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                        return depositResponse;
                    }

                    if (e.Code != 0)
                    {
                        string errorFullDescription = string.Empty;
                        if (nasPayResponse.error.code != null)
                        {
                            string res = getResultCodesFullDescription(nasPayResponse.error.code);
                            errorFullDescription = res != string.Empty ? res : nasPayResponse.error.code;
                        }
                        else
                        {
                            try
                            {
                                foreach (var item in nasPayResponse.error.fieldErrors)
                                {
                                    errorFullDescription += item.message + " ";
                                }
                            }
                            catch{}
                        }
                                                
                        depositResponse.ErrorDescripotion = errorFullDescription;
                        Log(string.Format("Response: Provider=NasPay3DS; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                        // fill the class FailedDeposit with details in order to send email to support later
                        FailedDeposit failedDeposit = new FailedDeposit();
                        failedDeposit.provider = depositRequest.provider;
                        failedDeposit.Email = depositRequest.Email;
                        failedDeposit.TransactionID = depositResponse.TransactionID;
                        failedDeposit.Amount = depositRequest.Amount;
                        failedDeposit.FirstName = depositRequest.FirstName;
                        failedDeposit.LastName = depositRequest.LastName;
                        failedDeposit.Description = errorFullDescription;

                        depositResponse.FailedDeposits.Add(failedDeposit);
                    }
                    else
                    {


                        parseResponse(nasPayRequest, depositResponse, nasPayResponse);
                    }
                }
                else
                {
                    depositResponse.ErrorDescripotion = "Building http request failed!";
                    Log(string.Format("Response: Provider=NasPay3DS; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                }

                depositResponse.error.Description = depositResponse.ErrorDescripotion;
            }
            else
            {
                depositResponse.ErrorDescripotion = "Failed in getting NasPay token!";
                Log(string.Format("Response: Provider=NasPay3DS; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = depositRequest.provider;
                failedDeposit.Email = depositRequest.Email;
                failedDeposit.TransactionID = depositResponse.TransactionID;
                failedDeposit.Amount = depositRequest.Amount;
                failedDeposit.FirstName = depositRequest.FirstName;
                failedDeposit.LastName = depositRequest.LastName;
                failedDeposit.Description = depositResponse.ErrorDescripotion;

                depositResponse.FailedDeposits.Add(failedDeposit);
            }
            return depositResponse;
        }

        // get Result Codes Full Description from json file located in CONTROLKEY attribute ( cashier.config )
        private string getResultCodesFullDescription(string code)
        {
            string errFullDesc = string.Empty;
            try
            {
                ResultCodeRootObject list = new ResultCodeRootObject();
                using (StreamReader r = new StreamReader(resultCodesPath))
                {
                    string json = r.ReadToEnd();
                    list = JSONSerializer<ResultCodeRootObject>.DeSerialize(json);
                }
                foreach (var item in list.resultCodes)
                {
                    if ( item.code == code)
                    {
                        errFullDesc = item.code + " - " + item.description;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log("Exception in getResultCodesFullDescription: " + ex.Message);
            }
            return errFullDesc;
        }

        private NasPayToken NasPayGetToken()
        {
            NasPayToken nt = new NasPayToken();
            try
            {
                // build authorization Header
                string credHeader = "Basic " + Base64Encode(MerchantID + ":" + MerchantPassword);

                Log(string.Format("Request: Provider=NasPay3DS; {0} {1}{2}", Url, "", Environment.NewLine));
                Error e = HttpReq("GET", Url, "", null, "application/json", "", 10000, 10000, credHeader);
                Log(string.Format("Response: Provider=NasPay3DS; {0}{1}", e.Description, Environment.NewLine));

                if (e.Code == 0)
                {
                    nt = JSONSerializer<NasPayToken>.DeSerialize(e.Description);
                }
            }
            catch (Exception ex)
            {
                Log("Exception in NasPayGetToken: " + ex.Message);
            }

            return nt;
        }

        private NasPayRequest BuildNasPayRequestObject(DepositRequest depositRequest, DepositResponse depositResponse)
        {
            if (depositRequest.CardExpYear.Length < 4)
            {
                depositRequest.CardExpYear = "20" + depositRequest.CardExpYear;
            }
            int expireMonth = Convert.ToInt32(depositRequest.CardExpMonth);
            int expireYear = Convert.ToInt32(depositRequest.CardExpYear);

            NasPayRequest nasPayRequest = new NasPayRequest();
            try
            {
                nasPayRequest.amount = depositRequest.Amount;

                // billingAddress                
                BillingAddress billingAddress = new BillingAddress();
                billingAddress.city = depositRequest.City;
                billingAddress.countryCode = depositRequest.CountryISOCode;
                billingAddress.line1 = depositRequest.Address;
                billingAddress.phone = depositRequest.PhoneNumber;
                billingAddress.postalCode = depositRequest.ZipCode;
                billingAddress.state = depositRequest.StateISOCode;

                // card
                Card card = new Card();
                card.cardholder = depositRequest.FirstName + " " + depositRequest.LastName;
                card.cvv = depositRequest.CardCVV2;
                card.expiryMonth = Convert.ToInt32(depositRequest.CardExpMonth);
                card.expiryYear = Convert.ToInt32(depositRequest.CardExpYear);
                card.pan = depositRequest.CardNumber;
                nasPayRequest.currency = depositRequest.CurrencyISOCode;

                nasPayRequest.card = card;
                nasPayRequest.card.billingAddress = billingAddress;

                // customer
                Customer customer = new Customer();
                customer.email = depositRequest.Email;
                customer.firstName = depositRequest.FirstName;
                customer.ip = depositRequest.IpAddress;
                customer.lastName = depositRequest.LastName;
                customer.merchantCustomerId = depositRequest.CustomerID;
                customer.phone = depositRequest.PhoneNumber;

                nasPayRequest.customer = customer;

                nasPayRequest.description = depositRequest.Description;
                nasPayRequest.intent = "SALE";
                nasPayRequest.merchantTransactionId = depositResponse.TransactionID;
            }
            catch (Exception ex)
            {
                Log("BuildNasPayRequestObject Exception: " + ex.Message);
            }
            return nasPayRequest;
        }

        private void parseResponse(NasPayRequest nasPayRequest, DepositResponse depositResponse, NasPayResponse nasPayResponse)
        {
            try
            {
                if (!string.IsNullOrEmpty(nasPayResponse.state) && nasPayResponse.state == "IN_PROGRESS")
                {
                    // 3ds
                    depositResponse.Receipt = nasPayResponse.id;
                    depositResponse.Is3DSecure = true;
                    depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                    depositResponse.ErrorCode = "0";
                    depositResponse.ErrorDescripotion = "";
                    depositResponse.error.Description = "";

                    if (nasPayResponse.links != null)
                    {
                        foreach (var item in nasPayResponse.links)
                        {
                            if (item.rel == "checkout")
                            {
                                depositResponse.RedirectUrl = item.href;
                            }
                        }
                    }
                }
                else
                {
                    // non 3ds
                    if (nasPayResponse.state == "COMPLETED")
                    {
                        depositResponse.Receipt = nasPayResponse.id;
                        depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        depositResponse.ErrorMessage = "";
                        depositResponse.error.Description = "";
                        depositResponse.ErrorDescripotion = depositResponse.error.Description;
                        depositResponse.ErrorCode = "0";
                    }
                    else
                    {
                        // failed
                        depositResponse.Receipt = nasPayResponse.id;
                        depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        depositResponse.ErrorCode = nasPayResponse.error.code;
                        depositResponse.ErrorMessage = nasPayResponse.error.message;
                        depositResponse.error.Description = string.Format("{0} - {1}", depositResponse.ErrorCode, depositResponse.ErrorMessage);
                        depositResponse.ErrorDescripotion = depositResponse.error.Description;
                    }
                }
            }
            catch (Exception exp)
            {
                depositResponse.ErrorDescripotion = "parseResponse: Fail in parsing parameters: " + exp.Message;
                Log(string.Format("Provider=NasPay3DS; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
            }

        }
        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        if ((we.Response is System.Net.HttpWebResponse))
                            resp.Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode);
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private static string Base64Encode(string plainText)
        {
            byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(encbuff);
        }

    }


}
