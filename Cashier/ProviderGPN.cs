﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderGPN : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private string apiKey;
        private string ReturnURL;
        private const string UNDEFINED = "undefined";
        private const string apiCmd = "700";

        private string ProviderName;      

        #region Constructor

        public ProviderGPN(ProviderConfigBase config)
            : base(config)
        {
            MerchantsWorldWideProviderConfig GPNConfig = config as MerchantsWorldWideProviderConfig;
            if (GPNConfig == null)
            {
                throw new ArgumentException("GPNConfig expects to get GPNProviderConfig object!");
            }

            apiKey = GPNConfig.PrivateKey;
            ReturnURL = GPNConfig.CallbackUrl;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.GPN; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            //string transactionID = ((long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds).ToString();
            _DepositResponse.provider = _DepositRequest.provider;
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString("0.00");

            ProviderName = _DepositRequest.provider.ToString();

            _DepositResponse.Is3DSecure = false;

            // build the signature
            string signature = string.Empty;
            try
            {
                string data = PrepareSHA1Data(_DepositRequest, transactionID);
                signature = GenerateSHA1(data);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=GPN; Fail construct signature string: {0}{1}", ex.Message, Environment.NewLine));
                _DepositResponse.ErrorDescripotion = "Fail construct signature string for GPN request";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            // build the request
            string req = BuildRequest(_DepositRequest, transactionID, signature);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for GPN";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=GPN; {0} {1}{2}", Url, req, Environment.NewLine));

            // send POST
            Error e = HttpReq("POST", Url, req, null, "application/xml", "", 20000, 20000);

            Log(string.Format("Response: Provider=GPN; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to GPN failed. " + e.Description;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                try
                {
                    string result = e.Description;

                    // check if its xml format
                    int startOfXML = result.IndexOf("<?xml");
                    if (startOfXML > -1)
                    {
                        string xmlResponse = result.Substring(startOfXML);

                        // Now load the string into an XmlDocument 
                        XmlDocument responseDocument = new XmlDocument();
                        responseDocument.LoadXml(xmlResponse);

                        ParseResponseXml(responseDocument, _DepositResponse);

                        if (_DepositResponse.ErrorMessage == "PENDING")
                        {
                            _DepositResponse.Is3DSecure = true;
                            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                            _DepositResponse.ErrorDescripotion = "";
                            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                            ParseResponse3DSXml(responseDocument, _DepositResponse);
                        }

                    }
                    else
                    {
                        _DepositResponse.ErrorDescripotion = "GPN: response is invalid: " + result;
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.ErrorCode = "1";
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    }

                }
                catch (UriFormatException)
                {
                    _DepositResponse.ErrorDescripotion = "GPN: Couldn't parse URL return";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    return _DepositResponse;
                }
            }

            return _DepositResponse;
        }

        private string BuildRequest(DepositRequest _DepositRequest, string TransactionID, string signature)
        {
            string req = string.Empty;

            try
            {
                // 2 digits month
                string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");

                // 4 digits year
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                string ExpYear = _DepositRequest.CardExpYear;
                if (ExpYear.Length == 2)
                    ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

                string street = _DepositRequest.Address;
                string city = _DepositRequest.City;
                string email = _DepositRequest.Email;
                string firstName = _DepositRequest.FirstName.Trim();
                string lastName = _DepositRequest.LastName.Trim();
                string fullName = _DepositRequest.FirstName.Trim() + " " + _DepositRequest.LastName.Trim();

                req = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""  ?>
                    <transaction>
                      <apiUser>{0}</apiUser>
                      <apiPassword>{1}</apiPassword>
                      <apiCmd>{2}</apiCmd>
                      <transaction>
                        <merchanttransid>{3}</merchanttransid>
                        <amount>{4}</amount>
                        <curcode>{5}</curcode>
                        <description>{6}</description>
                        <merchantspecific1>{7}</merchantspecific1>
                        <merchantspecific2>{8}</merchantspecific2>
                        <merchantspecific3>{9}</merchantspecific3>
                        <merchantspecific4>{10}</merchantspecific4>
                        <merchantspecific5>{11}</merchantspecific5>
                     </transaction>                                                           
                      <customer>
                        <firstname>{12}</firstname>
                        <lastname>{13}</lastname>
                        <birthday>{14}</birthday>
                        <birthmonth>{15}</birthmonth>
                        <birthyear>{16}</birthyear>
                        <email>{17}</email>
                        <countryiso>{18}</countryiso>
                        <stateregioniso>{19}</stateregioniso>
                        <zippostal>{20}</zippostal>
                        <city>{21}</city>
                        <address1>{22}</address1>
                        <address2>{23}</address2>
                        <phone1phone>{24}</phone1phone>
                        <phone2phone>{25}</phone2phone>
                        <accountid>{26}</accountid>
                        <ipaddress>{27}</ipaddress>
                     </customer>
                      <creditcard>
                        <ccnumber>{28}</ccnumber>
                        <cccvv>{29}</cccvv>
                        <expmonth>{30}</expmonth>
                        <expyear>{31}</expyear>
                        <nameoncard>{32}</nameoncard>
                        <billingcountryiso>{33}</billingcountryiso>
                        <billingstateregioniso>{34}</billingstateregioniso>
                        <billingzippostal>{35}</billingzippostal>
                        <billingcity>{36}</billingcity>
                        <billingaddress1>{37}</billingaddress1>
                        <billingaddress2>{38}</billingaddress2>
                        <billingphone1phone>{39}</billingphone1phone>
                     </creditcard>
                      <checksum>{40}</checksum>
                      <auth>
                        <type>{41}</type>
                        <sid>{42}</sid>
                     </auth>
                    </transaction>",
                    MerchantID, MerchantPassword, apiCmd, TransactionID, _DepositRequest.Amount.ToString("0.00"),
                    _DepositRequest.CurrencyISOCode, "Deposit", "", "", "", "", "",
                    firstName, lastName, "01", "01", "1990", email, _DepositRequest.CountryISOCodeThreeLetters,
                    _DepositRequest.StateISOCode, _DepositRequest.ZipCode, city, street, "",
                    _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", "").Replace(" ", ""), "", _DepositRequest.CustomerID,
                    _DepositRequest.IpAddress, _DepositRequest.CardNumber, _DepositRequest.CardCVV2,
                    ExpMonth, ExpYear, fullName, _DepositRequest.CountryISOCodeThreeLetters,
                    _DepositRequest.StateISOCode, _DepositRequest.ZipCode, city, street, "",
                    _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", "").Replace(" ", ""), signature, _DepositRequest.provider.ToString().Contains("3DS") ? "3DS" : "Direct", " ");
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=GPN; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }             

        private string PrepareSHA1Data(DepositRequest _DepositRequest, string TransactionID)
        {
            var data = new NameValueCollection
            {
                {"apiUser", MerchantID},
                {"apiPassword", MerchantPassword},
                {"apiCmd", apiCmd},
                {"merchanttransid", TransactionID},
                {"amount", _DepositRequest.Amount.ToString("0.00")},
                {"curcode", _DepositRequest.CurrencyISOCode},
                {"ccnumber", _DepositRequest.CardNumber},
                {"cccvv", _DepositRequest.CardCVV2},
                {"nameoncard",  _DepositRequest.FirstName.Trim() + " " + _DepositRequest.LastName.Trim()}
            };

            var keys = data.AllKeys;

            //2. Append the corresponding values together according to the alphabetical sequence of parameter names.
            var valuesRow = new StringBuilder();
            foreach (string key in keys)
            {
                valuesRow.Append(data[key]);
            }

            //3. Add the SignKey to the end of the string.
            valuesRow.Append(apiKey);

            return valuesRow.ToString();
        }

        private static string GenerateSHA1(string data)
        {
            string signature = string.Empty;
            byte[] bytes = Encoding.UTF8.GetBytes(data);
            using (var sha1 = System.Security.Cryptography.SHA1.Create())
            {
                byte[] hashBytes = sha1.ComputeHash(bytes);

                var result = new StringBuilder();
                foreach (byte b in hashBytes)
                {
                    result.Append(b.ToString("x2"));
                }

                signature = result.ToString();
            }

            return signature;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                /*ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );*/

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }


            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        #region Response parsing funcs

        //Parses response Xml. Returns DataCashDepositResponseParameters object
        private void ParseResponseXml(XmlDocument docResponse, DepositResponse response)
        {
            // Get the root node 
            XmlElement rootElm = docResponse.DocumentElement;

            string ErrorCode = GetXmlElementValue(rootElm, "errorcode");
            string Result = GetXmlElementValue(rootElm, "result");
            string ErrorMessage = GetXmlElementValue(rootElm, "errormessage");
            string ErrorDescription = GetXmlElementValue(rootElm, "description");

            response.error.ErrorCode = Result == "SUCCESS" ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
            response.ErrorMessage = Result;
            response.error.Description = ErrorMessage == "" ? ErrorDescription : ErrorMessage;
            response.ErrorDescripotion = response.error.Description;
        }

        private void ParseResponse3DSXml(XmlDocument docResponse, DepositResponse response)
        {
            // Get the root node 
            XmlElement rootElm = docResponse.DocumentElement;

            string sACS = GetXmlElementValue(rootElm, "ACS");

            response.RedirectUrl = sACS;

            response.RedirectParameters = new List<RedirectValues>();
            XmlNodeList nodes = docResponse.SelectNodes("//transaction/parameters");

            if (nodes != null && nodes[0].ChildNodes.Count > 0)
            {
                foreach (XmlNode n in nodes[0].ChildNodes)
                {
                    RedirectValues s = new RedirectValues();
                    s.Key = n.Name;
                    if (s.Key == "TermUrl")
                        s.Value = ReturnURL;
                    else
                        s.Value = n.InnerText;
                    response.RedirectParameters.Add(s);
                }
            }
        }

        //Returns Xml element value
        private string GetXmlElementValue(XmlElement rootElm, string elmName)
        {
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text
            string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
            return resultValue;
        }

        #endregion

        

    }
}
