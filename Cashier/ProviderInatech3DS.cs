﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using CashierUtils;

namespace Cashier
{
    class ProviderInatech3DS : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private string ReturnURL;

        #region Constructor

        public ProviderInatech3DS(ProviderConfigBase config)
            : base(config)
        {
            Inatech3DSProviderConfig inatech3DSConfig = config as Inatech3DSProviderConfig;
            if (inatech3DSConfig == null)
            {
                throw new ArgumentException("inatech3DSConfig expects to get Inatech3DSProviderConfig object!");
            }
            
            ReturnURL = inatech3DSConfig.ReturnURL;
		}       
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.INATECH3DS; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            //string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            string transactionID = ((long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds).ToString();
            _DepositResponse.provider = Globals.Providers.INATECH3DS;
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString("0.00");

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string signature = string.Empty;
            try
            {                
                string data = PrepareSHA1Data(_DepositRequest, transactionID, ExpMonth, ExpYear);
                signature = GenerateSHA1(data);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=Inatech3DS; Fail construct signature string: {0}{1}", ex.Message, Environment.NewLine));
                _DepositResponse.ErrorDescripotion = "Fail construct signature string for Inatech3DS request";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            string req = BuildRequest(_DepositRequest, transactionID, ExpMonth, ExpYear, signature);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for Inatech3DS";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=Inatech3DS; {0} {1}{2}", Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, null, "text/plain", "", 20000, 20000);

            Log(string.Format("Response: Provider=Inatech3DS; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {                
                _DepositResponse.ErrorDescripotion = "Http request to Inatech3DS failed. " + e.Description;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                if (string.IsNullOrEmpty(e.Description))
                {
                    _DepositResponse.ErrorDescripotion = "Empty response from Inatech3DS";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                }

                ParseResponse(System.Web.HttpUtility.UrlDecode(e.Description), _DepositResponse);
            }

            return _DepositResponse;
        }

        private string BuildRequest(DepositRequest _DepositRequest, string TransactionID, string ExpMonth, string ExpYear, string signature)
        {
            string req = string.Empty;

            try
            {
                string street = System.Web.HttpUtility.UrlEncode(_DepositRequest.Address);
                string city = System.Web.HttpUtility.UrlEncode(_DepositRequest.City);
                string email = System.Web.HttpUtility.UrlEncode(_DepositRequest.Email);
                string cardholder_name = System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName + " " + _DepositRequest.LastName);

                req = string.Format("merchantid={0}&amount={1}&currency={2}&orderid={3}&lastname={4}&street={5}&zip={6}&city={7}&country={8}&firstname={9}&email={10}&customerip={11}&payment_method=1&ccn={12}&cvc_code={13}&cardholder_name={14}&exp_month={15}&exp_year={16}&url_return={17}&signature={18}",
                    MerchantID, _DepositRequest.Amount.ToString("0.00"), _DepositRequest.CurrencyISOCode, TransactionID, _DepositRequest.LastName, street, _DepositRequest.ZipCode, city,
                _DepositRequest.CountryISOCodeThreeLetters, _DepositRequest.FirstName, email, _DepositRequest.IpAddress, _DepositRequest.CardNumber, _DepositRequest.CardCVV2, cardholder_name,
                ExpMonth, ExpYear, ReturnURL, signature);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=Inatech3DS; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private string PrepareSHA1Data(DepositRequest _DepositRequest, string TransactionID, string ExpMonth, string ExpYear)
        {
            var data = new NameValueCollection
			{
				{"merchantid", MerchantID},
				{"amount", _DepositRequest.Amount.ToString("0.00")},
				{"currency", _DepositRequest.CurrencyISOCode},
				{"orderid", TransactionID},
				
                {"firstname", _DepositRequest.FirstName},
				{"lastname", _DepositRequest.LastName},

				{"street", _DepositRequest.Address},
				{"zip", _DepositRequest.ZipCode},
				{"city", _DepositRequest.City},
				{"country", _DepositRequest.CountryISOCodeThreeLetters},
				
				{"email", _DepositRequest.Email },
				{"customerip", _DepositRequest.IpAddress},
				{"payment_method", "1"},
				{"ccn", _DepositRequest.CardNumber},
				{"cvc_code", _DepositRequest.CardCVV2},
				{"cardholder_name", _DepositRequest.FirstName + " " + _DepositRequest.LastName},
				{"exp_month", ExpMonth},
				{"exp_year", ExpYear},
				{"url_return", ReturnURL}
			};

            var keys = data.AllKeys.OrderBy(k => k);

            //2. Append the corresponding values together according to the alphabetical sequence of parameter names.
            var valuesRow = new StringBuilder();
            foreach (string key in keys)
            {
                valuesRow.Append(data[key]);
            }

            //3. Add the secret to the end of the string.
            valuesRow.Append(MerchantPassword);

            return valuesRow.ToString();

        }

        private static string GenerateSHA1(string data)
        {
            string signature = string.Empty;
            byte[] bytes = Encoding.UTF8.GetBytes(data);
            using (var sha1 = System.Security.Cryptography.SHA1.Create())
            {
                byte[] hashBytes = sha1.ComputeHash(bytes);

                var result = new StringBuilder();
                foreach (byte b in hashBytes)
                {
                    result.Append(b.ToString("x2"));
                }

                signature = result.ToString();
            }

            return signature;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                /*ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );*/

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;
              
                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }

                        
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private void ParseResponse(string Response, DepositResponse _DepositResponse)
        {
            NameValueCollection qscoll = System.Web.HttpUtility.ParseQueryString(Response);
            if (qscoll == null || qscoll.Count == 0)
            {
                Log(string.Format("Response: Provider=Inatech3DS; Fail parsing response string{1}", Environment.NewLine));
                _DepositResponse.ErrorDescripotion = "Fail parsing response string";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return;
            }

            if (!string.IsNullOrEmpty(qscoll["amount"]) && qscoll["transactionid"] != "amount")
                _DepositResponse.Amount = qscoll["amount"];

            if (!string.IsNullOrEmpty(qscoll["transactionid"]) && qscoll["transactionid"] != "undef")
                _DepositResponse.Receipt = qscoll["transactionid"];

            if (qscoll["status"] == "0")
            {
                _DepositResponse.ErrorDescripotion = "";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                
                return;
            }

            //pending - 3ds redirect
            if (qscoll["status"] == "2000")
            {
                _DepositResponse.Is3DSecure = true;

                _DepositResponse.ErrorDescripotion = "";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;

                _DepositResponse.RedirectUrl = qscoll["url_3ds"];
                return;
            }

            _DepositResponse.ErrorDescripotion = qscoll["errormessage"];
            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            
        }


    }
}
