﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.Xml;
using System.IO;
using System.Web;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
	class ProviderEMerchantPayGenesis3DS : ProviderBase
	{
		#region Constants

		#endregion

		#region Private members


		private const string UNDEFINED = "undefined";
		private string providerName;
		private string UrlExecute;
		private string UrlGetStatus;
		private string MerchantAccountPassword;
		private string MerchantAccountID;
		private string ReturnUrl;
		private string MerchantAccountUserName;

		private class Error
		{
			public int Code = 0;
			public string Description = string.Empty;
		}
		#endregion

		#region Constructor

		public ProviderEMerchantPayGenesis3DS(ProviderConfigBase config) : base(config)
		{
			WireCapitalProviderConfig emerchantPayGenesis3DSconfig = config as WireCapitalProviderConfig;
			if (emerchantPayGenesis3DSconfig == null)
			{
				throw new ArgumentException("emerchantPayGenesis3DSconfig expects to get WireCapitalProviderConfig object!");
			}

			UrlExecute = emerchantPayGenesis3DSconfig.UrlExecute;								// gate URL
			UrlGetStatus = emerchantPayGenesis3DSconfig.UrlGetStatus;							// currency and their tokens
			MerchantAccountPassword = emerchantPayGenesis3DSconfig.MerchantAccountPassword;		// password ID
			MerchantAccountID = emerchantPayGenesis3DSconfig.MerchantAccountID;					// username ID
			ReturnUrl = emerchantPayGenesis3DSconfig.ReturnUrl;									// callback url, success and fail redirect page
			MerchantAccountUserName = emerchantPayGenesis3DSconfig.MerchantAccountUserName;     // transaction_type (sale, sale3d)
		}

		#endregion

		#region Public methods

		public override DepositResponse Deposit(DepositRequest depositRequest)
		{
			string transactionID = Guid.NewGuid().ToString().Replace("-", "");
			DepositResponse _DepositResponse = new DepositResponse();
			_DepositResponse.provider = depositRequest.provider;
			_DepositResponse.ClearingUserID = MerchantID;
			_DepositResponse.Amount = depositRequest.Amount.ToString();

			providerName = depositRequest.provider.ToString();

			// build the request
			string req = BuildRequest(depositRequest, transactionID);
			if (string.IsNullOrEmpty(req))
			{
				_DepositResponse.ErrorDescripotion = "Fail construct request string for EmerchantPayGenesis3DS";
				_DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
				_DepositResponse.ErrorCode = "1";
				_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
				return _DepositResponse;
			}

			Log(string.Format("Request: Provider=EmerchantPayGenesis3DS; {0} {1}{2}", UrlExecute, req, Environment.NewLine));

			/* Adding Header to request */
			string username = MerchantAccountID;
			string password = MerchantAccountPassword;
			string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
			NameValueCollection requestHeader = new NameValueCollection();
			requestHeader.Add("Authorization", "Basic " + svcCredentials);

			/* Adding token to url */
			var dict = UrlGetStatus.Split(new[] { '@' }, StringSplitOptions.RemoveEmptyEntries)
						  .Select(part => part.Split(';'))
						  .ToDictionary(split => split[0], split => split[1]);
			string temp = dict[depositRequest.CurrencyISOCode];
			string[] cred = temp.Split(';');
			string token = cred[0];
			UrlExecute = UrlExecute + token;

			

			// send POST
			Error e = HttpReq("POST", UrlExecute, req, requestHeader, "text/xml", "", 20000, 20000);

			Log(string.Format("Response: Provider=EmerchantPayGenesis3DS; {0}{1}", e.Description, Environment.NewLine));

			if (e.Code != 0)
			{
				_DepositResponse.ErrorDescripotion = "Http request to EmerchantPayGenesis3DS failed. " + e.Description;
				_DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
				_DepositResponse.ErrorCode = "1";
				_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;				
			}
			else
			{
				try
				{
					string result = e.Description;

					// check if its xml format
					int startOfXML = result.IndexOf("<?xml");
					if (startOfXML > -1)
					{
						string xmlResponse = result.Substring(startOfXML);

						// Now load the string into an XmlDocument 
						XmlDocument responseDocument = new XmlDocument();
						responseDocument.LoadXml(xmlResponse);
						ParseResponseXml(responseDocument, _DepositResponse);
					}
					else
					{
						_DepositResponse.ErrorDescripotion = "EmerchantPayGenesis3DS: response is invalid: " + result;
						_DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
						_DepositResponse.ErrorCode = "1";
						_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;                        
                    }

				}
				catch (UriFormatException)
				{
					_DepositResponse.ErrorDescripotion = "EmerchantPayGenesis3DS: Couldn't parse URL return";
					_DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
					_DepositResponse.ErrorCode = "1";
					_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
					return _DepositResponse;
				}
			}

            // fill the class FailedDeposit with details in order to send email to support later
            if (_DepositResponse.error.ErrorCode == Globals.ErrorCodes.FAIL)
            {
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = depositRequest.provider;
                failedDeposit.Email = depositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = depositRequest.Amount;
                failedDeposit.FirstName = depositRequest.FirstName;
                failedDeposit.LastName = depositRequest.LastName;
                failedDeposit.Description = _DepositResponse.error.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }

            return _DepositResponse;
		}

		#endregion

		#region Private methods

		private string BuildRequest(DepositRequest depositRequest, string transactionId)
		{

			string req = string.Empty;

			try
			{
				string transaction_type = MerchantAccountUserName;
				string transaction_id = transactionId;
				string remote_ip = depositRequest.IpAddress;
				int amount = Convert.ToInt32(depositRequest.Amount * 100);
				string currency = depositRequest.CurrencyISOCode;
				string cardHolderName = depositRequest.FirstName.Trim() + " " + depositRequest.LastName.Trim();
				string cardNumber = depositRequest.CardNumber;
				string cvv = depositRequest.CardCVV2;
				string cardExpireMonth = depositRequest.CardExpMonth;
				string cardExpireYear = depositRequest.CardExpYear;
				string customer_email = depositRequest.Email;
				string customer_phone = depositRequest.PhoneNumber;
				string first_name = depositRequest.FirstName;
				string last_name = depositRequest.LastName;
				string address1 = depositRequest.Address;
				string address2 = depositRequest.Address;
				string zip_code = depositRequest.ZipCode;
				string city = depositRequest.City;
				string state = depositRequest.StateISOCode;
				string country = depositRequest.CountryISOCode;
				string birth_date = GetRandomDate(new DateTime(1970, 01, 01), new DateTime(1999, 01, 01)).ToString("dd-MM-yyyy");
				if (string.IsNullOrEmpty(birth_date))
				{
					birth_date = "01-01-1990";
				}
				if (currency == "JPY")
				{
					amount = Convert.ToInt32(depositRequest.Amount); 
				}
				if (country == "US" || country == "CA")
				{
					state = depositRequest.CountryISOCode;
				}
				System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
				if (cardExpireYear.Length == 2)
					cardExpireYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(depositRequest.CardExpYear)).ToString();
				if (transaction_type == "sale")
				{
					req = string.Format(@"<?xml version='1.0' encoding='UTF-8'?> 
						   <payment_transaction>
							  <transaction_type xsi:type='xsd:string'>{0}</transaction_type>
							  <transaction_id xsi:type='xsd:string'>{1}</transaction_id>
							  <remote_ip xsi:type='xsd:string'>{2}</remote_ip>            
							  <amount xsi:type='xsd:string'>{3}</amount>
							  <currency xsi:type='xsd:int'>{4}</currency>
							  <card_holder xsi:type='xsd:string'>{5}</card_holder>
							  <card_number xsi:type='xsd:string'>{6}</card_number>
							  <cvv xsi:type='xsd: string'>{7}</cvv>
							  <expiration_month xsi:type='xsd:string'>{8}</expiration_month>
							  <expiration_year xsi:type='xsd:string'>{9}</expiration_year>
							  <customer_email xsi:type='xsd:string'>{10}</customer_email>
							  <customer_phone xsi:type='xsd:string'>{11}</customer_phone>
							  <birth_date xsi:type='xsd:string'>{12}</birth_date>
							 <billing_address>
							  <first_name xsi:type='xsd:string'>{13}</first_name>
							  <last_name xsi:type='xsd:string'>{14}</last_name>
							  <address1 xsi:type='xsd:string'>{15}</address1>
							  <address2 xsi:type='xsd:string'>{16}</address2>
							  <zip_code xsi:type='xsd:string'>{17}</zip_code>
							  <city xsi:type='xsd:string'>{18}</city>
							  <state xsi:type='xsd:string'>{19}</state>
							  <country xsi:type='xsd:string'>{20}</country> 
							 </billing_address>         
						   </payment_transaction>
						", transaction_type, transaction_id, remote_ip, amount,
										currency, cardHolderName, cardNumber, cvv,
										cardExpireMonth, cardExpireYear, customer_email,
										customer_phone, birth_date, first_name,
										last_name, address1, address2,
										zip_code, city, state, country);
				}
				if (transaction_type == "sale3d")
				{
					/* Split ReturnUrl to notification_url, success_url and failure_url  */
					var dict1 = ReturnUrl.Split(new[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
					string[] cred1 = dict1;
					string notification_url = cred1[0];
					string return_success_url = cred1[1];
					string return_failure_url = cred1[2];

					req = string.Format(@"<?xml version='1.0' encoding='UTF-8'?> 
						   <payment_transaction>
							  <transaction_type xsi:type='xsd:string'>{0}</transaction_type>
							  <transaction_id xsi:type='xsd:string'>{1}</transaction_id>
							  <notification_url xsi:type='xsd:string'>{2}</notification_url>
							  <return_success_url xsi:type='xsd:string'>{3}</return_success_url>
							  <return_failure_url xsi:type='xsd:string'>{4}</return_failure_url>
							  <remote_ip xsi:type='xsd:string'>{5}</remote_ip>            
							  <amount xsi:type='xsd:string'>{6}</amount>
							  <currency xsi:type='xsd:int'>{7}</currency>
							  <card_holder xsi:type='xsd:string'>{8}</card_holder>
							  <card_number xsi:type='xsd:string'>{9}</card_number>
							  <cvv xsi:type='xsd: string'>{10}</cvv>
							  <expiration_month xsi:type='xsd:string'>{11}</expiration_month>
							  <expiration_year xsi:type='xsd:string'>{12}</expiration_year>
							  <customer_email xsi:type='xsd:string'>{13}</customer_email>
							  <customer_phone xsi:type='xsd:string'>{14}</customer_phone>
							  <birth_date xsi:type='xsd:string'>{15}</birth_date>
							 <billing_address>
							  <first_name xsi:type='xsd:string'>{16}</first_name>
							  <last_name xsi:type='xsd:string'>{17}</last_name>
							  <address1 xsi:type='xsd:string'>{18}</address1>
							  <address2 xsi:type='xsd:string'>{19}</address2>
							  <zip_code xsi:type='xsd:string'>{20}</zip_code>
							  <city xsi:type='xsd:string'>{21}</city>
							  <state xsi:type='xsd:string'>{22}</state>
							  <country xsi:type='xsd:string'>{23}</country> 
							 </billing_address>         
						   </payment_transaction>
						", transaction_type, transaction_id, notification_url, return_success_url, return_failure_url, remote_ip, amount,
						currency, cardHolderName, cardNumber, cvv,
						cardExpireMonth, cardExpireYear, customer_email,
						customer_phone, birth_date, first_name,
						last_name, address1, address2,
						zip_code, city, state, country);
				}
				else
					Log(string.Format(" Fail! Transaction_type is not define correctly"));
			}
			catch (Exception ex)
			{
				Log(string.Format("Provider=EmerchantPayGenesis3DS; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
			}

			return req;
		}

		private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
		{
			Error resp = new Error();

			try
			{
				System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

				HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

				req.Method = Method.ToUpper();

				if (Header is System.Collections.Specialized.NameValueCollection)
					req.Headers.Add(Header);
				if (!string.IsNullOrEmpty(ContentType))
					req.ContentType = ContentType;
				if (!string.IsNullOrEmpty(Accept))
					req.Accept = Accept;
				if (!string.IsNullOrEmpty(Cred))
					req.Headers[HttpRequestHeader.Authorization] = Cred;

				req.Timeout = ConnectTimeoutMSec;
				req.ReadWriteTimeout = ReadWriteTimeoutMSec;

				if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
				{
					req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

					using (Stream stm = req.GetRequestStream())
					{
						stm.WriteTimeout = ReadWriteTimeoutMSec;
						using (StreamWriter stmw = new StreamWriter(stm))
						{
							stmw.Write(Parameters);
						}
					}
				}

				HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
				using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
				{
					resp.Description = rd.ReadToEnd();
				}


			}
			catch (WebException we)
			{
				if (we.Response != null)
				{
					using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
					{
						resp.Code = -1;
						resp.Description = responseReader.ReadToEnd();
					}
				}
				else
				{
					if ((we.Response is System.Net.HttpWebResponse))
						resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
					else
						resp.Code = (int)we.Status;
					resp.Description = resp.Code.ToString() + ", " + we.Message;
				}
			}
			catch (Exception ex)
			{
				resp.Code = 1;
				resp.Description = ex.Message;
			}

			return resp;
		}

		//Parses response Xml. Returns DataCashDepositResponseParameters object
		private void ParseResponseXml(XmlDocument docResponse, DepositResponse response)
		{
			// Get the root node 
			XmlElement rootElm = docResponse.DocumentElement;

			string transaction_type = string.Empty;			
			string status = string.Empty;
			string response_code = string.Empty;
			string unique_id = string.Empty;
			string transaction_id = string.Empty;
			string code = string.Empty;
			string technical_message = string.Empty;
			string message = string.Empty;
			string mode = string.Empty;
			string timestamp = string.Empty;
			string descriptor = string.Empty;
			string amount = string.Empty;
			string currency = string.Empty;
			string sent_to_acquirer = string.Empty;
			string redirect_url = string.Empty;

			transaction_type = GetXmlElementValue(rootElm, "transaction_type");
			status = GetXmlElementValue(rootElm, "status");
			response_code = GetXmlElementValue(rootElm, "response_code");
			unique_id = GetXmlElementValue(rootElm, "unique_id");
			transaction_id = GetXmlElementValue(rootElm, "transaction_id");
			redirect_url = GetXmlElementValue(rootElm, "redirect_url");
			code = GetXmlElementValue(rootElm, "code");
			technical_message = GetXmlElementValue(rootElm, "technical_message");
			message = GetXmlElementValue(rootElm, "message");
			mode = GetXmlElementValue(rootElm, "mode");
			timestamp = GetXmlElementValue(rootElm, "timestamp");
			descriptor = GetXmlElementValue(rootElm, "descriptor");
			amount = GetXmlElementValue(rootElm, "amount");
			currency = GetXmlElementValue(rootElm, "currency");
			sent_to_acquirer = GetXmlElementValue(rootElm, "sent_to_acquirer");
			

			response.TransactionID = transaction_id;

			if (status == "approved")
			{
				response.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
				response.ErrorDescripotion = response.error.Description;
				return;
			}
			if (status == "pending_async")
			{

				// get redirect url
				if (GetXmlElementValue(rootElm, "redirect_url") != "undefined")
				{				
					response.RedirectUrl = redirect_url;
				}
				
				response.Is3DSecure = true;
				response.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
				response.ErrorDescripotion = response.error.Description;
				return;
			}
			else
			{
				response.error.ErrorCode = Globals.ErrorCodes.FAIL;
				response.ErrorMessage = technical_message;
				response.error.Description = message  + "," + technical_message;
				response.ErrorDescripotion = response.error.Description;
				return;
			} 

		}

		//Returns Xml element value
		private string GetXmlElementValue(XmlElement rootElm, string elmName)
		{
			// Find all descendants with the desired name 
			XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

			// Assume only one "hit" and return the contained text
			string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
			return resultValue;
		}

		static readonly Random rnd = new Random();
		public static DateTime GetRandomDate(DateTime from, DateTime to)
		{
			var range = to - from;

			var randTimeSpan = new TimeSpan((long)(rnd.NextDouble() * range.Ticks));

			return from + randTimeSpan;
		}

		#endregion

		protected override Globals.Providers ProviderType
		{
			get { return Globals.Providers.EMERCHANTPAYGENESIS3DS; }
			set { }
		}
	}
}
