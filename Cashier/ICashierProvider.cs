﻿using System;
using System.Collections.Generic;
using System.Linq;
using CashierUtils;

namespace Cashier
{
    internal interface ICashierProvider
    {
        DepositResponse Deposit(DepositRequest _DepositRequest);
        string Url { get; set; }
        string Url2 { get; set; }
        string MerchantID { get; set; }
        string MerchantPassword { get; set; }
        string CallbackUrl { get; set; }
    }
}
