﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using System.Security.Cryptography;
using CashierUtils;

namespace Cashier
{
    class ProviderPayTechnique : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        string ChannelID = string.Empty;
        string SoapAction = string.Empty;
        string ReturnUrl = string.Empty;

        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            {"visa", "VISA"},
            {"Visa", "VISA"},
            {"VISA", "VISA"},
            {"mastercard", "MASTERCARD"},
            {"Mastercard", "MASTERCARD"},
            {"MASTERCARD", "MASTERCARD"},
            {"diners club", "DINERSCLUB"},
            {"dinersclub", "DINERSCLUB"},
            {"DINERSCLUB", "DINERSCLUB"},
            {"dinersclubintl", "DINERSCLUB"},
            {"Diners Club", "DINERSCLUB"},
            {"american express", "AMEX"},
            {"American Express", "AMEX"},
            {"amex", "AMEX"},
            {"maestro","MAESTRO"},
            {"Maestro","MAESTRO"},
            {"discover","DISCOVER"}
        };

        #region Constants
        const string NAMESPACE_PREFIX1 = "SOAP-ENV";
        const string NAMESPACE_URI1 = "SOAP-http://schemas.xmlsoap.org/soap/envelope/";
        const string NAMESPACE_PREFIX2 = "ns1";
        const string NAMESPACE_URI2 = "urn:PayTechnique";

        const string XPATH_FAULTSTRING = "//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/faultstring";
        const string XPATH_DETAIL = "//SOAP-ENV:Envelope/SOAP-ENV:Body/SOAP-ENV:Fault/detail";
        const string XPATH_TRANSACTIONSTATUS = "//SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:saleResponse/saleReturn/transactionStatus";
        const string XPATH_TRANSACTIONERROR = "//SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:saleResponse/saleReturn/transactionError";
        const string XPATH_VERIFICATIONURL = "//SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:saleResponse/saleReturn/verificationUrl";
        const string XPATH_VERIFACTIONPARAMS = "//SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:saleResponse/saleReturn/verificationParams";
        #endregion

        #region Constructor

        public ProviderPayTechnique(ProviderConfigBase config)
            : base(config)
        {
            PayTechniqueProviderConfig payTechniqueConfig = config as PayTechniqueProviderConfig;
            if (payTechniqueConfig == null)
            {
                throw new ArgumentException("payTechniqueConfig expects to get PayTechniqueProviderConfig object!");
            }

            ChannelID = payTechniqueConfig.ChannelID;
            SoapAction = payTechniqueConfig.SoapAction;
            ReturnUrl = payTechniqueConfig.ReturnUrl;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.PAYTECHNIQUE; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            string transactionID = ((int)(DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds).ToString();

            _DepositResponse.Is3DSecure = false;

            _DepositResponse.provider = _DepositRequest.provider;
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();

            string req = BuildRequestXML(_DepositRequest, transactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail building request XML";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            }
            else
            {
                Log(string.Format("Request: Provider=PayTechnique; {0}{1}", req, Environment.NewLine));

                //string SoapAction = "urn:PayTechniqueSoapServerAction";
                System.Collections.Specialized.NameValueCollection headers = new System.Collections.Specialized.NameValueCollection();
                headers.Add("SOAPAction", "\"" + SoapAction + "\"");

                Error e = HttpReq("POST", Url, req, headers, "text/xml;charset=UTF-8", "text/xml", 20000, 20000);

                Log(string.Format("Response: Provider=PayTechnique; {0}{1}", e.Description, Environment.NewLine));

                if (e.Code != 0)
                {
                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                    if (e.Code == -1)
                        _DepositResponse.ErrorDescripotion = ParseErrorResponse(e.Description);
                    else
                        _DepositResponse.ErrorDescripotion = "Http request to PayTechnique failed. " + e.Description;

                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = _DepositRequest.provider;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = transactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = e.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);
                }
                else
                {
                    ParseResponse(e.Description, _DepositRequest, _DepositResponse);
                }

            }

            return _DepositResponse;
        }

        private string ParseErrorResponse(string xml)
        {
            string error = string.Empty;

            XmlDocument x = new XmlDocument();
            x.LoadXml(xml);
            XmlNamespaceManager manager = new XmlNamespaceManager(x.NameTable);
            manager.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            manager.AddNamespace("ns1", "urn:PayTechnique");
            XmlNode faultstring = x.SelectSingleNode(XPATH_FAULTSTRING, manager);
            XmlNode detail = x.SelectSingleNode(XPATH_DETAIL, manager);

            if (faultstring is XmlNode)
                error += faultstring.InnerText + " , ";
            if (detail is XmlNode)
                error += detail.InnerText;

            return error;
        }

        private void ParseResponse(string XML, DepositRequest _DepositRequest, DepositResponse _DepositResponse)
        {
            try
            {
                if (XML.Contains("faultstring"))
                {
                    string error = ParseErrorResponse(XML);
                    _DepositResponse.ErrorDescripotion = error;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.ErrorCode = "1";
                    return;
                }

                XmlDocument x = new XmlDocument();
                x.LoadXml(XML);
                XmlNamespaceManager manager = new XmlNamespaceManager(x.NameTable);
                manager.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
                manager.AddNamespace("ns1", "urn:PayTechnique");

                //XmlNode statusCode = x.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:saleResponse/saleReturn/statusCode", manager);
                XmlNode transactionStatus = x.SelectSingleNode(XPATH_TRANSACTIONSTATUS, manager);
                if (transactionStatus == null)
                {
                    Log(string.Format("Request: Provider=PayTechnique; Invalid XML response. Could not extract 'transactionStatus' element from XML{1}", Environment.NewLine));
                    _DepositResponse.ErrorDescripotion = "Invalid XML response. Could not extract 'transactionStatus' element from XML";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.ErrorCode = "1";
                    return;
                }

                if (transactionStatus.InnerText == "SUCCESS")
                {
                    _DepositResponse.ErrorDescripotion = "";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                    _DepositResponse.ErrorCode = "0";
                }
                else if (transactionStatus.InnerText == "VERIFY")
                {
                    // 3ds connection
                    _DepositResponse.Is3DSecure = true;

                    // parse <verificationUrl> element
                    XmlNode redirectUrl = x.SelectSingleNode(XPATH_VERIFICATIONURL, manager);
                    if (redirectUrl == null)
                    {
                        Log(string.Format("Request: Provider=PayTechnique; Invalid XML response. Could not extract 'redirectUrl' element from XML{1}", Environment.NewLine));
                        _DepositResponse.ErrorDescripotion = "Invalid XML response. Could not extract 'redirectUrl' element from XML";
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        _DepositResponse.ErrorCode = "1";
                        return;
                    }

                    _DepositResponse.RedirectUrl = redirectUrl.InnerText;

                    // parse <verificationParams> element
                    _DepositResponse.RedirectParameters = new List<RedirectValues>();
                    XmlNode verificationParams = x.SelectSingleNode(XPATH_VERIFACTIONPARAMS, manager);
                    if (verificationParams == null)
                    {
                        Log(string.Format("Request: Provider=PayTechnique; Invalid XML response. Could not extract 'verificationParams' element from XML{1}", Environment.NewLine));
                        _DepositResponse.ErrorDescripotion = "Invalid XML response. Could not extract 'verificationParams' element from XML";
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        _DepositResponse.ErrorCode = "1";
                        return;
                    }
                    
                    string sVerificationParams = verificationParams.InnerText;
                    if (sVerificationParams.Trim().Length == 0)
                    {
                        Log(string.Format("Request: Provider=PayTechnique; Invalid XML response. 'verificationParams' is empty from XML{1}", Environment.NewLine));
                        _DepositResponse.ErrorDescripotion = "Invalid XML response. 'verificationParams' element is empty from XML";
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        _DepositResponse.ErrorCode = "1";
                        return;
                    }

                    System.Collections.Specialized.NameValueCollection qscoll = System.Web.HttpUtility.ParseQueryString(sVerificationParams);

                    _DepositResponse.RedirectParameters = new List<RedirectValues>();

                    RedirectValues pareq = new RedirectValues();
                    pareq.Key = "PaReq";
                    pareq.Value = qscoll["PaReq"];
                    _DepositResponse.RedirectParameters.Add(pareq);
                    
                    RedirectValues md = new RedirectValues();
                    md.Key = "MD";
                    md.Value = qscoll["MD"];
                    _DepositResponse.RedirectParameters.Add(md);
                    
                    RedirectValues termurl = new RedirectValues();
                    termurl.Key = "TermUrl";
                    termurl.Value = qscoll["TermUrl"];
                    _DepositResponse.RedirectParameters.Add(termurl);
                    
                    _DepositResponse.DataToSave = _DepositRequest.CardNumber.Substring(_DepositRequest.CardNumber.Length - 4) + "<SEP>";
                    _DepositResponse.DataToSave += _DepositRequest.CardType + "<SEP>";
                    _DepositResponse.DataToSave += _DepositRequest.Amount.ToString() + "<SEP>";
                    _DepositResponse.DataToSave += _DepositRequest.CurrencyISOCode;

                    _DepositResponse.DataToSaveKey = _DepositResponse.TransactionID;

                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                    _DepositResponse.ErrorDescripotion = "";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                }
                else
                {
                    XmlNode transactionError = x.SelectSingleNode(XPATH_TRANSACTIONERROR, manager);
                    if (transactionError != null)
                        _DepositResponse.ErrorDescripotion = transactionError.InnerText;
                    else
                        _DepositResponse.ErrorDescripotion = "FAIL";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.ErrorCode = "1";
                }

            }
            catch (Exception ex)
            {
                Log(string.Format("Request: Provider=PayTechnique; Exception on parsing XML response: {0}{1}", ex.Message, Environment.NewLine));
                _DepositResponse.ErrorDescripotion = "Fail parsing PayTechnique XML response: " + ex.Message;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.ErrorCode = "1";
            }
        }

        private string GenerateMD5String(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes 
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string. 
                return sBuilder.ToString();
            }
        }

        string BuildRequestXML(DepositRequest _DepositRequest, string TransactionID)
        {
            string req = string.Empty;

            try
            {
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                DateTime dt = new DateTime(ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)), 1, 1);
                string ExpYear = dt.ToString("yy");

                //MMYY
                string cardExpiration = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00") + ExpYear;

                string CardType;
                if (!cardTypeMapping.TryGetValue(_DepositRequest.CardType, out CardType))
                {
                    Log(string.Format("Response: Provider=PayTechnique; Not supported card type: {0}{1}", _DepositRequest.CardType, Environment.NewLine));
                    return req;
                }

                string description = "Fund Deposit";

                string Amount = _DepositRequest.Amount.ToString("F2");

                string signature = cardExpiration + _DepositRequest.CardNumber + CardType + _DepositRequest.CardCVV2 + _DepositRequest.Address + _DepositRequest.City + _DepositRequest.CountryISOCodeThreeLetters +
                    _DepositRequest.Email + _DepositRequest.IpAddress + _DepositRequest.FirstName + " " + _DepositRequest.LastName + _DepositRequest.PhoneNumber + _DepositRequest.ZipCode + MerchantID +
                    Amount + _DepositRequest.CurrencyISOCode + description + TransactionID + ReturnUrl + MerchantPassword;
                signature = GenerateMD5String(signature);

                req = string.Format(@"<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:PayTechnique'>
   <soapenv:Header/>
   <soapenv:Body>
      <urn:sale soapenv:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>
         <params xsi:type='urn:PayTechniqueSoapSaleRequest'>
            <cardExpiration xsi:type='xsd:string'>{0}</cardExpiration>
            <cardNumber xsi:type='xsd:string'>{1}</cardNumber>
            <cardType xsi:type='xsd:string'>{2}</cardType>            
            <cardVerificationCode xsi:type='xsd:string'>{3}</cardVerificationCode>
            <customerAddress xsi:type='xsd:string'>{4}</customerAddress>
            <customerCity xsi:type='xsd:string'>{5}</customerCity>
            <customerCountry xsi:type='xsd:string'>{6}</customerCountry>
            <customerEmail xsi:type='xsd:string'>{7}</customerEmail>
            <customerIP xsi:type='xsd:string'>{8}</customerIP>
            <customerName xsi:type='xsd:string'>{9}</customerName>
            <customerPhoneNumber xsi:type='xsd:string'>{10}</customerPhoneNumber>            
            <customerState xsi:type='xsd:string'></customerState>
            <customerZipCode xsi:type='xsd:string'>{11}</customerZipCode>            
            <initRecurring xsi:type='xsd:string'></initRecurring>
            <merchantID xsi:type='xsd:string'>{12}</merchantID>                        
            <orderAmount xsi:type='xsd:string'>{13}</orderAmount>
            <orderCurrency xsi:type='xsd:string'>{14}</orderCurrency>
            <orderDescription xsi:type='xsd:string'>{15}</orderDescription>
            <orderReference xsi:type='xsd:string'>{16}</orderReference>            
            <returnUrl xsi:type='xsd:string'>{17}</returnUrl>
            <signature xsi:type='xsd:string'>{18}</signature>
         </params>
      </urn:sale>
   </soapenv:Body>
</soapenv:Envelope>", cardExpiration, _DepositRequest.CardNumber, CardType, _DepositRequest.CardCVV2, _DepositRequest.Address, _DepositRequest.City, _DepositRequest.CountryISOCodeThreeLetters,
                    _DepositRequest.Email, _DepositRequest.IpAddress, _DepositRequest.FirstName + " " + _DepositRequest.LastName, _DepositRequest.PhoneNumber, _DepositRequest.ZipCode, MerchantID,
                    Amount, _DepositRequest.CurrencyISOCode, description, TransactionID, ReturnUrl, signature);
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider=PayTechnique; Exception while building request XML: {0}{1}", ex.Message, Environment.NewLine));
                req = string.Empty;
            }

            return req;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;


                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }
    }
}
