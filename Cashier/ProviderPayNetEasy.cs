﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using Cashier.Configuration.ProviderConfig;
using System.Security.Cryptography;
using Cashier.Configuration;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using CashierUtils;

namespace Cashier
{
    internal class ProviderPayNetEasy : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        #region Constants
        //SALE Request 

        private const string POST = "POST";
        private const string CLIENT_ORDERID = "client_orderid";//TransactionID
        private const string ORDER_DESC = "order_desc";
        private const string CARD_PRINTED_NAME = "card_printed_name";
        private const string FIRST_NAME = "first_name";
        private const string LAST_NAME = "last_name";
        private const string PHONE = "phone";
        private const string ADDRESS1 = "Address1";
        private const string CITY = "City";
        private const string ZIP_CODE = "zip_code";
        private const string STATE = "state";
        private const string COUNTRY = "Country";
        private const string EMAIL = "email";
        private const string AMOUNT = "amount";
        private const string CURRENCY = "currency";
        private const string CREDIT_CARD_NUMBER = "credit_card_number";
        private const string EXPIRE_MONTH = "expire_month";
        private const string EXPIRE_YEAR = "expire_year";
        private const string CVV2 = "CVV2";
        private const string IPADDRESS = "ipaddress";
        private const string COMMENT = "comment";
        private const string ORDERID_TEST = "orderid";
        private const string CALLBACK = "server_callback_url";

        private const string CONTROL = "control";
        private const string REDIRECT_URL = "redirect_url";

        private const string ENDPOINTID = "ENDPOINTID";
        private const string MERCHANT_CONTROL = "merchant_control";
        //SALE Response
        private const string TYPE = "type";
        private const string ORDERID = "paynet-order-id";
        private const string MERCHANT_ORDER_ID = "merchant-order-id";
        private const string SERIAL_NUMBER = "serial-number";
        private const string ERROR_MSG = "error-message";
        private const string ERROR_CODE = "error-code";

        //STATUS Request
        private const string LOGIN = "login";
        private const string ORDERID_REQ = "orderid";
        private const string SN_REQ = "by-request-sn";
        private const string CONTROL_REQ = "control";


        //STATUS Response
        private const string STATUS = "status";
        private const string PAYMENT_ORDERID = "paynet-order-id";
        private const string HTML = "html";
        private const string CARD_TYPE = "card-type";
        private const string GATE_PARTIAL_REVERSAL = "gate-partial-reversal";
        private const string GATE_PARTIAL_CAPTURE = "gate-partial-capture";
        private const string TRANSACTION_TYPE = "transaction-type";
        private const string PROCESSOR_RRN = "processor-rrn";
        private const string APPROVAL_CODE = "approval-code";
        private const string ORDER_STAGE = "order-stage";
        private const string LOYALTY_BALANCE = "loyalty-balance";
        private const string LOYALTY_MESSAGE = "loyalty-message";
        private const string LOYALTY_BONUS = "loyalty-bonus";
        private const string LOYALTY_PROGRAM = "loyalty-program";
        private const string LAST_4_DIGITS = "last-four-digits";
        private const string BIN = "bin";
        private const string RECEIPT_ID = "receipt-id";
        private const string BANK_NAME = "bank-name";
        private const string DATE = "paynet-processing-date";
        private const string CARD_HASH_ID = "card-hash-id";
        private const string SUCCESS_STATUS = "approved";

        private const string PAYMENT_FORM_REDIRECT = "redirect-url";


        private string controlKey;
        private string endPoint;
        private string CallbackUrl;
        private string ReturnUrl;
        private string SaleUrl;
        private string StatusUrl;

        private string ProviderName;
        //private readonly CashierConfigurator configurator;
        //private SQLiteDatabase db;
        #endregion

        #region Constructor

        public ProviderPayNetEasy(ProviderConfigBase config) : base(config)
        {
            FibonatixProviderConfig fibonatixConfig = config as FibonatixProviderConfig;
            if (fibonatixConfig == null)
            {
                throw new ArgumentException("ProviderPayNetEasy expects to get ProviderPayNetEasy object!");
            }

            controlKey = fibonatixConfig.CONTROLKEY;
            endPoint = fibonatixConfig.ENDPOINT;
            CallbackUrl = fibonatixConfig.CallbackUrl;
            ReturnUrl = fibonatixConfig.ReturnUrl;
            SaleUrl = fibonatixConfig.SaleUrl;
            StatusUrl = fibonatixConfig.StatusUrl;

            //string pathToCashierConfigFile = HttpContext.Current.Server.MapPath("~/Cashier.config");

            //configurator = new CashierConfigurator(pathToCashierConfigFile);

            /*try
            {
                db = new SQLiteDatabase(configurator.PathToCashierDB);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(configurator.PathToLogFile, DateTime.Now.ToString() + ": Cashier.ProviderPayNetEasy: ERROR creating sqlLite DB:  " + ex.Message + "\r\n");
                return;
            }*/
        }

        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.SOLIDPAYNETEASY; }
            set { }
        }

        #region Public methods

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();

            string TransactionID = Guid.NewGuid().ToString("n");

            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.TransactionID = TransactionID;
            _DepositResponse.provider = _DepositRequest.provider;
            ProviderName = _DepositRequest.provider.ToString();
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;

            //Log(_DepositRequest != null ? "OK" : "NULL");
            if (_DepositRequest != null)
            {
                try
                {
                    string req = CreateRequest(TransactionID, _DepositRequest);

                    string url = SaleUrl + endPoint;

                    Log(string.Format("Request: Provider=PayNetEasy; {0} {1}{2}", url, req, Environment.NewLine));

                    // send POST
                    Error e = HttpReq(POST, url, req, null, "application/x-www-form-urlencoded", "", 20000, 20000, null);

                    Log(string.Format("Response: Provider=PayNetEasy; {0}{1}", e.Description, Environment.NewLine));

                    if (e.Code != 0)
                    {
                        _DepositResponse.ErrorDescripotion = "Http request to PayNetEasy failed. " + e.Description;
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.ErrorCode = "1";
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                        // fill the class FailedDeposit with details in order to send email to support later
                        FailedDeposit failedDeposit = new FailedDeposit();
                        failedDeposit.provider = _DepositRequest.provider;
                        failedDeposit.Email = _DepositRequest.Email;
                        failedDeposit.TransactionID = TransactionID;
                        failedDeposit.Amount = _DepositRequest.Amount;
                        failedDeposit.FirstName = _DepositRequest.FirstName;
                        failedDeposit.LastName = _DepositRequest.LastName;
                        failedDeposit.Description = e.Description;

                        _DepositResponse.FailedDeposits.Add(failedDeposit);
                    }
                    else
                    {
                        e.Description = e.Description.Replace("\n", string.Empty);
                        _DepositResponse = ParseResponse(_DepositResponse, e.Description);
                        Log(string.Format("Type: {0}", _DepositResponse.Type));
                        Log(_DepositResponse.Type.Equals("async-response") ? "OK" : "NULL");
                        if ((_DepositResponse.Type).Equals("async-response"))
                        {
                            Log(string.Format("Response: {0}", _DepositResponse.MerchantOrderID));
                            string statusRequest =
                                CreateStatusRequest(MerchantID,
                                _DepositResponse.TransactionID,
                                _DepositResponse.PaynetOrderID,
                                _DepositResponse.SerialNnumber,
                                CalculateCheckSumResult(MerchantID, _DepositResponse.TransactionID, _DepositResponse.PaynetOrderID, controlKey));

                            url = StatusUrl + endPoint;

                            Log(string.Format("Status Request: Provider=PayNetEasy; {0} {1}{2}", url, statusRequest, Environment.NewLine));

                            NameValueCollection colStatus = new NameValueCollection();
                            colStatus["status"] = string.Empty;
                            colStatus["html"] = string.Empty;
                            int whileProcessing = 0;
                            Error e1 = new Error();
                            while (string.IsNullOrEmpty(e1.Description) || (colStatus["status"] == "processing" && string.IsNullOrEmpty(colStatus["html"])))
                            {


                                e1 = HttpReq(POST, url, statusRequest, null, "application/x-www-form-urlencoded", "", 20000, 20000, null);
                                if (whileProcessing == 0)
                                {
                                    Log(string.Format("Status Response: {0}", e1.Description));
                                }
                                e1.Description = e1.Description.Replace("\n", string.Empty);
                                colStatus = HttpUtility.ParseQueryString(e1.Description);
                                _DepositResponse = ParseResponseStatus(colStatus, _DepositResponse);
                                whileProcessing++;
                                Log(string.Format("Status : {0} ; Order stage: {1} ; HTML : {2} ; Processing # ; {3} ", colStatus["status"], colStatus["order-stage"], colStatus["html"], whileProcessing));

                                Thread.Sleep(200);
                            }
                            Log(string.Format("Status Response: {0}", e1.Description));
                        }
                        else if (e.Description.Contains("async-form-response"))
                        {
                            _DepositResponse = ParseFormResponse(_DepositResponse, e.Description);
                            if (!string.IsNullOrWhiteSpace(_DepositResponse.RedirectUrl))
                                _DepositResponse.Is3DSecure = true;

                            if (_DepositResponse.error.Description == null)
                                _DepositResponse.error.Description = "";
                        }
                        else
                        {
                            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                            _DepositResponse.error.Description = _DepositResponse.ErrorMessage;
                        }
                    }

                }
                catch (Exception ex)
                {
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.error.Description = ex.Message;

                }
            }
            else
            {
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.error.Description = "DepositRequest parameters can't be null";
            }

            /*if (!string.IsNullOrEmpty(_DepositResponse.error.Description))
            {
                _DepositResponse.error.Description = _DepositResponse.error.Description.Replace("+", " ");
                _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description;
            }*/

            _DepositResponse.error.Description = System.Web.HttpUtility.UrlDecode(_DepositResponse.error.Description);
            _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description;
            if (_DepositResponse.error.ErrorCode == Globals.ErrorCodes.FAIL)
            {
                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = _DepositResponse.TransactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            return _DepositResponse;
        }



        private string CreateStatusRequest(string merchantID, string merchantOrderID, string paynetOrderID, string serialNnumber, string control)
        {
            NameValueCollection data = new NameValueCollection()
            {
                { LOGIN , MerchantID},
                { CLIENT_ORDERID, merchantOrderID },
                { ORDERID_REQ, paynetOrderID },
                { SN_REQ, serialNnumber },
                { CONTROL_REQ, control }
            };

            var result = new StringBuilder("");

            foreach (string key in data.AllKeys)
            {
                string value = data[key];
                result.AppendFormat("{0}={1}&", key, value);
            }

            return result.ToString().TrimEnd('&');
        }

        #endregion.

        #region Private methods

        private string CreateRequest(string TransactionID, DepositRequest _DepositRequest)
        {
            Log(string.Format("controlKey: {0} ; endPoint: {1} ", controlKey, endPoint));

            /*try
            {
                if (string.IsNullOrEmpty(CallbackUrl))
                    CallbackUrl = "https://" + HttpContext.Current.Request.Url.Host + "/PayNetEasyHandler.aspx";
            }
            catch
            {
                CallbackUrl = "";
            }

            try
            {
                if (string.IsNullOrEmpty(ReturnUrl))
                    ReturnUrl = "https://" + HttpContext.Current.Request.Url.Host + "/PayNetEasyReturn.aspx";
            }
            catch
            {
                ReturnUrl = "";
            }*/

            if (string.IsNullOrEmpty(CallbackUrl))
                CallbackUrl = "";
            if (string.IsNullOrEmpty(ReturnUrl))
                ReturnUrl = "";

            if (_DepositRequest.CountryISOCode == "US") // united states
                _DepositRequest.StateISOCode = "MI";
            else if (_DepositRequest.CountryISOCode == "CA") // canada
                _DepositRequest.StateISOCode = "ON";
            else if (_DepositRequest.CountryISOCode == "AU") // australia
                _DepositRequest.StateISOCode = "NSW";

            Log("Country: " + _DepositRequest.CountryISOCode + " , State: " + _DepositRequest.StateISOCode);

            NameValueCollection nvc = new NameValueCollection()
            {
                { CLIENT_ORDERID, TransactionID},
                { EMAIL,HttpUtility.UrlEncode(_DepositRequest.Email) },
                { CURRENCY, _DepositRequest.CurrencyISOCode },
                { CREDIT_CARD_NUMBER, _DepositRequest.CardNumber },
                { CVV2, _DepositRequest.CardCVV2 },
                { CARD_PRINTED_NAME, HttpUtility.UrlEncode( _DepositRequest.FirstName + " " + _DepositRequest.LastName )},
                { FIRST_NAME, HttpUtility.UrlEncode(_DepositRequest.FirstName)},
                { LAST_NAME,  HttpUtility.UrlEncode(_DepositRequest.LastName) },
                { PHONE, HttpUtility.UrlEncode(_DepositRequest.PhoneNumber) },
                { ADDRESS1, HttpUtility.UrlEncode(_DepositRequest.Address) },
                { CITY, HttpUtility.UrlEncode(_DepositRequest.City) },
                { STATE, _DepositRequest.StateISOCode },
                { ZIP_CODE, HttpUtility.UrlEncode(_DepositRequest.ZipCode) },
                { COUNTRY, _DepositRequest.CountryISOCode },
                { EXPIRE_MONTH, _DepositRequest.CardExpMonth },
                { EXPIRE_YEAR, _DepositRequest.CardExpYear },
                { AMOUNT, _DepositRequest.Amount.ToString() },
                { ORDER_DESC,HttpUtility.UrlEncode(_DepositRequest.Description) },
                { IPADDRESS, _DepositRequest.IpAddress},
                {CONTROL,CalculateCheckSum(endPoint,TransactionID,_DepositRequest.Amount,_DepositRequest.Email,controlKey)}             
                //{REDIRECT_URL,ReturnUrl},
                //{CALLBACK,CallbackUrl}
            };

            if (!string.IsNullOrEmpty(ReturnUrl))
                nvc.Add(REDIRECT_URL, ReturnUrl);
            if (!string.IsNullOrEmpty(CallbackUrl))
                nvc.Add(CALLBACK, CallbackUrl);

            var result = new StringBuilder("");

            foreach (string key in nvc.AllKeys)
            {
                string value = nvc[key];
                result.AppendFormat("{0}={1}&", key, value);
            }

            return result.ToString().TrimEnd('&');

        }


        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                /*ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );*/

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }


            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private string SendRequest(NameValueCollection parametersCollection, string serviceUrl)
        {
            using (WebClient webClient = new WebClient())
            {
                try
                {

                    byte[] response = webClient.UploadValues(serviceUrl + endPoint, POST, parametersCollection);
                    return Encoding.UTF8.GetString(response);
                }
                catch (Exception ex)
                {
                    throw new Exception(ProviderName + ": An error has occured during sending request: " + ex.Message);
                }
            }
        }

        private DepositResponse ParseResponse(DepositResponse _DepositResponse, string response)
        {
            try
            {
                string[] responseSplit = response.Split('&');
                foreach (string value in responseSplit)
                {

                    string[] splitValue = value.Split('=');
                    switch (splitValue[0])
                    {
                        case TYPE:
                            _DepositResponse.Type = splitValue[1];
                            break;
                        case PAYMENT_ORDERID:
                            _DepositResponse.PaynetOrderID = splitValue[1];
                            break;
                        case MERCHANT_ORDER_ID:
                            _DepositResponse.TransactionID = splitValue[1];
                            break;
                        case SERIAL_NUMBER:
                            _DepositResponse.SerialNnumber = splitValue[1];
                            break;
                        case ERROR_MSG:
                            _DepositResponse.ErrorMessage = splitValue[1];
                            break;
                        case ERROR_CODE:
                            _DepositResponse.ErrorCode = splitValue[1];
                            break;
                    }
                }
                return _DepositResponse;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: An error has occured during parsing response '{1}'. Exception: {2}", ProviderName, response, ex.Message));
            }
        }

        private DepositResponse ParseFormResponse(DepositResponse _DepositResponse, string response)
        {
            try
            {
                string[] responseSplit = response.Split('&');
                foreach (string value in responseSplit)
                {

                    string[] splitValue = value.Split('=');
                    switch (splitValue[0])
                    {
                        case TYPE:
                            _DepositResponse.Type = splitValue[1];
                            break;
                        case PAYMENT_ORDERID:
                            _DepositResponse.PaynetOrderID = splitValue[1];
                            break;
                        case MERCHANT_ORDER_ID:
                            _DepositResponse.TransactionID = splitValue[1];
                            break;
                        case SERIAL_NUMBER:
                            _DepositResponse.SerialNnumber = splitValue[1];
                            break;
                        case ERROR_MSG:
                            _DepositResponse.ErrorMessage = splitValue[1];
                            break;
                        case ERROR_CODE:
                            _DepositResponse.ErrorCode = splitValue[1];
                            break;
                        case PAYMENT_FORM_REDIRECT:
                            _DepositResponse.RedirectUrl = System.Web.HttpUtility.UrlDecode(splitValue[1]);
                            break;
                    }
                }
                return _DepositResponse;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: An error has occured during parsing response '{1}'. Exception: {2}", ProviderName, response, ex.Message));
            }
        }

        private DepositResponse ParseResponseStatus(NameValueCollection colStatus, DepositResponse _DepositResponse)
        {
            if (colStatus[MERCHANT_ORDER_ID] == _DepositResponse.TransactionID && colStatus[SN_REQ] == _DepositResponse.SerialNnumber)
            {
                _DepositResponse.Status = colStatus[STATUS];
                _DepositResponse.Amount = colStatus[AMOUNT];
                _DepositResponse.PaynetOrderID = colStatus[PAYMENT_ORDERID];
                _DepositResponse.html3D = colStatus[HTML];
                _DepositResponse.CardLast4Digits = colStatus[LAST_4_DIGITS];
                _DepositResponse.bin = colStatus[BIN];
                _DepositResponse.CardType = colStatus[CARD_TYPE];
                _DepositResponse.TransactionType = colStatus[TRANSACTION_TYPE];
                _DepositResponse.Receipt = colStatus[RECEIPT_ID];
                _DepositResponse.BankName = colStatus[BANK_NAME];
                _DepositResponse.Date = colStatus[DATE];
                _DepositResponse.ApprovalCode = colStatus[APPROVAL_CODE];
                _DepositResponse.OrderStage = colStatus[ORDER_STAGE];
                _DepositResponse.ErrorDescripotion = colStatus[ERROR_MSG];
                _DepositResponse.ErrorCode = colStatus[ERROR_CODE];
                _DepositResponse.SerialNnumberStatus = colStatus[SERIAL_NUMBER];
                _DepositResponse.CardHashID = colStatus[CARD_HASH_ID];

            }
            else
            {
                Log(string.Format("MERCHANT_ORDER_ID Status: {0} ; MERCHANT_ORDER_ID Sale: {1} ; SerialNnumber Staus: {2} ; SerialNnumber Sale: {3}", colStatus[MERCHANT_ORDER_ID], _DepositResponse.TransactionID, colStatus[SN_REQ], _DepositResponse.SerialNnumber));
                _DepositResponse.ErrorDescripotion = colStatus[ERROR_MSG];
            }

            _DepositResponse.error.ErrorCode = _DepositResponse.Status == SUCCESS_STATUS ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
            if (!(string.IsNullOrEmpty(_DepositResponse.html3D)))
            {
                _DepositResponse.Is3DSecure = true;
                Log("Is3DSecure= " + _DepositResponse.Is3DSecure.ToString());
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
            }


            return _DepositResponse;
        }

        private string CalculateCheckSum(string endpointID, string clientOrderID, double amount, string email, string merchantControl)
        {
            double valueAmount = amount * 100.0;
            string newAmount = string.Format("{0}", Convert.ToInt32(valueAmount));
            string valuesRow = endpointID + clientOrderID + newAmount + email + merchantControl;

            byte[] bytes = Encoding.UTF8.GetBytes(valuesRow);
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);

            var result = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

        private string CalculateCheckSumResult(string MerchantID, string MerchantOrderID, string PaynetOrderID, string controlKey)
        {
            string valuesRow = MerchantID + MerchantOrderID + PaynetOrderID + controlKey;

            byte[] bytes = Encoding.UTF8.GetBytes(valuesRow);
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);

            var result = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

        #endregion
    }
}
