﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Collections.Specialized;
using CashierUtils;

namespace Cashier
{
	class ProviderEcommPay3DS : ProviderBase
	{
		private class Error
		{
			public int Code = 0;
			public string Description = string.Empty;
		}


		#region Create Purchase
		//Request


		[DataContract]
		public class CreateRequest
		{
			[DataMember]
			public string action { get; set; }
			[DataMember]
			public string site_id { get; set; }
			[DataMember]
			public string external_id { get; set; }
			[DataMember]
			public string signature { get; set; }
			[DataMember]
			public string amount { get; set; }
			[DataMember]
			public string currency { get; set; }
			[DataMember]
			public string exp_month { get; set; }
			[DataMember]
			public string exp_year { get; set; }
			[DataMember]
			public string card { get; set; }
			[DataMember]
			public string holder { get; set; }
			[DataMember]
			public string customer_ip { get; set; }
			[DataMember]
			public string cvv { get; set; }
			[DataMember]
			public string description { get; set; }
		}

		[DataContract]
		public class reservation_data
		{
			[DataMember]
			public string customer_name { get; set; }
		}


		[DataContract]
		public class CreateResponseObject
		{
			[DataMember]
			public int code { get; set; }
			[DataMember]
			public string message { get; set; }
			[DataMember]
			public string acquirer_id { get; set; }
			[DataMember]
			public string transaction_id { get; set; }
			[DataMember]
			public string processor_id { get; set; }
			[DataMember]
			public string processor_code { get; set; }
			[DataMember]
			public string processor_message { get; set; }
			[DataMember]
			public string amount { get; set; }
			[DataMember]
			public string currency { get; set; }
			[DataMember]
			public string real_amount { get; set; }
			[DataMember]
			public string real_currency { get; set; }
			[DataMember]
			public string order_status { get; set; }
			[DataMember]
			public string response_status { get; set; }
			[DataMember]
			public string signature { get; set; }
			[DataMember]
			public string external_id { get; set; }
			[DataMember]
			public string authcode { get; set; }
			[DataMember]
			public string acs_url { get; set; }
			[DataMember]
			public string term_url { get; set; }
			[DataMember]
			public string pa_req { get; set; }
			[DataMember]
			public string md { get; set; }
		}

		#endregion

		private string ReturnURL;

		public const string ACTION = "action";
		public const string SITE_ID = "site_id";
		public const string EXTERNAL_ID = "external_id";
		public const string AMOUNT = "amount";
		public const string CURRENCY = "currency";
		public const string EXP_MONTH = "exp_month";
		public const string EXP_YEAR = "exp_year";
		public const string CARD = "card";
		public const string HOLDER = "holder";
		public const string CUSTOMER_IP = "customer_ip";
		public const string CVV = "cvv";
		public const string DESCRIPTION = "description";
		public const string SIGNATURE = "signature";

		public ProviderEcommPay3DS(ProviderConfigBase config)
			: base(config)
		{
			ReturnURL = CallbackUrl;
		}

		#region Constructor

		protected override Globals.Providers ProviderType
		{
			get { return Globals.Providers.ECOMMPAY3DS; }
            set { }
        }

		#endregion

		public override DepositResponse Deposit(DepositRequest depositRequest)
		{
			DepositResponse depositResponse = new DepositResponse();
			//depositResponse.error = new Cashier.Error();
			depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
			depositResponse.ErrorCode = "1";
			string transactionID = Guid.NewGuid().ToString().Replace("-", "");
			depositResponse.Amount = depositRequest.Amount.ToString();
			depositResponse.CurrencyISOCode = depositRequest.CurrencyISOCode;
			depositResponse.provider = depositRequest.provider;
			depositResponse.ClearingUserID = MerchantID;
			depositResponse.TransactionID = transactionID;

			#region Trim            
			depositRequest.Address = depositRequest.Address.Trim();
			depositRequest.CardCVV2 = depositRequest.CardCVV2.Trim();
			depositRequest.CardExpMonth = depositRequest.CardExpMonth.Trim();
			depositRequest.CardExpYear = depositRequest.CardExpYear.Trim();
			depositRequest.CardNumber = depositRequest.CardNumber.Trim();
			depositRequest.City = depositRequest.City.Trim();
			depositRequest.CountryISOCode = depositRequest.CountryISOCode.Trim();
			if (!string.IsNullOrEmpty(depositRequest.CountryISOCodeThreeLetters))
				depositRequest.CountryISOCodeThreeLetters = depositRequest.CountryISOCodeThreeLetters.Trim();
			depositRequest.CurrencyISOCode = depositRequest.CurrencyISOCode.Trim();
			if (!string.IsNullOrEmpty(depositRequest.Description))
				depositRequest.Description = depositRequest.Description.Trim();
			depositRequest.Email = depositRequest.Email.Trim();
			depositRequest.FirstName = depositRequest.FirstName.Trim();
			depositRequest.LastName = depositRequest.LastName.Trim();
			if (!string.IsNullOrEmpty(depositRequest.NameOnCard))
				depositRequest.NameOnCard = depositRequest.NameOnCard.Trim();
			depositRequest.PhoneNumber = depositRequest.PhoneNumber.Trim();
			if (!string.IsNullOrEmpty(depositRequest.StateISOCode))
				depositRequest.StateISOCode = depositRequest.StateISOCode.Trim();
			depositRequest.ZipCode = depositRequest.ZipCode.Trim();

			#endregion

			CreateRequest createRequest = BuildPurchaseRequestObject(depositRequest, depositResponse);

			var json = JSONSerializer<CreateRequest>.Serialize(createRequest).Replace("\\/", "/");

			Log(string.Format("Request: Provider=EcommPay; {0} {1}{2}", Url, json, Environment.NewLine));

			Error e = HttpReq("POST", Url, json, null, "application/json", "application/json", 20000, 20000);
			Log(string.Format("Response : Provider=EcommPay; {0}{1}", e.Description, Environment.NewLine));
			if (e.Code != 0)
			{
				depositResponse.ErrorDescripotion = depositResponse.error.Description = "HTTP request for create PURCHASE failed: " + e.Description;
				Log(string.Format("Response: Provider=EcommPay; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

				// fill the class FailedDeposit with details in order to send email to support later
				FailedDeposit failedDeposit = new FailedDeposit();
				failedDeposit.provider = depositRequest.provider;
				failedDeposit.Email = depositRequest.Email;
				failedDeposit.TransactionID = depositResponse.TransactionID;
				failedDeposit.Amount = depositRequest.Amount;
				failedDeposit.FirstName = depositRequest.FirstName;
				failedDeposit.LastName = depositRequest.LastName;
				failedDeposit.Description = e.Description;

				depositResponse.FailedDeposits.Add(failedDeposit);

				return depositResponse;
			}

			CreateResponseObject createResponseObject = null;
			try
			{
				createResponseObject = JSONSerializer<CreateResponseObject>.DeSerialize(e.Description);

			}
			catch
			{
				depositResponse.ErrorDescripotion = depositResponse.error.Description = "Fail parsing http response to JSON: " + e.Description;
				Log(string.Format("Response: Provider=EcommPay; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
				return depositResponse;
			}

			parseResponse(createRequest, depositResponse, createResponseObject);

			return depositResponse;
		}

		private void parseResponse(CreateRequest createRequest, DepositResponse depositResponse, CreateResponseObject createResponseObject)
		{
			try
			{
				if (createResponseObject.code != 50)
				{
					// non 3ds
					if (createResponseObject.code == 0)
					{
						depositResponse.Receipt = createResponseObject.transaction_id;
						depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
						depositResponse.ErrorDescripotion = "";
						depositResponse.error.Description = depositResponse.ErrorDescripotion;
					}
					else
					{
						// failed
						depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
						depositResponse.ErrorCode = createResponseObject.code.ToString();
						depositResponse.ErrorMessage = createResponseObject.message;
						depositResponse.error.Description = string.Format("{0} - {1}", createResponseObject.code, createResponseObject.message);
						depositResponse.ErrorDescripotion = depositResponse.error.Description;
					}
				}
				else
				{
					// 3ds
					if (createResponseObject.acs_url != null)
					{
						depositResponse.Is3DSecure = true;

						depositResponse.Receipt = createResponseObject.transaction_id;

						depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
						depositResponse.ErrorCode = "0";
						depositResponse.ErrorDescripotion = "";
						depositResponse.error.Description = "";

						depositResponse.RedirectUrl = createResponseObject.acs_url;

						depositResponse.RedirectParameters = new List<RedirectValues>();

						RedirectValues pareq = new RedirectValues();
						pareq.Key = "PaReq";
						pareq.Value = createResponseObject.pa_req;
						depositResponse.RedirectParameters.Add(pareq);

						RedirectValues md = new RedirectValues();
						md.Key = "MD";
						md.Value = createResponseObject.transaction_id;
						depositResponse.RedirectParameters.Add(md);

						RedirectValues termurl = new RedirectValues();
						termurl.Key = "TermUrl";
						termurl.Value = ReturnURL;
						depositResponse.RedirectParameters.Add(termurl);

						//depositResponse.DataToSave = createRequest.customer_ip + "<SEP>";
						//depositResponse.DataToSave += pareq.Value  + "<SEP>";
						//depositResponse.DataToSave += createResponseObject.md + "<SEP>";						
						//depositResponse.DataToSave += createRequest.amount.ToString() + "<SEP>";
						//depositResponse.DataToSave += createRequest.currency;

						//depositResponse.DataToSaveKey = depositResponse.TransactionID;
					}
					else
					{
						depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
						depositResponse.ErrorCode = createResponseObject.code.ToString();
						depositResponse.ErrorMessage = createResponseObject.message;
						depositResponse.error.Description = string.Format("{0} - {1}", createResponseObject.code, createResponseObject.message);
						depositResponse.ErrorDescripotion = depositResponse.error.Description;
					}
				}
			}
			catch (Exception exp)
			{
				depositResponse.ErrorDescripotion = depositResponse.error.Description = "parseResponse: Fail in parsing parameters: " + exp.Message;
				Log(string.Format("Provider=EcommPay; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
			}
		}

		private CreateRequest BuildPurchaseRequestObject(DepositRequest request, DepositResponse response)
		{
			string action = "purchase";
			string amount =  (request.Amount * 100).ToString();

			// build reservation data as requested by DreamsPay email ( no docs )
			//example: {  "phonemobile": "+12345678",  "customer_address": "15 gannet street elspark",  "customer_country": "US",  "customer_state": "NY",  "customer_name": "Brandon Nyathi",  "customer_city": "New York",  "customer_zip": "1401"}

			var data = new NameValueCollection
			{
				{ACTION, action},
				{SITE_ID, MerchantID},
				{EXTERNAL_ID, response.TransactionID},
				{AMOUNT, amount},
				{CURRENCY, request.CurrencyISOCode},
				{EXP_MONTH, request.CardExpMonth},
				{EXP_YEAR, request.CardExpYear},
				{CARD, request.CardNumber},
				{HOLDER, request.FirstName + " " + request.LastName},
				{CUSTOMER_IP, request.IpAddress},
				{DESCRIPTION, request.Description},
				{CVV, request.CardCVV2}
			};

			CreateRequest createRequest = new CreateRequest();

			createRequest.action = action;
			createRequest.site_id = MerchantID;
			createRequest.external_id = response.TransactionID;
			createRequest.amount = amount;
			createRequest.currency = request.CurrencyISOCode;
			createRequest.exp_month = request.CardExpMonth;
			createRequest.exp_year = request.CardExpYear;
			createRequest.card = request.CardNumber;
			createRequest.holder = request.FirstName + " " + request.LastName;
			createRequest.customer_ip = request.IpAddress;
			createRequest.cvv = request.CardCVV2;
			createRequest.description = request.Description;
			createRequest.signature = CalculateCheckSum(data, MerchantPassword);

			return createRequest;
		}

		private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
		{
			Error resp = new Error();

			try
			{
				System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
				HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
				req.Method = Method.ToUpper();
				if (Header is System.Collections.Specialized.NameValueCollection)
					req.Headers.Add(Header);
				if (!string.IsNullOrEmpty(ContentType))
					req.ContentType = ContentType;
				if (!string.IsNullOrEmpty(Accept))
					req.Accept = Accept;
				if (!string.IsNullOrEmpty(Cred))
					req.Headers[HttpRequestHeader.Authorization] = Cred;

				req.Timeout = ConnectTimeoutMSec;
				req.ReadWriteTimeout = ReadWriteTimeoutMSec;

				if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
				{
					req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

					using (Stream stm = req.GetRequestStream())
					{
						stm.WriteTimeout = ReadWriteTimeoutMSec;
						using (StreamWriter stmw = new StreamWriter(stm))
						{
							stmw.Write(Parameters);
						}
					}
				}

				HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
				using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
				{
					resp.Description = rd.ReadToEnd();
				}


			}
			catch (WebException we)
			{
				if (we.Response != null)
				{
					using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
					{
						resp.Code = -1;
						resp.Description = responseReader.ReadToEnd();
					}
				}
				else
				{
					if ((we.Response is System.Net.HttpWebResponse))
						resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
					else
						resp.Code = (int)we.Status;
					resp.Description = resp.Code.ToString() + ", " + we.Message;
				}
			}
			catch (Exception ex)
			{
				resp.Code = 1;
				resp.Description = ex.Message;
			}

			return resp;
		}


		/// <summary>
		/// The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
		/// Parameters must not be URL encoded before signature calculation.
		/// Please follow these steps:
		/// 1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
		/// 2. Append the corresponding values together according to the alphabetical sequence of parameter names.
		/// 3. Add the secret to the end of the string.
		/// 4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
		/// </summary>
		private string CalculateCheckSum(NameValueCollection data, string secretKey)
		{
			//The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
			//Parameters must not be URL encoded before signature calculation.
			//Please follow these steps:
			//1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
			//2. Append the corresponding values together according to the alphabetical sequence of parameter names.
			//3. Add the secret to the end of the string.
			//4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.

			//1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
			var keys = data.AllKeys.OrderBy(k => k);

			var valuesRow = new StringBuilder();

			//2. Append the corresponding values together according to the alphabetical sequence of parameter names.		

			foreach (string key in keys)
			{
				valuesRow.Append(key + ":" + data[key] + ";");
			}

			//3. Add the secret to the last of the string.
			valuesRow.Append(secretKey);

			//4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
			byte[] bytes = Encoding.UTF8.GetBytes(valuesRow.ToString().Substring(0, valuesRow.Length));
			var sha1 = SHA1.Create();
			byte[] hashBytes = sha1.ComputeHash(bytes);

			var result = new StringBuilder();
			foreach (byte b in hashBytes)
			{
				result.Append(b.ToString("x2"));
			}

			return result.ToString();
		}

	}
}
