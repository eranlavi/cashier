﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using System.Xml;
using System.Security.Cryptography;
using CashierUtils;

namespace Cashier
{
    class ProviderCoala : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private Dictionary<string, string> ResultCodeTable { get; set; }
        private const string langCode = "EN";
        private const string versionNO = "20120101";
        private const string capture = "Y";
        private const string contract = "EXPRESS";
        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            {"UnionPay","UP" },
            {"unionpay","UP" },
            {"visa", "VI"},
            {"Visa", "VI"},
            {"mastercard", "MC"},
            {"Mastercard", "MC"},
            {"MasterCard", "MC"},
            {"diners club", "DN"},
            {"dinersclub", "DN"},
            {"dinersclubintl", "DN"},
            {"Diners Club", "DN"},
            {"american express", "AM"},
            {"American Express", "AM"},
            {"amex", "AX"},
            {"maestro","MR"},
            {"Maestro","MR"},
            {"Discover","DI"},
            {"discover","DI"},
            {"JCB","JB"}
        };
        private const string UNDEFINED = "undefined";

        #region Constructor

        public ProviderCoala(ProviderConfigBase config)
            : base(config)
        {
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.COALA; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();
            
            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = ProviderType;

            string TransactionID = Guid.NewGuid().ToString().Replace("-", "");
            _DepositResponse.TransactionID = TransactionID;

            string req = BuildRequest(_DepositRequest, TransactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for Coala";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=Coala; {0} {1}{2}", Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider=Coala; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to Coala failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = _DepositResponse.TransactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                try
                {
                    string result = e.Description;

                    // We get the HTTP Headers to start with, so 
                    // knock them off before creating the XML Document 
                    int startOfXML = result.IndexOf("<?xml");

                    // Make sure response is valid
                    if (startOfXML < 0)
                    {
                        _DepositResponse.ErrorDescripotion = "Coala: XML response is invalid: " + result;
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        return _DepositResponse;
                    }

                    string xmlResponse = result.Substring(startOfXML);

                    // Now load the string into an XmlDocument 
                    XmlDocument responseDocument = new XmlDocument();
                    responseDocument.LoadXml(xmlResponse);

                    _DepositResponse = ParseResponseXml(responseDocument);

                }
                catch (UriFormatException)
                {
                    _DepositResponse.ErrorDescripotion = "Coala: Couldn't parse URL return";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    return _DepositResponse;
                }

            }
            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

            return _DepositResponse;

        }

        private string BuildRequest(DepositRequest _DepositRequest, string TransactionID)
        {
            string clientKey = MerchantID;
            string clientRef = TransactionID;
            string amount = _DepositRequest.Amount.ToString("#.##").Replace(".", "").PadLeft(12, '0');
            string currCode = _DepositRequest.CurrencyISOCode;
            string pMethod = GetCardBrandByCardType(_DepositRequest.CardType);

            string secure_hash_secret = MerchantPassword;
            string secureHash = clientKey + "|" + clientRef + "|" + amount + "|" + currCode + "|" + langCode + "|" + pMethod + "|" + versionNO + "|" + contract + "|" + secure_hash_secret;
            
            //secureHash = CalculateCheckSum(pre_secureHash, secure_hash_secret);
            //SHA - 1(Original 160 - bit hash function)

            string shopperRef = _DepositRequest.CustomerID;

            string cardHolderName = System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName + _DepositRequest.LastName);

            string issuingBank = "";
            string billingAddress1 = System.Web.HttpUtility.UrlEncode(_DepositRequest.Address);
            string billingCity = System.Web.HttpUtility.UrlEncode(_DepositRequest.City);
            string billingState = (string.IsNullOrEmpty(_DepositRequest.StateISOCode)) ? ("NA") : (_DepositRequest.StateISOCode);

            string cardExpiryMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            string cardExpiryYear = Convert.ToInt32(_DepositRequest.CardExpYear).ToString("00");

            string req = string.Empty;

            try
            {
                //req = "clientKey=32554545&clientRef=312&amount=123&currCode=USD&langCode=EN&pMethod=VI&shopperRef=43142314&shopperEmail=johnson@sample.com&orderDesc=TMALLstoreorder&orderDate=20130819133848&capture=Y&cardNumber=4381919809810231&cardHolderName=John Smith&issuingBank=HSBC Corp&cardCvc=535&cardExpiryMonth=08";

                req = string.Format(@"clientKey={0}&clientRef={1}&amount={2}&currCode={3}&langCode={4}
                                        &pMethod={5}&versionNO={6}&secureHash={7}&capture={8}&contract={9}
                                        &shopperRef={10}&shopperEmail={11}
                                        &cardNumber={12}&cardHolderName={13}&issuingBank={14}
                                        &cardCvc={15}&cardExpiryMonth={16}&cardExpiryYear={17}&billingName={18}
                                        &billingAddress1={19}&billingAddress2={20}&billingCity={21}
                                        &billingState={22}&billingPostCode={23}&billingCountry={24}
                                        &billingEMail={25}&billingPhone={26}
                                        &userLoginID={27}&userEmail={28}&userPhone={29}",
                                    clientKey, clientRef, amount, currCode, langCode,
                                    pMethod, versionNO, secureHash, capture, contract,
                                    shopperRef, _DepositRequest.Email,
                                    _DepositRequest.CardNumber, cardHolderName, issuingBank,
                                    _DepositRequest.CardCVV2, cardExpiryMonth, cardExpiryYear, cardHolderName,
                                    billingAddress1, "", billingCity,
                                     billingState, _DepositRequest.ZipCode, _DepositRequest.CountryISOCodeThreeLetters,
                                    _DepositRequest.Email, _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", ""),
                                    _DepositRequest.CustomerID, _DepositRequest.Email, _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", "")
                                     );
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=Coala; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }

            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                            Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode).ToString() + " ";
                        resp.Code = -1;
                        resp.Description = Code + responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        #region Response parsing funcs

        //Parses response Xml. Returns DataCashDepositResponseParameters object
        private DepositResponse ParseResponseXml(XmlDocument docResponse)
        {
            // Get the root node 
            XmlElement rootElm = docResponse.DocumentElement;

            DepositResponse responseParams = new DepositResponse()
            {
                RetCode = GetXmlElementValue(rootElm, "resultCode"),
                ErrorCode = GetXmlElementValue(rootElm, "code"),
                ErrorMessage = GetXmlElementValue(rootElm, "msg")
            };

            responseParams.error.ErrorCode = responseParams.RetCode == "0" ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
            responseParams.error.Description = responseParams.ErrorMessage;

            return responseParams;
        }

        //Returns Xml element value
        private string GetXmlElementValue(XmlElement rootElm, string elmName)
        {
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text
            string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
            return resultValue;
        }

        #endregion

        private string GetCardBrandByCardType(string cardType)
        {
            string cardBrand = string.Empty;
            cardTypeMapping.TryGetValue(cardType, out cardBrand);
            return cardBrand;
        }

        /// <summary>
        /// The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
        /// Parameters must not be URL encoded before signature calculation.
        /// Please follow these steps:
        /// 1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
        /// 2. Append the corresponding values together according to the alphabetical sequence of parameter names.
        /// 3. Add the secret to the end of the string.
        /// 4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
        /// </summary>
        private string CalculateCheckSum(NameValueCollection data, string secretKey)
        {
            //The signature parameter is used in all payment requests to ensure, that the request is really coming from the merchant and has not been tampered with.
            //Parameters must not be URL encoded before signature calculation.
            //Please follow these steps:
            //1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
            //2. Append the corresponding values together according to the alphabetical sequence of parameter names.
            //3. Add the secret to the end of the string.
            //4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.

            //1. Sort all submitted parameter names alphabetically. (Note: Do not include the signature parameter.)
            var keys = data.AllKeys.OrderBy(k => k);

            //2. Append the corresponding values together according to the alphabetical sequence of parameter names.
            var valuesRow = new StringBuilder();
            foreach (string key in keys)
            {
                valuesRow.Append(data[key]);
            }

            //3. Add the secret to the end of the string.
            valuesRow.Append(secretKey);

            //4. Calculate a SHA-1 hex value of the string. This hash value must be in all lowercase letters.
            byte[] bytes = Encoding.UTF8.GetBytes(valuesRow.ToString());
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);

            var result = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

    }
}

