﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
	class ProviderSafeCharge : ProviderBase
	{
		#region Constants

		#region Request Credit/Debit Card Details

		/// <summary>
		/// Mandatory: YES. Size: 70. 
		/// The card holder’s name, as appearing on the card.
		/// </summary>
		private const string CARD_NAME_ON_CARD = "sg_NameOnCard";

		/// <summary>
		/// Mandatory: YES. Size: 20. 
		/// The full card number.
		/// </summary>
		private const string CARD_NUMBER = "sg_CardNumber";

		/// <summary>
		/// Mandatory: YES. Size: 2. 
		/// The expiry month of the card (2 digit).
		/// </summary>
		private const string CARD_EXP_MONTH = "sg_ExpMonth";

		/// <summary>
		/// Mandatory: YES. Size: 2. 
		/// The expiry year of the card (2 digit).
		/// </summary>
		private const string CARD_EXP_YEAR = "sg_ExpYear"; 

		/// <summary>
		/// Mandatory: Sale/Auth. Size: 3 or 4. 
		/// The 3 or 4 digits Card Verification Value printed on the back of the card
		/// </summary>
		private const string CARD_CVV2 = "sg_CVV2";

		#endregion

		#region Request Transaction Details
		
		/// <summary>
		/// Mandatory: YES. Size: 20. 
		/// Possible values: "Sale", "Auth", "Settle", "Credit", "AVSOnly", “Void”, “Auth3D”.
		/// </summary>
		private const string TRANS_TYPE = "sg_TransType";

		/// <summary>
		/// Mandatory: YES. Size: 3. 
		/// The Transaction’s ISO currency code.
		/// </summary>
		private const string TRANS_CURRENCY = "sg_Currency";

		/// <summary>
		/// Mandatory: YES. Size: 10. 
		/// The monetary amount of the Transaction. 
		/// This parameter may be omitted for an AVSOnly Transaction. 
		/// Either accepted, round amount or with two digits after decimal point
		/// </summary>
		private const string TRANS_AMOUNT = "sg_Amount";

		/// <summary>
		/// Mandatory: YES. Size: 24. 
		/// The Merchant’s processing login name (ending with TRX), as provide by SafeCharge’s tech support team.
		/// </summary>
		private const string TRANS_CLIENT_LOGIN_ID = "sg_ClientLoginID";

		/// <summary>
		/// Mandatory: YES. Size: 24. 
		/// The Merchant’s processing password, as provided by SafeCharge’s tech support team.
		/// </summary>
		private const string TRANS_CLIENT_PASSWORD = "sg_ClientPassword";

		/// <summary>
		/// Mandatory: NO. Size: 50. 
		/// The Merchant’s Website identifier.
		/// </summary>
		private const string TRANS_WEB_SITE = "sg_WebSite";
		
		/// <summary>
		/// Mandatory: YES. Size: . 
		/// </summary>
		private const string TRANS_RESPONSE_FORMAT = "sg_ResponseFormat"; // Size/Required:  1 Yes 4 = XML

		/// <summary>
		/// Mandatory: YES. Size: . 
		/// </summary>
		private const string TRANS_VERSION = "sg_Version"; // Size/Required:  sg_version 5 No Gateway version. latest version is 4.0.2 If no value sent the default version will be 1.8.2

		#endregion

		#region Request Customer Details

		private const string CUSTOMER_FIRST_NAME = "sg_FirstName"; // Size/Required: 30 Yes  The Customer’s first name 
		private const string CUSTOMER_LAST_NAME = "sg_LastName"; // Size/Required: 40  Yes  The Customer’s last name
		private const string CUSTOMER_ADDRESS = "sg_Address"; // Size/Required:   60  Yes  The Customer’s address: street name and number.
		private const string CUSTOMER_CITY = "sg_City"; // Size/Required:  30  Yes  The Customer’s city
		private const string CUSTOMER_ZIP = "sg_Zip"; // Size/Required:  10  Yes  The Customer’s zip code.
		private const string CUSTOMER_COUNTRY = "sg_Country"; // Size/Required:  3  Yes  The Customer’s country in an ISO code (see table “Country ISO Codes” on page 29).
		private const string CUSTOMER_PHONE = "sg_Phone"; // Size/Required:   18 Yes  The Customer’s telephone number in the following format: <CountryCode>(0)<AreaCode><PhoneNumber>
		private const string CUSTOMER_IP_ADDRESS = "sg_IPAddress"; // Size/Required:  15  Yes  The IP address of the PC used by the Customer to make the Transaction
		private const string CUSTOMER_EMAIL = "sg_Email"; // Size/Required:   100  Yes  The Customer’s e-mail address
		private const string CUSTOMER_STATE = "sg_State"; // Size/Required:  30  No  The Customer’s state in an ISO code. Mandatory Only in US\Canada. See “State ISO Codes” table on page 31.
		
		#endregion

		#region Responce Error Codes Description

		private static readonly Dictionary<int, string> ErrorCodes = new Dictionary<int, string>
		{
			{0, string.Empty},
			{1001, "Invalid Expiration Date"},
			{1106, "Credit Amount Exceeds Total Charges"},
			{1002, "Expiration Date Too Old"},
			{1107, "Cannot Credit this CC company"},
			{1101, "Invalid Card Number (Alpha Numeric)"},
			{1108, "Illegal interval between Auth and Settle"},
			{1102, "Invalid Card Number (Digits Count)"},
			{1109, "Not allowed to process this CC company"},
			{1103, "Invalid Card Number (MOD 10)"},
			{1110, "Unrecognized CC company"},
			{1104, "Invalid CVV2"},
			{1111, "This Transaction was charged back"},
			{1105, "Auth Code/Trans ID/CC Number Mismatch"},
			{1112, "Sale/Settle was already credited"},
			{1113, "Terminal is not configured to work with this CC company"},
			{1140, "Card must be processed in the GW System"},
			{1114, "Blocked (Black-listed) card number"},
			{1141, "Transaction type is not allowed"},
			{1115, "Illegal BIN number"},
			{1142, "AVS required fields are missing or incorrect"},
			{1116, "<Custom Fraud Screen Filter>"},
			{1143, "Country does not match ISO Code"},
			{1118, "'N' Cannot be a Positive CVV2 Reply"},
			{1144, "Must provide UserID in a Rebill transaction"},
			{1119, "'B'/'N' Cannot be a Positive AVS Reply"},
			{1145, "Your Rebill profile does not support this Transaction type"},
			{1120, "Invalid AVS"},
			{1146, "Void is not allowed due to CC restriction"},
			{1121, "CVV2 check is not allowed in Credit/Settle/Void"},
			{1147, "Invalid Account Number"},
			{1122, "AVS check is not allowed in Credit/Settle/Void"},
			{1148, "Invalid Cheque Number"},
			{1124, "Credits total amount exceeds restriction"},
			{1149, "Account Number/Trans ID Mismatch"},
			{1125, "Format Error"},
			{1150, "UserID/Trans Type /Trans ID Mismatch"},
			{1126, "Credit amount exceeds ceiling"},
			{1151, "Transaction does not exist in the rebill system."},
			{1127, "Limit exceeding amount"},
			{1152, "Transaction was already canceled"},
			{1128, "Invalid Transaction Type Code"},
			{1153, "Invalid Bank Code (Digits Count)"},
			{1129, "General Filter Error"},
			{1154, "Invalid Bank Code (Alpha Numeric)"},
			{1130, "Bank required fields are missing or incorrect"},
			{1155, "VBV-Related transaction is missing or incorrect"},
			{1131, "This transaction type is not allowed for this bank"},
			{1156, "Debit card required fields are missing or incorrect"},
			{1132, "Amount exceeds bank limit"},
			{1157, "No updated parameters were supplied"},
			{1133, "GW required fields are missing"},
			{1158, "VBV PaRes value is incorrect"},
			{1134, "AVS Processor Error"},
			{1159, "State does not match ISO Code"},
			{1135, "Only one credit per sale is allowed"},
			{1160, "Invalid Bank Code (Checksum Digit)"},
			{1136, "Mandatory fields are missing"},
			{1161, "This Bank allows only 3 digits in CVV2"},
			{1137, "Credit count exceeded CCC restriction"},
			{1162, "Age verification Failed"},
			{1138, "Invalid Credit Type"},
			{1163, "Transaction must contain a Card number/Token"},
			{1139, "This card is not supported in the CFT Program"},
			{1164, "Invalid Token"},
			{1165, "Token Mismatch"},
			{1178, "Cannot void a void transaction"},
			{1166, "Invalid Email address"},
			{1179, "Cannot perform this void"},
			{1167, "Transaction already settled"},
			{1180, "Invalid start date"},
			{1168, "Transaction already voided"},
			{1181, "Merchant Name is too long (>25)"},
			{1169, "sg_ResponseFormat field is not valid"},
			{1182, "Transaction must be send as 3Dsecure"},
			{1170, "Version field is missing or incorrect"},
			{1183, "Account is not 3D enabled"},
			{1171, "Issuing Country is invalid"},
			{1184, "Transaction 3D status is incorrect"},
			{1172, "Phone is missing or format error"},
			{1185, "Related transaction must be of type ‘AUTH3D’"},
			{1173, "Check number is missing or incorrect"},
			{1186, "Related transaction must be 3D authenticated"},
			{1174, "Birth date format error"},
			{1187, "Country does not support CFT program"},
			{1175, "Zip code format error"},
			{1201, "Invalid Amount"},
			{1176, "Cannot void an auth transaction"},
			{1202, "Invalid Currency"},
			{1177, "Can’t Void a Credit Transaction"},
		};

		#endregion

		#region Responce Details

		private const string RESPONSE_TRANSACTION_ID = "TransactionID"; //A 32-bit unique integer ID generated by the Gateway, used for tracking purposes and as the sg_TransactionID (related Transaction) to perform a Settle, a Credit and a Void Transaction.

		/// <summary>
		/// The general outcome of the Transaction. Values can be:
		/// Code		Description
		/// Approved	An approved Auth, Sale, Settle, Credit ,Void or Auth3D Transaction
		/// Success		A successful AVSOnly Transaction
		/// Declined	A declined Auth, Sale, Settle, Credit or Void or Auth3D Transaction
		/// Error		An error outcome
		/// Pending		Transaction request was submitted to the bank but response is not yet available. A lasting Pending status requires further investigation.
		/// </summary>
		private const string RESPONSE_STATUS = "Status";

		private const string RESPONSE_AUTH_CODE = "AuthCode"; //An authorization code (up to 35 characters) returned for each approved or pending Transaction. This parameter will also appear in the Customer’s credit card statement.

		/// <summary>
		/// The AVS processor’s response (where applicable – empty when an AVSOnly request was not submitted):
		/// A = The street address matches, the zip code does not
		/// W = Whole 9-digits zip code match, the street address does not
		/// Y = Both the 5-digits zip code and the street address match
		/// X = An exact match of both the 9-digits zip code and the street address
		/// Z = Only the 5-digits zip code match, the street code does not
		/// U = Issuer is unavailable
		/// S = Not Supported
		/// R = Retry
		/// B = Not authorized (declined)
		/// N = Both street address and zip code do not match
		/// </summary>
		private const string RESPONSE_AVS_CODE = "AVSCode";

		/// <summary>
		/// CVV2 response. Possible values are:
		/// M = CVV2 Match
		/// N = CVV2 No Match
		/// P = Not Processed
		/// U = Issuer is not certified and/or has not provided Visa the encryption keys
		/// S = CVV2 processor is unavailable.
		/// </summary>
		private const string RESPONSE_CVV2_REPLY = "CVV2Reply";

		/// <summary>
		/// Acquiring bank unique ID. Numeric. 2 digits. Possible values will be provided per merchant integration.
		/// IMPORTANT:
		///  The value 0 represents an invalid bank code. This value will appear in cases where the transaction was filtered prior to our bank routing decision process.
		///  Bare in mind that additional acquiring banks may be introduced over time. Please keep this parameter configurable and dynamic.
		/// </summary>
		private const string RESPONSE_ACQUIRER_ID = "AcquirerID";

		/// <summary>
		/// Credit card issuing bank name. Alphanumeric. Maximum length is limited to 125 characters.
		/// IMPORTANT:
		///  The value “No information exists” will appear in cases where the issuing bank BIN was not yet analyzed and updated on our systems
		///  In case of most transaction based errors, the parameter will be left blank.
		///  Do not rely on this value to remain static over time. Bank names may change over time due to acquisitions and mergers.
		/// </summary>
		private const string RESPONSE_ISSUER_BANK_NAME = "IssuerBankName";

		/// <summary>
		/// Credit card issuing bank location country code. 2 character country ISO code (Refer to Appendix I: General Codes Tables– Country ISO codes for possible values)
		/// IMPORTANT:
		///  The value “ZZ” will appear in cases where the issuing bank BIN was not yet analyzed and updated on our systems
		///  In case of most transaction based errors, the parameter will be left blank.
		/// </summary>
		private const string RESPONSE_ISSUER_BANK_COUNTRY = "IssuerBankCountry"; //

		private const string RESPONSE_REASON = "Reason"; //A text description for the decline/error status.
		private const string RESPONSE_ERR_CODE = "ErrCode"; //The numeric value of the error. Refer to “Table 2: General Response Codes” below.
		private const string RESPONSE_EX_ERR_CODE = "ExErrCode"; //An extended error code for the error. Refer to “Table 2: General Response Codes” below.
		private const string RESPONSE_CUSTOM_DATA = "CustomData"; //The value submitted in the sg_CustomData field.
		private const string RESPONSE_REFERENCE = "Reference"; //Reserved field.
		private const string RESPONSE_AGV_CODE = "AGVCode"; //For clients who uses the age verification service.
		private const string RESPONSE_AGV_ERROR = "AGVError"; //For clients who uses the age verification service.
		private const string RESPONSE_UNIQUE_CC = "UniqueCC"; //A CC unique identifier. allow to track all the transactions of a same CC number.
		private const string RESPONSE_TOKEN = "Token";

		#endregion

		#endregion

		#region Private members

		private string siteId;

		#endregion

		public ProviderSafeCharge(ProviderConfigBase config)  : base(config)
		{
			SafeChargeProviderConfig safeChargeConfig = config as SafeChargeProviderConfig;
			if(safeChargeConfig == null)
			{
				throw new ArgumentException("SafeChargeProvider expects to get SafeChargeProviderConfig object!");
			}

			siteId = safeChargeConfig.SiteId;
		}

		protected override Globals.Providers ProviderType
		{
			get { return Globals.Providers.SAFECHARGE; }
            set { }
        }
				
		public override DepositResponse Deposit(DepositRequest _DepositRequest)
		{                                    
			DepositResponse _DepositResponse = new DepositResponse();
			
			if (_DepositRequest != null)
			{
				try
				{
					NameValueCollection requestCollection = CreateRequestDataCollection(_DepositRequest);

					Log(string.Format("Request: Provider=SafeCharge; UserName={0}; Currency={1}; Amount={2}; MerchantID={3}; Password={4}; Url={5}{6}",
									_DepositRequest.Email, _DepositRequest.CurrencyISOCode, _DepositRequest.Amount, MerchantID, MerchantPassword, Url, Environment.NewLine));

					string request = BuildRequestUrl(requestCollection);
					Log(string.Format("SafeCharge Requeset: {0}", request));

					Log(requestCollection);

					string response = SendRequest(Url, requestCollection);
					Log(string.Format("SafeCharge Response: {0}", response));

					_DepositResponse = ParseResponse(response);
					
					//SafeCharge does not rerutn amount and currency in response -> use amount and currency from Request object
					_DepositResponse.Amount = _DepositRequest.Amount.ToString();
					_DepositResponse.CurrencyISOCode = _DepositRequest.CurrencyISOCode;

				}
				catch (Exception ex)
				{
					_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
					_DepositResponse.error.Description = ex.Message;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = Globals.Providers.SAFECHARGE;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = _DepositResponse.TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = _DepositResponse.error.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);

                }
            }
			else
			{
				_DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
				_DepositResponse.error.Description = "SafeCharge DepositRequest parameters can't be null";
			}

			_DepositResponse.provider = Globals.Providers.SAFECHARGE;
            
			return _DepositResponse;
		}

		#region Private methods

		private string SendRequest(string url, NameValueCollection data)
		{
			using (var webClient = new WebClient())
			{
				try
				{
					byte[] xmlResponse = webClient.UploadValues(url, "POST", data);
					return Encoding.UTF8.GetString(xmlResponse);
				}
				catch (Exception ex)
				{
					string errorMessage = String.Format("An error has occured during sending request: {0}", ex.Message);
					Log(errorMessage);
					throw new WebException(errorMessage, ex);
				}
			}
		}

		private string BuildRequestUrl(NameValueCollection data)
		{
			var result = new StringBuilder(Url.TrimEnd('?'));
			result.Append('?');

			foreach (string key in data.AllKeys)
			{
				string value = data[key];
				result.AppendFormat("{0}={1}&", key, HttpUtility.UrlEncode(value));
			}

			return result.ToString().TrimEnd('&');
		}

		#region Parse response

		private DepositResponse ParseResponse(string response)
		{
			try
			{
				XDocument doc = XDocument.Parse(response);

				DepositResponse responseParams = new DepositResponse()
				{
					TransactionID = TryToGetXmlValue(doc, RESPONSE_TRANSACTION_ID),
					Status = TryToGetXmlValue(doc, RESPONSE_STATUS),
					AuthCode = TryToGetXmlValue(doc, RESPONSE_AUTH_CODE),
					Reply = TryToGetXmlValue(doc, RESPONSE_CVV2_REPLY),
					Issuer = TryToGetXmlValue(doc, RESPONSE_ISSUER_BANK_NAME),
					Reason = TryToGetXmlValue(doc, RESPONSE_REASON),
					RetCode = String.Format("Error: {0}; Exception: {1}", TryToGetXmlValue(doc, RESPONSE_ERR_CODE), TryToGetXmlValue(doc, RESPONSE_EX_ERR_CODE)),
					ErrorDescripotion = GetErrorDescription(TryToGetXmlValue(doc, RESPONSE_ERR_CODE), TryToGetXmlValue(doc, RESPONSE_EX_ERR_CODE)),
					ClearingUserID = MerchantID 
				};

				responseParams.Receipt = responseParams.TransactionID;

				responseParams.error.ErrorCode = (TryToGetXmlValue(doc, RESPONSE_ERR_CODE) == "0" && TryToGetXmlValue(doc, RESPONSE_EX_ERR_CODE) == "0") ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
				responseParams.error.Description = responseParams.ErrorDescripotion;

				return responseParams;
			}
			catch (Exception ex)
			{
				string errorMessage = string.Format("An error has occured during parsing response '{0}'. Exception: {1}", response, ex.Message);
				Log(errorMessage);
				throw new InvalidDataException(errorMessage, ex);
			}
		}

		private string GetErrorDescription(string errorCode, string exErrorCode)
		{
			int error;
			int exError;

			if (!Int32.TryParse(errorCode, out error) || !Int32.TryParse(exErrorCode, out exError))
			{
				string errorMessage = string.Format("Can not parse Error Code '{0}' or Extended Error Code '{1}'", errorCode, exErrorCode);
				Log(errorMessage);
				return null;
			}

			//	0		0	APPROVED
			if (error == 0 && exError == 0)
			{
				return "APPROVED";
			}

			//	-1		0	DECLINED*
			if (error == -1 && exError == 0)
			{
				return "DECLINED";
			}

			//	0		-2	PENDING
			if (error == 0 && exError == -2)
			{
				return "PENDING";
			}

			//	-1100	> 0	ERROR (Filter error)**
			if (error == -1100 && exError > 0)
			{
				if (ErrorCodes.ContainsKey(exError))
				{
					return String.Format("ERROR: {0}", ErrorCodes[exError]);
				}

				return String.Format("ERROR. Unknown error code: {0}", exError);
			}

			//	< 0		!= 0	ERROR (Gateway/Bank Error)
			if (error < 0 && exError != 0)
			{
				return "ERROR (Gateway/Bank Error)";
			}

			//	-1001	0	Invalid Login
			if (error == -1001 && exError == 0)
			{
				return "Invalid Login";
			}

			//	-1005	0	IP out of range
			if (error == -1005 && exError == 0)
			{
				return "IP out of range";
			}

			//	-1203	0	Timeout/Retry
			if (error == -1203 && exError == 0)
			{
				return "Timeout/Retry";
			}

			return null;
		}

		#endregion

		#region Create Request

		private NameValueCollection CreateRequestDataCollection(DepositRequest request)
		{
			if (string.IsNullOrWhiteSpace(request.NameOnCard))
			{
				request.NameOnCard = string.Format("{0} {1}", request.FirstName, request.LastName);
			}

			if (request.CardExpYear.Length > 2)
			{
				request.CardExpYear = request.CardExpYear.Substring(request.CardExpYear.Length - 2);
			}

			// Validate fields lenght
			ValidateRequestData(request);

			var data = new NameValueCollection
			{
				{CARD_NAME_ON_CARD, request.NameOnCard},
				{CARD_NUMBER, request.CardNumber}, 
				{CARD_EXP_MONTH, request.CardExpMonth}, 
				{CARD_EXP_YEAR, request.CardExpYear},
				{CARD_CVV2, request.CardCVV2},
				
				{TRANS_TYPE, "Sale"},						
				{TRANS_CURRENCY, request.CurrencyISOCode},
				{TRANS_AMOUNT, request.Amount.ToString("0.00")}, 
				{TRANS_CLIENT_LOGIN_ID, MerchantID},
				{TRANS_CLIENT_PASSWORD, MerchantPassword},
				{TRANS_RESPONSE_FORMAT, "4"}, // 4 - XML
				
				{CUSTOMER_FIRST_NAME, request.FirstName}, 
				{CUSTOMER_LAST_NAME, request.LastName},
				{CUSTOMER_ADDRESS, request.Address},
				{CUSTOMER_CITY, request.City},
				
				{CUSTOMER_ZIP, request.ZipCode},
				{CUSTOMER_COUNTRY, request.CountryISOCode},
				{CUSTOMER_PHONE, request.PhoneNumber},
				{CUSTOMER_IP_ADDRESS, request.IpAddress},
				{CUSTOMER_EMAIL, request.Email},
				
				{TRANS_VERSION, "4.0.2"},

				{TRANS_WEB_SITE, this.siteId},
			};

			if (request.CountryISOCode.Equals("US", StringComparison.InvariantCultureIgnoreCase)
				|| request.CountryISOCode.Equals("CA", StringComparison.InvariantCultureIgnoreCase))
			{
				data.Add(CUSTOMER_STATE, request.StateISOCode); //TODO:  What should we do with empty
			}

			return data;
		}

		private void ValidateRequestData(DepositRequest request)
		{
			// Validate Mandatory fields
			ValidateRequiredField(request.FirstName, CUSTOMER_FIRST_NAME, 30);
			ValidateRequiredField(request.LastName, CUSTOMER_LAST_NAME, 40);
			ValidateRequiredField(request.Address, CUSTOMER_ADDRESS, 60);
			ValidateRequiredField(request.City, CUSTOMER_CITY, 30);
			ValidateRequiredField(request.ZipCode, CUSTOMER_ZIP, 10);
			ValidateRequiredField(request.CountryISOCode, CUSTOMER_COUNTRY, 3);
			ValidateRequiredField(request.PhoneNumber, CUSTOMER_PHONE, 18);
			ValidateRequiredField(request.IpAddress, CUSTOMER_IP_ADDRESS, 15);
			ValidateRequiredField(request.Email, CUSTOMER_EMAIL, 100);
			ValidateNotRequiredField(request.StateISOCode, CUSTOMER_STATE, 30);

			ValidateRequiredField(request.NameOnCard, CARD_NAME_ON_CARD, 70);		
			ValidateRequiredField(request.CardNumber, CARD_NUMBER, 20);				
			ValidateRequiredField(request.CardExpMonth, CARD_EXP_MONTH, 2);			
			ValidateRequiredField(request.CardExpYear, CARD_EXP_YEAR, 2);			
			ValidateRequiredField(request.CardCVV2, CARD_CVV2, 4);					
			
			ValidateRequiredField(request.CurrencyISOCode, TRANS_CURRENCY, 3);		
			ValidateRequiredField(MerchantID, TRANS_CLIENT_LOGIN_ID, 24);			
			ValidateRequiredField(MerchantPassword, TRANS_CLIENT_PASSWORD, 24);		
		}

		#endregion

		

		#endregion

	}
}
