﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using CashierUtils;

namespace Cashier
{
    class ProviderIMGlobal : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private Dictionary<string, string> ResultCodeTable { get; set; }

        #region Constructor

        public ProviderIMGlobal(ProviderConfigBase config)
            : base(config)
        {
            ResultCodeTable = new Dictionary<string, string>()
            {
                { "100", "Transaction was approved."},
                { "200", "Transaction was declined by processor."},
                { "201", "Do not honor."},
                { "202", "Insufficient funds."},
                { "203", "Over limit."},
                { "204", "Transaction not allowed."},
                { "220", "Incorrect payment information."},
                { "221", "No such card issuer."},
                { "222", "No card number on file with issuer."},
                { "223", "Expired card."},
                { "224", "Invalid expiration date."},
                { "225", "Invalid card security code."},
                { "240", "Call issuer for further information."},
                { "250", "Pick up card."},
                { "251", "Lost card."},
                { "252", "Stolen card."},
                { "253", "Fraudulent card."},
                { "260", "Declined with further instructions available. (See response text)"},
                { "261", "Declined - Stop all recurring payments."},
                { "262", "Declined - Stop this recurring program."},
                { "263", "Declined - Update cardholder data available."},
                { "264", "Declined - Retry in a few days."},
                { "300", "Transaction was rejected by gateway."},
                { "400", "Transaction error returned by processor."},
                { "410", "Invalid merchant configuration."},
                { "411", "Merchant account is inactive."},
                { "420", "Communication error."},
                { "421", "Communication error with issuer."},
                { "430", "Duplicate transaction at processor."},
                { "440", "Processor format error."},
                { "441", "Invalid transaction information."},
                { "460", "Processor feature not available."},
                { "461", "Unsupported card type."}
            };
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.IMGLOBAL; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = ProviderType;

            string req = BuildRequestXML(_DepositRequest, transactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for IMGlobal";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=IMGlobal; {0} {1}{2}", Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider=IMGlobal; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to IMGlobal failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = _DepositResponse.TransactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
                ParseResponse(e.Description, _DepositResponse);

            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

            return _DepositResponse;

        }

        private string BuildRequestXML(DepositRequest _DepositRequest, string transactionID)
        {
            string CardExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            string CardExpYear = Convert.ToInt32(_DepositRequest.CardExpYear).ToString("00");
            
            string req = string.Empty;

            try
            {
                req = string.Format("username={0}&password={1}&type=sale&payment=creditcard&amount={2}&ccnumber={3}&ccexp={4}&cvv={5}&firstname={6}&lastname={7}&email={8}&address1={9}&city={10}&country={11}&zip={12}&phone={13}&orderid={14}&currency={15}",
                    MerchantID, MerchantPassword, _DepositRequest.Amount.ToString(), _DepositRequest.CardNumber, CardExpMonth + CardExpYear, _DepositRequest.CardCVV2, System.Web.HttpUtility.UrlEncode( _DepositRequest.FirstName), System.Web.HttpUtility.UrlEncode(_DepositRequest.LastName), (_DepositRequest.Email),
                    System.Web.HttpUtility.UrlEncode(_DepositRequest.Address), System.Web.HttpUtility.UrlEncode(_DepositRequest.City), _DepositRequest.CountryISOCode, System.Web.HttpUtility.UrlEncode(_DepositRequest.ZipCode), _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", ""), transactionID, _DepositRequest.CurrencyISOCode);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=IMGlobal; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }

            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                            Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode).ToString() + " ";
                        resp.Code = -1;
                        resp.Description = Code + responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private void ParseResponse(string data, DepositResponse _DepositResponse)
        {
            NameValueCollection qscoll = HttpUtility.ParseQueryString(data);

            if (qscoll.Count == 0)
            {
                Log(string.Format("Response: Provider=IMGlobal; Invalid http response"));
                _DepositResponse.ErrorDescripotion = "IMGlobal returned empty response for deposit request";
                return;
            }

            //check success
            if (qscoll["response_code"].Trim() == "100")
            {
                _DepositResponse.Receipt = qscoll["transactionid"];
                _DepositResponse.AuthCode = qscoll["authcode"];
                _DepositResponse.ErrorDescripotion = "";
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
            }
            else
            {
                string err_code = qscoll["response_code"].Trim();
                _DepositResponse.ErrorDescripotion = "Code: " + err_code + " , Message: " + qscoll["responsetext"];

                if (ResultCodeTable.ContainsKey(err_code))
                {
                    _DepositResponse.ErrorDescripotion += " , Description: " + ResultCodeTable[err_code];
                }
            }
        }


    }
}
