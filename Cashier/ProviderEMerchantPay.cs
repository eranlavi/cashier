﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.Xml;
using System.Xml;
using System.IO;
using System.Web;
using System.Linq;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    internal class ProviderEMerchantPay : ProviderBase
    {
        #region Constants
        
        //Mandatory
        private const string CLIENT_ID = "client_id";
        private const string API_KEY = "api_key";
        private const string ORDER_REFERENCE = "order_reference";
        private const string ORDER_CURRENCY = "order_currency";
        
        private const string ITEM_1_CODE = "item_1_code";
        private const string ITEM_1_qty = "item_1_qty";
        private const string ITEM_1_PREDEFINED = "item_1_predefined";
        private const string ITEM_1_NAME = "item_1_name";
        private const string ITEM_1_UNIT_PRICE = "item_1_unit_price_";

        private const string CARD_NUMBER = "card_number";
        private const string CVV = "cvv";
        private const string EXP_MONTH = "exp_month";
        private const string EXP_YEAR = "exp_year";
        private const string CREDIT_CARD_TRANS_TYPE = "credit_card_trans_type"; // (sale)
        private const string THM_SESSION_ID = "thm_session_id"; 

        //Optional
        private const string PAYMENT_TYPE = "payment_type";
        private const string TEST_TRANSACTION = "test_transaction";
        private const string PASS_THROUGH = "pass_through";
        private const string NOTIFY = "notify";
        
        private const string CUSTOMER_FIRST_NAME = "customer_first_name";
        private const string CUSTOMER_LAST_NAME =  "customer_last_name";
        private const string CUSTOMER_ADDRESS = "customer_address";
        private const string CUSTOMER_CITY = "customer_city";
        private const string CUSTOMER_STATE =  "customer_state";
        private const string CUSTOMER_COUNTRY = "customer_country";
        private const string CUSTOMER_POSTCODE = "customer_postcode";
        private const string CUSTOMER_EMAIL = "customer_email";
        private const string CUSTOMER_PHONE = "customer_phone";
        private const string CARD_HOLDER_NAME = "card_holder_name";
        private const string IP_ADDRESS = "ip_address";

        private const string ITEM_1_DIGITAL = "item_1_digital"; // 1

        private const string POST = "POST";
        private const string UNDEFINED = "undefined";        

        //Response
        private const string ORDER = "order";
        private const string FAILURE = "failure";
        private const string DECLINE = "decline";
        private const string ORDER_TOTAL = "order_total"; 
        private const string RESPONSE_TEXT = "response_text";
        private const string RESPONSE_CODE = "response_code";
        private const string TRANS_ID = "trans_id";
        private const string TYPE = "type"; 
        private const string CODE = "code";
        private const string TEXT = "text";
        private const string AUTH = "auth";
		        
        private const string PROVIDER = "Provider";
        private const string ATTRIBUTE_NAME = "Name";
        private const string ATTRIBUTE_MODE = "Mode";
        private const string MODE_LIVE = "Live";
       
       #endregion

        #region Private members

        private string transactionID;

        #endregion

        #region Constructor

        public ProviderEMerchantPay(ProviderConfigBase config): base(config)
        {
        }

        #endregion

        #region Public methods

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _response = new DepositResponse();

            if (_DepositRequest != null)
            {
                try
                {
                    NameValueCollection col = CreateRequestCollection(_DepositRequest);

					Log(col);
										
					StringBuilder values = new StringBuilder();
					                   
                    XmlDocument responseDoc = SendRequest(col, Url);

					//Log(string.Format("Response: {0}", responseDoc.OuterXml));
                    
                    _response = ParseResponseXml(responseDoc);

					if (string.IsNullOrWhiteSpace(_response.CurrencyISOCode)) 
					{
						_response.CurrencyISOCode = _DepositRequest.CurrencyISOCode;
					}
				}
                catch (Exception ex)
                {
                    Log("eMerchant Exception: " + ex.Message);
                    _response.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _response.error.Description = ex.Message;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = _DepositRequest.provider;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = transactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = _response.error.Description;

                    _response.FailedDeposits.Add(failedDeposit);
                }
            }
            else 
            {
                Log("DepositRequest parameters is NULL");
                _response.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _response.error.Description = "DepositRequest parameters can't be null";
            }

            _response.provider = _DepositRequest.provider;
            return _response;
        }

        #endregion

        #region Private methods

        private string PrepareExpYear(string expYear)
        {
            return expYear.Length > 2 ? expYear.Substring(expYear.Length - 2) : expYear;
        }

        private NameValueCollection CreateRequestCollection(DepositRequest _DepositRequest)
        {
            // Generate TransactionID
            transactionID = CashierUtils.CalcTransactionID(_DepositRequest.CustomerID); 
            
            NameValueCollection col = new NameValueCollection()
            {
                {CLIENT_ID, MerchantID},
                {API_KEY, MerchantPassword},
                {NOTIFY, "0"},
                {PAYMENT_TYPE, "creditcard"},
                {ORDER_REFERENCE, transactionID},
                {ORDER_CURRENCY, _DepositRequest.CurrencyISOCode},
                {PASS_THROUGH, "PassThroughData"},
                {CUSTOMER_FIRST_NAME, _DepositRequest.FirstName},
                {CUSTOMER_LAST_NAME, _DepositRequest.LastName},
                {CUSTOMER_ADDRESS, _DepositRequest.Address},
                {CUSTOMER_CITY, _DepositRequest.City},
                {CUSTOMER_STATE, _DepositRequest.StateISOCode},
                {CUSTOMER_COUNTRY, _DepositRequest.CountryISOCode},
                {CUSTOMER_POSTCODE, _DepositRequest.ZipCode},
                {CUSTOMER_EMAIL, _DepositRequest.Email},
                {CUSTOMER_PHONE, _DepositRequest.PhoneNumber},
                {CARD_HOLDER_NAME, string.Format("{0} {1}", _DepositRequest.FirstName, _DepositRequest.LastName)},
                {IP_ADDRESS, _DepositRequest.IpAddress},
                {ITEM_1_CODE, "Deposit"},
                {ITEM_1_qty, "1"},
                {ITEM_1_PREDEFINED, "0"},
                {ITEM_1_NAME, "Deposit"},
                {ITEM_1_DIGITAL, "1"},
                {ITEM_1_UNIT_PRICE + _DepositRequest.CurrencyISOCode, _DepositRequest.Amount.ToString()},
                {CARD_NUMBER, _DepositRequest.CardNumber},
                {CVV, _DepositRequest.CardCVV2},
                {EXP_MONTH, _DepositRequest.CardExpMonth},
                {EXP_YEAR, PrepareExpYear(_DepositRequest.CardExpYear)},
                {CREDIT_CARD_TRANS_TYPE, "sale"}
            };

            if (!string.IsNullOrWhiteSpace(_DepositRequest.ThmSessionId))
            {
                col.Add(THM_SESSION_ID, _DepositRequest.ThmSessionId);
            }

            if (Mode == Configuration.Mode.TEST)
            {
                col.Add(TEST_TRANSACTION, "1");
            }
            else
            {
                col.Add(TEST_TRANSACTION, "0");
            }
			
            return col;
        }

        private XmlDocument SendRequest(NameValueCollection parametersCollection, string serviceUrl)
        {
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                    byte[] response = webClient.UploadValues(serviceUrl, POST, parametersCollection);
                    string xmlResponse = Encoding.UTF8.GetString(response);
                    
                    Log("eMerchant Response: " + xmlResponse);

                    // Now load the string into an XmlDocument 
                    XmlDocument responseDocument = new XmlDocument();
                    responseDocument.LoadXml(xmlResponse);
                    
                    return responseDocument;
                }
                catch (Exception ex)
                {
                    throw new Exception("eMerchantPay: An error has occured during sending request: " + ex.Message);
                }
            }
        }

        private DepositResponse ParseResponseXml(XmlDocument doc)
        {
            XmlElement root = doc.DocumentElement;
            DepositResponse response = new DepositResponse();

            response.ClearingUserID = MerchantID;
            switch (root.LocalName)
            {
                case ORDER:
                    {
                        response.Amount = GetXmlElementValue(root, ORDER_TOTAL);
                        response.Status = GetXmlElementValue(root, RESPONSE_TEXT);
                        response.RetCode = GetXmlElementValue(root, RESPONSE_CODE);
                        response.TransactionID = transactionID;
                        response.TransactionType = GetXmlElementValue(root, TYPE);
                        response.Receipt = GetXmlElementValue(root, TRANS_ID) == UNDEFINED ? "" : GetXmlElementValue(root, TRANS_ID);
                        response.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        break;
                    }
                case FAILURE:
                    {
                        response.RetCode = GetXmlElementValue(root, CODE);
                        response.ErrorDescripotion = GetXmlElementValue(root, TEXT);
                        response.TransactionID = transactionID;
                        response.Receipt = GetXmlElementValue(root, TRANS_ID) == UNDEFINED ? "" : GetXmlElementValue(root, TRANS_ID);
                        response.error.Description = response.ErrorDescripotion;
                        response.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        break;
                    }
                case DECLINE:
                    {
                        response.TransactionType = GetXmlElementValue(root, AUTH);
                        response.RetCode = GetXmlElementValue(root, RESPONSE_CODE);
                        response.ErrorDescripotion = GetXmlElementValue(root, RESPONSE_TEXT);
                        response.TransactionID = transactionID;
                        response.Receipt = GetXmlElementValue(root, TRANS_ID) == UNDEFINED ? "" : GetXmlElementValue(root, TRANS_ID);
                        response.error.Description = response.ErrorDescripotion;
                        response.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        break;
                    }
                default:
                    {
                        throw new Exception("eMerchantPay: An error has occured when parsing server response");
                    }
            }

            return response;
        }

        //Returns Xml element value
        private string GetXmlElementValue(XmlElement rootElm, string elmName)
        {
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text
            string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
            return resultValue;
        }
      
        #endregion

		protected override Globals.Providers ProviderType
		{
			get { return Globals.Providers.EMERCHANTPAY; }
            set { }
        }
	}
}