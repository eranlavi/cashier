﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    internal class ProviderYouPay : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private const string TRANSCTION_MODE_TEST = "CONNECTOR_TEST";
        private const string TRANSCTION_MODE_LIVE = "LIVE";

        private const string RETURN_CODE_SUCCESS_1 = "000.000.000";
        private const string RETURN_CODE_SUCCESS_2 = "000.100.110";
        private const string RETURN_CODE_SUCCESS_3 = "000.100.111";
        private const string RETURN_CODE_SUCCESS_4 = "000.100.112";
        private const string STATUS_CODE_SUCCESS_1 = "90.00";
        private const string STATUS_CODE_SUCCESS_2 = "90.70";

        private string sender;
        private string channel;

        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            { "visa", "VISA" },
            { "Visa", "VISA" },
            { "mastercard", "MASTER" },
            { "Mastercard", "MASTER" },
            { "diners club", "DINERS" },
            { "dinersclubintl", "DINERS" },
            { "dinersclub", "DINERS" },
            { "Diners Club", "DINERS" },
            { "american express", "AMEX" },
            { "American Express", "AMEX" },
            { "amex", "AMEX" },
            { "maestro", "MAESTRO" },
            { "Maestro", "MAESTRO" }
        };

        public ProviderYouPay(ProviderConfigBase config)
            : base(config)
        {
            var YouPayConfig = config as YouPayProviderConfig;
            if (YouPayConfig == null)
            {
                throw new ArgumentException("YouPayProvider expects to get YouPayProviderConfig object!");
            }

            sender = YouPayConfig.Sender;
            channel = YouPayConfig.Channel;
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            var _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();
            var TransactionID = Guid.NewGuid().ToString().Replace("-", string.Empty);

            _DepositResponse.provider = Globals.Providers.YOUPAY;
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = TransactionID;

            var req = BuildRequestXML(_DepositRequest, TransactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ErrorDescripotion = "Fail building request XML";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            }
            else
            {
                Log(string.Format("Request: Provider=YouPay; {0}{1}", req, Environment.NewLine));

                var e = HttpReq("POST", Url, "load=" + req, null, "application/x-www-form-urlencoded;charset=UTF-8", string.Empty, 20000, 20000);

                Log(string.Format("Response: Provider=YouPay; {0}{1}", e.Description, Environment.NewLine));

                if (e.Code != 0)
                {
                    _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                    _DepositResponse.ErrorDescripotion = "Http request to YouPay failed. " + e.Description;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = _DepositRequest.provider;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = e.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);
                }
                else
                {
                    ParseResponse(e.Description, _DepositResponse);
                }
            }

            return _DepositResponse;
        }

        private void ParseResponse(string XML, DepositResponse _DepositResponse)
        {
            XmlDocument xDoc = null;

            try
            {
                xDoc = new XmlDocument();
                xDoc.LoadXml(XML);
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on parsing response (1): Provider=YouPay; {0}{1}", ex.Message, Environment.NewLine));
                _DepositResponse.ErrorDescripotion = ex.Message;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.ErrorCode = "1";
                return;
            }

            try
            {
                var statusCode = xDoc.SelectSingleNode("//Response/Transaction/Processing/Status").Attributes["code"].Value;
                var reasonCode = xDoc.SelectSingleNode("//Response/Transaction/Processing/Reason").Attributes["code"].Value;
                var returnCode = xDoc.SelectSingleNode("//Response/Transaction/Processing/Return").Attributes["code"].Value;
                _DepositResponse.ErrorDescripotion = "STATUS: " + xDoc.SelectSingleNode("//Response/Transaction/Processing/Status").InnerText + " REASON: " + xDoc.SelectSingleNode("//Response/Transaction/Processing/Reason").InnerText +
                    " RETURN: " + xDoc.SelectSingleNode("//Response/Transaction/Processing/Return").InnerText;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.TransactionID = xDoc.SelectSingleNode("//Response/Transaction/Identification/TransactionID").InnerText;
                _DepositResponse.Receipt = xDoc.SelectSingleNode("//Response/Transaction/Identification/UniqueID").InnerText;

                _DepositResponse.error.ErrorCode = WasTransactionSuccessful(statusCode, reasonCode, returnCode) ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;

                if (_DepositResponse.error.ErrorCode == Globals.ErrorCodes.NO_ERROR)
                {
                    _DepositResponse.ErrorCode = "0";
                    _DepositResponse.Amount = xDoc.SelectSingleNode("//Response/Transaction/Payment/Clearing/Amount").InnerText;
                    _DepositResponse.CurrencyISOCode = xDoc.SelectSingleNode("//Response/Transaction/Payment/Clearing/Currency").InnerText;
                    _DepositResponse.Time = xDoc.SelectSingleNode("//Response/Transaction/Processing/Timestamp").InnerText;
                }
                else
                {
                    _DepositResponse.ErrorCode = "1";
                }
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on parsing response (2): Provider=YouPay; {0}{1}", ex.Message, Environment.NewLine));
                _DepositResponse.ErrorDescripotion = ex.Message;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.ErrorCode = "1";
            }
        }
        private bool WasTransactionSuccessful(string statusCode, string reasonCode, string returnCode)
        {
            var status = string.Format("{0}.{1}", statusCode, reasonCode);

            return ((status == STATUS_CODE_SUCCESS_1 || status == STATUS_CODE_SUCCESS_2) &&
                    (returnCode == RETURN_CODE_SUCCESS_1 || returnCode == RETURN_CODE_SUCCESS_2 ||
                    returnCode == RETURN_CODE_SUCCESS_3 || returnCode == RETURN_CODE_SUCCESS_4));
        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            var resp = new Error();

            try
            {
                var req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                {
                    req.Headers.Add(Header);
                }
                if (!string.IsNullOrEmpty(ContentType))
                {
                    req.ContentType = ContentType;
                }
                if (!string.IsNullOrEmpty(Accept))
                {
                    req.Accept = Accept;
                }
                if (!string.IsNullOrEmpty(Cred))
                {
                    req.Headers[HttpRequestHeader.Authorization] = Cred;
                }
                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    var byteArray = Encoding.UTF8.GetBytes(Parameters);
                    req.ContentLength = byteArray.Length;
                    using (var stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        stm.Write(byteArray, 0, byteArray.Length);
                    }
                }

                var webResponse = req.GetResponse() as HttpWebResponse;
                using (var rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if ((we.Response is System.Net.HttpWebResponse))
                {
                    resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                }
                else
                {
                    resp.Code = (int)we.Status;
                }
                resp.Description = resp.Code.ToString() + ", " + we.Message;
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private string BuildRequestXML(DepositRequest _DepositRequest, string TransactionID)
        {
            var req = string.Empty;

            var transactionMode = Mode == Configuration.Mode.LIVE ? TRANSCTION_MODE_LIVE : TRANSCTION_MODE_TEST;

            try
            {
                req = string.Format(@"<?xml version='1.0' encoding='UTF-8'?>
                <Request version='1.0'>
                    <Header>
                        <Security sender='{0}' />
                    </Header>
                    <Transaction response='SYNC' mode='{1}' channel='{2}'>
                        <Identification>
                            <TransactionID>{3}</TransactionID>            
                        </Identification>
                        <User pwd='{4}' login='{5}'/>
                        <Account>
                            <Holder>{6}</Holder>
                            <Number>{7}</Number>
                            <Verification>{8}</Verification>
                            <Brand>{9}</Brand>
                            <Year>{10}</Year>
                            <Month>{11}</Month>
                        </Account>
                        <Payment code='CC.DB'>
                            <Presentation>
                                <Amount>{12}</Amount>
                                <Usage>{13}</Usage>
                                <Currency>{14}</Currency>
                            </Presentation>
                        </Payment>
                        <Customer>
                            <Contact>
                                <Email>{15}</Email>
                                <Ip>{16}</Ip>
                            </Contact>
                            <Address>
                                <State>{17}</State>
                                <Zip>{18}</Zip>
                                <City>{19}</City>
                                <Street>{20}</Street>
                                <Country>{21}</Country>
                            </Address>
                            <Name>                
                                <Given>{22}</Given>
                                <Family>{23}</Family>
                            </Name>
                        </Customer>
                    </Transaction>
                </Request>", sender, transactionMode, channel, TransactionID, MerchantPassword, MerchantID, _DepositRequest.FirstName + " " + _DepositRequest.LastName, _DepositRequest.CardNumber,
                               _DepositRequest.CardCVV2, GetYouPayCardBrandByCardType(_DepositRequest.CardType), _DepositRequest.CardExpYear, _DepositRequest.CardExpMonth, _DepositRequest.Amount,
                               _DepositRequest.Description, _DepositRequest.CurrencyISOCode, _DepositRequest.Email, _DepositRequest.IpAddress, _DepositRequest.StateISOCode,
                               _DepositRequest.ZipCode, _DepositRequest.City, _DepositRequest.Address, _DepositRequest.CountryISOCode, _DepositRequest.FirstName, _DepositRequest.LastName);
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on building request XML string (2): Provider=YouPay; {0}{1}", ex.Message, Environment.NewLine));
                req = string.Empty;
            }

            return req;
        }

        private string GetYouPayCardBrandByCardType(string cardType)
        {
            var cardBrand = string.Empty;
            cardTypeMapping.TryGetValue(cardType, out cardBrand);
            return cardBrand;
        }

        protected override Globals.Providers ProviderType
        {
            get
            {
                return Globals.Providers.YOUPAY;
            }
            set { }
        }
    }
}
