﻿using Cashier.Configuration.ProviderConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderGlobalCollect : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        public ProviderGlobalCollect(ProviderConfigBase config)
            : base(config)
		{
            
		}

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.GLOBALCOLLECT; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            long OrderID = (long)(DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;

            try
            {
                string ExpYear = "";
                if (_DepositRequest.CardExpYear.Length == 2)
                    ExpYear = _DepositRequest.CardExpYear;
                else
                    ExpYear = _DepositRequest.CardExpYear.Substring(2);

                string CardType = ConvertCardTypeToGCCardType(_DepositRequest.CardType);
                string req = string.Format(@"<XML>
                            <REQUEST>
                            <ACTION>INSERT_ORDERWITHPAYMENT</ACTION>
                            <META>
                                <MERCHANTID>{18}</MERCHANTID> 
                                <IPADDRESS>{19}</IPADDRESS>                                                             
                            </META>
                            <PARAMS>
                                <ORDER>
                                    <IPADDRESSCUSTOMER>{20}</IPADDRESSCUSTOMER>
                                    <CUSTOMERID>{21}</CUSTOMERID>
                                    <MERCHANTREFERENCE>{0}</MERCHANTREFERENCE>
                                    <ORDERID>{1}</ORDERID>
                                    <AMOUNT>{2}</AMOUNT>
                                    <CURRENCYCODE>{3}</CURRENCYCODE>
                                    <COUNTRYCODE>{4}</COUNTRYCODE>
                                    <LANGUAGECODE>en</LANGUAGECODE>
                                    <FIRSTNAME>{5}</FIRSTNAME>
                                    <SURNAME>{6}</SURNAME>
                                    <STREET>{7}</STREET>
                                    <ZIP>{8}</ZIP>
                                    <CITY>{9}</CITY>
                                </ORDER>
                                <PAYMENT>
                                    <PAYMENTPRODUCTID>{10}</PAYMENTPRODUCTID>
                                    <AMOUNT>{11}</AMOUNT>
                                    <CURRENCYCODE>{12}</CURRENCYCODE>
                                    <CREDITCARDNUMBER>{13}</CREDITCARDNUMBER>
                                    <CVV>{14}</CVV>
                                    <EXPIRYDATE>{15}</EXPIRYDATE>
                                    <COUNTRYCODE>{16}</COUNTRYCODE>
                                    <LANGUAGECODE>en</LANGUAGECODE>
                                    <EMAIL>{17}</EMAIL>
                                </PAYMENT>    
                            </PARAMS>
                            </REQUEST>
                        </XML>", Guid.NewGuid().ToString(), OrderID, _DepositRequest.Amount * 100.0, _DepositRequest.CurrencyISOCode,
                                   _DepositRequest.CountryISOCode, _DepositRequest.FirstName, _DepositRequest.LastName, _DepositRequest.Address, _DepositRequest.ZipCode, _DepositRequest.City,
                                   CardType, _DepositRequest.Amount * 100.0, _DepositRequest.CurrencyISOCode, _DepositRequest.CardNumber, _DepositRequest.CardCVV2, _DepositRequest.CardExpMonth + ExpYear,
                                   _DepositRequest.CountryISOCode, _DepositRequest.Email, MerchantID, _DepositRequest.IpAddress, _DepositRequest.IpAddress, _DepositRequest.CustomerID);
                
                Log(string.Format("Request: Provider=GlobalCollect; {0}{1}", req, Environment.NewLine));

                Error e = HttpRequest("POST", Url, req, "", "", 10000, 30000);

                Log(string.Format("Response: Provider=GlobalCollect; {0}{1}", e.Description, Environment.NewLine));

                _DepositResponse.TransactionID = OrderID.ToString();
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ClearingUserID = MerchantID;

                if (e.Code != 0)
                {
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.error.Description = e.Description;
                    _DepositResponse.ErrorDescripotion = e.Description;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = Globals.Providers.GLOBALCOLLECT;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = _DepositResponse.TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = e.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);
                }
                else
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(e.Description);

                    _DepositResponse.Receipt = xDoc.SelectSingleNode("//RESPONSE/META/REQUESTID").InnerText;
                                        
                    XmlNode node = xDoc.SelectSingleNode("//RESPONSE/RESULT");
                   
                    if (node.InnerText == "NOK")
                    {
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        _DepositResponse.ErrorDescripotion = "Code: " + xDoc.SelectSingleNode("//RESPONSE/ERROR/CODE").InnerText + ", Description: " + xDoc.SelectSingleNode("//RESPONSE/ERROR/MESSAGE").InnerText;
                       
                        _DepositResponse.Status = xDoc.SelectSingleNode("//RESPONSE/ERROR/CODE").InnerText;
                    }
                    else
                    {
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                        _DepositResponse.error.Description = string.Empty;
                        _DepositResponse.ErrorDescripotion = string.Empty;                                              
                    }
                }
            }
            catch (Exception ex)
            {
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.error.Description = "Cashier Exception: " + ex.Message;
                _DepositResponse.ErrorDescripotion = "Cashier Exception: " + ex.Message;

                _DepositResponse.TransactionID = OrderID.ToString();
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ClearingUserID = MerchantID;
            }

            _DepositResponse.provider = Globals.Providers.GLOBALCOLLECT;

            return _DepositResponse;
        }

        private string ConvertCardTypeToGCCardType(string Card)
        {
            switch(Card)
            {
                case "visa":
                case "Visa":
                    return "1";
                case "amex":
                case "American Express":
                    return "2";
                case "mastercard":
                case "Mastercard":
                    return "3";
                case "maestro":
                case "Maestro":
                    return "117";
                case "dinersclubintl":
                case "Diners Club":
                case "dinersclub":
                    return "132";
                default:
                    return string.Empty;
            }
        }

        private Error HttpRequest(string Method, string Url, string Parameters, string Header, string ContentType, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec)
        {
            Error err = new Error();

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (!string.IsNullOrEmpty(Header))
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                WebResponse webResponse = req.GetResponse();
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    err.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                err.Code = 1;
                err.Description = we.Status.ToString() + ", " + we.Message;
            }
            catch (Exception ex)
            {
                err.Code = 2;
                err.Description = ex.Message;
            }

            return err;
        }
    }
}
