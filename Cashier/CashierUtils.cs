﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Security.Cryptography;
using CashierUtils;

namespace Cashier
{
    public static class CashierUtils
    {
        #region Members
        
        private static Random rand = new Random();
        
        #endregion
        
        #region Public methods

        //Use for AllCharge
        public static string CalcTransactionID(string customerID, string DepositType)
        {
            string currDate = DateTime.Now.ToString();
            string result = customerID + currDate + DepositType;
            return CalculateMD5Hash(result);
        }
        
        //Use for DataCash
        public static string CalcTransactionID(string customerID)
        {
            return customerID + Create16DigitString();
        }

		/// <summary>
		/// Returns Name of Enum with <see cref="EnumDescription"/> attribute
		/// </summary>
		public static string GetName(Enum value)
		{
			FieldInfo fi = value.GetType().GetField(value.ToString());
			if (fi == null)
				return string.Empty;

			var description = fi.GetCustomAttributes(typeof(EnumDescription), false) as EnumDescription[];
			if (description != null && description.Length > 0)
				return description[0].Name;

			return value.ToString();
		}

		/// <summary>
		/// Returns Description of Enum with <see cref="EnumDescription"/> attribute
		/// </summary>
		public static string GetDescription(Enum value)
		{
			FieldInfo fi = value.GetType().GetField(value.ToString());
			if (fi == null)
				return string.Empty;

			var description = fi.GetCustomAttributes(typeof(EnumDescription), false) as EnumDescription[];
			if (description != null && description.Length > 0)
				return description[0].Description;

			return value.ToString();
		}

        #endregion
            
        #region Private methods

        private static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        
        private static string Create16DigitString()
        {
            var builder = new StringBuilder();
            while (builder.Length < 16)
            {
                builder.Append(rand.Next(10).ToString());
            }
            return builder.ToString();
        }
        
        #endregion
    }
}
