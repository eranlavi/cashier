﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    internal abstract class ProviderFactory
    {
        public abstract ICashierProvider GetProvider(Globals.Providers provider, ProviderConfigBase config);
    }
}
