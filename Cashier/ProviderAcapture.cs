﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using System.Xml;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using CashierUtils;

namespace Cashier
{
    class ProviderAcapture : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        //private Dictionary<string, string> ResultCodeTable { get; set; }

        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            {"visa", "VISA"},
            {"Visa", "VISA"},
            {"VISA", "VISA"},
            {"mastercard", "MASTER"},
            {"Mastercard", "MASTER"},
            {"MasterCard", "MASTER"},
            { "ELO","ELO"},
            { "JCB","JB"},
            { "amex", "AMEX"},
            { "american express", "AMEX"},
            {"American Express", "AMEX"},
            {"VPAY", "VPAY"},
            {"AXESS", "AXESS"},
            {"BONUS", "BONUS"},
            {"CABAL", "CABAL"},
            {"WORLD", "WORLD"},
            {"diners club", "DINERS"},
            {"dinersclub", "DINERS"},
            {"dinersclubintl", "DINERS"},
            {"Diners Club", "DINERS"},
            {"BITCOIN", "BITCOIN"},
            {"DANKORT", "DANKORT"},
            {"maestro","MAESTRO"},
            {"Maestro","MAESTRO"},
            {"MAXIMUM", "MAXIMUM"},
            {"AFTERPAY", "AFTERPAY"},
            {"ASYACARDb", "ASYACARD"},
            {"Discover","DISCOVER"},
            {"discover","DISCOVER"},
            {"POSTEPAY", "POSTEPAY"},
            {"SERVIRED", "SERVIRED"},
            {"HIPERCARD", "HIPERCARD"},
            {"VISADEBIT", "VISADEBIT"},
            {"CARDFINANS", "CARDFINANS"},
            {"CARTEBLEUE", "CARTEBLEUE"},
            {"VISAELECTRON", "VISAELECTRON"},
            {"CARTEBANCAIRE", "CARTEBANCAIRE"},
            {"KLARNA_INVOICE", "KLARNA_INVOICE"},
            {"DIRECTDEBIT_SEPA", "DIRECTDEBIT_SEPA"}
        };
        private const string UNDEFINED = "undefined";
        private string channel;
        private string sender;

        #region Constructor

        public ProviderAcapture(ProviderConfigBase config)
            : base(config)
        {
            SolidProviderConfig solidConfig = config as SolidProviderConfig;
            if (solidConfig == null)
            {
                throw new ArgumentException("AcaptureProvider expects to get SolidProviderConfig object!");
            }
            channel = solidConfig.Channel;
            sender = solidConfig.Sender;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.ACAPTURE; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new CashierUtils.Error();
            
            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = _DepositRequest.provider;

            string TransactionID = Guid.NewGuid().ToString().Replace("-", "");
            _DepositResponse.TransactionID = TransactionID;

            string req = BuildRequest(_DepositRequest, TransactionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for Acapture";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=Acapture; {0} {1}{2}", Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider=Acapture; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code < 100)
            {
                _DepositResponse.ErrorDescripotion = "Http request to Acapture failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = _DepositResponse.TransactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }

            ResponseObject ResponseObject = null;
            try
            {
                ResponseObject = JSONSerializer<ResponseObject>.DeSerialize(e.Description);
            }
            catch
            {
                _DepositResponse.ErrorDescripotion = _DepositResponse.error.Description = "Fail parsing http PAYMENT response: " + e.Description;
                Log(string.Format("Response Deposit: Provider=Acapture; {0}{1}", _DepositResponse.ErrorDescripotion, Environment.NewLine));
                return _DepositResponse;
            }

            if (ResponseObject.result.code == "000.000.000" || ResponseObject.result.code == "000.300.000")
            {
                //success                
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.ErrorDescripotion = ResponseObject.result.description;
            }
            else
            {
                _DepositResponse.ErrorCode = ResponseObject.result.code;
                _DepositResponse.ErrorDescripotion = ResponseObject.result.code + " , " + ResponseObject.result.description;
            }

            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
            return _DepositResponse;
        }

        //yyyy-MM-dd
        private string GenerateRandomDob()
        {
            Random r = new Random(DateTime.Now.Millisecond + 1);
            int year = DateTime.Now.Year - r.Next(19, 61);
            int month = r.Next(1, 13);
            int day = r.Next(1, 28);

            return string.Format("{0}-{1}-{2}", year, month.ToString("00"), day.ToString("00"));
        }

        private string BuildRequest(DepositRequest _DepositRequest, string TransactionID)
        {
            string amount = _DepositRequest.Amount.ToString("F2");
            string paymentBrand = GetCardBrandByCardType(_DepositRequest.CardType);
            string cardHolderName = System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName + _DepositRequest.LastName);
            string firstName = System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName);
            string lastName = System.Web.HttpUtility.UrlEncode(_DepositRequest.LastName);

            string billingAddress1 = System.Web.HttpUtility.UrlEncode(_DepositRequest.Address);
            string billingCity = System.Web.HttpUtility.UrlEncode(_DepositRequest.City);
            string billingState = (string.IsNullOrEmpty(_DepositRequest.StateISOCode)) ? ("NA") : (_DepositRequest.StateISOCode);

            string cardExpiryMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");

            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string cardExpiryYear = _DepositRequest.CardExpYear;
            if (cardExpiryYear.Length == 2)
                cardExpiryYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string req = string.Empty;

            try
            {
                //req = "authentication.userId = 8a8294184e01a2c3014e06888fb10ea7&authentication.password=TGxDp3Mjat" +
                //        "&authentication.entityId=8a8294184e01a2c3014e06888fa30ea3&amount=92.00" +
                //        "&currency=EUR&paymentBrand=VISA&paymentType=DB" +
                //        "&card.number=4200000000000000&card.holder=Jane Jones&card.expiryMonth=05" +
                //        "&card.expiryYear=2018&card.cvv=123";

                if(_DepositRequest.provider != Globals.Providers.SWISH)
                    req = string.Format(@"authentication.userId={0}&authentication.password={1}&authentication.entityId={2}&amount={3}&currency={4}&paymentBrand={5}&paymentType={6}&card.number={7}&card.holder={8}&card.expiryMonth={9}&card.expiryYear={10}&card.cvv={11}&merchantTransactionId={12}&customer.merchantCustomerId={13}&customer.givenName={14}&customer.surname={15}&customer.phone={16}&customer.email={17}&customer.ip={18}&billing.street1={19}&billing.city={20}&billing.state={21}&billing.postcode={22}&billing.country={23}",
                                        MerchantID, MerchantPassword, channel,
                                        amount, _DepositRequest.CurrencyISOCode, paymentBrand,
                                        "DB", _DepositRequest.CardNumber, cardHolderName,
                                        cardExpiryMonth, cardExpiryYear, _DepositRequest.CardCVV2,
                                        TransactionID, _DepositRequest.CustomerID, firstName,
                                        lastName, _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", ""), _DepositRequest.Email,
                                        _DepositRequest.IpAddress, billingAddress1, billingCity,
                                        billingState, _DepositRequest.ZipCode, _DepositRequest.CountryISOCode
                                         );
                else
                    req = string.Format(@"authentication.userId={0}&authentication.password={1}&authentication.entityId={2}&amount={3}&currency={4}&paymentBrand={5}&paymentType={6}&card.number={7}&card.holder={8}&card.expiryMonth={9}&card.expiryYear={10}&card.cvv={11}&merchantTransactionId={12}&customer.merchantCustomerId={13}&customer.givenName={14}&customer.surname={15}&customer.phone={16}&customer.email={17}&customer.ip={18}&billing.street1={19}&billing.city={20}&billing.state={21}&billing.postcode={22}&billing.country={23}&customer.middleName={24}&customer.birthDate={25}&risk.merchantWebsite={26}",
                                        MerchantID, MerchantPassword, channel,
                                        amount, _DepositRequest.CurrencyISOCode, paymentBrand,
                                        "DB", _DepositRequest.CardNumber, cardHolderName,
                                        cardExpiryMonth, cardExpiryYear, _DepositRequest.CardCVV2,
                                        TransactionID, _DepositRequest.CustomerID, firstName,
                                        lastName, _DepositRequest.PhoneNumber.Replace("-", "").Replace("+", ""), _DepositRequest.Email,
                                        _DepositRequest.IpAddress, billingAddress1, billingCity,
                                        billingState, _DepositRequest.ZipCode, _DepositRequest.CountryISOCode,
                                        "none", GenerateRandomDob(), sender);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=Acapture; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = new
                System.Net.Security.RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                    resp.Code = 200;
                }

            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        //string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                            resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                        //Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode).ToString() + " ";
                        else
                            resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        #region Response
        [DataContract]
        public class ResponseObject
        {
            [DataMember]
            public string id { get; set; }
            [DataMember]
            public string paymentType { get; set; }
            [DataMember]
            public string paymentBrand { get; set; }
            [DataMember]
            public string amount { get; set; }
            [DataMember]
            public string currency { get; set; }
            [DataMember]
            public string descriptor { get; set; }
            [DataMember]
            public ResponseResult result { get; set; }
            [DataMember]
            public ResponseCard card { get; set; }
            [DataMember]
            public ResponseRisk risk { get; set; }
            [DataMember]
            public string buildNumber { get; set; }
            [DataMember]
            public string timestamp { get; set; }
            [DataMember]
            public string ndc { get; set; }
        }

        [DataContract]
        public class ResponseResult
        {
            [DataMember]
            public string code { get; set; }
            [DataMember]
            public string description { get; set; }
        }

        [DataContract]
        public class ResponseCard
        {
            [DataMember]
            public string bin { get; set; }
            [DataMember]
            public string last4Digits { get; set; }
            [DataMember]
            public string holder { get; set; }
            [DataMember]
            public string expiryMonth { get; set; }
            [DataMember]
            public string expiryYear { get; set; }
        }

        [DataContract]
        public class ResponseRisk
        {
            [DataMember]
            public string risk { get; set; }
        }


        #endregion

        private string GetCardBrandByCardType(string cardType)
        {
            string cardBrand = string.Empty;
            cardTypeMapping.TryGetValue(cardType, out cardBrand);
            return cardBrand;
        }
    }
}