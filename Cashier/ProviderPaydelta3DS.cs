﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderPaydelta3DS : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        private string ProduceName;
        private string ProduceURL;
        private string CallbackUrl;

        #region Constructor

        public ProviderPaydelta3DS(ProviderConfigBase config)
            : base(config)
        {
            Paydelta3DSProviderConfig paydelta3DSConfig = config as Paydelta3DSProviderConfig;
            if (paydelta3DSConfig == null)
            {
                throw new ArgumentException("paydelta3DSConfig expects to get Paydelta3DSProviderConfig object!");
            }

            ProduceName = paydelta3DSConfig.ProduceName;
            ProduceURL = paydelta3DSConfig.ProduceURL;
            CallbackUrl = paydelta3DSConfig.CallbackUrl;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.PAYDELTA3DS; }
            set { }
        }


        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            _DepositResponse.ErrorCode = "1";
            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Is3DSecure = false;
            _DepositResponse.IsPending = false;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = _DepositRequest.provider;

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string req = BuildRequestXML(_DepositRequest, transactionID, ExpMonth, ExpYear);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for Paydelta3DS";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=Paydelta3DS; {0} {1}{2}", Url, req, Environment.NewLine));

            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 40000, 40000);

            Log(string.Format("Response: Provider=Paydelta3DS; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to Paydelta3DS failed. " + e.Description;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
                ParseResponse(e.Description, _DepositResponse);

            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

            return _DepositResponse;
        }

        private void ParseResponse(string Xml, DepositResponse _DepositResponse)
        {
            //this is 3DS
            if (Xml.StartsWith("http") || Xml.StartsWith("HTTP") || Xml.StartsWith("https") || Xml.StartsWith("HTTPS"))
            {
                _DepositResponse.Is3DSecure = true;

                _DepositResponse.RedirectUrl = Xml;

                _DepositResponse.ErrorDescripotion = "";
                _DepositResponse.ErrorCode = "0";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                return;
            }

            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.LoadXml(Xml);
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider=Paydelta3DS; XML parse exception: {0}{1}", ex.Message, Environment.NewLine));

                _DepositResponse.ErrorDescripotion = "Invalid xml response. " + ex.Message;
                return;
            }

            try
            {
                XmlNode node = xDoc.SelectSingleNode("//paydeltaxml/result/result_code");
                if (node is XmlNode)
                {
                    //success
                    if (node.InnerText == "000")
                    {
                        node = xDoc.SelectSingleNode("//paydeltaxml/trans_data/transaction_id");
                        if (node is XmlNode)
                            _DepositResponse.Receipt = node.InnerText;

                        _DepositResponse.ErrorDescripotion = "";
                        _DepositResponse.ErrorCode = "0";
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                    }
                    else
                    {
                        string code = node.InnerText;
                        string description = "unkown error";
                        node = xDoc.SelectSingleNode("//paydeltaxml/result/result_text");
                        if (node is XmlNode)
                            description = node.InnerText;

                        _DepositResponse.ErrorDescripotion = code + " , " + description;
                    }
                }
                else
                    _DepositResponse.ErrorDescripotion = "Invalid xml response - missing result code";
            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider=Paydelta3DS; Parsing xml response exception: {0}{1}", ex.Message, Environment.NewLine));

                _DepositResponse.ErrorDescripotion = "Parsing xml response exception. " + ex.Message;
            }

        }

        private string BuildRequestXML(DepositRequest _DepositRequest, string TransactionID, string CardExpMonth, string CardExpYear)
        {
            string req = string.Empty;

            try
            {
                string Amount = string.Empty;
                if (_DepositRequest.CurrencyISOCode.ToUpper() == "JPY")
                    Amount = _DepositRequest.Amount.ToString();
                else
                {
                    Amount = Math.Floor(_DepositRequest.Amount * 100.0).ToString();
                }

                string signature = MerchantID + "&" + TransactionID + "&" + Amount + "&" + _DepositRequest.CurrencyISOCode + "&" + ProduceName + "&" + _DepositRequest.CardNumber.Substring(_DepositRequest.CardNumber.Length - 4) + "&" + MerchantPassword + "&";
                signature = GenerateMD5(signature).ToUpper();

                string state = (string.IsNullOrEmpty(_DepositRequest.StateISOCode)) ? ("NA") : (_DepositRequest.StateISOCode);

                req = string.Format(@"<paydeltaxml>
	<header>
		<user>{0}</user>
		<mac>{1}</mac>
		<return_url>{2}</return_url>
	</header>
	<request>
		<order_number>{3}</order_number>		
		<cardholder>
			<first_name>{4}</first_name>
			<last_name>{5}</last_name>
			<address>{6}</address>
			<zip>{7}</zip>
			<city>{8}</city>
			<state>{9}</state>
			<country>{10}</country>
			<phone>{11}</phone>
			<mail>{12}</mail>
			<trans_type>0</trans_type>			
			<ip>{13}</ip>
		</cardholder>
		<card>
			<card_nr>{14}</card_nr>
			<card_exp_month>{15}</card_exp_month>
			<card_exp_year>{16}</card_exp_year>
			<card_cvv>{17}</card_cvv>
		</card>
		<amount>
			<order_value>{18}</order_value>
			<order_currency>{19}</order_currency>
		</amount>
		<product>
			<product_name>{20}</product_name>
			<product_url>{21}</product_url>
		</product>
	</request>
</paydeltaxml>", MerchantID, signature, CallbackUrl, TransactionID, _DepositRequest.FirstName, _DepositRequest.LastName,
                   _DepositRequest.Address, _DepositRequest.ZipCode, _DepositRequest.City, state, _DepositRequest.CountryISOCode, _DepositRequest.PhoneNumber,
                   _DepositRequest.Email, _DepositRequest.IpAddress, _DepositRequest.CardNumber, CardExpMonth, CardExpYear, _DepositRequest.CardCVV2, Amount, _DepositRequest.CurrencyISOCode,
                   ProduceName, ProduceURL);
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on building request XML string (2): Provider=Paydelta3DS; {0}{1}", ex.Message, Environment.NewLine));
                req = string.Empty;
            }
            return req;
        }

        private string GenerateMD5(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes 
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string. 
                return sBuilder.ToString();
            }
        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();
            resp.Code = 0;

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        string Code = "";
                        if ((we.Response is System.Net.HttpWebResponse))
                        {
                            resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                            Code = resp.Code.ToString() + " ";
                        }
                        else
                            resp.Code = -1;

                        resp.Description = Code + responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

    }
}
