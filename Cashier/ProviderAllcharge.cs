﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using Cashier.Configuration.ProviderConfig;
using CashierUtils;

namespace Cashier
{
    internal class ProviderAllcharge : ProviderBase
    {
        private const string POST = "POST";
        private const string SUCCESSFUL_RET_CODE = "0";
        private const string DCP_ID = "DCPId";
        private const string DCP_PASSWORD = "DCPPassword";
        private const string EMAIL = "email";
        private const string CURRENCY = "currency";
        private const string PM_TYPE = "pmType";
        private const string CARD_TYPE = "cardType";
        private const string CARD_NUM = "cardNum";
        private const string CVV2_PIN = "CVV2/PIN";
        private const string FIRST_NAME = "FirstName";
        private const string LAST_NAME = "LastName";
        private const string ADDRESS = "Address";
        private const string CITY = "City";
        private const string POST_CODE = "postCode";
        private const string COUNTRY = "Country";
        private const string PHONE = "Phone";
        private const string EXP_MONTH = "ExpMonth";
        private const string EXP_YEAR = "ExpYear";
        private const string AMOUNT = "amount";
        private const string STATE = "State";
        private const string TRANSACTION_ID = "transactionID";
        private const string DESC = "Desc";
        private const string IP = "ipaddress";

        private const string RAW_ERR_DESC = "RawErrDesc";
        private const string ERR_DESC = "ErrDesc";
        private const string RECEIPT = "receipt";
        private const string RET_CODE = "retCode";

        private const string TRANSACTION_ID_VALUE = "Automatic";

        private const string CARD_TYPE_VALUE = "4";
        private const string PM_TYPE_VALUE = "2";

        public ProviderAllcharge(ProviderConfigBase config)
            : base(config)
        {
        }


        protected override Globals.Providers ProviderType
        {
            get
            {
                return Globals.Providers.ALLCHARGE;
            }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            var _DepositResponse = new DepositResponse();
            if (_DepositRequest != null)
            {
                try
                {
                    var requestCollection = CreateRequestCollection(_DepositRequest);
                    Log(requestCollection);

                    var strResponse = SendRequest(requestCollection, Url);

                    Log(string.Format(string.Format("Response: {0}", strResponse)));
                    _DepositResponse = ParseResponse(strResponse);
                }
                catch (Exception ex)
                {
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.error.Description = ex.Message;

                    // fill the class FailedDeposit with details in order to send email to support later
                    FailedDeposit failedDeposit = new FailedDeposit();
                    failedDeposit.provider = _DepositRequest.provider;
                    failedDeposit.Email = _DepositRequest.Email;
                    failedDeposit.TransactionID = _DepositResponse.TransactionID;
                    failedDeposit.Amount = _DepositRequest.Amount;
                    failedDeposit.FirstName = _DepositRequest.FirstName;
                    failedDeposit.LastName = _DepositRequest.LastName;
                    failedDeposit.Description = _DepositResponse.error.Description;

                    _DepositResponse.FailedDeposits.Add(failedDeposit);
                }
            }
            else
            {
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.error.Description = "DepositRequest parameters can't be null";
            }
            _DepositResponse.provider = ProviderType;
            return _DepositResponse;
        }

        private NameValueCollection CreateRequestCollection(DepositRequest _DepositRequest)
        {
            return new NameValueCollection()
            {
                { DCP_ID, MerchantID },
                { DCP_PASSWORD, MerchantPassword },
                { EMAIL, _DepositRequest.Email },
                { CURRENCY, _DepositRequest.CurrencyISOCode },
                { PM_TYPE, _DepositRequest.PmType },
                { CARD_TYPE, CARD_TYPE_VALUE }, { CARD_NUM, _DepositRequest.CardNumber },
                { CVV2_PIN, _DepositRequest.CardCVV2 },
                { FIRST_NAME, _DepositRequest.FirstName },
                { LAST_NAME, _DepositRequest.LastName },
                { ADDRESS, _DepositRequest.Address },
                { CITY, _DepositRequest.City },
                { POST_CODE, _DepositRequest.ZipCode },
                { COUNTRY, _DepositRequest.CountryISOCode },
                { PHONE, _DepositRequest.PhoneNumber },
                { EXP_MONTH, _DepositRequest.CardExpMonth },
                { EXP_YEAR, _DepositRequest.CardExpYear },
                { AMOUNT, (_DepositRequest.Amount * 100).ToString() }, { STATE, _DepositRequest.StateISOCode },
                { TRANSACTION_ID, TRANSACTION_ID_VALUE },
                { DESC, _DepositRequest.Description },
                { IP, _DepositRequest.IpAddress }
            };
        }
        private string SendRequest(NameValueCollection parametersCollection, string serviceUrl)
        {
            using (var webClient = new WebClient())
            {
                try
                {
                    var response = webClient.UploadValues(serviceUrl, POST, parametersCollection);
                    return Encoding.UTF8.GetString(response);
                }
                catch (Exception ex)
                {
                    throw new Exception("AllCharge: An error has occured during sending request: " + ex.Message);
                }
            }
        }

        private DepositResponse ParseResponse(string response)
        {
            try
            {
                var col = HttpUtility.ParseQueryString(response);
                var _DepositResponse = new DepositResponse()
                { TransactionID = col[TRANSACTION_ID],
                    Amount = (Convert.ToInt64(col[AMOUNT]) / 100).ToString(),
                    CurrencyISOCode = col[CURRENCY],
                    Receipt = col[RECEIPT],
                    ErrorDescripotion = !string.IsNullOrEmpty(col[RAW_ERR_DESC]) ? col[RAW_ERR_DESC] : col[ERR_DESC],
                    RetCode = col[RET_CODE],
                    ClearingUserID = MerchantID };

                _DepositResponse.error.ErrorCode = _DepositResponse.RetCode == SUCCESSFUL_RET_CODE ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                return _DepositResponse;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AllCharge: An error has occured during parsing response '{0}'. Exception: {1}", response, ex.Message));
            }
        }
    }
}
