﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cashier.Config
{
	class ProviderConfig
	{
		public Mode Mode { get; set;}
		public string Url { get; set; }
		public string MerchantId { get; set; }
		public string Password { get; set; }
	}
}
