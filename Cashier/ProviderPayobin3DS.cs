﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using CashierUtils;

namespace Cashier
{
    class ProviderPayobin3DS : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }

        /*private class RedirectValues
        {
            public string Key;
            public string Value;
        }
        List<RedirectValues> RedirectParameters;*/

        private string Servicekey;
        private string Routingkey;
        private string RoutingkeyCombined;
        private string RedirectUrl;
        private string CallbackUrl;

        private string ProviderName;

        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            {"visa", "VISA"},
            {"Visa", "VISA"},
            {"mastercard", "MASTERCARD"},
            {"Mastercard", "MASTERCARD"},
            {"diners club", "DINERSCLUB"},
            {"dinersclubintl", "DINERSCLUB"},
            {"dinersclub", "DINERSCLUB"},
            {"Diners Club", "DINERSCLUB"},
            {"american express", "AMEX"},
            {"American Express", "AMEX"},
            {"amex", "AMEX"},
            {"maestro","MAESTRO"},
            {"Maestro","MAESTRO"}
        };

        #region Constructor

        public ProviderPayobin3DS(ProviderConfigBase config)
            : base(config)
        {
            Payobin3DSProviderConfig payobin3DSConfig = config as Payobin3DSProviderConfig;
            if (payobin3DSConfig == null)
            {
                throw new ArgumentException("payobin3DSProvider expects to get Payobin3DSProviderConfig object!");
            }

            //RedirectParameters = new List<RedirectValues>();

            Servicekey = payobin3DSConfig.Servicekey;
            Routingkey = payobin3DSConfig.Routingkey;
            RoutingkeyCombined = payobin3DSConfig.RoutingKeyCombined;
            RedirectUrl = payobin3DSConfig.RedirectUrl;
            CallbackUrl = payobin3DSConfig.CallbackUrl;
        }
        #endregion

        protected override Globals.Providers ProviderType { get; set; }
        
        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            string sessionID = Guid.NewGuid().ToString().Replace("-", "");
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();
            _DepositResponse.provider = _DepositRequest.provider; //Globals.Providers.PAYOBIN3DS;
            _DepositResponse.ClearingUserID = Servicekey;
            _DepositResponse.TransactionID = transactionID;

            ProviderName = _DepositRequest.provider.ToString();
            ProviderType = _DepositRequest.provider;

            string req = BuildRequestXML(_DepositRequest, transactionID, sessionID);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ErrorDescripotion = "Fail building request XML";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            }
            else
            {
                Log(string.Format("Request: Provider={2}; {0}{1}", req, Environment.NewLine, ProviderName));

                System.Collections.Specialized.NameValueCollection headers = new System.Collections.Specialized.NameValueCollection();
                headers.Add("Service-Key", Servicekey);
                Log(string.Format("Request: Provider={2}; Using service key: {0}{1}", Servicekey, Environment.NewLine, ProviderName));
                if (!string.IsNullOrEmpty(Routingkey))
                {
                    headers.Add("Routing-Key", Routingkey);
                    Log(string.Format("Request: Provider={2}; Using routing key: {0}{1}", Routingkey, Environment.NewLine, ProviderName));
                }
                else if (!string.IsNullOrEmpty(RoutingkeyCombined))
                {
                    var dict = RoutingkeyCombined.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                           .Select(part => part.Split('='))
                           .ToDictionary(split => split[0], split => split[1]);
                    if (!dict.ContainsKey(GetCardBrandByCardType(_DepositRequest.CardType)))
                    {
                        _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                        _DepositResponse.ErrorDescripotion = "No routing key value found for credit card type: " + _DepositRequest.CardType;
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                        _DepositResponse.ErrorCode = "1";
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        return _DepositResponse;
                    }

                    string rt = dict[GetCardBrandByCardType(_DepositRequest.CardType)];
                    headers.Add("Routing-Key", rt);
                    Log(string.Format("Request: Provider={2}; Using routing key: {0}{1}", rt, Environment.NewLine, ProviderName));
                }
                else
                {
                    _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                    _DepositResponse.ErrorDescripotion = "Routing key value is missing";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    return _DepositResponse;
                }

                Error e = HttpReq("POST", Url, req, headers, "application/xml; charset=UTF-8", "application/xml; charset=UTF-8", 20000, 20000);

                Log(string.Format("Response: Provider={2}; {0}{1}", e.Description, Environment.NewLine, ProviderName));

                if (e.Code != 0)
                {
                    _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                    _DepositResponse.ErrorDescripotion = "Http request to Payobin3DS failed. " + e.Description;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                    // fill the class FailedDeposit with details in order to send email to support later
                    //FailedDeposit failedDeposit = new FailedDeposit();
                    //failedDeposit.provider = _DepositRequest.provider;
                    //failedDeposit.Email = _DepositRequest.Email;
                    //failedDeposit.TransactionID = transactionID;
                    //failedDeposit.Amount = _DepositRequest.Amount;
                    //failedDeposit.FirstName = _DepositRequest.FirstName;
                    //failedDeposit.LastName = _DepositRequest.LastName;
                    //failedDeposit.Description = e.Description;

                    //_DepositResponse.FailedDeposits.Add(failedDeposit);
                }
                else
                {
                    ParseResponse(e.Description, _DepositResponse);
                }
            }

            if (!string.IsNullOrEmpty(_DepositResponse.ErrorDescripotion))
            {
                _DepositResponse.ErrorDescripotion = _DepositResponse.ErrorDescripotion.Replace("'", "");
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
            }

            if (_DepositResponse.error.ErrorCode == Globals.ErrorCodes.FAIL)
            {
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = _DepositResponse.error.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }

            return _DepositResponse;
        }

        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                /*ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );*/

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                Log(string.Format("Request Headers: Provider={2}; {0}{1}", req.Headers, Environment.NewLine, ProviderName));
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if ((we.Response is System.Net.HttpWebResponse))
                    resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                else
                    resp.Code = (int)we.Status;
                resp.Description = resp.Code.ToString() + ", " + we.Message;
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private string BuildRequestXML(DepositRequest _DepositRequest, string TransactionID, string SessionID)
        {
            string req = null;

            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");
            string connectionType = "sync";
            if (ProviderName == "WIROPAY3DS")
            {
                connectionType = "async";
            }

            req = $@"<?xml version='1.0' encoding='UTF-8' standalone='yes'?>
    <ProcessDebitRequest mode='{connectionType}' sessionId='{SessionID}'>
    <transactions mcTxId='{TransactionID}' currency='{_DepositRequest.CurrencyISOCode}' amount='{(int)(_DepositRequest.Amount * 100.00)}' usageL1='Fund Deposit' usageL2='' started='0' callbackURL='{CallbackUrl}'>
        <method>
            <{GetCardBrandByCardType(_DepositRequest.CardType)} number='{_DepositRequest.CardNumber}' verification='{_DepositRequest.CardCVV2}' ownerFirstName='{_DepositRequest.FirstName}' ownerMiddleName='' ownerLastName='{_DepositRequest.LastName}' expireYear='{ExpYear}' expireMonth='{ExpMonth}'/>
        </method>        
        <interactive redirectUrl='{RedirectUrl}' sessionId='{SessionID}'/>
        <customer email='{_DepositRequest.Email}' ip='{_DepositRequest.IpAddress}' userAgent='' acceptHeader='text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' firstName='{_DepositRequest.FirstName}' lastName='{_DepositRequest.LastName}' salutation='' birthYear='' birthMonth='' birthDay='' gender=''>
            <addresses city='{_DepositRequest.City}' street='{_DepositRequest.Address}' streetNo='' state='{_DepositRequest.StateISOCode}' extension='' country='{_DepositRequest.CountryISOCode}' zip='{_DepositRequest.ZipCode}' />
            <phones number='{_DepositRequest.PhoneNumber}'/>
        </customer>
    </transactions>
</ProcessDebitRequest>";

            return req;
        }

        private string GetCardBrandByCardType(string cardType)
        {
            string cardBrand = string.Empty;
            cardTypeMapping.TryGetValue(cardType, out cardBrand);
            return cardBrand;
        }

        private void ParseResponse(string XML, DepositResponse _DepositResponse)
        {
            XmlDocument xDoc = null;
            XmlNode node = null;
            string status = string.Empty;
            string message = string.Empty;
            string reciept = string.Empty;
            string amount = string.Empty;

            try
            {
                xDoc = new XmlDocument();
                xDoc.LoadXml(XML);
            }
            catch (Exception ex)
            {
                Log(string.Format("Exception on parsing response: Provider={2}; {0}{1}", ex.Message, Environment.NewLine, ProviderName));
                _DepositResponse.ErrorDescripotion = ex.Message;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.ErrorCode = "1";
                return;
            }

            #region Handle Payobin3DS General Error Response            
            //check for general error
            node = xDoc.SelectSingleNode("//error_response");
            if (node != null)
            {
                try
                {
                    status = node.Attributes["status"].Value;
                    message = node.Attributes["message"].Value;

                    _DepositResponse.ErrorDescripotion = message;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.ErrorCode = status;
                    return;
                }
                catch (Exception ex)
                {
                    Log(string.Format("Exception on parsing response (2): Provider={0}; {1}{2}", ProviderName, ex.Message, Environment.NewLine));
                    _DepositResponse.ErrorDescripotion = ex.Message;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.ErrorCode = "1";
                    return;
                }
            }
            #endregion

            node = xDoc.SelectSingleNode("//ProcessDebitResponse/transactionResults");
            if (node == null)
            {
                Log(string.Format("Response: Provider={1}; Invalid xml{0}", Environment.NewLine, ProviderName));
                _DepositResponse.ErrorDescripotion = "Invalid xml response from Payobin";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.ErrorCode = "1";
                return;
            }

            try
            {
                status = node.Attributes["status"].Value;
                message = node.Attributes["message"].Value;
                if (node.Attributes["txId"] != null)
                    _DepositResponse.Receipt = node.Attributes["txId"].Value;
                //amount = (Convert.ToDouble(node.Attributes["creditAmount"].Value) / 100.0).ToString("F2");
                _DepositResponse.Amount = (Convert.ToDouble(node.Attributes["creditAmount"].Value) / 100.0).ToString("F2");

                //handle 3DS pending
                if (status == "05.001.001.000")
                {
                    _DepositResponse.Is3DSecure = true;

                    node = xDoc.SelectSingleNode("//ProcessDebitResponse/continueResult");
                    if (node == null)
                    {
                        Log(string.Format("Response: Provider={1}; Invalid xml. Got pending result but continueResult element is missing {0}", Environment.NewLine, ProviderName));
                        _DepositResponse.ErrorDescripotion = "Invalid xml response from Payobin";
                        _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        _DepositResponse.ErrorCode = "1";
                        return;
                    }

                    _DepositResponse.RedirectUrl = node.Attributes["redirectUrl"].Value;

                    XmlNodeList nodes = xDoc.SelectNodes("//ProcessDebitResponse/continueResult/parameter");
                    if (nodes != null && nodes.Count > 0)
                    {
                        _DepositResponse.RedirectParameters = new List<RedirectValues>();

                        foreach (XmlNode n in nodes)
                        {
                            RedirectValues s = new RedirectValues();
                            s.Key = n.Attributes["name"].Value;
                            s.Value = n.Attributes["value"].Value;

                            _DepositResponse.RedirectParameters.Add(s);
                        }
                    }
                }
                else if (status == "00.001.000.000") //success
                {
                    _DepositResponse.ErrorDescripotion = "";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                    _DepositResponse.ErrorCode = "0";
                }
                else
                {
                    _DepositResponse.ErrorDescripotion = "Status: " + status + " , " + message;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    _DepositResponse.ErrorCode = "1";
                }


            }
            catch (Exception ex)
            {
                Log(string.Format("Response: Provider={1}; Exception while parsing xml: " + ex.Message + "{0}", Environment.NewLine, ProviderName));
                _DepositResponse.ErrorDescripotion = "Fail parsing xml response";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                _DepositResponse.ErrorCode = "1";
                return;
            }



        }


    }
}
