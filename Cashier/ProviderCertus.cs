﻿using Cashier.Configuration.ProviderConfig;
using CashierUtils;
using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;

namespace Cashier
{
    #region DataContract
    public class Error
    {
        public int Code;
        public string Description;
    }
    [DataContract]
    public class ResponseResult
    {
        [DataMember]
        public string code { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string errorId { get; set; }
        [DataMember]
        public ErrorResponse[] error { get; set; }
        [DataMember]
        public string reasonCode { get; set; }
    }
    [DataContract]

    public class ErrorResponse
    {
        [DataMember]
        public string code { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string advice { get; set; }
    }
    [DataContract]
    public class SourceAmount
    {
        [DataMember]

        public string amount { get; set; }
        [DataMember]

        public string currencyCode { get; set; }
    }

    public class CertusCallBack
    {
        public string responseTime { get; set; }
        public ResponseResult result { get; set; }
        public string signature { get; set; }
        public string txId { get; set; }
        public string txTypeId { get; set; }
        public string txType { get; set; }
        public string recurrentTypeId { get; set; }
        public string requestId { get; set; }
        public string orderId { get; set; }
        public SourceAmount sourceAmount { get; set; }
        public Amount amount { get; set; }
        public string ccNumber { get; set; }
        public string cardId { get; set; }
        public string auth3DUrl { get; set; }
    }

    [DataContract]
    public class CreateResponseObject
    {
        [DataMember]
        public string responseTime { get; set; }
        [DataMember]
        public ResponseResult result { get; set; }
        [DataMember]
        public string signature { get; set; }

        [DataMember]
        public string txId { get; set; }
        [DataMember]
        public string txTypeId { get; set; }
        [DataMember]
        public string txType { get; set; }
        [DataMember]
        public string recurrentTypeId { get; set; }
        [DataMember]
        public string requestId { get; set; }
        [DataMember]
        public string orderId { get; set; }

        [DataMember]
        public SourceAmount sourceAmount { get; set; }
        [DataMember]
        public Amount amount { get; set; }
        [DataMember]

        public string ccNumber { get; set; }
        [DataMember]
        public string cardId { get; set; }
        [DataMember]
        public string auth3DUrl { get; set; }

        [DataMember]
        public string currencyCode { get; set; }
        [DataMember]
        public object returnUrl { get; set; }
        [DataMember]
        public object failUrl { get; set; }

    }
    [DataContract]
    public class Amount
    {
        [DataMember]

        public string amount { get; set; }
        [DataMember]

        public string currencyCode { get; set; }
    }
    [DataContract]
    public class CreateRequest
    {
        [DataMember]
        public string requestIP { get; set; }
        [DataMember]
        public string requestTime { get; set; }
        [DataMember]
        public string merchantIdHash { get; set; }
        [DataMember]
        public string merchantAccountIdHash { get; set; }
        [DataMember]
        public string encryptedAccountUsername { get; set; }
        [DataMember]
        public string encryptedAccountPassword { get; set; }
        [DataMember]
        public string signature { get; set; }
        [DataMember]
        public object lang { get; set; }
        [DataMember]
        public MetaData metaData { get; set; }
        [DataMember]
        public CreateTransactionInfo transactionInfo { get; set; }
    }
    [DataContract]
    public class CreateTransactionInfo
    {
        [DataMember]
        public string apiVersion { get; set; }
        [DataMember]
        public string requestId { get; set; }

        [DataMember]
        public string recurrentType { get; set; }

        [DataMember]
        public CreateOrderData orderData { get; set; }
        [DataMember]
        public string statementText { get; set; }
        [DataMember]
        public string cancelUrl { get; set; }
        [DataMember]
        public string returnUrl { get; set; }
        [DataMember]
        public string notificationUrl { get; set; }
    }
    public class CCCard
    {
        public string cvv { get; set; }
        public string expirationMonth { get; set; }
        public string expirationYear { get; set; }
        public string cardHolderName { get; set; }
        public string ccNumber { get; set; }
    }
    [DataContract]
    public class CreateOrderData
    {
        [DataMember]
        public string orderId { get; set; }
        [DataMember]
        public string orderDescription { get; set; }
        [DataMember]
        public string amount { get; set; }
        [DataMember]
        public string currencyCode { get; set; }
        [DataMember]
        public CCCard cc { get; set; }
        [DataMember]
        public CreateBillingAddress billingAddress { get; set; }

        public OrderDetail orderDetail { get; set; }

    }
    [DataContract]
    public class OrderDetail
    {
        [DataMember]
        public string invoiceNo { get; set; }
        [DataMember]
        public string invoiceDate { get; set; }
        [DataMember]
        public string merchantAddCity { get; set; }
        [DataMember]
        public string merchantAddCountry { get; set; }
        [DataMember]
        public string merchantAddLine1 { get; set; }
        [DataMember]
        public string merchantAddLine2 { get; set; }
        [DataMember]
        public string merchantAddState { get; set; }
        [DataMember]
        public string merchantAddZipcode { get; set; }
        [DataMember]
        public string merchantBusinessName { get; set; }
        [DataMember]
        public string merchantEmail { get; set; }
        [DataMember]
        public string merchantFirstName { get; set; }
        [DataMember]
        public string merchantLastName { get; set; }
        [DataMember]
        public string merchantMemo { get; set; }
        [DataMember]
        public string merchantPhone { get; set; }
        [DataMember]
        public string note { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public int totalDiscountAmt { get; set; }
        [DataMember]
        public int totalGrossAmt { get; set; }
        [DataMember]
        public int totalNetAmtTaxInc { get; set; }
        [DataMember]
        public int totalShippingAmt { get; set; }
        [DataMember]
        public string amtCurrencyCode { get; set; }
        [DataMember]
        public OrderItem[] orderItems { get; set; }
    }

    public class OrderItem
    {
        [DataMember]
        public string brand { get; set; }
        [DataMember]
        public string category { get; set; }
        [DataMember]
        public string imgUrl { get; set; }
        [DataMember]
        public string itemName { get; set; }
        [DataMember]
        public int qty { get; set; }
        [DataMember]
        public int unitDiscountAmt { get; set; }
        [DataMember]
        public int unitGrossAmt { get; set; }
        [DataMember]
        public int unitNetAmtTaxInc { get; set; }
    }
    [DataContract]
    public class CreateBillingAddress
    {
        [DataMember]
        public string firstName { get; set; }
        [DataMember]
        public string lastName { get; set; }
        [DataMember]
        public string address1 { get; set; }
        [DataMember]
        public string address2 { get; set; }
        [DataMember]
        public string city { get; set; }
        [DataMember]
        public string zipcode { get; set; }
        [DataMember]
        public string stateCode { get; set; }
        [DataMember]
        public string countryCode { get; set; }
        [DataMember]
        public string mobile { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string fax { get; set; }
    }
    public class MetaData
    {
        [DataMember]
        public string merchantUserId { get; set; }
    }
    #endregion

    internal class ProviderCertus : ProviderBase
    {
        #region memmbers
        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.CERTUS; }
            set { }
        }
        private string MerchantAccountUserName;
        private string MerchantAccountPassword;
        private string MerchantAccountID;
        private int GetStatusTimeoutSeconds;
        private string ReturnUrl;
        private string CancelUrl;
        #endregion

        public ProviderCertus(ProviderConfigBase config)
           : base(config)
        {
            WireCapitalProviderConfig certusConfig = config as WireCapitalProviderConfig;

            if (certusConfig == null)
            {
                throw new ArgumentException("WireCapitalProviderConfig expects to get CertusProviderConfig object!");
            }

            MerchantAccountUserName = certusConfig.MerchantAccountUserName;
            MerchantAccountPassword = certusConfig.MerchantAccountPassword;
            MerchantAccountID = certusConfig.MerchantAccountID;

            ReturnUrl = certusConfig.ReturnUrl;
            CancelUrl = certusConfig.CancelUrl;
            if (!int.TryParse(certusConfig.GetStatusTimeoutSeconds, out GetStatusTimeoutSeconds) || GetStatusTimeoutSeconds <= 0)
                GetStatusTimeoutSeconds = 20 * 2; //default 20 seconds

        }

        public override DepositResponse Deposit(DepositRequest depositRequest)
        {
            DepositResponse depositResponse = new DepositResponse();

            depositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
            depositResponse.ErrorCode = "1";
            string transactionID = depositResponse.TransactionID = Guid.NewGuid().ToString().Replace("-", "");
            depositResponse.Amount = depositRequest.Amount.ToString();
            depositResponse.CurrencyISOCode = depositRequest.CurrencyISOCode;
            depositResponse.provider = depositRequest.provider;
            depositResponse.ClearingUserID = MerchantID;
            depositResponse.TransactionID = transactionID;
            #region Trim            
            depositRequest.Address = depositRequest.Address.Trim();
            depositRequest.CardCVV2 = depositRequest.CardCVV2.Trim();
            depositRequest.CardExpMonth = depositRequest.CardExpMonth.Trim();
            depositRequest.CardExpYear = depositRequest.CardExpYear.Trim();
            depositRequest.CardNumber = depositRequest.CardNumber.Trim();
            depositRequest.City = depositRequest.City.Trim();
            depositRequest.CountryISOCode = depositRequest.CountryISOCode.Trim();
            if (!string.IsNullOrEmpty(depositRequest.CountryISOCodeThreeLetters))
                depositRequest.CountryISOCodeThreeLetters = depositRequest.CountryISOCodeThreeLetters.Trim();
            depositRequest.CurrencyISOCode = depositRequest.CurrencyISOCode.Trim();
            if (!string.IsNullOrEmpty(depositRequest.Description))
                depositRequest.Description = depositRequest.Description.Trim();
            depositRequest.Email = depositRequest.Email.Trim();
            depositRequest.FirstName = depositRequest.FirstName.Trim();
            depositRequest.LastName = depositRequest.LastName.Trim();
            if (!string.IsNullOrEmpty(depositRequest.NameOnCard))
                depositRequest.NameOnCard = depositRequest.NameOnCard.Trim();
            depositRequest.PhoneNumber = depositRequest.PhoneNumber.Trim();
            if (!string.IsNullOrEmpty(depositRequest.StateISOCode))
                depositRequest.StateISOCode = depositRequest.StateISOCode.Trim();
            depositRequest.ZipCode = depositRequest.ZipCode.Trim();

            #endregion

            #region Purchase Phase
            CreateRequest createRequest = BuildPurchaseRequestObject(depositRequest, transactionID);

            var json = JSONSerializer<CreateRequest>.Serialize(createRequest).Replace("\\/", "/");

            Log(string.Format("Request [Purchase Phase]: Provider=Certus; {0} {1}{2}", Url, json, Environment.NewLine));

            Error e = HttpReq("POST", Url, json, null, "application/json", "", 40000, 40000);
            Log(string.Format("Response [Purchase Phase]: Provider=Certus; {0}{1}", e.Description, Environment.NewLine));
            CreateResponseObject createResponseObject = null;
            if (e.Code != 0)
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "HTTP request for create PURCHASE failed: " + e.Description;
                Log(string.Format("Response [Purchase Phase]: Provider=Certus; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = depositRequest.provider;
                failedDeposit.Email = depositRequest.Email;
                failedDeposit.TransactionID = depositResponse.TransactionID;
                failedDeposit.Amount = depositRequest.Amount;
                failedDeposit.FirstName = depositRequest.FirstName;
                failedDeposit.LastName = depositRequest.LastName;
                createResponseObject = JSONSerializer<CreateResponseObject>.DeSerialize(e.Description);

                var errorResult = string.Empty;
                if (createResponseObject != null && createResponseObject.result != null && createResponseObject.result.error != null &&
                    createResponseObject.result.error.Length > 0)
                {
                    foreach (var error in createResponseObject.result.error)
                    {
                        if (string.IsNullOrWhiteSpace(errorResult))
                        {
                            errorResult = $"{error.message} -- {error.advice} -- {error.code}";
                        }
                        else
                        {
                            errorResult = $"{errorResult}, {error.message} -- {error.advice} -- {error.code}";
                        }
                    }
                }

                if (string.IsNullOrWhiteSpace(errorResult))
                {
                    failedDeposit.Description = "Faild deposit - unknown error";
                }
                else
                {
                    failedDeposit.Description = errorResult;
                }

                depositResponse.FailedDeposits.Add(failedDeposit);

                return depositResponse;
            }


            try
            {
                createResponseObject = JSONSerializer<CreateResponseObject>.DeSerialize(e.Description);
            }
            catch
            {
                depositResponse.ErrorDescripotion = depositResponse.error.Description = "Fail parsing http PURCHASE response: " + e.Description;
                Log(string.Format("Response [Purchase Phase]: Provider=Certus; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));
                return depositResponse;
            }

            if (createResponseObject.result.code == "1")
            {
                depositResponse.Receipt = createResponseObject.txId;
                depositResponse.Type = createResponseObject.txType;
                depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                depositResponse.ErrorCode = "0";
            }
            else if (createResponseObject.result.code == "-2")
            {
                depositResponse.Is3DSecure = true;
                depositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;
                depositResponse.ErrorCode = "0";
                depositResponse.RedirectUrl = createResponseObject.auth3DUrl;
            }
            else
            {
                if (createResponseObject.result.error is ErrorResponse[] && createResponseObject.result.error.Length > 0)
                {
                    depositResponse.ErrorCode = createResponseObject.result.error[0].code;
                    depositResponse.ErrorDescripotion = createResponseObject.result.error[0].code + " , " + createResponseObject.result.error[0].message;
                }
                else depositResponse.ErrorDescripotion = depositResponse.error.Description = "PURCHASE creation failed";
                Log(string.Format("Response [Purchase Phase]: Provider=Certus; {0}{1}", depositResponse.ErrorDescripotion, Environment.NewLine));

                return depositResponse;
            }
            #endregion

            depositResponse.error.Description = depositResponse.ErrorDescripotion;

            return depositResponse;

        }



        #region Private methods

        private CreateRequest BuildPurchaseRequestObject(DepositRequest depositRequest, string TransactionID)
        {
            string merchantIdHash = ToBase64(sha256(MerchantID));
            string merchantAccountIdHash = ToBase64(sha256(MerchantAccountID));
            string encryptedAccountUsername = Encrypt(MerchantAccountUserName, MerchantPassword);
            string encryptedAccountPassword = Encrypt(MerchantAccountPassword, MerchantPassword);

            CreateRequest createRequest = new CreateRequest();
            createRequest.transactionInfo = new CreateTransactionInfo();
            createRequest.transactionInfo.orderData = new CreateOrderData();
            createRequest.transactionInfo.orderData.billingAddress = new CreateBillingAddress();

            createRequest.encryptedAccountPassword = encryptedAccountPassword;
            createRequest.encryptedAccountUsername = encryptedAccountUsername;
            createRequest.lang = "EN";
            createRequest.merchantAccountIdHash = merchantAccountIdHash;
            createRequest.merchantIdHash = merchantIdHash;
            createRequest.requestTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            createRequest.transactionInfo.apiVersion = "1.0.0";
            createRequest.transactionInfo.cancelUrl = (string.IsNullOrEmpty(CancelUrl)) ? ("") : (CancelUrl);
            createRequest.transactionInfo.notificationUrl = (string.IsNullOrEmpty(CallbackUrl)) ? ("") : (CallbackUrl);
            createRequest.transactionInfo.recurrentType = "1";
            createRequest.transactionInfo.requestId = Guid.NewGuid().ToString().Replace("-", "");
            createRequest.transactionInfo.returnUrl = (string.IsNullOrEmpty(ReturnUrl)) ? ("") : (ReturnUrl);
            createRequest.transactionInfo.statementText = "Fund Deposit";

            createRequest.transactionInfo.orderData.amount = depositRequest.Amount.ToString();
            createRequest.transactionInfo.orderData.currencyCode = depositRequest.CurrencyISOCode;
            createRequest.transactionInfo.orderData.orderDescription = "Fund Deposit";
            createRequest.transactionInfo.orderData.orderId = TransactionID;

            createRequest.transactionInfo.orderData.billingAddress.address1 = depositRequest.Address;
            createRequest.transactionInfo.orderData.billingAddress.address2 = "";
            createRequest.transactionInfo.orderData.billingAddress.city = depositRequest.City;
            createRequest.transactionInfo.orderData.billingAddress.countryCode = depositRequest.CountryISOCode;
            createRequest.transactionInfo.orderData.billingAddress.email = depositRequest.Email;
            createRequest.transactionInfo.orderData.billingAddress.firstName = depositRequest.FirstName;
            createRequest.transactionInfo.orderData.billingAddress.lastName = depositRequest.LastName;
            createRequest.transactionInfo.orderData.billingAddress.phone = depositRequest.PhoneNumber.Replace("+", "").Replace("-", "");
            createRequest.transactionInfo.orderData.billingAddress.zipcode = depositRequest.ZipCode;
            createRequest.transactionInfo.orderData.billingAddress.mobile = "";
            createRequest.transactionInfo.orderData.billingAddress.fax = "";
            createRequest.transactionInfo.orderData.billingAddress.stateCode = "";
            createRequest.transactionInfo.orderData.orderDetail = new OrderDetail()
            {

                orderItems = new OrderItem[1]
            };
            createRequest.transactionInfo.orderData.orderDetail.orderItems[0] = new OrderItem();
            createRequest.transactionInfo.orderData.orderDetail.orderItems[0].itemName = "deposit";

            createRequest.requestIP = depositRequest.IpAddress;

            createRequest.transactionInfo.orderData.cc = new CCCard()
            {
                cardHolderName = depositRequest.FirstName,
                ccNumber = depositRequest.CardNumber,
                cvv = depositRequest.CardCVV2,
                expirationMonth = depositRequest.CardExpMonth,
                expirationYear = depositRequest.CardExpYear
            };


            createRequest.signature = CreateSignature(createRequest);
            createRequest.metaData = new MetaData() { merchantUserId = TransactionID };
            return createRequest;

        }
        private Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;
                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;
                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }
                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }
            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        if ((we.Response is System.Net.HttpWebResponse))
                            resp.Code = ((int)(we.Response as System.Net.HttpWebResponse).StatusCode);
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        private byte[] sha256(string text)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            return crypt.ComputeHash(Encoding.UTF8.GetBytes(text), 0, Encoding.UTF8.GetByteCount(text));
        }

        private string ToBase64(byte[] input)
        {
            return Convert.ToBase64String(input);
        }

        private string CreateSignature(CreateRequest cr)
        {
            string sig = cr.requestTime + cr.merchantIdHash + cr.merchantAccountIdHash + cr.encryptedAccountUsername + cr.encryptedAccountPassword + cr.transactionInfo.apiVersion;
            sig += cr.transactionInfo.requestId + cr.transactionInfo.recurrentType + cr.transactionInfo.orderData.orderId + cr.transactionInfo.orderData.orderDescription;
            sig += cr.transactionInfo.orderData.amount + cr.transactionInfo.orderData.currencyCode + cr.transactionInfo.orderData.cc.ccNumber + cr.transactionInfo.orderData.cc.cardHolderName +
                cr.transactionInfo.orderData.cc.cvv + cr.transactionInfo.orderData.cc.expirationMonth + cr.transactionInfo.orderData.cc.expirationYear + cr.transactionInfo.orderData.billingAddress.firstName + cr.transactionInfo.orderData.billingAddress.lastName;
            sig += cr.transactionInfo.orderData.billingAddress.address1 + cr.transactionInfo.orderData.billingAddress.address2 + cr.transactionInfo.orderData.billingAddress.city;
            sig += cr.transactionInfo.orderData.billingAddress.zipcode + cr.transactionInfo.orderData.billingAddress.stateCode + cr.transactionInfo.orderData.billingAddress.countryCode;
            sig += cr.transactionInfo.orderData.billingAddress.mobile + cr.transactionInfo.orderData.billingAddress.phone + cr.transactionInfo.orderData.billingAddress.email;
            sig += cr.transactionInfo.orderData.billingAddress.fax + cr.transactionInfo.statementText + cr.transactionInfo.cancelUrl + cr.transactionInfo.returnUrl + cr.transactionInfo.notificationUrl;

            sig = sig.Trim();

            var plainBytes = Encoding.UTF8.GetBytes(sig);
            byte[] b = Encrypt(plainBytes, GetRijndaelManaged(MerchantPassword));
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            b = crypt.ComputeHash(b, 0, b.Length);
            var bb = encryptDataInBase64(sig, MerchantPassword);
            return Convert.ToBase64String(b);
            //  return bb;

        }
        public string encryptDataInBase64(string data, string key)
        {
            byte[] keyArray = Encoding.UTF8.GetBytes(key);
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(data);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            rDel.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray =
            cTransform.TransformFinalBlock(toEncryptArray, 0,
            toEncryptArray.Length);
            string encryptDataInBase64 = Convert.ToBase64String(resultArray);
            return encryptDataInBase64;
        }
        /// <summary>
        /// Encrypts plaintext using AES 128bit key and a Chain Block Cipher and returns a base64 encoded string
        /// </summary>
        /// <param name="plainText">Plain text to encrypt</param>
        /// <param name="key">Secret key</param>
        /// <returns>Base64 encoded string</returns>
        private String Encrypt(String plainText, String key)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManaged(key)));
        }

        /// <summary>
        /// Decrypts a base64 encoded string using the given key (AES 128bit key and a Chain Block Cipher)
        /// </summary>
        /// <param name="encryptedText">Base64 Encoded String</param>
        /// <param name="key">Secret Key</param>
        /// <returns>Decrypted String</returns>
        private String Decrypt(String encryptedText, String key)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(key)));
        }

        private byte[] Encrypt(byte[] plainBytes, System.Security.Cryptography.RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private byte[] Decrypt(byte[] encryptedData, System.Security.Cryptography.RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }
        private System.Security.Cryptography.RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new System.Security.Cryptography.RijndaelManaged
            {
                Mode = System.Security.Cryptography.CipherMode.ECB,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        #endregion
    }
}