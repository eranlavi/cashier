﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cashier.Configuration.ProviderConfig;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using CashierUtils;

namespace Cashier
{
    class ProviderMeikoPay : ProviderBase
    {
        private class Error
        {
            public int Code = 0;
            public string Description = string.Empty;
        }
        private string apiCode;
        private string signKey;
        private string ReturnURL;
        private const string UNDEFINED = "undefined";

        private string ProviderName;

        private Dictionary<string, string> cardTypeMapping = new Dictionary<string, string>()
        {
            {"visa", "VISA"},
            {"VISA", "VISA"},
            {"Visa", "VISA"},
            {"mastercard", "MASTERCARD"},
            {"Mastercard", "MASTERCARD"},
            {"MASTERCARD", "MASTERCARD"},
            {"diners club", "DINERSCLUB"},
            {"dinersclubintl", "DINERSCLUB"},
            {"dinersclub", "DINERSCLUB"},
            {"Diners Club", "DINERSCLUB"},
            {"american express", "AMEX"},
            {"American Express", "AMEX"},
            {"amex", "AMEX"},
            {"maestro","MAESTRO"},
            {"Maestro","MAESTRO"}
        };

        #region Constructor

        public ProviderMeikoPay(ProviderConfigBase config)
            : base(config)
        {
            MerchantsWorldWideProviderConfig MeikoPayConfig = config as MerchantsWorldWideProviderConfig;
            if (MeikoPayConfig == null)
            {
                throw new ArgumentException("MeikoPayConfig expects to get MeikoPayProviderConfig object!");
            }
            apiCode = MeikoPayConfig.TerminalId;
            signKey = MeikoPayConfig.PrivateKey;
            ReturnURL = MeikoPayConfig.CallbackUrl;
        }
        #endregion

        protected override Globals.Providers ProviderType
        {
            get { return Globals.Providers.MEIKOPAY; }
            set { }
        }

        public override DepositResponse Deposit(DepositRequest _DepositRequest)
        {
            DepositResponse _DepositResponse = new DepositResponse();
            //_DepositResponse.error = new Cashier.Error();

            string transactionID = Guid.NewGuid().ToString().Replace("-", "");
            //string transactionID = ((long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds).ToString();
            _DepositResponse.provider = _DepositRequest.provider;
            _DepositResponse.ClearingUserID = MerchantID;
            _DepositResponse.TransactionID = transactionID;
            _DepositResponse.Amount = _DepositRequest.Amount.ToString();

            ProviderName = _DepositRequest.provider.ToString();

            _DepositResponse.Is3DSecure = false;

            // 2 digits month
            string ExpMonth = Convert.ToInt32(_DepositRequest.CardExpMonth).ToString("00");

            // 4 digits year
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            string ExpYear = _DepositRequest.CardExpYear;
            if (ExpYear.Length == 2)
                ExpYear = ci.Calendar.ToFourDigitYear(Convert.ToInt32(_DepositRequest.CardExpYear)).ToString();

            // get apiCode
            if (!string.IsNullOrEmpty(apiCode))
            {
                var dict = apiCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                       .Select(part => part.Split('='))
                       .ToDictionary(split => split[0], split => split[1]);
                if (!dict.ContainsKey(GetCardBrandByCardType(_DepositRequest.CardType)))
                {
                    _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                    _DepositResponse.ErrorDescripotion = "No apiCode value found for credit card type: " + _DepositRequest.CardType;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    return _DepositResponse;
                }

                string rt = dict[GetCardBrandByCardType(_DepositRequest.CardType)];
                apiCode = rt;
                Log(string.Format("Request: Provider={2}; Using apiCode: {0}{1}", rt, Environment.NewLine, ProviderName));
            }
            else
            {
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ErrorDescripotion = "apiCode ( TerminalId ) value is missing";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            // get signKey
            if (!string.IsNullOrEmpty(signKey))
            {
                var dict = signKey.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                       .Select(part => part.Split('='))
                       .ToDictionary(split1 => split1[0], split1 => split1[1]);
                if (!dict.ContainsKey(GetCardBrandByCardType(_DepositRequest.CardType)))
                {
                    _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                    _DepositResponse.ErrorDescripotion = "No signKey value found for credit card type: " + _DepositRequest.CardType;
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    return _DepositResponse;
                }

                string rt = dict[GetCardBrandByCardType(_DepositRequest.CardType)];
                signKey = rt;
                Log(string.Format("Request: Provider={2}; Using signKey: {0}{1}", rt, Environment.NewLine, ProviderName));
            }
            else
            {
                _DepositResponse.Amount = _DepositRequest.Amount.ToString();
                _DepositResponse.ErrorDescripotion = "signKey ( PrivateKey ) value is missing";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;

                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            // build the signature
            string signature = string.Empty;
            try
            {
                string data = PrepareSHA256Data(_DepositRequest, transactionID, ExpMonth, ExpYear);
                signature = sha256(data);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=MeikoPay; Fail construct signature string: {0}{1}", ex.Message, Environment.NewLine));
                _DepositResponse.ErrorDescripotion = "Fail construct signature string for MeikoPay request";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            // build the request
            string req = BuildRequest(_DepositRequest, transactionID, ExpMonth, ExpYear, signature);
            if (string.IsNullOrEmpty(req))
            {
                _DepositResponse.ErrorDescripotion = "Fail construct request string for MeikoPay";
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                return _DepositResponse;
            }

            Log(string.Format("Request: Provider=MeikoPay; {0} {1}{2}", Url, req, Environment.NewLine));

            // send POST
            Error e = HttpReq("POST", Url, req, null, "application/x-www-form-urlencoded;charset=UTF-8", "", 20000, 20000);

            Log(string.Format("Response: Provider=MeikoPay; {0}{1}", e.Description, Environment.NewLine));

            if (e.Code != 0)
            {
                _DepositResponse.ErrorDescripotion = "Http request to MeikoPay failed. " + e.Description;
                _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                _DepositResponse.ErrorCode = "1";
                _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;

                // fill the class FailedDeposit with details in order to send email to support later
                FailedDeposit failedDeposit = new FailedDeposit();
                failedDeposit.provider = _DepositRequest.provider;
                failedDeposit.Email = _DepositRequest.Email;
                failedDeposit.TransactionID = transactionID;
                failedDeposit.Amount = _DepositRequest.Amount;
                failedDeposit.FirstName = _DepositRequest.FirstName;
                failedDeposit.LastName = _DepositRequest.LastName;
                failedDeposit.Description = e.Description;

                _DepositResponse.FailedDeposits.Add(failedDeposit);
            }
            else
            {
                try
                {
                    string result = e.Description;

                    // check if its non 3DS ( xml format )
                    int startOfXML = result.IndexOf("<?xml");
                    if (startOfXML > -1)
                    {
                        string xmlResponse = result.Substring(startOfXML);

                        // Now load the string into an XmlDocument 
                        XmlDocument responseDocument = new XmlDocument();
                        responseDocument.LoadXml(xmlResponse);

                        ParseResponseXml(responseDocument, _DepositResponse);
                    }
                    else
                    {
                        // check if its 3DS ( html format )
                        int startOfHTML = result.IndexOf("<html");
                        if (startOfHTML > -1)
                        {
                            _DepositResponse.Is3DSecure = true;

                            Log("Is3DSecure= " + _DepositResponse.Is3DSecure.ToString());
                            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.NO_ERROR;

                            _DepositResponse.html3D = result;

                            _DepositResponse.ErrorDescripotion = "";
                            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                        }
                        else
                        {
                            _DepositResponse.ErrorDescripotion = "MeikoPay: response is invalid: " + result;
                            _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                            _DepositResponse.ErrorCode = "1";
                            _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                        }
                    }
                }
                catch (UriFormatException)
                {
                    _DepositResponse.ErrorDescripotion = "MeikoPay: Couldn't parse URL return";
                    _DepositResponse.error.Description = _DepositResponse.ErrorDescripotion;
                    _DepositResponse.ErrorCode = "1";
                    _DepositResponse.error.ErrorCode = Globals.ErrorCodes.FAIL;
                    return _DepositResponse;
                }
            }

            return _DepositResponse;
        }

        private string BuildRequest(DepositRequest _DepositRequest, string TransactionID, string ExpMonth, string ExpYear, string signature)
        {
            string req = string.Empty;

            try
            {
                string street = System.Web.HttpUtility.UrlEncode(_DepositRequest.Address);
                string city = System.Web.HttpUtility.UrlEncode(_DepositRequest.City);
                string email = System.Web.HttpUtility.UrlEncode(_DepositRequest.Email);
                string firstName = System.Web.HttpUtility.UrlEncode(_DepositRequest.FirstName.Trim());
                string lastName = System.Web.HttpUtility.UrlEncode(_DepositRequest.LastName.Trim());

                string csid = "";

                req = string.Format("merchantCode={0}&apiCode={1}&orderNo={2}&tradeCurrency={3}&tradeAmount={4}&cardNum={5}&cardExpireMonth={6}&cardExpireYear={7}&cvv={8}&issuingBank={9}&firstName={10}&lastName={11}&email={12}&phoneNum={13}&billCountry={14}&billCity={15}&billAddress={16}&billZip={17}&billState={18}&ipaddress={19}&sign={20}&remark={21}&returnUrl={22}&csid={23}",
                    MerchantID, apiCode, TransactionID, _DepositRequest.CurrencyISOCode, _DepositRequest.Amount, _DepositRequest.CardNumber, ExpMonth, ExpYear, _DepositRequest.CardCVV2, "unknown", firstName, lastName, email, _DepositRequest.PhoneNumber, _DepositRequest.CountryISOCodeThreeLetters, city, street, _DepositRequest.ZipCode, _DepositRequest.StateISOCode, _DepositRequest.IpAddress, signature, "", ReturnURL, csid);
            }
            catch (Exception ex)
            {
                Log(string.Format("Provider=MeikoPay; Fail construct request string: {0}{1}", ex.Message, Environment.NewLine));
            }

            return req;
        }

        private string GetCardBrandByCardType(string cardType)
        {
            string cardBrand = string.Empty;
            cardTypeMapping.TryGetValue(cardType, out cardBrand);
            return cardBrand;
        }

        private string PrepareSHA256Data(DepositRequest _DepositRequest, string TransactionID, string ExpMonth, string ExpYear)
        {

            //return MerchantID + apiCode + TransactionID + _DepositRequest.CurrencyISOCode + _DepositRequest.Amount.ToString() + _DepositRequest.FirstName.Replace(" ", "%20") + _DepositRequest.LastName + _DepositRequest.CardNumber + ExpYear + ExpMonth + _DepositRequest.CardCVV2 + _DepositRequest.Email + signKey;

            var data = new NameValueCollection
            {
                {"merchantCode", MerchantID},
                {"apiCode", apiCode},
                {"orderNo", TransactionID},
                {"tradeCurrency", _DepositRequest.CurrencyISOCode},
                {"tradeAmount", _DepositRequest.Amount.ToString()},
                {"firstName", _DepositRequest.FirstName.Trim()},
                {"lastName", _DepositRequest.LastName.Trim()},
                {"cardNum",_DepositRequest.CardNumber },
                {"cardExpireYear", ExpYear},
                {"cardExpireMonth", ExpMonth},
                {"cvv", _DepositRequest.CardCVV2 },
                {"email", _DepositRequest.Email}
            };

            var keys = data.AllKeys;

            //2. Append the corresponding values together according to the alphabetical sequence of parameter names.
            var valuesRow = new StringBuilder();
            foreach (string key in keys)
            {
                valuesRow.Append(data[key]);
            }

            //3. Add the SignKey to the end of the string.
            valuesRow.Append(signKey);

            return valuesRow.ToString();

        }

        string sha256(string input)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(input), 0, Encoding.UTF8.GetByteCount(input));
            foreach (byte bit in crypto)
            {
                hash.Append(bit.ToString("x2"));
            }
            return hash.ToString();
        }

        private static Error HttpReq(string Method, string Url, string Parameters, System.Collections.Specialized.NameValueCollection Header, string ContentType, string Accept, int ConnectTimeoutMSec, int ReadWriteTimeoutMSec, string Cred = "")
        {
            Error resp = new Error();

            try
            {
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                /*ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );*/

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = Method.ToUpper();

                if (Header is System.Collections.Specialized.NameValueCollection)
                    req.Headers.Add(Header);
                if (!string.IsNullOrEmpty(ContentType))
                    req.ContentType = ContentType;
                if (!string.IsNullOrEmpty(Accept))
                    req.Accept = Accept;
                if (!string.IsNullOrEmpty(Cred))
                    req.Headers[HttpRequestHeader.Authorization] = Cred;

                req.Timeout = ConnectTimeoutMSec;
                req.ReadWriteTimeout = ReadWriteTimeoutMSec;

                if (Method.ToUpper() == "POST" && !string.IsNullOrEmpty(Parameters))
                {
                    req.ContentLength = Encoding.UTF8.GetBytes(Parameters).Length;

                    using (Stream stm = req.GetRequestStream())
                    {
                        stm.WriteTimeout = ReadWriteTimeoutMSec;
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(Parameters);
                        }
                    }
                }

                HttpWebResponse webResponse = req.GetResponse() as HttpWebResponse;
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    resp.Description = rd.ReadToEnd();
                }


            }
            catch (WebException we)
            {
                if (we.Response != null)
                {
                    using (var responseReader = new StreamReader(we.Response.GetResponseStream()))
                    {
                        resp.Code = -1;
                        resp.Description = responseReader.ReadToEnd();
                    }
                }
                else
                {
                    if ((we.Response is System.Net.HttpWebResponse))
                        resp.Code = (int)(we.Response as System.Net.HttpWebResponse).StatusCode;
                    else
                        resp.Code = (int)we.Status;
                    resp.Description = resp.Code.ToString() + ", " + we.Message;
                }
            }
            catch (Exception ex)
            {
                resp.Code = 1;
                resp.Description = ex.Message;
            }

            return resp;
        }

        #region Response parsing funcs

        //Parses response Xml. Returns DataCashDepositResponseParameters object
        private void ParseResponseXml(XmlDocument docResponse, DepositResponse response)
        {
            // Get the root node 
            XmlElement rootElm = docResponse.DocumentElement;

            //string RetCode = GetXmlElementValue(rootElm, "responseCode");
            string ErrorCode = GetXmlElementValue(rootElm, "status");
            string ErrorMessage = GetXmlElementValue(rootElm, "bankInfo");

            response.error.ErrorCode = ErrorCode == "1" ? Globals.ErrorCodes.NO_ERROR : Globals.ErrorCodes.FAIL;
            response.error.Description = ErrorMessage;
            response.ErrorMessage = ErrorMessage;
        }

        //Returns Xml element value
        private string GetXmlElementValue(XmlElement rootElm, string elmName)
        {
            // Find all descendants with the desired name 
            XmlNodeList nodes = rootElm.GetElementsByTagName(elmName);

            // Assume only one "hit" and return the contained text
            string resultValue = nodes.Count > 0 ? nodes[0].InnerText : UNDEFINED;
            return resultValue;
        }

        #endregion


        private string getCredByCardType(string Card)
        {

            switch (Card)
            {
                case "visa":
                case "Visa":
                    return "VISA";
                case "amex":
                case "American Express":
                case "american express":
                    return "AMEX";
                case "mastercard":
                case "Mastercard":
                    return "MASTERCARD";
                case "maestro":
                case "Maestro":
                    return "";
                case "dinersclubintl":
                case "Diners Club":
                case "dinersclub":
                    return "";
                default:
                    return string.Empty;
            }
        }

    }
}