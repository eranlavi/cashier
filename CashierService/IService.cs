﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashierService
{
    public interface IService
    {        
        ServiceResult Connect(string ConnStr);
        void Close();
        DataTable GetProviders(int BrandID, string Country, string Currency, string CardType,  double Amount);
        int GetRoundRobinIndex(int BrandID, int Level, string Pattern);
        void InsertRoundRobin(int BrandID, int Level, string Pattern);
        void UpdateRoundRobinIndex(int BrandID, int Level, string Pattern, int Index);
        event Action<string> Message;
    }
}
