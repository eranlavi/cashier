﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CashierService
{
    public class Service : IService
    {
        public event Action<string> Message;
        MySqlConnection conn;

        public ServiceResult Connect(string ConnStr)
        {
            ServiceResult res = new ServiceResult();

            try
            {
                conn = new MySqlConnection(ConnStr);            
                conn.Open();
                res.IsSuccess = true;
            }
            catch (MySqlException ex)
            {
                res.Message = ex.Message;
            }
            catch (Exception ex)
            {
                res.Message = ex.Message;
            }

            return res;
        }

        public void Close()
        {
            try
            {
                conn.Close();
            }
            catch { }
            try
            {
                conn.Dispose();
            }
            catch{ }
        }

        public DataTable GetProviders(int BrandID, string Country, string Currency, string CardType, double Amount)
        {
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "clearedby_priority";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@brand_id", BrandID);
                    cmd.Parameters.AddWithValue("@creditcard_type_code", CardType);
                    cmd.Parameters.AddWithValue("@currency", Currency);
                    cmd.Parameters.AddWithValue("@country_iso2", Country);                                        
                    cmd.Parameters.AddWithValue("@amount_minimum", Amount);
                    cmd.Parameters.AddWithValue("@amount_maximum", Amount);
                    
                    
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr != null && dr.HasRows)
                        {
                            DataSet ds = new DataSet();
                            DataTable dataTable = new DataTable();
                            ds.Tables.Add(dataTable);
                            ds.EnforceConstraints = false;
                            dataTable.Load(dr);
                            return dataTable;
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                Message?.Invoke("DB Cashier GetProviders exception: " + ex.Message);
            }
            catch (Exception ex)
            {
                Message?.Invoke("Cashier GetProviders exception: " + ex.Message);
            }

            return null;            
        }

        public int GetRoundRobinIndex(int BrandID, int Level, string Pattern)
        {
            int res = -1;

            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "clearedby_round_robin";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@level", Level);
                    cmd.Parameters.AddWithValue("@clearedby", Pattern);
                    cmd.Parameters.AddWithValue("@brand_id", BrandID);

                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr != null && dr.HasRows && dr.Read())
                        {
                            res = Convert.ToInt32(dr["index"]);
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                Message?.Invoke("DB Cashier GetRoundRobinIndex exception: " + ex.Message);
            }
            catch (Exception ex)
            {
                Message?.Invoke("Cashier GetRoundRobinIndex exception: " + ex.Message);
            }

            return res;
        }

        public void InsertRoundRobin(int BrandID, int Level, string Pattern)
        {
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "clearedby_round_robin_insert";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@level", Level);
                    cmd.Parameters.AddWithValue("@clearedby", Pattern);
                    cmd.Parameters.AddWithValue("@brand_id", BrandID);

                    int i = cmd.ExecuteNonQuery();                    
                }
            }
            catch (MySqlException ex)
            {
                Message?.Invoke("DB Cashier InsertRoundRobin exception: " + ex.Message);
            }
            catch (Exception ex)
            {
                Message?.Invoke("Cashier InsertRoundRobin exception: " + ex.Message);
            }
        }

        public void UpdateRoundRobinIndex(int BrandID, int Level, string Pattern, int Index)
        {
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "clearedby_round_robin_update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@level", Level);
                    cmd.Parameters.AddWithValue("@clearedby", Pattern);
                    cmd.Parameters.AddWithValue("@index", Index);
                    cmd.Parameters.AddWithValue("@brand_id", BrandID);

                    int i = cmd.ExecuteNonQuery();
                }
            }
            catch (MySqlException ex)
            {
                Message?.Invoke("DB Cashier UpdateRoundRobinIndex exception: " + ex.Message);
            }
            catch (Exception ex)
            {
                Message?.Invoke("Cashier UpdateRoundRobinIndex exception: " + ex.Message);
            }
        }

    }
}
