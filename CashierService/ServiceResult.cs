﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashierService
{
    public class ServiceResult
    {
        public bool IsSuccess;
        public string Message = string.Empty;
    }
}
